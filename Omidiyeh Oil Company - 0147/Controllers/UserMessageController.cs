﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{
	[Authorize()]
    public class UserMessageController : Controller
    {
		private readonly IUnitOfWork _unitOfWork;

		public UserMessageController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }

        [Authorize]
        public ActionResult My()
        {
            return PartialView();
        }

        public async Task<JsonResult> ListMy()
        {
            var user = Convert.ToInt32(User.Identity.Name);

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 5 : length;
            var includeProperties = "TblUser1.TblPerson";
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblUserMessage>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrderedSync(a => a.User== user,
                    orderBy: order, take: length, skip: start,includeProperties: includeProperties) :
                    iResult.GetOrderedSync(a => a.User == user,orderBy: order, take: length, skip: start, includeProperties:includeProperties);
            var (items, count, total) =  result;

            var data = items.Select(a => new {
                id = a.Id,
                DT_RowId = a.Id,
                a.Title,
                a.Body,
                CreateDate = new PersianDateTime(a.CreateDate).ToString("yyyy/MM/dd HH:mm"),
                a.Id,
                CreateUser=  a.TblUser1.TblPerson.Fullname(),
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        // GET: UserMessage
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
			var iResult = _unitOfWork.GetRepository<TblUserMessage>();
            var result = !string.IsNullOrEmpty(searchValue) ? 
                iResult.GetOrdered(a => a.Title.Contains(searchValue) ,
                    orderBy: order,take:length, skip:start) :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start);
            var (items, count,total) = await result;

            var data = items.Select(a => new { id = " ", 
                 DT_RowId = a.Id,
                                    a.User,
                                    a.Title,
                                    a.Body,
                                    a.CreateDate,
                                    a.Status,
                                    a.Id,
                                    a.CreateUser,
                                    a.TblUser,
                                    a.TblUser1,
                                edit= $"<a href='/UserMessage/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> "+
                      $"<a href='/UserMessage/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

		// GET: UserMessage/Details/5
		public async Task<ActionResult> Details(int id)
        {
			if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblUserMessage>();
            var result = await iResult.GetSingle(a=>a.Id==id);
			if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }


        // GET: UserMessage/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.User = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblUser>().Get(), "Id", "UserName");
            ViewBag.CreateUser = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblUser>().Get(), "Id", "UserName");
            return View();
        }

        // POST: UserMessage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "User,Title,Body,CreateDate,Status,Id,CreateUser")] TblUserMessage entity)
        {
			try
            {
            if (ModelState.IsValid)
            {
				var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblUserMessage>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                return RedirectToAction("Index");
            }

            ViewBag.User = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblUser>().Get(), "Id", "UserName");
            ViewBag.CreateUser = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblUser>().Get(), "Id", "UserName");
			}
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: UserMessage/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblUserMessage>();
            var result = await iResult.GetSingle(a=>a.Id==id);
			if (result == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.User = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblUser>().Get(), "Id", "UserName");
            ViewBag.CreateUser = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblUser>().Get(), "Id", "UserName");
            return View(result);
        }

        // POST: UserMessage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "User,Title,Body,CreateDate,Status,Id,CreateUser")] TblUserMessage entity)

        {
			try
            {
            if (ModelState.IsValid)
            {
					var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblUserMessage>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
					return RedirectToAction("Index");
            }
            ViewBag.User = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblUser>().Get(), "Id", "UserName");
            ViewBag.CreateUser = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblUser>().Get(), "Id", "UserName");
			}
			catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: UserMessage/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblUserMessage>();
            var result = await iResult.GetSingle(a=>a.Id==id);
			if (result == null)
            {
                return HttpNotFound();
            }      
            return View(result);
        }

        // POST: UserMessage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
			try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblUserMessage>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new {id});
        }

    }
}
