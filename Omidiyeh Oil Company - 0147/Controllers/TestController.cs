﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Text;
using Omidiyeh_Oil_Company___0147.FilterAttribute;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{
    public class TestController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private dbEntities _db = new dbEntities();
        public TestController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        //[GuestAuthorize]
        //[AllowAnonymous]
        //[Authorize]
        [AllowAnonymous]
        public string Info()
        {
            return typeof(string).Assembly.ImageRuntimeVersion;
        }
        [Authorize]
        public string Info1()
        {
            return typeof(string).Assembly.ImageRuntimeVersion;
        }
        [MyAuthorize(Roles = "200")]
        public string Info2()
        {
            return typeof(string).Assembly.ImageRuntimeVersion;
        }
        [MyAuthorize(Roles = "218")]
        public string Info3()
        {
            return typeof(string).Assembly.ImageRuntimeVersion;
        }
        public string ToFarsiOffice()
        {

            var i = 0;
            var j = 0;
            var msg = "";
            foreach (var item in _db.TblOffices.ToList())
            {
                if (item.Title != item.Title.ToFarsi())
                {
                    item.Title = item.Title.ToFarsi();
                    i++;
                    try
                    {
                        _db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        item.Title = item.Title.ToFarsi() + " *" + item.Id;
                        try
                        {
                            _db.SaveChanges();
                        }
                        catch
                        {
                            msg += item.Id + "; " + e.Message + "; ";
                        }
                        

                    }
                }
             
            }
            return i + ";" + j + ";" + msg;
        }
         public string ToFarsiOfficeDepartment()
        {

            var i = 0;
            var j = 0;
            var msg = "";
            foreach (var item in _db.TblOfficeDepartments.ToList())
            {
                if (item.Title != item.Title.ToFarsi())
                {
                    item.Title = item.Title.ToFarsi();
                    i++;
                    try
                    {
                        _db.SaveChanges();
                    }
                    catch (Exception e)
                    {

                        item.Title = item.Title.ToFarsi() + " *" + item.Id;
                        try
                        {
                            _db.SaveChanges();
                        }
                        catch
                        {
                            msg += item.Id + "; " + e.Message + "; ";
                        }

                    }
                }
             
            }
            return i + ";" + j + ";" + msg;
        }
         public string ToFarsiOfficeDepartmentAccountNumber()
        {

            var i = 0;
            var j = 0;
            var msg = "";
            foreach (var item in _db.TblOfficeDepartmentAccountNumbers.ToList())
            {
                if (item.Title != item.Title.ToFarsi())
                {
                    item.Title = item.Title.ToFarsi();
                    i++;
                    try
                    {
                        _db.SaveChanges();
                    }
                    catch (Exception e)
                    {

                        msg += item.Id + "; " + e.Message + "; ";

                    }
                }
             
            }
            return i + ";" + j + ";" + msg;
        }
           public string ToFarsiManagement()
        {

            var i = 0;
            var j = 0;
            var msg = "";
            foreach (var item in _db.TblManagements.ToList())
            {
                if (item.Title != item.Title.ToFarsi())
                {
                    item.Title = item.Title.ToFarsi();
                    i++;
                    try
                    {
                        _db.SaveChanges();
                    }
                    catch (Exception e)
                    {

                        msg += item.Id + "; " + e.Message + "; ";

                    }
                }
             
            }
            return i + ";" + j + ";" + msg;
        }
            public string ToFarsiSubsidiaries()
        {

            var i = 0;
            var j = 0;
            var msg = "";
            foreach (var item in _db.TblSubsidiaries.ToList())
            {
                if (item.Title != item.Title.ToFarsi())
                {
                    item.Title = item.Title.ToFarsi();
                    i++;
                    try
                    {
                        _db.SaveChanges();
                    }
                    catch (Exception e)
                    {

                        msg += item.Id + "; " + e.Message + "; ";

                    }
                }
             
            }
            return i + ";" + j + ";" + msg;
        } 
            public string ToFarsiCompanies()
        {

            var i = 0;
            var j = 0;
            var msg = "";
            foreach (var item in _db.TblCompanies.ToList())
            {
                if (item.Title != item.Title.ToFarsi())
                {
                    item.Title = item.Title.ToFarsi();
                    i++;
                    try
                    {
                        _db.SaveChanges();
                    }
                    catch (Exception e)
                    {

                        msg += item.Id + "; " + e.Message + "; ";

                    }
                }
             
            }
            return i + ";" + j + ";" + msg;
        }

        public string ToFarsiPerson()
        {

            var i = 0;
            var j = 0;
            var msg = "";
            foreach (var item in _db.TblPersons.ToList())
            {
                if (item.Name != item.Name.ToFarsi())
                {
                    item.Name = item.Name.ToFarsi();
                    i++;
                    try
                    {
                        _db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        
                            msg += item.Id + "; "+e.Message+ "; ";
                        
                    }
                }
                if (item.Family != item.Family.ToFarsi())
                {
                    item.Family = item.Family.ToFarsi();
                    j++;
                    try
                    {
                        _db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        
                            msg += item.Id + "; "+e.Message+ "; ";
                        
                    }
                }
            }
            return i+";"+j+";"+ msg;
        }

        public string ToFarsi()
        {
            
            var i = 0;
            var msg = "";
            foreach (var item in _db.TblProducts.ToList())
            {
                if (item.Title != item.Title.ToFarsi())
                {
                    item.Title = item.Title.ToFarsi();
                    i++;
                    try
                    {
                        _db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        item.Title = item.Title.ToFarsi() + " *"+ item.Id;
                        try
                        {
                            _db.SaveChanges();
                        }
                        catch
                        {
                            msg += item.Id + ";";
                        }
                    }
                }
            }
            return msg;
        }
        public string TestDb2()
        {
         dbEntities _db2 = new dbEntities();

        var list =  _db2.TblFoodReservations.OrderBy(a=>a.Id).Take(10).ToList();
        var str = list.First().TblFood.Title;
         return str;
        }
    public string ReadDb()
        {
            var item = _db.TblPersons.First();
           // var items = _db.TblFoodReservatio
            return item.Id.ToString();
        }

        public string pd()
        {
            var message = "";
            var list = _db.TblTemp3.ToList();
            //foreach (var item in list)
            //{
            //    if (!_db.TblOfficeDepartments.Any(a => a.Id == item.OfficeDepartment))
            //    {
            //        message += item.OfficeDepartment + "\r\n";
            //    }
                
            //}
            //var list1 = list.Where(a => a.NationalCode != "          " ).ToList();
            //foreach (var item in list1)
            //{
            //    var p = _db.TblPersons.FirstOrDefault(a => a.NationalCode == item.NationalCode);
            //    if (p == null) message += item.NationalCode + ";\r\n";
            //    else
            //    {
            //        p.OfficeDepartment = item.OfficeDepartment;
            //        try
            //        {
            //            _db.SaveChanges();
            //        }
            //        catch (Exception e)
            //        {
            //            message += item.NationalCode + $"; {item.OfficeDepartment} ({e.InnerException?.InnerException?.Message}) ;\r\n";

            //        }
            //    }
            //}

            var list2 = list.Where(a => a.NationalCode == "          " && a.PersonnelCode !="").ToList();
            foreach (var item in list2)
            {
                var p = _db.TblPersons.Where(a => a.PersonnelCode == item.PersonnelCode);
                if (p == null) message += item.PersonnelCode + ";";
                else
                {
                    foreach (var pp in p)
                    {
                        pp.OfficeDepartment = item.OfficeDepartment;
                       
                    }
                }
            }

            _db.SaveChanges();
            return message;
        }

        public string GetAscii(string str)
        {
            var result = "";
            foreach (char c in str)
            {
                result += " " + (int)c;
            }
            return result;
        }

        //Driver Vehicle
        public string Dv()
        {
            var res = "";
            var list = _db.TblTemps.ToList();
            foreach (var item in list)
            {
                var v = _db.TblVehicles.FirstOrDefault(a => a.Title == item.Title);
                var p = _db.TblPersons.FirstOrDefault(a => a.NationalCode == item.NationalCode);
                if (v != null && p != null)
                {
                    if (_db.TblVehicleDrivers.Any(a => a.Person == p.Id && a.Vehicle == v.Id)) continue;
                    var vd = new TblVehicleDriver
                    {
                        Person = p.Id,
                        Vehicle = v.Id
                    };
                    _db.TblVehicleDrivers.Add(vd);

                    _db.SaveChanges();
                }
                else
                {
                    res += "v:" + item.Title + "p:" + item.NationalCode + "; ";
                }
            }
            return res;
        }


        public string DV2()
        {
            var c1 = 0; var c2 = 0;
            var temp = _db.TblTemp2.ToList();
            foreach(var item in temp)
            {
                if(_db.TblVehicles.Any(a=>a.Title == item.Shomare))
                {
                    var v = _db.TblVehicles.First(a => a.Title == item.Shomare);
                    v.Plate = item.Pelak;
                    v.TypeText = item.NoeKhodro;
                    v.Des = item.EdareEstefade;
                    c1++;
                }
                else
                {
                    var vn = new TblVehicle
                    {
                        Capacity = 1,
                        Des = item.EdareEstefade,
                        Office = 9,
                        Enable = true,
                        Plate = item.Pelak,
                        Title = item.Shomare,
                        Type = 1,
                        TypeText = item.NoeKhodro
                    };
                    _db.TblVehicles.Add(vn);
                    c2++;
                }
            }
            _db.SaveChanges();
            return c1+";"+c2;
        }
        public string PP()
        {
            var c1 = 0; var c2 = 0;
            var c4 = 0; var c3 = 0;
            var list = _db.TblTemp2.ToList();
            foreach (var item in list)
            {
                if (item.KodMelli1.Length != 10) continue;
                if (_db.TblPersons.Any(a => a.NationalCode == item.KodMelli1))
                {
                    var p = _db.TblPersons.First(a => a.NationalCode == item.KodMelli1);
                    if (p.Name == "بی")
                    {
                        p.Name = item.Rannade1;
                        p.Family = "--";
                    }
                    c1++;
                }
                else
                {
                    var pn = new TblPerson()
                    {
                        BirthDate = DateTime.Now.AddYears(-50),
                        Disable = false,
                        Family = "--",
                        FamilyRelationship = 0,
                        NationalCode = item.KodMelli1,
                        Group = 8,
                        OfficeDepartment = 86,
                        ShiftWorker = true,
                        Retierd = false,
                        Name = item.Rannade1
                    };
                    _db.TblPersons.Add(pn);
                    c2++;
                }
                _db.SaveChanges();
            }
            _db.SaveChanges();
            foreach (var item in list)
            {
                if (item.KodMelli2.Length != 10) continue;
                if (_db.TblPersons.Any(a => a.NationalCode == item.KodMelli2))
                {
                    var p = _db.TblPersons.First(a => a.NationalCode == item.KodMelli2);
                    if (p.Name == "بی")
                    {
                        p.Name = item.Rannade2;
                        p.Family = "--";
                    }
                    c3++;
                }
                else
                {
                    var pn = new TblPerson()
                    {
                        BirthDate = DateTime.Now.AddYears(-50),
                        Disable = false,
                        Family = "--",
                        FamilyRelationship = 0,
                        NationalCode = item.KodMelli2,
                        Group = 8,
                        OfficeDepartment = 86,
                        ShiftWorker = true,
                        Retierd = false,
                        Name = item.Rannade2
                    };
                    _db.TblPersons.Add(pn);
                    c4++;
                }
                _db.SaveChanges();
            }
           
            return c1 + ";" + c2 + ";" + c3 + ";" + c4;
        }
        public string DV3()
        {
            var res = "";
            var list = _db.TblTemp2.ToList();
            foreach (var item in list)
            {
                var v = _db.TblVehicles.FirstOrDefault(a => a.Title == item.Shomare);
                var p = _db.TblPersons.FirstOrDefault(a => a.NationalCode == item.KodMelli1);
                if (v != null && p != null)
                {
                    if (_db.TblVehicleDrivers.Any(a => a.Person == p.Id && a.Vehicle == v.Id)) continue;
                    var vd = new TblVehicleDriver
                    {
                        Person = p.Id,
                        Vehicle = v.Id
                    };
                    _db.TblVehicleDrivers.Add(vd);

                    _db.SaveChanges();
                }
                else
                {
                    //res += "v:" + item.Title + "p:" + item.NationalCode + "; ";
                }


                var p2 = _db.TblPersons.FirstOrDefault(a => a.NationalCode == item.KodMelli2);
                if (v != null && p2 != null)
                {
                    if (_db.TblVehicleDrivers.Any(a => a.Person == p2.Id && a.Vehicle == v.Id)) continue;
                    var vd2 = new TblVehicleDriver
                    {
                        Person = p2.Id,
                        Vehicle = v.Id
                    };
                    _db.TblVehicleDrivers.Add(vd2);

                    _db.SaveChanges();
                }
            }
            return res;
        }
        public async Task<string> testAwait()
        {
            for (var i = 0; i < 100000; i++)
            {
                var t = new TblTest() {Title = i.ToString()};
                _db.TblTests.Add(t);
            }

            _db.SaveChangesAsync();
            return "yasin";

        }
        
        public string convert()
        {
            var i ="204.1".ToRemoveAt(".");
            return "";
        }
      
        public ActionResult DataTable()
        {
            return View();
        }
        public ActionResult testdb()
        {
            var i = new TblContractStatu
            {
                Title = "تست",
                Des = "1"
            };
            var r1 = _db.TblContractStatus.Add(i);
            var r2 = _db.TblContractStatus.Add(i);
            _db.SaveChanges();

            return null;
        }

        public ActionResult test1()
        {
            var a = new int[] { 2, 3, 4, 5, 6 };
            var b = new int[] { 3, 4, 5, 7, 8, 9 };
            var e = a.Except(b);
            var e2 = b.Except(a);
            var d = a.Intersect(b);
            var d2 = b.Intersect(a);
            //a => a.TblUserRoles.Select(b => b.TblRole)
            //var test = _db.TblRestaurantFoodPrograms.OrderBy("Date desc").ToList();
            //var test2 = _db.TblRestaurantFoodPrograms.OrderBy("Date desc,Meal desc").ToList();

            return null;
        }

        public string test2()
        {
            var str = "";
            for (int i = 10; i < 24604; i++)
            {
                if (!_db.TblPersons.Any(a => a.Id == i))
                    str += i + ";";
            }
            return str;
        }
        public ActionResult report()
        {
            return View();
        }

        public ActionResult Room()
        {
            return View();
        }
        //public async Task<JsonResult> Items()
        //{
        //   // var (company1, subsidiary, management, office, department, account) = await new Helpers().UserAccess(Convert.ToInt32(User.Identity.Name));
        //    var company = await new Helpers().UserCompany(Convert.ToInt32(User.Identity.Name));
        //    var company = await new Helpers().UserSubsidiary(Convert.ToInt32(User.Identity.Name));
        //    var company = await new Helpers().UserManagement(Convert.ToInt32(User.Identity.Name));
        //    var company = await new Helpers().UserOffice(Convert.ToInt32(User.Identity.Name));
        //    var company = await new Helpers().UserOfficeDepartment(Convert.ToInt32(User.Identity.Name));
        //    //Expression<Func<TblOffice, bool>> filter = (a) =>
        //    //    (offices.Any(b => b == a.Id));
        //    //var list = await _unitOfWork.GetRepository<TblOffice>().Get(filter
        //    //    , orderBy: b => b.OrderBy(c => c.Title), includeProperties: "TblOfficeDepartments");
        //    return Json(new {company, subsidiary, management, office, department, account}, JsonRequestBehavior.AllowGet);
        //}
        public async Task<ActionResult> Insert()
        {
            var j = 1;
            foreach (var item in _db.TblPersons.ToList())
            {
                item.Id = j++;
            }

            _db.SaveChanges();

            return null;

        }
        //public async Task<ActionResult> Update()
        //{
        //    var r = _unitOfWork.GetRepository<TblRole>().GetSingle(a=>a.Id==8);
        //    r.Name = "Abbasi1";
        //    r.Title = "Admin1";



        //    var result = await _unitOfWork.GetRepository<TblRole>().Update(r, 1);

        //    return null;

        //}
        // GET: Test
        public ActionResult Index()
        {
            // var result = unitOfWork.GetRepository<TblPerson>().Get(includeProperties: "TblUsers");
            //  _db.TblPersons.Include(a=>a.TblUsers)
            return View();
        }

        public ActionResult Video()
        {
            return View();
        }

        [Authorize]
        public ActionResult vi()
        {

                var filePath = Server.MapPath("/images/Instagram.mp4");
                var mimeType = "video/mp4";

                return File(filePath, mimeType);
        }
        //public JsonResult Table()
        //{
        //   //Server Side Parameter
        //   var start = Convert.ToInt32( Request["start"]);
        //   var length = Convert.ToInt32(Request["length"]);
        //   var searchValue = Request["search[value]"];
        //   var sortColumnName = Request["columns["+Request["order[0][column]"]+"][name]"];
        //   var sortDirection = Request["order[0][dir]"];
        //   var draw = Request["draw"];

        //    var iResult = _unitOfWork.GetRepository<TblUser>();
        //    var result = !string.IsNullOrEmpty(searchValue) ? iResult.Get(a => a.UserName.Contains(searchValue)) :
        //        iResult.GetOrdered(includeProperties:"TblOffice", orderBy: sortColumnName + " " + sortDirection);

        //    var data = result.Select(a => new {id = " ",a.UserName, a.Password, a.PasswordSalt, a.Title, DT_RowId = a.Id}).ToList();
        //    var tblUsers = result.ToList();
        //    var res = new
        //    {
        //        draw = draw, recordsTotal = tblUsers.Count().ToString(), recordsFiltered =tblUsers.Count().ToString(),
        //        data = data
        //    };
        //    //var json = new JsonResult {Data = result};

        //    //string json2 = JsonConvert.SerializeObject(res, Formatting.Indented);
        //    return Json(res, JsonRequestBehavior.AllowGet);
        //}
    }
}