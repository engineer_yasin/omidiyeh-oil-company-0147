﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{
   
    public class OfficeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public OfficeController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        /// <summary>
        /// لیست اداراتی که یک کاربر دسترسی دارد
        /// هفت روز کش می شود بر روی کلاینت کاربر
        /// </summary>
        /// <returns></returns>
        //[OutputCache(Duration = 86400, VaryByCustom ="byUser")]
        public JsonResult Items(int id = 0)
        {
            var list =  new Helpers().UserOffice(Convert.ToInt32(User.Identity.Name),  id);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// لیست اداراتی که یک کاربر دسترسی دارد
        /// هفت روز کش می شود بر روی کلاینت کاربر
        /// </summary>
        /// <returns></returns>
        //[OutputCache(Duration = 86400, VaryByCustom ="byUser",Location = System.Web.UI.OutputCacheLocation.Client)]
        public async Task<JsonResult> Items2()
        {
          var (offices, onlyOfficeDepartment, officeDepartment)  = await new Helpers().UserAccess1(Convert.ToInt32(User.Identity.Name));
            Expression<Func<TblOffice, bool>> filter = (a) =>
                (offices.Any(b=>b == a.Id));
            var list = await _unitOfWork.GetRepository<TblOffice>().Get(filter
                , orderBy: b => b.OrderBy(c => c.Title), includeProperties: "TblOfficeDepartments");
            return Json(onlyOfficeDepartment ? 
                list?.Select(a => new { a.Id, a.Title, Department = a.TblOfficeDepartments.Where(b=>b.Id==officeDepartment).Select(b => new { b.Title, b.Id }) }) :
                list?.Select(a => new { a.Id, a.Title, Department = a.TblOfficeDepartments.Select(b=>new {b.Title,b.Id}) }), JsonRequestBehavior.AllowGet);
        }


        [Authorize(Roles = "48")]
        // GET: Office
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblOffice>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Title.Contains(searchValue) || a.Body.Contains(searchValue),
                    orderBy: order, take: length, skip: start,includeProperties: "TblManagement,TblManagement.TblSubsidiary,TblOfficeDepartments") :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start,includeProperties: "TblManagement,TblManagement.TblSubsidiary,TblOfficeDepartments");
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                a.Title,
                Management = a.TblManagement.Title,
                Subsidiary = a.TblManagement.TblSubsidiary.Title,
                OfficeDepartmentCount = a.TblOfficeDepartments.Count()
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        

        // GET: Office/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOffice>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        [Authorize(Roles = "49")]
        // GET: Office/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Management = new SelectList(await _unitOfWork.GetRepository<TblManagement>().Get(), "Id", "Title");
            ViewBag.Company = new SelectList(await _unitOfWork.GetRepository<TblCompany>().Get(), "Id", "Title");
            ViewBag.Subsidiary = new SelectList(await _unitOfWork.GetRepository<TblSubsidiary>().Get(), "Id", "Title");

            return View();
        }

        // POST: Office/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "49")]

        public async Task<ActionResult> Create( TblOffice entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOffice>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }

                ViewBag.Management = new SelectList(await _unitOfWork.GetRepository<TblManagement>().Get(), "Id", "Title");
                ViewBag.Alternative = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
                ViewBag.Boss = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "50")]

        // GET: Office/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOffice>();
            var result = await iResult.GetSingle(a => a.Id == id,includeProperties: "TblManagement.TblSubsidiary, TblManagement");

            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.Management = new SelectList(await _unitOfWork.GetRepository<TblManagement>().Get(), "Id", "Title",result.Management);
            ViewBag.Company = new SelectList(await _unitOfWork.GetRepository<TblCompany>().Get(), "Id", "Title",result.TblManagement.TblSubsidiary.Company);
            ViewBag.Subsidiary = new SelectList(await _unitOfWork.GetRepository<TblSubsidiary>().Get(), "Id", "Title",result.TblManagement.Subsidiary);
           return View(result);
        }

        // POST: Office/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "50")]

        public async Task<ActionResult> Edit(TblOffice entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOffice>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }
                ViewBag.Management = new SelectList(await _unitOfWork.GetRepository<TblManagement>().Get(), "Id", "Title");
                ViewBag.Alternative = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
                ViewBag.Boss = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "51")]

        // GET: Office/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOffice>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Office/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "51")]

        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOffice>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
