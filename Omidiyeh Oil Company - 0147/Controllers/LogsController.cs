﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;


namespace Omidiyeh_Oil_Company___0147.Controllers
{
    //[Authorize()]
    public class LogsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        //private readonly IUnitOfWork _db;

        public LogsController()
        {
            this._unitOfWork = new UnitOfWork<logEntities>();
           // this._db = new UnitOfWork<dbEntities>();
        }

        public string Test()
        {
            var _log = new logEntities();
           var l= _log.Logs.OrderByDescending(a => a.Time).First();
           return l.Id + " - " + l.Time;
        }
        // GET: Logs
        public ActionResult Index()
        {
            return View();
        }
      //  [HttpPost]
        public JsonResult List(DateTime startDate , DateTime endDate, string body)
        {
            var _log = new logEntities();
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            var draw = Request["draw"];
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);

           

            var iResult = _unitOfWork.GetRepository<Log>();
            var result = !string.IsNullOrEmpty(body)
                ? _log.Logs
                    .Where(a=>a.Time >= startDate && a.Time<=endDate)
                    .Where(a => a.Title.Contains(body) || a.Body.Contains(body))
                    .OrderByDescending(a => a.Time).Take(length).Skip(start)
                : _log.Logs
                    .OrderByDescending(a => a.Time).Take(length).Skip(start);
                //iResult.GetOrdered(a => a.Title.Contains(searchValue) || a.Body.Contains(searchValue),
                //    orderBy: order, take:length, skip:start) 
                // :
                //iResult.GetOrdered(orderBy: order, take: length, skip: start);
           // var (items, count,total) =  result;
            var items =  result.ToList();
            var data = items?.Select(a => new
            {
                id = " ",
                a.Title,
                Body = " 1",//JToken.Parse(a.Body).ToString(Formatting.Indented),
                //a.Body,
                a.Type,
                Result=  a.Result.ToString(),
                a.Device,
                a.User,
                Time= new PersianDateTime(a.Time).ToLongDateString(),
                //Time= a.Time,
                DT_RowId = a.Id,
                //edit = $"<a href='/Logs/Edit/{a.Id.ToString()}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> " 
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = _log.Logs.Count(),
                recordsFiltered = items.Count(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Logs/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<Log>();
            var result = await iResult.GetSingle(a=>a.Id==id);
           if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }


        // GET: Logs/Create
        public async Task<ActionResult> Create()
        {
            return View();
        }

        // POST: Logs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Body,Type,Result,Device,User,Time")] Log entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    entity.Id = Guid.NewGuid();
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<Log>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Logs/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            var _db = new dbEntities();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<Log>();
            
            var result = await iResult.GetSingle(a=>a.Id==id);
            ViewBag.User = _db.TblUsers.Include(a=>a.TblPerson).First(a => a.Id == result.User).TblPerson.Fullname();
                //(await _db.GetRepository<TblUser>().GetFirst(a => a.Id == result.User, includeProperties: "TblPerson")).TblPerson.Fullname();

            if (result == null)
            {
                return HttpNotFound();
            }
           // JToken parsedJson = JToken.Parse(result.Body);
            result.Body = JToken.Parse(result.Body).ToString(Formatting.Indented);
            return View(result);
        }

        // POST: Logs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Body,Type,Result,Device,User,Time")] Log entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    //var user = int.Parse(User.Identity.Name);
                    //var (result, message) = await _unitOfWork.GetRepository<Log>().Update(entity, user);
                    //if (result)
                    //{
                    //    TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                    //    return RedirectToAction("Index");
                    //}
                    //TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    //return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Logs/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<Log>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Logs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<Log>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
