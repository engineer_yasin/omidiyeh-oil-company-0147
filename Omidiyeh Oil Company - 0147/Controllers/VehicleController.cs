﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{
    [Authorize()]
    public class VehicleController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db;
        public VehicleController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            _db = new dbEntities();
        }

        public JsonResult Items()
        {
            var list = ( new Helpers().UserOffice(Convert.ToInt32(User.Identity.Name), -3)).Select(a => a.Id);
            var vehicle =  _unitOfWork.GetRepository<TblVehicle>().GetSync(
                a => list.Contains(a.Office), orderBy: b => b.OrderByDescending(c => c.Title),
                includeProperties: "TblOffice,TblOffice.TblManagement");
            var data = vehicle.Select(a => new
            {
                a.Id, a.Title, Office = a.TblOffice.Title, Management= a.TblOffice.TblManagement.Title
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        // GET: Vehicle
        public ActionResult Index()
        {
            return View();
        }

        public  JsonResult List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            var includeProperties = "TblVehicleType,TblOffice,TblOffice.TblManagement";
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblVehicle>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrderedSync(a => a.Title.Contains(searchValue) 
                    || a.TypeText.Contains(searchValue)
                    || a.Plate.Contains(searchValue)
                    || a.Des.Contains(searchValue)
                    //|| a.TblVehicleDrivers.AsEnumerable().Any(b=>b.TblPerson.Name.Contains(searchValue))
                    //|| a.TblVehicleDrivers.AsEnumerable().Any(b=>b.TblPerson.Family.Contains(searchValue))
                    || a.TblOffice.Title.Contains(searchValue)
                    || a.TblOffice.TblManagement.Title.Contains(searchValue)
                    
                    ,
                    orderBy: order, take: length, skip: start, includeProperties: includeProperties) :
                    iResult.GetOrderedSync(orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                a.Title,
                a.TypeText,
                a.Plate,
                Type = a.TblVehicleType.Title,
                a.Des,
                a.Capacity,
                Enable = a.Enable ? "فعال" : "غیرفعال",
                Office = a.TblOffice.Title,
                Management = a.TblOffice.TblManagement.Title,
                edit = ""
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ListDriver(int id)
        {
            var list = await _unitOfWork.GetRepository<TblVehicleDriver>().Get(a => a.Vehicle == id, includeProperties: "TblPerson,TblPerson.TblOfficeDepartment.TblOffice");
            var data = list.Select(a => new
            {
                a.Id,
                a.TblPerson.Name,
                a.TblPerson.Family,
                a.TblPerson.NationalCode,
                Office = a.TblPerson.TblOfficeDepartment.TblOffice.Title,

            }).OrderBy(a => a.Family);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> AddDriver(int id, string driver)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var p = await _unitOfWork.GetRepository<TblPerson>().GetFirst(a => a.NationalCode == driver);
                    var vd = new TblVehicleDriver { Vehicle = id, Person = p.Id };
                    var (result, msg) = await _unitOfWork.GetRepository<TblVehicleDriver>().Insert(vd, user);
                    m.Msg = result
                        ? string.Format(Resources.Public.AddSuccessMessage, "اختصاص راننده")
                        : string.Format(Resources.Public.AddErrorMessage, "اختصاص راننده", msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteDriver(int id)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var (result, msg) = await _unitOfWork.GetRepository<TblVehicleDriver>().Delete(id, user);
                    m.Msg = result ? string.Format(Resources.Public.DeleteSuccessMessage) :
                        string.Format(Resources.Public.DeleteErrorMessage, msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
#pragma warning disable 4014
                Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
#pragma warning restore 4014
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
        // GET: Vehicle/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblVehicle>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }


        // GET: Vehicle/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblVehicleType>().Get(), "Id", "Title");
            return View();
        }

        // POST: Vehicle/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Title,Type,Capacity,Enable,Office")] TblVehicle entity, bool? addAnother)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblVehicle>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successToast"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return addAnother == true ? RedirectToAction("Create") : RedirectToAction("Index");
                    }
                    TempData["errorToast"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }

                ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblVehicleType>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorToast"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Vehicle/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            var result = _db.TblVehicles
                .Include(a => a.TblOffice)
                .Include(a => a.TblOffice.TblManagement)
                .Include(a => a.TblOffice.TblManagement.TblSubsidiary)
                .FirstOrDefault(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            ViewBag.Office = result.Office;
            ViewBag.Management = result.TblOffice.Management;
            ViewBag.Subsidiary = result.TblOffice.TblManagement.Subsidiary;
            ViewBag.Company = result.TblOffice.TblManagement.TblSubsidiary.Company;

            ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblVehicleType>().Get(), "Id", "Title");
            return View(result);
        }

        // POST: Vehicle/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(TblVehicle entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblVehicle>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }
                ViewBag.Office = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblOffice>().Get(), "Id", "Title");
                ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblVehicleType>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Vehicle/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblVehicle>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Vehicle/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblVehicle>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
