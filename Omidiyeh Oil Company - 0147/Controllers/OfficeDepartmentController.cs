﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{
    [Authorize()]
    public class OfficeDepartmentController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public OfficeDepartmentController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }

        [OutputCache(Duration = 86400, VaryByParam = "id")]
        public string Name(int id)
        {
            var db = new dbEntities();
            var title = db.TblOfficeDepartments.Find(id)?.Title;
            return title;
        }

        [Authorize(Roles = "54")]

        // GET: OfficeDepartment
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// لیست اداراتی که یک کاربر دسترسی دارد
        /// هفت روز کش می شود بر روی کلاینت کاربر
        /// </summary>
        /// <returns></returns>
        //[OutputCache(Duration = 86400,VaryByCustom ="byUser",Location = System.Web.UI.OutputCacheLocation.Client)]
        public  JsonResult Items(int id = 0)
        {
            var list =  new Helpers().UserOfficeDepartment(Convert.ToInt32(User.Identity.Name), id);

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// لیست واحد های یک اداره
        /// هفت روز کش می شود بر روی کلاینت کاربر
        /// </summary>
        /// <param name="office"></param>
        /// <returns></returns>
        //[OutputCache(Duration = 86400, Location =System.Web.UI.OutputCacheLocation.Client)]
        public async Task<JsonResult> Items2(int office = 0)
        {
            Expression<Func<TblOfficeDepartment, bool>> filter = (a) =>
                (office == 0 || a.Office == office);
            var list = await _unitOfWork.GetRepository<TblOfficeDepartment>().Get(filter
                , orderBy: b => b.OrderByDescending(c => c.Title),includeProperties: "TblOffice,TblOffice.TblManagement");
            return Json(list.Select(a=> new {a.Id, Office=a.TblOffice.Title, a.Title, Management=a.TblOffice.TblManagement.Title}), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblOfficeDepartment>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Title.Contains(searchValue) || a.Body.Contains(searchValue),
                    orderBy: order, take: length, skip: start,includeProperties: "TblOffice,TblOffice.TblManagement,TblOfficeDepartmentAccountNumbers,TblPersons") :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start, includeProperties: "TblOffice,TblOffice.TblManagement,TblOfficeDepartmentAccountNumbers,TblPersons");
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                a.Title,
                Office = a.TblOffice.Title,
                Management = a.TblOffice.TblManagement.Title,
                AccountNumbersCount = a.TblOfficeDepartmentAccountNumbers.Count(),
                Person = a.TblPersons.Count()
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: OfficeDepartment/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeDepartment>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        [Authorize(Roles = "56")]

        // GET: OfficeDepartment/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Office = new SelectList(await _unitOfWork.GetRepository<TblOffice>().Get(), "Id", "Title");
            ViewBag.Alternative = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
            ViewBag.Boss = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
            return View();
        }

        // POST: OfficeDepartment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "56")]

        public async Task<ActionResult> Create(TblOfficeDepartment entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeDepartment>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }

                ViewBag.Office = new SelectList(await _unitOfWork.GetRepository<TblOffice>().Get(), "Id", "Title");
                ViewBag.Alternative = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
                ViewBag.Boss = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "57")]

        // GET: OfficeDepartment/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeDepartment>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.Office = new SelectList(await _unitOfWork.GetRepository<TblOffice>().Get(), "Id", "Title");
            ViewBag.Alternative = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
            ViewBag.Boss = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
            return View(result);
        }

        // POST: OfficeDepartment/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "57")]

        public async Task<ActionResult> Edit( TblOfficeDepartment entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeDepartment>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }
                ViewBag.Office = new SelectList(await _unitOfWork.GetRepository<TblOffice>().Get(), "Id", "Title");
                ViewBag.Alternative = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
                ViewBag.Boss = new SelectList(await _unitOfWork.GetRepository<TblUser>().Get(), "Id", "UserName");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "58")]

        // GET: OfficeDepartment/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeDepartment>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: OfficeDepartment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "58")]

        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeDepartment>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
