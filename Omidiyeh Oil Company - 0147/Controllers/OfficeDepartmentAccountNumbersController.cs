﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{

    public class OfficeDepartmentAccountNumbersController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public OfficeDepartmentAccountNumbersController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        /// <summary>
        /// لیست شماره حساب هایی که یک کاربر دسترسی دارد
        /// هفت روز کش می شود
        /// </summary>
        /// <returns></returns>
      //  //[OutputCache(Duration = 86400, VaryByCustom = "byUser")]
        [AllowAnonymous]
        public JsonResult Items()
        {
            var list =  new Helpers().UserOfficeDepartment(Convert.ToInt32(User.Identity.Name), -3).Select(a => a.Id);
            var account =  _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>().GetSync(
                a => list.Contains(a.OfficeDepartment), orderBy: b => b.OrderByDescending(c => c.Title));
            return Json(account, JsonRequestBehavior.AllowGet);
        }


        // GET: OfficeDepartmentAccountNumbers/JsonListByUser/
        //[OutputCache(Duration = 86400)]
        public async Task<JsonResult> JsonListByUser()
        {
            var user = Convert.ToInt32(User.Identity.Name);
            var office = await UserOfficeDepartment(user);
            return await JsonList(office);
        }

        private async Task<int> UserOfficeDepartment(int user)
        {
            return (await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == user, includeProperties: "TblPerson")).TblPerson.OfficeDepartment;
        }

        // GET: OfficeDepartmentAccountNumbers/JsonList/?office=5
        //[OutputCache(Duration = 86400)]
        public async Task<JsonResult> JsonList(int office = 0)
        {
            Expression<Func<TblOfficeDepartmentAccountNumber, bool>> filter = (a) =>
                (office == 0 || a.OfficeDepartment == office);
            var list = await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>().Get(filter
                , orderBy: b => b.OrderByDescending(c => c.Title));
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        // GET: OfficeDepartmentAccountNumbers/JsonOffice/?id=5
        [OutputCache(Duration = 86400, VaryByParam ="id")]
        public async Task<JsonResult> JsonOffice(int id)
        {
            Expression<Func<TblOfficeDepartmentAccountNumber, bool>> filter = (a) =>
                (id == a.Id);
            var item = await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>().GetSingle(filter, includeProperties: "TblOfficeDepartment,TblOfficeDepartment.TblOffice");
            return Json(new
            {
                OfficeDepartment = item.TblOfficeDepartment.Title, Office = item.TblOfficeDepartment.TblOffice.Title
            }, JsonRequestBehavior.AllowGet);
        }
         public async Task<JsonResult> JsonOffice2(int id)
        {
            Expression<Func<TblOfficeDepartmentAccountNumber, bool>> filter = (a) =>
                (id == a.Id);
            var item = await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>().GetSingle(filter, includeProperties: "TblOfficeDepartment,TblOfficeDepartment.TblOffice");
            return Json(new
            {
                OfficeDepartmentTitle = item.TblOfficeDepartment.Title, OfficeTitle = item.TblOfficeDepartment.TblOffice.Title,
                item.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company,
                item.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary,
                item.TblOfficeDepartment.TblOffice.Management,
                item.TblOfficeDepartment.Office,
                item.OfficeDepartment,
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "63")]

        // GET: OfficeDepartmentAccountNumbers
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Title.Contains(searchValue) || a.AccountNumber.Contains(searchValue),
                    orderBy: order, take: length, skip: start, includeProperties: "TblOfficeDepartment,TblOfficeDepartment.TblOffice") :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start, includeProperties: "TblOfficeDepartment,TblOfficeDepartment.TblOffice");
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                OfficeDepartment = a.TblOfficeDepartment.Title,
                Office = a.TblOfficeDepartment.TblOffice.Title,
                a.AccountNumber,
                a.Title
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: OfficeDepartmentAccountNumbers/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        [Authorize(Roles = "64")]

        // GET: OfficeDepartmentAccountNumbers/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.OfficeDepartment = new SelectList(await _unitOfWork.GetRepository<TblOfficeDepartment>().Get(), "Id", "Title");
            return View();
        }

        // POST: OfficeDepartmentAccountNumbers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "64")]

        public async Task<ActionResult> Create(TblOfficeDepartmentAccountNumber entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }

                ViewBag.OfficeDepartment = new SelectList(await _unitOfWork.GetRepository<TblOfficeDepartment>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "65")]

        // GET: OfficeDepartmentAccountNumbers/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>();
            var result = await iResult.GetSingle(a => a.Id == id,
                includeProperties: @"TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary,
                                    TblOfficeDepartment.TblOffice.TblManagement,
                                    TblOfficeDepartment.TblOffice");
            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.OfficeDepartment = new SelectList(await _unitOfWork.GetRepository<TblOfficeDepartment>().Get(), "Id", "Title", result.OfficeDepartment);
            return View(result);
        }

        // POST: OfficeDepartmentAccountNumbers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "65")]

        public async Task<ActionResult> Edit( TblOfficeDepartmentAccountNumber entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }
                ViewBag.OfficeDepartment = new SelectList(await _unitOfWork.GetRepository<TblOfficeDepartment>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "66")]

        // GET: OfficeDepartmentAccountNumbers/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: OfficeDepartmentAccountNumbers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "66")]

        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
