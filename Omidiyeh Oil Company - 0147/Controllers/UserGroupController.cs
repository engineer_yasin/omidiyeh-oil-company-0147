﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{

    public class UserGroupController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserGroupController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        [Authorize(Roles = "97")]
        // GET: UserGroup
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "97")]
        [HttpPost]
        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblUserGroup>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Title.Contains(searchValue) || a.Des.Contains(searchValue),
                    orderBy: order, take: length, skip: start) :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start);
            var (items, count, total) = await result;

            var data = items.Where(a=>a.Id!=0).Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                a.Title,
                a.Des,
                a.TblUsers,
                a.TblUserGroupRoles
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: UserGroup/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblUserGroup>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        [Authorize(Roles = "99")]
        // GET: UserGroup/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserGroup/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "98")]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Des")] TblUserGroup entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblUserGroup>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "99")]
        // GET: UserGroup/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblUserGroup>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }

            var t1 = await _unitOfWork.GetRepository<TblUserGroupRole>().Get(a => a.Group == id);
            var t2 = await _unitOfWork.GetRepository<TblRole>().Get(orderBy: o => o.OrderBy(a => a.Title));
            //  await Task.WhenAll(t1, t2);
            ViewBag.UserGroupRole = t1;
            ViewBag.Roles = t2;
            return View(result);
        }

        // POST: UserGroup/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "99")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Des")] TblUserGroup entity, List<int> userGroupRole)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblUserGroup>().Update(entity, user);
                    var newRole = userGroupRole;
                    var oldRole = (await _unitOfWork.GetRepository<TblUserGroupRole>().Get(a => a.Group == entity.Id))
                        .Select(a => a.Role).ToList();
                    if (newRole != null)
                    {
                        var deleteRole = oldRole.Except(newRole).ToList();
                        if (deleteRole.Any())
                        {
                            var (resultDelete, _, messageDelete) = await _unitOfWork.GetRepository<TblUserGroupRole>()
                                .DeleteRange(a => a.Group == entity.Id && deleteRole.Contains(a.Role), user);
                        }

                        var list = newRole.Except(oldRole).ToList()
                            .Select(item => new TblUserGroupRole { Role = item, Group = entity.Id }).ToList();
                        if (list.Any()) await _unitOfWork.GetRepository<TblUserGroupRole>().InsertRange(list, user);

                        if (result)
                        {
                            TempData["successToast"] =
                                string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                            return RedirectToAction("Index");
                        }

                    }
                    else
                    {
                        var (resultDelete, _, messageDelete) = await _unitOfWork.GetRepository<TblUserGroupRole>()
                            .DeleteRange(a => a.Group == entity.Id, user);
                        if (resultDelete)
                        {
                            TempData["successToast"] =
                                string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            TempData["errorToast"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, messageDelete);
                            return RedirectToAction("Edit", entity.Id);
                        }
                    }




                    TempData["errorToast"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Edit", entity.Id);
                }
            }
            catch (Exception e)
            {
                TempData["errorToast"]  = string.Format(Resources.Public.ErrorMessage, e.Message);
                return RedirectToAction("Edit", entity.Id);
            }

            return View();
        }

        [Authorize(Roles = "100")]
        // GET: UserGroup/Delete/5
        public async Task<ActionResult> Delete(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblUserGroup>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }


        // POST: UserGroup/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "100")]
        public async Task<ActionResult> DeleteConfirmed(byte id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblUserGroup>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
