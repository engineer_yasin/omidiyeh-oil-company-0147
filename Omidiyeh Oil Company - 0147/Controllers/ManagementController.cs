﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;

using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{
    [Authorize()]
    public class ManagementController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ManagementController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        /// <summary>
        /// لیست اداراتی که یک کاربر دسترسی دارد
        /// هفت روز کش می شود بر روی کلاینت کاربر
        /// </summary>
        /// <returns></returns>
        //[OutputCache(Duration = 120,VaryByCustom ="byUser",Location = System.Web.UI.OutputCacheLocation.Client)]
        public JsonResult Items(int id = 0)
        {
            var list =  new Helpers().UserManagement(Convert.ToInt32(User.Identity.Name), subsidiary: id);

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(Duration = 86400, VaryByParam = "id")]
        public string Name(int id)
        {
            var db = new dbEntities();
            var title = db.TblManagements.Find(id)?.Title;
            return title;
        }
        [Authorize(Roles = "44")]
        // GET: Welfare/Management
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblManagement>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Title.Contains(searchValue) || a.TblSubsidiary.Title.Contains(searchValue)
                                                                      || a.TblSubsidiary.TblCompany.Title.Contains(searchValue),
                    orderBy: order, take: length, skip: start,includeProperties: "TblSubsidiary,TblSubsidiary.TblCompany,TblOffices") :
                iResult.GetOrdered(orderBy: order, take: length, skip: start, includeProperties: "TblSubsidiary,TblSubsidiary.TblCompany,TblOffices");
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                a.Title,
                a.Tel,
                Subsidiary = a.TblSubsidiary.Title,
                Company = a.TblSubsidiary.TblCompany.Title,
                OfficeCount = a.TblOffices.Count(),
                DT_RowId = a.Id
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Welfare/Management/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblManagement>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        [Authorize(Roles = "45")]

        // GET: Welfare/Management/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Company = new SelectList(await _unitOfWork.GetRepository<TblCompany>().Get(), "Id", "Title");
            ViewBag.Subsidiary = new SelectList(await _unitOfWork.GetRepository<TblSubsidiary>().Get(), "Id", "Title");

            return View();
        }

        // POST: Welfare/Management/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "45")]

        public async Task<ActionResult> Create( TblManagement entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblManagement>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "46")]
        // GET: Welfare/Management/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblManagement>();
            var result = await iResult.GetSingle(a=>a.Id==id,includeProperties: "TblSubsidiary");
            ViewBag.Company = new SelectList(await _unitOfWork.GetRepository<TblCompany>().Get(), "Id", "Title",result.TblSubsidiary.Company);
            ViewBag.Subsidiary = new SelectList(await _unitOfWork.GetRepository<TblSubsidiary>().Get(), "Id", "Title",result.Subsidiary);
            if (result == null)
            {
                return HttpNotFound();
            }

            return View(result);
        }

        // POST: Welfare/Management/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "46")]

        public async Task<ActionResult> Edit(TblManagement entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblManagement>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "47")]

        // GET: Welfare/Management/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblManagement>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Welfare/Management/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "47")]

        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblManagement>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
