﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{
    public class DashboardController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public DashboardController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        // GET: Dashboard
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Company()
        {
            ViewBag.Company = _unitOfWork.GetRepository<TblCompany>().Count().Item1;
            ViewBag.Subsidiary = _unitOfWork.GetRepository<TblSubsidiary>().Count().Item1;
            ViewBag.Management = _unitOfWork.GetRepository<TblManagement>().Count().Item1;
            ViewBag.Office = _unitOfWork.GetRepository<TblOffice>().Count().Item1;
            ViewBag.OfficeDepartment = _unitOfWork.GetRepository<TblOfficeDepartment>().Count().Item1;
            ViewBag.OfficeDepartmentAccountNumber = _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>().Count().Item1;
            return View();
        }
    }
}