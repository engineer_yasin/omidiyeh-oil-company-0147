﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{
	
    public class SubsidiaryController : Controller
    {
		private readonly IUnitOfWork _unitOfWork;

		public SubsidiaryController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        /// <summary>
        /// لیست اداراتی که یک کاربر دسترسی دارد
        /// هفت روز کش می شود بر روی کلاینت کاربر
        /// </summary>
        /// <returns></returns>
        //[OutputCache(Duration = 86400, VaryByCustom ="byUser",Location = System.Web.UI.OutputCacheLocation.Client)]
        public JsonResult Items(int id = 0)
        {
            var list =  new Helpers().UserSubsidiary(Convert.ToInt32(User.Identity.Name),company:id);

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(Duration = 86400, VaryByParam = "id")]
        public string Name(int id)
        {
            var db = new dbEntities();
            var title = db.TblSubsidiaries.Find(id)?.Title;
            return title;
        }

        [Authorize(Roles = "59")]

        // GET: Subsidiary
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
			var iResult = _unitOfWork.GetRepository<TblSubsidiary>();
            var result = !string.IsNullOrEmpty(searchValue) ? 
                iResult.GetOrdered(a => a.Title.Contains(searchValue) || a.Des.Contains(searchValue),
                    orderBy: order,take:length, skip:start,includeProperties: "TblCompany,TblManagements") :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start, includeProperties: "TblCompany,TblManagements");
            var (items, count,total) = await result;

            var data = items.Select(a => new { id = " ", 
                 DT_RowId = a.Id,
                                    a.Id,
                                    a.Title,
                                    a.Des,
                                    Company= a.TblCompany.Title,
                                   ManagementCount= a.TblManagements.Count()
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

		// GET: Subsidiary/Details/5
		public async Task<ActionResult> Details(byte id)
        {
			if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblSubsidiary>();
            var result = await iResult.GetSingle(a=>a.Id==id);
			if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        [Authorize(Roles = "60")]

        // GET: Subsidiary/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Company = new SelectList(await _unitOfWork.GetRepository<TblCompany>().Get(), "Id", "Title");
            return View();
        }

        // POST: Subsidiary/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "60")]

        public async Task<ActionResult> Create( TblSubsidiary entity)
        {
			try
            {
            if (ModelState.IsValid)
            {
				var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblSubsidiary>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                return RedirectToAction("Index");
            }

            ViewBag.Company = new SelectList(await _unitOfWork.GetRepository<TblCompany>().Get(), "Id", "Title");
			}
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "61")]

        // GET: Subsidiary/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblSubsidiary>();
            var result = await iResult.GetSingle(a=>a.Id==id);
			if (result == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.Company = new SelectList(await _unitOfWork.GetRepository<TblCompany>().Get(), "Id", "Title");
            return View(result);
        }

        // POST: Subsidiary/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "61")]

        public async Task<ActionResult> Edit( TblSubsidiary entity)

        {
			try
            {
            if (ModelState.IsValid)
            {
					var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblSubsidiary>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
					return RedirectToAction("Index");
            }
            ViewBag.Company = new SelectList(await _unitOfWork.GetRepository<TblCompany>().Get(), "Id", "Title");
			}
			catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Subsidiary/Delete/
        [Authorize(Roles = "62")]

        public async Task<ActionResult> Delete(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblSubsidiary>();
            var result = await iResult.GetSingle(a=>a.Id==id);
			if (result == null)
            {
                return HttpNotFound();
            }      
            return View(result);
        }

        // POST: Subsidiary/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "62")]

        public async Task<ActionResult> DeleteConfirmed(byte id)
        {
			try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblSubsidiary>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new {id});
        }

    }
}
