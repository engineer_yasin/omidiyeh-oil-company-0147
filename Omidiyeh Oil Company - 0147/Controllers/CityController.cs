﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{
    public class CityController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db;
        public CityController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            _db = new dbEntities();
        }

        public JsonResult Items()
        {
            var items = _db.TblCities.Where(a => a.Enable).OrderBy(a=>a.Title).ToList();
            var data = items.Select(a => new
            {
                a.Id,
                a.Title,
               
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: City
        public ActionResult Index()
        {
            return View();
        }
    }
}