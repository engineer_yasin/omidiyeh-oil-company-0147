﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using System.Web.Razor.Parser;

namespace Omidiyeh_Oil_Company___0147.Controllers
{

    public class PersonController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db;
        public PersonController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            _db = new dbEntities();

        }

        public async Task<JsonResult> GetUser(string id)
        {
            if (string.IsNullOrEmpty(id))
                return Json("NotFound", JsonRequestBehavior.AllowGet);
            var iResult = _unitOfWork.GetRepository<TblUser>();

            var result = await iResult.GetSingle(a => a.TblPerson.PersonnelCode == id || a.TblPerson.NationalCode == id, includeProperties: "TblPerson,TblPerson.TblOfficeDepartment.TblOffice,TblPerson.TblOfficeDepartment");
            if (result == null) return Json("NotFound", JsonRequestBehavior.AllowGet);
            var data = new
            {
                Id = result.Id,
                Name = result.TblPerson.Name,
                Family = result.TblPerson.Family,
                NationalCode = result.TblPerson.NationalCode,
                Office = result.TblPerson.TblOfficeDepartment.TblOffice.Title,
                OfficeDepartment = result.TblPerson.TblOfficeDepartment.Title,
                PersonnelCode = result.TblPerson.PersonnelCode,
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Item(string code)
        {
            var result = _db.TblPersons.FirstOrDefault(a =>
                (a.NationalCode == code || a.PersonnelCode == code) && a.FamilyRelationship == 0);
            if (result == null) return Json("NotFound", JsonRequestBehavior.AllowGet);
            var data = new
            {
                result.Id,
                result.Name,
                result.Family,
                result.NationalCode,
                //Office = result.TblOfficeDepartment.TblOffice.Title,
                //OfficeDepartment = result.TblPerson.TblOfficeDepartment.Title,
                result.PersonnelCode,
                result.Mobile
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Details(string nationalCode)
        {
            var result = _db.TblPersons.FirstOrDefault(a => a.NationalCode == nationalCode);
            if (result == null) return Json("NotFound", JsonRequestBehavior.AllowGet);
            var data = new
            {
                result.Id,
                result.Name,
                result.Family,
                result.NationalCode,
                //Office = result.TblOfficeDepartment.TblOffice.Title,
                //OfficeDepartment = result.TblPerson.TblOfficeDepartment.Title,
                result.PersonnelCode,
                result.Mobile
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> DetailsJson(string personnel = "", string nationalCode = "", int family = 0)
        {
            if (string.IsNullOrEmpty(personnel) && string.IsNullOrEmpty(nationalCode))
                return Json("NotFound", JsonRequestBehavior.AllowGet);
            var iResult = _unitOfWork.GetRepository<TblPerson>();
            Expression<Func<TblPerson, bool>> filter = null;
            if (personnel != "")
                filter = a => a.PersonnelCode.Trim() == personnel && a.FamilyRelationship == family;
            else if (nationalCode != "")
                filter = a => a.NationalCode == nationalCode;

            var result = await iResult.GetSingle(filter, includeProperties: "TblOfficeDepartment.TblOffice,TblOfficeDepartment,TblFamilyRelationship");
            if (result == null) return Json("NotFound", JsonRequestBehavior.AllowGet);
            var data = new Person
            {
                Id = result.Id,
                Name = result.Name,
                Family = result.Family,
                NationalCode = result.NationalCode,
                Office = result.TblOfficeDepartment.TblOffice.Title,
                OfficeDepartment = result.TblOfficeDepartment.Title,
                PersonnelCode = result.PersonnelCode,
                FamilyRelationship = result.TblFamilyRelationship.Title,

                Fullname = result.Name + " " + result.Family,
                Mobile = result.Mobile,
                Tel = result.Tel,
                //  PersonnelCode=  result.PersonnelCode

            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DetailsByPersonal(string personnel = "", int family = 0)
        {
            if (string.IsNullOrEmpty(personnel))
                return Json("NotFound", JsonRequestBehavior.AllowGet);
            var iResult = _unitOfWork.GetRepository<TblPerson>();
            Expression<Func<TblPerson, bool>> filter = null;

            filter = a => (a.PersonnelCode.Trim() == personnel || a.NationalCode == personnel) && a.FamilyRelationship == family;


            var result = await iResult.GetSingle(filter, includeProperties: "TblOfficeDepartment.TblOffice,TblOfficeDepartment,TblFamilyRelationship");
            if (result == null) return Json("NotFound", JsonRequestBehavior.AllowGet);
            var data = new Person
            {
                Id = result.Id,
                Name = result.Name,
                Family = result.Family,
                NationalCode = result.NationalCode,
                Office = result.TblOfficeDepartment.TblOffice.Title,
                OfficeDepartment = result.TblOfficeDepartment.Title,
                PersonnelCode = result.PersonnelCode,
                FamilyRelationship = result.TblFamilyRelationship.Title,

                Fullname = result.Name + " " + result.Family,
                Mobile = result.Mobile,
                Tel = result.Tel,
                //  PersonnelCode=  result.PersonnelCode

            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FamilyJson(string code)
        {
            if (string.IsNullOrEmpty(code))
                return Json("NotFound", JsonRequestBehavior.AllowGet);
            var iResult = _unitOfWork.GetRepository<TblPerson>();
            List<TblPerson> persons;
            if (code.Length > 6)
            {
                if (_db.TblPersons.FirstOrDefault(a => a.NationalCode == code && a.FamilyRelationship == 0)?.PersonnelCode?.Length > 1)
                {
                    code = _db.TblPersons.First(a => a.NationalCode == code).PersonnelCode;
                    persons = _db.TblPersons.AsNoTracking()
                        .Where(a => a.PersonnelCode == code)
                        .Include(a => a.TblOfficeDepartment.TblOffice)
                        .Include(a => a.TblOfficeDepartment)
                        .Include(a => a.TblFamilyRelationship)
                        .OrderBy(a => a.FamilyRelationship).ToList();
                }
                else
                {
                    persons = _db.TblPersons.AsNoTracking()
                        .Where(a => a.NationalCode == code)
                        .Include(a => a.TblOfficeDepartment.TblOffice)
                        .Include(a => a.TblOfficeDepartment)
                        .Include(a => a.TblFamilyRelationship)
                        .OrderBy(a => a.FamilyRelationship).ToList();
                }

            }
            else
            {

                persons = _db.TblPersons.AsNoTracking()
                    .Where(a => a.PersonnelCode == code)
                    .Include(a => a.TblOfficeDepartment.TblOffice)
                    .Include(a => a.TblOfficeDepartment)
                    .Include(a => a.TblFamilyRelationship)
                    .OrderBy(a => a.FamilyRelationship).ToList();
            }


            if (!persons.Any()) return Json("NotFound", JsonRequestBehavior.AllowGet);

            var list = persons.Select(item => new Person
            {
                Name = item.Name,
                Family = item.Family,
                FamilyRelationship = item.TblFamilyRelationship.Title,
                Fullname = item.Name + " " + item.Family,
                Id = item.Id,
                NationalCode = item.NationalCode,

                Office = item.TblOfficeDepartment.TblOffice.Title,
                OfficeDepartment = item.TblOfficeDepartment.Title,
                PersonnelCode = item.PersonnelCode
            })
                .ToList();

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserAccess()
        {
            

            return PartialView();
        }
        // [HttpPost]
        //[Authorize]
        public async Task<JsonResult> Items(SearchAdvancedPerson model)
        {
            model.Name = model.Name?.ToFarsi();
            model.Family = model.Family?.ToFarsi();

            // model.FamilyRelationship = new List<byte> {0}; 
            var user = Convert.ToInt32(User.Identity.Name);
            var accessDepartment = new Helpers().UserOfficeDepartment(user, -3).Select(a => a.Id);
            Expression<Func<TblPerson, bool>> filter = (a) =>
               (string.IsNullOrEmpty(model.Name) || a.Name.Contains(model.Name)) &&
               (string.IsNullOrEmpty(model.Family) || a.Family.Contains(model.Family)) &&
               (string.IsNullOrEmpty(model.NationalCode) || a.NationalCode == model.NationalCode) &&
               (string.IsNullOrEmpty(model.PersonnelCode) || a.PersonnelCode == model.PersonnelCode) &&
               //(string.IsNullOrEmpty(model.Address) || a.Address.Contains(model.Address)) &&
               //(string.IsNullOrEmpty(model.Tel) || a.Tel.Contains(model.Tel)) &&
               //(string.IsNullOrEmpty(model.Mobile) || a.Mobile.Contains(model.Mobile)) &&
               //(string.IsNullOrEmpty(model.Mobile) || a.Mobile2.Contains(model.Mobile)) &&
               //(model.Company == 0 || a.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == model.Company) &&
               //(model.Subsidiary == 0 || a.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == model.Subsidiary) &&
               //(model.Management == 0 || a.TblOfficeDepartment.TblOffice.Management == model.Management) &&
               //(model.Office == 0 || a.TblOfficeDepartment.Office == model.Office) &&
               (model.OfficeDepartment == -1 || a.OfficeDepartment == model.OfficeDepartment) &&
               (model.Group.Contains(0) || model.Group.Contains(a.Group)) &&
               (model.ShiftWorker == null || model.ShiftWorker == a.ShiftWorker) &&
               (model.Retierd == null || model.Retierd == a.Retierd) &&
               (model.Disable == null || model.Disable == a.Disable) &&
               (model.FamilyRelationship.Contains(255) || model.FamilyRelationship.Contains(a.FamilyRelationship)) &&
               (accessDepartment.Contains(a.OfficeDepartment)) &&

               (a.Id != -1 && a.Id != -2)
               ;
            // (string.IsNullOrEmpty(model.BirthDate) || a.BirthDate == model.BirthDate) &&

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = "TblUsers,TblOfficeDepartment,TblOfficeDepartment.TblOffice,TblOfficeDepartment.TblOffice.TblManagement,TblPersonGroup,TblFamilyRelationship";
            var iResult = _unitOfWork.GetRepository<TblPerson>();
            var result =
                iResult.GetOrdered(filter: filter,
                    orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                //a.Id,
                a.Name,
                a.Family,
                a.NationalCode,
                a.PersonnelCode,
                FamilyRelationship = a.TblFamilyRelationship.Title,
                Office = a.TblOfficeDepartment.TblOffice.Title,
                OfficeDepartment = a.TblOfficeDepartment.Title,
                Management = a.TblOfficeDepartment.TblOffice.TblManagement.Title,
                Status = a.Disable ? "غیرفعال" : "فعال",
                Group = a.TblPersonGroup.Title,
                User = a.TblUsers.Any()
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);


        }
        [Authorize(Roles = "67")]
        // GET: Person
        //[OutputCache(Duration = 86400,VaryByCustom ="byUser",Location = System.Web.UI.OutputCacheLocation.Client)]
        public async Task<ActionResult> Index()
        {
            ViewBag.FamilyRelationship = new SelectList(await _unitOfWork.GetRepository<TblFamilyRelationship>().Get(), "Id", "Title");
            //// ViewBag.Management = new SelectList(await _unitOfWork.GetRepository<TblManagement>().Get(), "Id", "Title");
            //ViewBag.Office = new SelectList(await _unitOfWork.GetRepository<TblOffice>().Get(), "Id", "Title");
            // ViewBag.OfficeDepartment = new SelectList(await _unitOfWork.GetRepository<TblOfficeDepartment>().Get(), "Id", "Title");
            ViewBag.Disable = new SelectList(await _unitOfWork.GetRepository<TblPersonStatu>().Get(), "Id", "Title");
            ViewBag.Group = new SelectList(await _unitOfWork.GetRepository<TblPersonGroup>().Get(), "Id", "Title");
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> List(SearchAdvancedPerson model)
        {
            model.Name = model.Name?.ToFarsi();
            model.Family = model.Family?.ToFarsi();
            Expression<Func<TblPerson, bool>> filter = (a) =>
                (string.IsNullOrEmpty(model.Name) || a.Name.Contains(model.Name)) &&
                (string.IsNullOrEmpty(model.Family) || a.Family.Contains(model.Family)) &&
                (string.IsNullOrEmpty(model.NationalCode) || a.NationalCode.Contains(model.NationalCode)) &&
                (string.IsNullOrEmpty(model.PersonnelCode) || a.PersonnelCode.Contains(model.PersonnelCode)) &&
                (string.IsNullOrEmpty(model.Address) || a.Address.Contains(model.Address)) &&
                (string.IsNullOrEmpty(model.Tel) || a.Tel.Contains(model.Tel)) &&
                (string.IsNullOrEmpty(model.Mobile) || a.Mobile.Contains(model.Mobile)) &&
                (string.IsNullOrEmpty(model.Mobile) || a.Mobile2.Contains(model.Mobile)) &&
                (model.Company == 0 || a.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == model.Company) &&
                (model.Subsidiary == 0 || a.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == model.Subsidiary) &&
                (model.Management == 0 || a.TblOfficeDepartment.TblOffice.Management == model.Management) &&
                (model.Office == 0 || a.TblOfficeDepartment.Office == model.Office) &&
                (model.OfficeDepartment == -1 || a.OfficeDepartment == model.OfficeDepartment) &&
                (model.Group.Contains(0) || model.Group.Contains(a.Group)) &&
                (model.ShiftWorker == null || model.ShiftWorker == a.ShiftWorker) &&
                (model.Disable == null || model.Disable == a.Disable) &&
                (model.FamilyRelationship.Contains(255) || model.FamilyRelationship.Contains(a.FamilyRelationship)) &&
                (a.Id != -1 && a.Id != -2)
                ;
            // (string.IsNullOrEmpty(model.BirthDate) || a.BirthDate == model.BirthDate) &&

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = "TblUsers,TblOfficeDepartment,TblOfficeDepartment.TblOffice,TblOfficeDepartment.TblOffice.TblManagement,TblPersonGroup,TblFamilyRelationship";
            var iResult = _unitOfWork.GetRepository<TblPerson>();
            var result =
                iResult.GetOrdered(filter: filter,
                    orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = await result;

            var data = items.Where(a => a.Id > 1).Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                //a.Id,
                a.Name,
                a.Family,
                a.NationalCode,
                a.PersonnelCode,
                FamilyRelationship = a.TblFamilyRelationship.Title,
                FamilyRelationshipId = a.FamilyRelationship,
                BirthDate = new PersianDateTime(a.BirthDate).ToShortDateString(),
                Office = a.TblOfficeDepartment.TblOffice.Title,
                OfficeDepartment = a.TblOfficeDepartment.Title,
                Management = a.TblOfficeDepartment.TblOffice.TblManagement.Title,
                Status = a.Disable ? "غیرفعال" : "فعال",
                a.Tel,
                a.Mobile,
                a.Mobile2,
                a.Address,
                Group = a.TblPersonGroup.Title,
                User = a.TblUsers.Any()
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }



        [Authorize(Roles = "69")]
        [ChildActionOnly]
        public async Task<PartialViewResult> CreateChild()
        {
            return PartialView();
        }

        [Authorize(Roles = "69")]

        [HttpPost]
        public async Task<JsonResult> AddPerson(string name, string family, string nationalCode, DateTime birthDate, string tel, string mobile, string mobile2)
        {
            var entity = new TblPerson
            {
                BirthDate = birthDate,
                Family = family,
                FamilyRelationship = 1,


                Mobile = mobile,
                Mobile2 = mobile2,
                Name = name,
                NationalCode = nationalCode,

                OfficeDepartment = 1,
                PersonnelCode = null,
                Disable = false,
                Tel = tel
            };
            var user = int.Parse(User.Identity.Name);
            var (result, message) = await _unitOfWork.GetRepository<TblPerson>().Insert(entity, user);
            var type = result ? "success" : "error";

            var msg = new Message { Msg = message, Result = result, Type = type };

            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "70")]

        // GET: Person/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.FamilyRelationship = new SelectList(await _unitOfWork.GetRepository<TblFamilyRelationship>().Get(), "Id", "Title", 0);
            ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblPersonStatu>().Get(), "Id", "Title");
            ViewBag.Group = new SelectList(await _unitOfWork.GetRepository<TblPersonGroup>().Get(), "Id", "Title");
            return View();
        }

        // POST: Person/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "70")]

        public async Task<ActionResult> Create(TblPerson entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblPerson>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Name + ' ' + entity.Family);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Name + ' ' + entity.Family, message);

                }

                ViewBag.FamilyRelationship = new SelectList(await _unitOfWork.GetRepository<TblFamilyRelationship>().Get(), "Id", "Title");
                ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblPersonStatu>().Get(), "Id", "Title");
                ViewBag.Group = new SelectList(await _unitOfWork.GetRepository<TblPersonGroup>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "71")]

        // GET: Person/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblPerson>();
            var result = await iResult.GetSingle(a => a.Id == id, includeProperties: "TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary,TblOfficeDepartment.TblOffice.TblManagement,TblOfficeDepartment.TblOffice,TblOfficeDepartment");
            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.FamilyRelationship = new SelectList(await _unitOfWork.GetRepository<TblFamilyRelationship>().Get(), "Id", "Title", result.FamilyRelationship);
            ViewBag.Management = new SelectList(await _unitOfWork.GetRepository<TblManagement>().Get(), "Id", "Title");
            ViewBag.Company = result.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company;
            ViewBag.Subsidiary = result.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary;
            ViewBag.Management = result.TblOfficeDepartment.TblOffice.Management;
            ViewBag.Office = result.TblOfficeDepartment.Office;
            ViewBag.OfficeDepartment = result.OfficeDepartment;
            // ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblPersonStatu>().Get(), "Id", "Title", result.Disable);
            ViewBag.Group = new SelectList(await _unitOfWork.GetRepository<TblPersonGroup>().Get(), "Id", "Title", result.Group);

            return View(result);
        }

        // POST: Person/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "71")]

        public async Task<ActionResult> Edit(TblPerson entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    entity.Name = entity.Name.Trim();
                    entity.Family = entity.Family.Trim();
                    entity.Mobile = entity.Mobile?.Trim();
                    entity.Tel = entity.Tel?.Trim();
                    entity.Mobile2 = entity.Mobile2?.Trim();
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblPerson>().Update(entity, user);
                    if (result)
                    {
                        //اگر وضعیت غیر فعال شد تحت تکلف غیرفعال شود
                        if (entity.Disable && !string.IsNullOrEmpty( entity.PersonnelCode) &&entity.PersonnelCode.Length > 3)
                        {
                           
                            foreach (var item in _db.TblPersons.Where(a => a.PersonnelCode == entity.PersonnelCode))
                            {
                                item.Disable = true;
                            }
                            _db.SaveChanges();
                            //Todo Logs
                        }
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Fullname());
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Name, message);
                    return RedirectToAction("Index");
                }
                ViewBag.FamilyRelationship = new SelectList(await _unitOfWork.GetRepository<TblFamilyRelationship>().Get(), "Id", "Title", entity.FamilyRelationship);
                ViewBag.Management = new SelectList(await _unitOfWork.GetRepository<TblManagement>().Get(), "Id", "Title");
                ViewBag.Company = entity.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company;
                ViewBag.Subsidiary = entity.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary;
                ViewBag.Management = entity.TblOfficeDepartment.TblOffice.Management;
                ViewBag.Office = entity.TblOfficeDepartment.Office;
                ViewBag.OfficeDepartment = entity.OfficeDepartment;
               // ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblPersonStatu>().Get(), "Id", "Title", entity.Disable);
                ViewBag.Group = new SelectList(await _unitOfWork.GetRepository<TblPersonGroup>().Get(), "Id", "Title", entity.Group);
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "72")]

        // GET: Person/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblPerson>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Person/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "72")]

        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblPerson>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

        public string CheckKodMeli()
        {
            foreach (var item in _db.TblPersons.ToList())
            {
                try
                {
                    if (item.NationalCode.Trim().Length < 10)
                    {
                      item.NationalCode =  item.NationalCode.PadLeft(10 - item.NationalCode.Trim().Length, '0');
                    }
                    var chArray = item.NationalCode.ToCharArray();
                    var num0 = Convert.ToInt32(chArray[0].ToString()) * 10;
                    var num2 = Convert.ToInt32(chArray[1].ToString()) * 9;
                    var num3 = Convert.ToInt32(chArray[2].ToString()) * 8;
                    var num4 = Convert.ToInt32(chArray[3].ToString()) * 7;
                    var num5 = Convert.ToInt32(chArray[4].ToString()) * 6;
                    var num6 = Convert.ToInt32(chArray[5].ToString()) * 5;
                    var num7 = Convert.ToInt32(chArray[6].ToString()) * 4;
                    var num8 = Convert.ToInt32(chArray[7].ToString()) * 3;
                    var num9 = Convert.ToInt32(chArray[8].ToString()) * 2;
                    var a = Convert.ToInt32(chArray[9].ToString());
                    var b = (((((((num0 + num2) + num3) + num4) + num5) + num6) + num7) + num8) + num9;
                    var c = b % 11;
                    var n = (((c < 2) && (a == c)) || ((c >= 2) && ((11 - c) == a)));
                    item.KodMeli = n;
                }
                catch { item.KodMeli = false; }

            }

            try
            {
                _db.SaveChanges();
            }
            catch (Exception e)
            {

            }

            return "F";
        }

    }
}
