﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{
    public class HomeController : Controller
    {
        private dbEntities db = new dbEntities();
        [Authorize]
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public string test()
        {
            return db.TblUsers.Single(a => a.Id == 1).UserName;
        }

        public ActionResult Index2()
        {
            ViewBag.Title = "Home Page";
            var res = new SelectList(db.TblRestaurants, "Id", "Title");
           // res.Prepend(new SelectListItem{Disabled = true, Text = "انتخاب نمایید...", Value = "", Selected=true});
            ViewBag.Restaurant = res;
            ViewBag.Status = new SelectList(db.TblRoomStatus, "Id", "Title");
            ViewBag.Type = new SelectList(db.TblRoomTypes, "Id", "Title");
            return View();
        }

        public ActionResult Index3()
        {
            ViewBag.Title = "Home Page";
            ViewBag.Restaurant = new SelectList(db.TblRestaurants, "Id", "Title");
            ViewBag.Status = new SelectList(db.TblRoomStatus, "Id", "Title");
            ViewBag.Type = new SelectList(db.TblRoomTypes, "Id", "Title");
            return View();
        }


        
    }
}
