﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Controllers
{

    public class UserController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db;
        public UserController()
        {
            _db = new dbEntities();
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }

        // GET: User
        public ActionResult Index()
        {
            List<byte> list = _db.TblUserGroups.Where(a => a.Id != 6 && a.Id != 21)
                    .Select(a=>a.Id)
                    .ToList()
                    ;

            ViewBag.Group = new MultiSelectList( _unitOfWork.GetRepository<TblUserGroup>().GetSync(), "Id", "Title", list);
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> List(SearchAdvancedUser model)
        {
            Expression<Func<TblUser, bool>> filter = (a) =>
                (string.IsNullOrEmpty(model.UserName) || a.UserName.Contains(model.UserName))
                &&
                (model.FoodReservationLock == null || model.FoodReservationLock == a.FoodReservationLock) &&
                //(string.IsNullOrEmpty(model.Family) || a.Family.Contains(model.Family)) &&
                //(string.IsNullOrEmpty(model.NationalCode) || a.NationalCode.Contains(model.NationalCode)) &&
                //(string.IsNullOrEmpty(model.PersonnelCode) || a.PersonnelCode.Contains(model.PersonnelCode)) &&
                //(string.IsNullOrEmpty(model.Address) || a.Address.Contains(model.Address)) &&
                //(string.IsNullOrEmpty(model.Tel) || a.Tel.Contains(model.Tel)) &&
                //(string.IsNullOrEmpty(model.Mobile) || a.Mobile.Contains(model.Mobile)) &&
                //(string.IsNullOrEmpty(model.Mobile) || a.Mobile2.Contains(model.Mobile)) &&
                //(model.Company == 0 || a.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == model.Company) &&
                //(model.Subsidiary == 0 || a.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == model.Subsidiary) &&
                //(model.Management == 0 || a.TblOfficeDepartment.TblOffice.Management == model.Management) &&
                //(model.Office == 0 || a.TblOfficeDepartment.Office == model.Office) &&
                //(model.OfficeDepartment == 0 || a.OfficeDepartment == model.OfficeDepartment) &&
                (model.Group.Contains(255) || model.Group.Contains(a.Group)) &&
                //(model.FamilyRelationship.Contains(255) || model.FamilyRelationship.Contains(a.FamilyRelationship))
                a.Id > 0
                ;
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = "TblUser2.TblPerson,TblPerson,TblUserGroup,TblUserAccessTo,TblPerson.TblOfficeDepartment,TblPerson.TblOfficeDepartment.TblOffice";

            var iResult = _unitOfWork.GetRepository<TblUser>();
            var result = iResult.GetOrdered(filter: filter,
                orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                a.UserName,
                Person = a.TblPerson.Fullname(),
                ImportantMessage= a.ImportantMessage ? "بله" : "خیر",
                FoodReservationLock=  a.FoodReservationLock ? "بله" : "خیر",
                Creator = a.TblUser2.TblPerson.Fullname(),
                Group = a.TblUserGroup.Title,
                AccessTo = a.TblUserAccessTo.Title,
                OfficeDepartment =  a.TblPerson.TblOfficeDepartment.Title,
                Office=  a.TblPerson.TblOfficeDepartment.TblOffice.Title,
                PersonId = a.Person
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        //public async Task<ActionResult> Lock(int id)
        //{
        //    var user = int.Parse(User.Identity.Name);
        //    var us = _unitOfWork.GetRepository<TblUser>().GetSingleSync(a => a.Id == id);
        //    us.FoodReservationLock = true;
        //   await _unitOfWork.GetRepository<TblUser>().Update(us, user);
            
        //}
        // GET: User/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblUser>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        public async Task<string> CreateDefault()
        {
            var persons = await _unitOfWork.GetRepository<TblPerson>().Get(a =>a.Id>1&& a.FamilyRelationship == 0 && a.TblUsers.Any() == false && a.PersonnelCode.Length>1);
            var user = int.Parse(User.Identity.Name);
            var list = new List<TblUser>();
            foreach (var person in persons)
            {
                var entity = new TblUser
                {
                    Person = person.Id,
                    AccessTo = 7,
                    Creator = user,
                    Group = 6,
                    Password = person.NationalCode.Substring(6),
                    PasswordSalt = Guid.NewGuid().ToString("N"),
                    UserName = person.PersonnelCode
                };
                entity.Password = Statics.HashedPassword(entity.Password, entity.PasswordSalt);
                list.Add(entity);
            }

            var result = await _unitOfWork.GetRepository<TblUser>().InsertRange(list, user);

            return result.result + " " + result.message;
        }

        // GET: User/Create
        public async Task<ActionResult> Create(int id)
        {
            //  var id = Convert.ToInt32(TempData["Person"]);
            ViewBag.Person = await _unitOfWork.GetRepository<TblPerson>().GetSingle(a => a.Id == id,
                includeProperties: @"TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.TblCompany,
                                    TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary,
                                    TblOfficeDepartment.TblOffice.TblManagement,
                                    TblOfficeDepartment.TblOffice,
                                    TblOfficeDepartment"
                );
            ViewBag.Group = new SelectList(await _unitOfWork.GetRepository<TblUserGroup>().Get(), "Id", "Title");
            ViewBag.AccessTo = new SelectList(await _unitOfWork.GetRepository<TblUserAccessTo>().Get(), "Id", "Title");
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(string userName, string password, int person, byte group, byte accessTo, List<int> restaurants)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);

                    var entity = new TblUser
                    {
                        Person = person,
                        AccessTo = accessTo,
                        Creator = user,
                        Group = group,
                        Password = password,
                        PasswordSalt = Guid.NewGuid().ToString("N"),
                        UserName = userName
                    };
                    entity.Password = Statics.HashedPassword(entity.Password, entity.PasswordSalt);


                    var (result, message) = await _unitOfWork.GetRepository<TblUser>().Insert(entity, user);
                    if (result)
                    {
                        if (restaurants.Any())
                        {
                            foreach (var ru in restaurants.Select(item => new TblRestaurantUser
                            {
                                User = entity.Id,
                                Restaurant = item
                            }))
                            {
                                var (res, msg) = await _unitOfWork.GetRepository<TblRestaurantUser>().Insert(ru, user);
                            }
                        }
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.UserName);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.UserName, message);
                    return RedirectToAction("Index");
                }

                ViewBag.Person = await _unitOfWork.GetRepository<TblPerson>().GetSingle(a => a.Id == person,
                    includeProperties: @"TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.TblCompany,
                                    TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary,
                                    TblOfficeDepartment.TblOffice.TblManagement,
                                    TblOfficeDepartment.TblOffice,
                                    TblOfficeDepartment"
                );
                ViewBag.Group = new SelectList(await _unitOfWork.GetRepository<TblUserGroup>().Get(), "Id", "Title");
                ViewBag.AccessTo = new SelectList(await _unitOfWork.GetRepository<TblUserAccessTo>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View();
        }

        // GET: User/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblUser>();
            var result = await iResult.GetSingle(a => a.Person == id,includeProperties: "TblRestaurantUsers,TblStoreUsers");
            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.Person = await _unitOfWork.GetRepository<TblPerson>().GetSingle(a => a.Id == id,
                includeProperties: @"TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.TblCompany,
                                    TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary,
                                    TblOfficeDepartment.TblOffice.TblManagement,
                                    TblOfficeDepartment.TblOffice,
                                    TblOfficeDepartment
                                    "
            );
            ViewBag.Group = new SelectList(await _unitOfWork.GetRepository<TblUserGroup>().Get(), "Id", "Title", result.Group);
            ViewBag.AccessTo = new SelectList(await _unitOfWork.GetRepository<TblUserAccessTo>().Get(), "Id", "Title", result.AccessTo);
            ViewBag.Restaurants = await _unitOfWork.GetRepository<TblRestaurant>().Get();
            ViewBag.Stores = _unitOfWork.GetRepository<TblStore>().GetSync().ToList();
            return View(result);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(string userName, string newPassword, string des, int person, byte group, byte accessTo, bool foodReservationLock, List<int> restaurants, List<int> stores)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var entity = await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Person == person);
                    entity.UserName = userName;
                    entity.Des = des;
                    entity.Group = group;
                    entity.AccessTo = accessTo;
                    entity.FoodReservationLock = foodReservationLock;
                    if (!string.IsNullOrEmpty(newPassword))
                        entity.Password = Statics.HashedPassword(newPassword, entity.PasswordSalt); ;
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblUser>().Update(entity, user);
                    if (result)
                    {
                        var resDelete = await _unitOfWork.GetRepository<TblRestaurantUser>().DeleteRange(a => a.User == entity.Id, user);
                        if (restaurants?.Count > 0)
                        {
                            foreach (var ru in restaurants.Select(item => new TblRestaurantUser
                            {
                                User = entity.Id,
                                Restaurant = item
                            }))
                            {
                                var (res, msg) =  _unitOfWork.GetRepository<TblRestaurantUser>().InsertSync(ru, user);
                            }
                        }
                        var storeDelete = await _unitOfWork.GetRepository<TblStoreUser>().DeleteRange(a => a.User == entity.Id, user);
                        if (stores?.Count > 0)
                        {
                            foreach (var ru in stores.Select(item => new TblStoreUser
                            {
                                User = entity.Id,
                                Store = item
                            }))
                            {
                                var (res, msg) =  _unitOfWork.GetRepository<TblStoreUser>().InsertSync(ru, user);
                            }
                        }
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.UserName);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.UserName, message);

                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return RedirectToAction("Edit", new { id = person });
        }

        // GET: User/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblUser>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblUser>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
