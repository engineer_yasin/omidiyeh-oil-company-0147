//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omidiyeh_Oil_Company___0147.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TblSurveyQuestion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TblSurveyQuestion()
        {
            this.TblSurveyQuestionOptions = new HashSet<TblSurveyQuestionOption>();
            this.TblSurveyResponses = new HashSet<TblSurveyResponse>();
        }
    
        public int Id { get; set; }
        public int Survey { get; set; }
        public string Title { get; set; }
        public bool Necessary { get; set; }
        public bool Anatomical { get; set; }
        public bool Status { get; set; }
    
        public virtual TblSurvey TblSurvey { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblSurveyQuestionOption> TblSurveyQuestionOptions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblSurveyResponse> TblSurveyResponses { get; set; }
    }
}
