//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omidiyeh_Oil_Company___0147.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TblBed
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TblBed()
        {
            this.TblBedReceptions = new HashSet<TblBedReception>();
            this.TblBedStatusHistories = new HashSet<TblBedStatusHistory>();
            this.TblFoodReservations = new HashSet<TblFoodReservation>();
        }
    
        public int Id { get; set; }
        public int Room { get; set; }
        public byte Status { get; set; }
        public int Number { get; set; }
        public string Des { get; set; }
        public byte Capacity { get; set; }
        public Nullable<int> FoodReservationGroup { get; set; }
        public Nullable<int> Person { get; set; }
    
        public virtual TblFoodReservationGroup TblFoodReservationGroup { get; set; }
        public virtual TblBedStatu TblBedStatu { get; set; }
        public virtual TblRoom TblRoom { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblBedReception> TblBedReceptions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblBedStatusHistory> TblBedStatusHistories { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblFoodReservation> TblFoodReservations { get; set; }
    }
}
