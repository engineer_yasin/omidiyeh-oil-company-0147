//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omidiyeh_Oil_Company___0147.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblBlacklist
    {
        public int ID { get; set; }
        public Nullable<int> goodID { get; set; }
        public Nullable<int> officeID { get; set; }
        public Nullable<System.DateTime> cDate { get; set; }
        public Nullable<bool> deleted { get; set; }
    
        public virtual tblGood tblGood { get; set; }
    }
}
