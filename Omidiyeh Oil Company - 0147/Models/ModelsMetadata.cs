﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Omidiyeh_Oil_Company___0147.Models
{
    //[StringLength(30, MinimumLength = 3, ErrorMessage = "نام باید بیش از 3 کاراکتر باشد.")]
    //[Required(ErrorMessage = " ")]
    //[DataType(DataType.Password)]
    //[DataType(DataType.Date)]
    //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

    //[Range(1, 100)]
    //[DataType(DataType.Currency)]
    //[Remote("doesUsernameOrMobileExist", "Helpers", HttpMethod = "POST", ErrorMessage = "این نام کاربری یا شماره همراه موجود نیست.")]
    //[RegularExpression(@"^[0-9]+$", ErrorMessage = "لطفا شماره شناسنامه صحیح نوشته شود.")]
    //[RegularExpression(@"^13\d{6}$", ErrorMessage = "لطفا تاریخ تولد صحیح نوشته شود.")]
    //[RegularExpression(@"^\d{10}$", ErrorMessage = "لطفا کدملی صحیح نوشته شود.")]
    //[RegularExpression(@"^09?\d{9}$", ErrorMessage = "لطفا تلفن همراه صحیح نوشته شود.")]
    //[RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "لطفا ایمیل صحیح نوشته شود.")]
    //[RegularExpression(@"^\d{10}$", ErrorMessage = "لطفا کدپستی صحیح نوشته شود.")]
    internal class ChangePasswordMetadata
    {
        [Required(ErrorMessage = " ")]
        [StringLength(30, MinimumLength = 4, ErrorMessage = "گذرواژه 4 تا 30 حرف باید باشد")]
        [Display(Name = "گذرواژه کنونی")]
        [DataType(DataType.Password)]

        public string Password { get; set; }

        [Required(ErrorMessage = " ")]
        [StringLength(30, MinimumLength = 4, ErrorMessage = "گذرواژه 4 تا 30 حرف باید باشد")]
        [Display(Name = "گذرواژه جدید")]
        [DataType(DataType.Password)]

        public string NewPassword { get; set; }

        [Required(ErrorMessage = " ")]
        [StringLength(30, MinimumLength = 4, ErrorMessage = "گذرواژه 4 تا 30 حرف باید باشد")]
[Display(Name = "تکرار گذرواژه")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "گذرواژه و تکرار آن باید یکسان باشد.")]
        public string NewPasswordRepeat { get; set; }
    }

    internal class HolidayMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "تاریخ", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime Date { get; set; }

        [Display(Name = "توضیحات", Description = " ")]
        public string Title { get; set; }


    }
    internal class BedMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "اتاق", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Room { get; set; }


        [Display(Name = "وضعیت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Status { get; set; }


        [Display(Name = "شماره", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Number { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }

        [Display(Name = "ظرفیت", Description = " ")]
        [Required(ErrorMessage = " ")]
        //[Range(1,10)]
        public byte Capacity { get; set; } = 1;

    }
   
    //Todo: ارتباط رزرو تخت و رزرو غذا مشخص شود
    internal class BedReservationMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public Guid Id { get; set; }


        [Display(Name = "شخص", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Person { get; set; }



        [Display(Name = "کاربر", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int User { get; set; }


        [Display(Name = "اداره", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Office { get; set; }


        [Display(Name = "اداره - واحد", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int OfficeDepartment { get; set; }


        [Display(Name = "از تاریخ", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime FromDate { get; set; }


        [Display(Name = "تا تاریخ", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime ToDate { get; set; }


        [Display(Name = "تعداد تخت درخواستی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Count { get; set; }


        [Display(Name = "وضعیت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Status { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Des { get; set; }



    }

    internal class BedReservationStatusMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public byte Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


    }


    internal class BedStatusMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public byte Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


    }

    internal class ContractMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "پیمانکار", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Contractor { get; set; }


        [Display(Name = "توضیحات", Description = " ")]

        public string Body { get; set; }


        [Display(Name = "تاریخ شروع", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime StartDate { get; set; }


        [Display(Name = "تاریخ پایان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime EndDate { get; set; }


        [Display(Name = "شماره قرارداد", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Number { get; set; }


        [Display(Name = "وضعیت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Status { get; set; }

        [Display(Name = "تعداد غذا", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int InitialQuota { get; set; }

        [Display(Name = "تعداد استفاده شده", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int UsedQuota { get; set; }

        [Display(Name = "تعداد باقی مانده", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int RemainingQuota { get; set; }

        [Display(Name = "مبلغ پیمان", Description = " ")]
        [Required(ErrorMessage = " ")]
        [DisplayFormat(DataFormatString = "{0:0,0}")]
        public long Amount { get; set; }


    }

    internal class ContractFoodMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "غذا", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Food { get; set; }


        [Display(Name = "قرارداد", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Contract { get; set; }


        [Display(Name = "تعداد", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int InitialQuota { get; set; }

        [Display(Name = "تعداد استفاده شده", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int UsedQuota { get; set; }

        [Display(Name = "تعداد باقی مانده", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int RemainingQuota { get; set; }
    }

    internal class ContractPriceSeriesFoodMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }

        [Display(Name = "قیمت", Description = " ")]
        public int Price { get; set; }

        [Display(Name = "قیمت آزاد", Description = " ")]
        public int FreePrice { get; set; }
    }

    internal class ContractFoodAdjustmentMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "قرارداد غذا", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int ContractFood { get; set; }


        [Display(Name = "قیمت جدید", Description = " ")]
        [Required(ErrorMessage = " ")]
        public decimal Price { get; set; }

        [Display(Name = "قیمت قدیم", Description = " ")]
        [Required(ErrorMessage = " ")]
        public decimal OldPrice { get; set; }

        [Display(Name = "درصد تغییر", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Percent { get; set; }

        [Display(Name = "کاربر", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int User { get; set; }

        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


        [Display(Name = "تاریخ تعدیل نرخ", Description = " ")]
        [Required(ErrorMessage = " ")]
        public System.DateTime Date { get; set; }

    }
    internal class ContractStatusMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public byte Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


    }

    internal class ContractorMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "نام", Description = " ")]
        public string Name { get; set; }

        [Display(Name = "نام خانوادگی", Description = " ")]
        public string Family { get; set; }

        [Display(Name = "تلفن", Description = " ")]
        public string Tel { get; set; }

        [Display(Name = "موبایل", Description = " ")]
        public string Mobile { get; set; }

        [Display(Name = "آدرس", Description = " ")]
        public string Address { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Body { get; set; }

        [Display(Name = "شناسه اقتصادی", Description = " ")]
        public string EconomicID { get; set; }

        [Display(Name = "شناسه ملی", Description = " ")]
        public string NationalID { get; set; }


        [Display(Name = "شماره ثبت", Description = " ")]
        public string RegistrationNumber { get; set; }

        [Display(Name = "کدپستی", Description = " ")]
        public string PostalCode { get; set; }

        [Display(Name = "شماره حساب", Description = " ")]
        public string AccountNumber { get; set; }

        [Display(Name = "تاریخ ایجاد", Description = " ")]
        public System.DateTime CreateTime { get; set; }

        [Display(Name = "تاریخ ویرایش", Description = " ")]
        public System.DateTime UpdateTime { get; set; }

        [Display(Name = "وضعیت", Description = " ")]
        public byte Status { get; set; }


    }

    internal class ContractorStatusMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public byte Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


    }
    internal class FoodMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


        [Display(Name = "سهمیه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public double Quota { get; set; } = 1;


        [Display(Name = "دسته بندی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Category { get; set; }



    }


    internal class FoodCategoryMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


    }

    internal class FoodReservationMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "شخص", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Person { get; set; } = 1;


        [Display(Name = "غذا", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Food { get; set; }


        [Display(Name = "وعده", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Meal { get; set; }


        [Display(Name = "تاریخ", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime Date { get; set; }





        //[Display(Name = "توضیحات", Description = " ")]
        //public string Des { get; set; }

        [Display(Name = "رستوران", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Restaurant { get; set; }

        

        [Display(Name = "گروه درخواست", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Group { get; set; }

        [Display(Name = "نظرسنجی", Description = " ")]
      //  [Required(ErrorMessage = " ")]
        public bool Survey { get; set; } = false;

    }

    internal class FoodReservationLetterMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }

        [Display(Name = "موضوع نامه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Subject { get; set; }

        [Display(Name = "تاریخ نامه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public System.DateTime Date { get; set; }

        [Display(Name = "شماره نامه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Number { get; set; }

        [Display(Name = " اداره (معرف)", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Office { get; set; }

        [Display(Name = " ارگان دولتی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int OfficeFor { get; set; }

        

        [Display(Name = "کاربر", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int User { get; set; }

        [Display(Name = " تعداد نفر", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Count { get; set; }

    }
    internal class FoodReservationStatusMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public byte Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


    }

    internal class MealMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "ساعت شروع سرو غذا", Description = " ")]
        [Required(ErrorMessage = " ")]
        public TimeSpan StartTime { get; set; }


        [Display(Name = "ساعت پایان سرو غذا", Description = " ")]
        [Required(ErrorMessage = " ")]
        public TimeSpan EndTime { get; set; }



        [Display(Name = "ساعت پایان رزرو", Description = " ")]
        [Required(ErrorMessage = " ")]
        public TimeSpan ReservationEndTime { get; set; }


        [Display(Name = "وضعیت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Status { get; set; } = 0;




        [Display(Name = "روز پایان رزرو", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int ReservationEndDay { get; set; }


        [Display(Name = "ساعت پایان رزرو غذای پیش فرض", Description = " ")]
        [Required(ErrorMessage = " ")]
        public TimeSpan ReservationDefaultFoodTime { get; set; }

        [Display(Name = "روز پایان رزرو غذای پیش فرض", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int ReservationDefaultFoodDay { get; set; }

        [Display(Name = "تاریخ فعال سازی", Description = " ")]
        public Nullable<System.DateTime> ActivationDate { get; set; }
        [Display(Name = "تاریخ غیرفعال سازی", Description = " ")]
        public Nullable<System.DateTime> DeactivationDate { get; set; }
    }

    internal class CompanyMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }
    }

    internal class SubsidiaryMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }

        [Display(Name = "شرکت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Company { get; set; }

        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }
    }
    internal class ManagementMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "شرکت تابعه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Subsidiary { get; set; }

        [Display(Name = "رئیس", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Boss { get; set; }

        [Display(Name = "جانشین", Description = " ")]
        public Nullable<int> Alternative { get; set; }


        [Display(Name = "آدرس", Description = " ")]
        public string Address { get; set; }

        [Display(Name = "تلفن", Description = " ")]
        public string Tel { get; set; }

        [Display(Name = "توضیحات", Description = " ")]
        public string Body { get; set; }
    }

    internal class OfficeMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }




        [Display(Name = "مدیریت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Management { get; set; }


        [Display(Name = "رئیس", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Boss { get; set; }

        [Display(Name = "جانشین", Description = " ")]
        public Nullable<int> Alternative { get; set; }

        [Display(Name = "آدرس", Description = " ")]
        public string Address { get; set; }


        [Display(Name = "تلفن", Description = " ")]
        public string Tel { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Body { get; set; }


    }

    internal class OfficeDepartmentMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }




        [Display(Name = "اداره", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Office { get; set; }


        [Display(Name = "آدرس", Description = " ")]
        public string Address { get; set; }


        [Display(Name = "تلفن", Description = " ")]
        public string Tel { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Body { get; set; }

        [Display(Name = "کد", Description = " ")]
        //[Required(ErrorMessage = " ")]
        public int Code { get; set; }
    }

    internal class OfficeDepartmentAccountNumberMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }

        [Display(Name = "واحد", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int OfficeDepartment { get; set; }


        [Display(Name = "شماره حساب", Description = " ")]
        [Required(ErrorMessage = " ")]

        public string AccountNumber { get; set; }


    }
    internal class OfficeFoodQuotaMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "اداره", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Office { get; set; }



        [Display(Name = "سهمیه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int InitialQuota { get; set; }


        [Display(Name = "سهمیه استفاده شده", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int UsedQuota { get; set; } = 0;


        [Display(Name = "سهمیه باقی مانده", Description = " ")]
        public int RemainingQuota { get; set; } = 0;


        [Display(Name = "تاریخ شروع", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime StartDate { get; set; }


        [Display(Name = "تاریخ پایان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime EndDate { get; set; }


    }

    internal class OfficeRestaurantMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "اداره", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Office { get; set; }


        [Display(Name = "اداره - واحد", Description = " ")]

        public int OfficeDepartment { get; set; }


        [Display(Name = "رستوران", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Restaurant { get; set; }


    }

    internal class PersonMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "نام", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Name { get; set; }


        [Display(Name = "نام خانوادگی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Family { get; set; }


        [Display(Name = "کدملی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string NationalCode { get; set; }


        [Display(Name = "شماره پرسنلی", Description = " ")]
 
        public string PersonnelCode { get; set; }


        [Display(Name = "نسبت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte FamilyRelationship { get; set; }


        [Display(Name = "تاریخ تولد", Description = " ")]
        [Required(ErrorMessage = " ")]

        public DateTime BirthDate { get; set; }




        [Display(Name = "واحد", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int OfficeDepartment { get; set; }



        //[Display(Name = "وضعیت", Description = " ")]
        //public bool Status { get; set; }

        [Display(Name = "گروه", Description = " ")]
        public byte Group { get; set; }

        [Display(Name = "تلفن", Description = " ")]
        public string Tel { get; set; }

        [Display(Name = "موبایل", Description = " ")]
        public string Mobile { get; set; }

        [Display(Name = "موبایل 2", Description = " ")]
        public string Mobile2 { get; set; }


        [Display(Name = "آدرس", Description = " ")]
        public string Address { get; set; }
        
        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }

    }

    internal class RestaurantMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "آدرس", Description = " ")]

        public string Address { get; set; }


        [Display(Name = "تلفن", Description = " ")]
        public string Tel { get; set; }


        [Display(Name = "تعداد تخت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int NumberOfRooms { get; set; } = 0;


        [Display(Name = "وضعیت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Status { get; set; }


        [Display(Name = "نوع", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Type { get; set; }


        [Display(Name = "تایید رزرو توسط خدمات رفاهی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public bool ConfirmReservation { get; set; }


    }

    internal class RestaurantStatusMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public byte Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


    }

    internal class RestaurantTypeMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public byte Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


    }

    internal class RestaurantFoodProgramMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public Guid Id { get; set; }


        [Display(Name = "غذا", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Food { get; set; }


        [Display(Name = "رستوران", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Restaurant { get; set; }


        [Display(Name = "وعده غذایی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Meal { get; set; }


        [Display(Name = "تاریخ", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime Date { get; set; }


    }

    internal class RoleMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "نام انگلیسی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Name { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


    }

    public class RoomMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "رستوران - مهمانسرا", Description = " ")]
        [Required(ErrorMessage = "لطفا مقدار وارد شود")]
        public int Restaurant { get; set; }


        [Display(Name = "شماره", Description = " ")]
        [Required(ErrorMessage = "لطفا مقدار وارد شود")]
        public int Number { get; set; }


        [Display(Name = "وضعیت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Status { get; set; }


        [Display(Name = "ظرفیت", Description = " ")]
        [Required(ErrorMessage = " ")]
        //[Range(1,10)]
        public int Capacity { get; set; } = 1;


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


        [Display(Name = "نوع", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Type { get; set; }



    }

    internal class RoomTypeMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public byte Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


    }

    internal class RoomStatusMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public byte Id { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }


    }

    internal class UserMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public int Id { get; set; }


        [Display(Name = "نام کاربری", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string UserName { get; set; }


        [Display(Name = "گذرواژه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Password { get; set; }


        //[Display(Name = "", Description = " ")]
        public string PasswordSalt { get; set; }


        [Display(Name = "شخص", Description = " ")]

        public int Person { get; set; }


        [Display(Name = "گروه کاری", Description = " ")]
        public byte Group { get; set; }



        [Display(Name = "پیام مهم", Description = " ")]
        public bool ImportantMessage { get; set; } = false;
         
        [Display(Name = "قفل رزرو غذا", Description = " ")]
        public bool FoodReservationLock { get; set; } = false;

        [Display(Name = "ایجادکننده", Description = " ")]

        public int Creator { get; set; }

        [Display(Name = "محدوه دسترسی", Description = " ")]

        public int AccessTo { get; set; }
        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }
    }

    internal class UserMessageMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        public Guid Id { get; set; }


        [Display(Name = "کاربر", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int User { get; set; }


        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "شرح", Description = " ")]
        public string Body { get; set; }


        [Display(Name = "تاریخ ثبت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime CreateDate { get; set; }


        [Display(Name = "وضعیت", Description = " ")]
        public Nullable<byte> Status { get; set; }

        [Display(Name = "از طرف", Description = " ")]
        public int CreateUser { get; set; }
    }


    internal class BookingCodeMetadata
    {
        [Display(Name = "کد", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Id { get; set; }

        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }

        [Display(Name = "ساعت پایان درخواست", Description = " ")]
        [Required(ErrorMessage = " ")]
        public TimeSpan RequestEndTime { get; set; }


        [Display(Name = "روز پایان درخواست", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int RequestEndDay { get; set; }
    }

    internal class RouteMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Id { get; set; }


        [Display(Name = "مبدا", Description = " ")]
        [Required(ErrorMessage = " ")]
        public short Origin { get; set; }

        [Display(Name = "مقصد", Description = " ")]
        [Required(ErrorMessage = " ")]
        public short Destination { get; set; }

        [Display(Name = "مسیر راه دور", Description = " ")]
        [Required(ErrorMessage = " ")]
        public bool LongDistance { get; set; } = false;

        [Display(Name = "نوع", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Type { get; set; }
    }


    internal class VehicleTypeMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Id { get; set; }

        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }


        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }
    }

    internal class VehicleMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Id { get; set; }

        [Display(Name = "شماره شرکتی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }

        [Display(Name = "دسته بندی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte Type { get; set; }
        
        [Display(Name = "نوع خودرو", Description = " ")]
        
        public byte TypeText { get; set; }
        
        [Display(Name = "پلاک", Description = " ")]
        public byte Plate { get; set; }

        [Display(Name = "وضعیت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public bool Enable { get; set; } = true;

        [Display(Name = "ظرفیت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Capacity { get; set; } = 1;
        
        [Display(Name = "اداره", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Office { get; set; }

        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }
    }

    internal class DriverMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Id { get; set; }

        //[Display(Name = "نام و نام خانوادگی", Description = " ")]
        //public string FullName { get; set; }

        [Display(Name = "نام", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Name { get; set; }

        [Display(Name = "نام خانوادگی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Family { get; set; }

        [Display(Name = "کدملی", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string NationalCode { get; set; }

        [Display(Name = "تاریخ تولد", Description = " ")]
        [Required(ErrorMessage = " ")]
        public System.DateTime BirthDate { get; set; }

        [Display(Name = "شماره گواهینامه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string DrivingLicence { get; set; }
    }


    internal class TripMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Id { get; set; }




        [Display(Name = "مسیر", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Route { get; set; }

        [Display(Name = "ساعت", Description = " ")]
        [Required(ErrorMessage = " ")]
        public System.TimeSpan Time { get; set; }


        [Display(Name = "تاریخ", Description = " ")]
        [Required(ErrorMessage = " ")]
        public System.DateTime Date { get; set; }





        [Display(Name = "نوع وسیله نقلیه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public byte VehicleType { get; set; }


        [Display(Name = "ظرفیت کد 12", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Capacity { get; set; }


    }

    internal class BookingRequestMetadata
    {

        [Display(Name = "شناسه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Id { get; set; }

        [Display(Name = "شخص", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Person { get; set; }

       

        [Display(Name = "اداره - واحد", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int OfficeDepartment { get; set; }

        [Display(Name = "همراه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public bool FellowTraveler { get; set; }


        
        [Display(Name = "موبایل", Description = " ")]
        public string Mobile { get; set; }

        [Display(Name = "تلفن", Description = " ")]
        public string Tel { get; set; }

        public bool Residence { get; set; }
       

        [Display(Name = "توضیحات", Description = " ")]
        //[Required(ErrorMessage = " ")]

        public string Des { get; set; }
        public byte Status { get; set; }

        [Display(Name = "سفر", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Trip { get; set; }

        [Display(Name = "مبدا", Description = " ")]
        [Required(ErrorMessage = " ")]
        public short Origin { get; set; }

        [Display(Name = "مقصد", Description = " ")]
        [Required(ErrorMessage = " ")]
        public short Destination { get; set; }
        public byte Relationship { get; set; }
        public System.DateTime Date { get; set; }
        public System.TimeSpan Time { get; set; }
        
        [Display(Name = "وسیله نقلیه", Description = " ")]
        public byte VehicleType { get; set; }

        public bool ExtraChair { get; set; }
        public bool SpecialServices { get; set; }
        public bool FoodDinner { get; set; }
        public bool Returned { get; set; }
        public int? WentId { get; set; }
       

        [Display(Name = "مسیر", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Route { get; set; }
    }

    public class StoreMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Id { get; set; }

        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }

        [Display(Name = "کد انبار", Description = " ")]
        public string Code { get; set; }

        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }

    }

    public class Category1Metadata
    {
        [Display(Name = "شناسه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Id { get; set; }

        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }

        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }
    }

      public class Category2Metadata
    {
        [Display(Name = "شناسه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Id { get; set; }

        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }

        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }

        [Display(Name = "دسته بندی یک", Description = " ")]
        [Required(ErrorMessage = " ")]

        public int Category1 { get; set; }

        [Display(Name = "حد آستانه", Description = " ")]
        public int Threshold { get; set; }

        [Display(Name = "قابلیت درخواست", Description = " ")]
        public bool Requestable { get; set; }

        [Display(Name = "قابلیت تعمیر", Description = " ")]
        public bool Repairable { get; set; }
        
        //[Display(Name = "دسته بندی یک", Description = " ")]
        //public int Category1 { get; set; }
    }

    public class ProductMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public long Id { get; set; }

        [Display(Name = "عنوان", Description = " ")]
        [Required(ErrorMessage = " ")]
        public string Title { get; set; }

        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }

        [Display(Name = "دسته بندی یک", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Category1 { get; set; }

        [Display(Name = "دسته بندی دو", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Category2 { get; set; }

        [Display(Name = "عمر مفید", Description = " ")]

        public Nullable<int> Shelflife { get; set; }

        [Display(Name = "حد آستانه", Description = " ")]
        public int Threshold { get; set; }

        [Display(Name = "قابلیت درخواست", Description = " ")]
        public bool Requestable { get; set; }

        [Display(Name = "قابلیت تعمیر", Description = " ")]
        public bool Repairable { get; set; }


        public bool Remove { get; set; }

        [Display(Name = "با اموال", Description = " ")]
        public bool Show { get; set; }

        [Display(Name = "واحد", Description = " ")]
        public int Unit { get; set; }
    }
    public class EntryDocumentMetadata
    {
        [Display(Name = "شناسه", Description = " ")]
        [Required(ErrorMessage = " ")]
        public long Id { get; set; }

        [Display(Name = "تاریخ", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime Date { get; set; }
        [Display(Name = "تاریخ درخواست", Description = " ")]
        [Required(ErrorMessage = " ")]
        public DateTime RequestDate { get; set; }

        [Display(Name = "توضیحات", Description = " ")]
        public string Des { get; set; }

         [Display(Name = "شماره درخواست", Description = " ")]
         //[Remote("DoesRequestIdExist", "EntryDocuments", HttpMethod = "POST", ErrorMessage = "شماره سند تکراری می باشد.")]
        public string RequestId { get; set; } 
        
        [Display(Name = "دلیل درخواست", Description = " ")]
        public string RequestReason { get; set; }

        [Display(Name = "تعداد", Description = " ")]
      
        public int Count { get; set; }

        [Display(Name = "انبار", Description = " ")]
        [Required(ErrorMessage = " ")]
        public int Store { get; set; }

       
    }
    //internal class
    //{

    //}


    //internal class
    //{

    //}


    //internal class
    //{

    //}

    //internal class
    //{

    //}

    //internal class
    //{

    //}

    //internal class
    //{

    //}

    //internal class
    //{

    //}

    //internal class
    //{

    //}


}