//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omidiyeh_Oil_Company___0147.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TblEntryDocumentProduct
    {
        public long Id { get; set; }
        public long EntryDocument { get; set; }
        public long Product { get; set; }
        public string Code { get; set; }
        public bool Remove { get; set; }
        public bool Exit { get; set; }
        public decimal Count { get; set; }
        public Nullable<long> Lading { get; set; }
        public System.DateTime Date { get; set; }
        public Nullable<int> OldId { get; set; }
        public Nullable<System.DateTime> OldGoodDate { get; set; }
        public Nullable<bool> OldBarname { get; set; }
        public Nullable<int> OldStatus { get; set; }
        public string Des { get; set; }
    
        public virtual TblEntryDocument TblEntryDocument { get; set; }
        public virtual TblProduct TblProduct { get; set; }
    }
}
