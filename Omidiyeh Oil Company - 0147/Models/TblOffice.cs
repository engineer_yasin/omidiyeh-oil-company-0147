//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omidiyeh_Oil_Company___0147.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TblOffice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TblOffice()
        {
            this.TblBillOfLadings = new HashSet<TblBillOfLading>();
            this.TblEntryDocuments = new HashSet<TblEntryDocument>();
            this.TblFoodReservationLetters = new HashSet<TblFoodReservationLetter>();
            this.TblOfficeDepartments = new HashSet<TblOfficeDepartment>();
            this.TblOfficeFoodQuotas = new HashSet<TblOfficeFoodQuota>();
            this.TblOfficeRestaurants = new HashSet<TblOfficeRestaurant>();
            this.TblVehicles = new HashSet<TblVehicle>();
        }
    
        public int Id { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public string Tel { get; set; }
        public string Body { get; set; }
        public int Management { get; set; }
        public int Boss { get; set; }
        public Nullable<int> Alternative { get; set; }
    
        public virtual TblManagement TblManagement { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblBillOfLading> TblBillOfLadings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblEntryDocument> TblEntryDocuments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblFoodReservationLetter> TblFoodReservationLetters { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblOfficeDepartment> TblOfficeDepartments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblOfficeFoodQuota> TblOfficeFoodQuotas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblOfficeRestaurant> TblOfficeRestaurants { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblVehicle> TblVehicles { get; set; }
    }
}
