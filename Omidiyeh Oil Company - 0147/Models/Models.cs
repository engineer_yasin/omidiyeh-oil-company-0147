﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Omidiyeh_Oil_Company___0147.Models
{
    #region Custom
    [MetadataType(typeof(ChangePasswordMetadata))]
    public class ChangePassword
    {
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string NewPasswordRepeat { get; set; }
    }
    public class Item
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Des { get; set; }
    }
    public class Message
    {
        public bool Result { get; set; }
        public string Type { get; set; }
        public string Msg { get; set; }
        public int Id { get; set; } = 0;
    }
    public partial class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Family { get; set; }
        public string Fullname { get; set; }
        public string NationalCode { get; set; }
        public string PersonnelCode { get; set; }
        public string Office { get; set; }
        public string OfficeDepartment { get; set; }
        public string FamilyRelationship { get; set; }
        public string Mobile { get; set; }
        public string Tel { get; set; }

    }

    public class BookingRequestMission
    {
        public int Person { get; set; }
        public int Route { get; set; }
        public string Des { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        public bool Residence { get; set; }
        public bool FoodLunch { get; set; }
        public bool FoodDinner { get; set; }
        public byte VehicleType { get; set; }
        public int AccountNumber { get; set; }

        // public List<byte> Claim { get; set; }
    }

    public class SearchAdvancedUser : TblUser
    {
        public new List<byte> Group { get; set; } = new List<byte> { 255 };
        public new bool? FoodReservationLock { get; set; } = null;

    }
    public class SearchAdvancedPerson : TblPerson
    {
        public int Subsidiary { get; set; }
        public int Company { get; set; }
        public int Management { get; set; }
        public int Office { get; set; }
        public int OfficeDepartment { get; set; } = -1;
        public new List<byte> Group { get; set; } = new List<byte> { 0 };
        public new List<byte> FamilyRelationship { get; set; } = new List<byte> { 255 };
        public new bool? ShiftWorker { get; set; } = null;
        public new bool? Retierd { get; set; } = null;
        public new bool? Disable { get; set; } = null;
    }

    public class SearchAdvancedFoodReservation : TblFoodReservation
    {

        public new List<int> Restaurant { get; set; } = new List<int> { -5 };
        public new List<int> Meal { get; set; } = new List<int> { -5 };
        public new List<int> Food { get; set; } = new List<int> { -5 };
        public  List<int> AccountNumber { get; set; } = new List<int> { 0 };
        public new List<int> Status { get; set; } = new List<int> { 0 };

        public string UserName { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public new bool? Serving { get; set; } = null;
        public new bool? Confirm { get; set; } = null;
        public new int? Type { get; set; } = null;

    } 
    public class SearchAdvancedTblSurveyResponse : TblSurveyResponse
    {

        public new List<int> Restaurant { get; set; } = new List<int> { -5 };
        public new List<int> Question { get; set; } = new List<int> { -5 };
        public new List<int> Survey { get; set; } = new List<int> { -5 };
        public new List<int> Meal { get; set; } = new List<int> { -5 };
        public new List<int> Food { get; set; } = new List<int> { -5 };
        public  List<int> AccountNumber { get; set; } = new List<int> { 0 };
        public new List<int> Status { get; set; } = new List<int> { 0 };
        
        public string UserName { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public new bool? Serving { get; set; } = null;
        public new bool? Confirm { get; set; } = null;
        public new int? Type { get; set; } = null;

    }

    public class SearchAdvancedBookingRequest : TblBookingRequest
    {
        public new List<int> AccountNumber { get; set; } = new List<int> { 0 };
        public List<byte> BookingCode { get; set; } = new List<byte> { 0 };
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

     public class SearchAdvancedProduct : TblProduct
    {
        //public new List<int> Category1 { get; set; } = new List<int> { -5 };
        //public new List<int> Category2 { get; set; } = new List<int> { -5 };
        //public new List<int> Unit { get; set; } = new List<int> { -5 };

        public new bool Remove { get; set; } = false;

        public new bool? Repairable { get; set; } = null;

        public new bool? Requestable { get; set; } = null;

        public int Store { get; set; } = 0;
        public long? Lading { get; set; } = 0;


    } 
     public class SearchAdvancedEntryDocumentProduct : TblEntryDocumentProduct
    {
        //public new List<int> Category1 { get; set; } = new List<int> { -5 };
        //public new List<int> Category2 { get; set; } = new List<int> { -5 };
        //public new List<int> Unit { get; set; } = new List<int> { -5 };

        public new bool Remove { get; set; } = false;

        public  bool? Repairable { get; set; } = null;

        public  bool? Requestable { get; set; } = null;

        public int Store { get; set; } = 0;
        public int ToOfficeDepartment { get; set; } = 0;
        public int ToOffice { get; set; } = 0;
        public long? Lading { get; set; } = 0;
        public long? DocumentId { get; set; } = null;
        public string ProductCode { get; set; } = null;


    } 
     public class SearchAdvancedDocument : TblEntryDocument
    {
        //public new List<int> Category1 { get; set; } = new List<int> { -5 };
        //public new List<int> Category2 { get; set; } = new List<int> { -5 };
        //public new List<int> Unit { get; set; } = new List<int> { -5 };

        //public new bool Remove { get; set; } = false;

        //public new bool? Repairable { get; set; } = null;

        //public new bool? Requestable { get; set; } = null;
        public  int? Product { get; set; } = null;

        public new int Store { get; set; } = 0;
        public new int FromStore { get; set; } = 0;
        //public long? Lading { get; set; } = 0;
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }


    public class SearchAdvancedBookingRequest2
    {
        public string SearchDate { get; set; }

        public DateTime DateFrom()
        {
            return Convert.ToDateTime(SearchDate.Split('-')[0]);
        }
        public DateTime DateTo()
        {
            return Convert.ToDateTime(SearchDate.Split('-')[1]).AddDays(1);
        }

        public string SearchRequestDate { get; set; }

        public DateTime? RequestDateFrom()
        {
            return string.IsNullOrEmpty(SearchRequestDate) ? (DateTime?) null: Convert.ToDateTime(SearchRequestDate.Split('-')[0]);
        }
        public DateTime? RequestDateTo()
        {
            return string.IsNullOrEmpty(SearchRequestDate) ? (DateTime?)null : Convert.ToDateTime(SearchRequestDate.Split('-')[1]).AddDays(1);
        }
        public TimeSpan? SearchTimeGo { get; set; }
        public List<byte> SearchVehicleType { get; set; } = new List<byte> { 0 };
        public List<int> SearchOrigin { get; set; } = new List<int> { 0 };
        public List<int> SearchDestination { get; set; } = new List<int> { 0 };
        public List<int> SearchRoute { get; set; } = new List<int> { 0 };
        public List<int> SearchTrip { get; set; } = new List<int> { 0 };
        public List<int> SearchBookingCode { get; set; } = new List<int> { 0 };
        public List<int> SearchStatus { get; set; } = new List<int> { 0 };
        public List<int> SearchManagement { get; set; } = new List<int> { 0 };
        public List<int> SearchOffice { get; set; } = new List<int> { 0 };
        public List<int> SearchOfficeDepartment { get; set; } = new List<int> { 0 };
        public List<int> SearchAccountNumber { get; set; } = new List<int> { 0 };

        public string SearchPersonalCode { get; set; }
        public string SearchNationalCode { get; set; }
        public string SearchName { get; set; }
        public string SearchFamily { get; set; }
        public string SearchMobile { get; set; }
        public string SearchTel { get; set; }

        public string SearchDes { get; set; }
        public bool? SearchResidence { get; set; }
        public bool? SearchFoodLunch { get; set; }
        public bool? SearchFoodDinner { get; set; }
        public bool? SearchExtraChair { get; set; }
        public bool? SearchSpecialServices { get; set; }
        public int? SearchRequestNumber { get; set; }
        public bool? SearchPayment { get; set; }
        public bool? SearchCancel { get; set; }
    }

    public class BookingRequest
    {
        public int Person { get; set; }
        public int Route { get; set; }
        public int Trip { get; set; }
        public short Origin { get; set; }
        public short Destination { get; set; }
        public string Des { get; set; }
        public string Mobile { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        public bool Residence { get; set; }
        public bool FoodLunch { get; set; }
        public bool FoodBreakfast { get; set; }
        public bool FoodDinner { get; set; }
        public bool SpecialServices { get; set; }
        public bool ExtraChair { get; set; }
        public byte VehicleType { get; set; }
        public int AccountNumber { get; set; }
        public bool IsReturn { get; set; }
        public bool FellowTraveler { get; set; }
        public byte Relationship { get; set; }
        // public List<byte> Claim { get; set; }
    }


    #endregion
        [MetadataType(typeof(HolidayMetadata))]
    public partial class TblHoliday { }  
    
    [MetadataType(typeof(BedMetadata))]
    public partial class TblBed { }


    [MetadataType(typeof(BedReservationMetadata))]
    public partial class TblBedReservation { }


    [MetadataType(typeof(BedReservationStatusMetadata))]
    public partial class TblBedReservationStatu { }

    [MetadataType(typeof(BedStatusMetadata))]
    public partial class TblBedStatu { }


    [MetadataType(typeof(ContractMetadata))]
    public partial class TblContract { }


    [MetadataType(typeof(ContractFoodMetadata))]
    public partial class TblContractFood { }

    [MetadataType(typeof(ContractFoodAdjustmentMetadata))]
    public partial class TblContractFoodAdjustment { } 
    
    [MetadataType(typeof(ContractPriceSeriesFoodMetadata))]
    public partial class TblContractPriceSeriesFood { }

    [MetadataType(typeof(ContractStatusMetadata))]
    public partial class TblContractStatu { }

    [MetadataType(typeof(ContractorMetadata))]
    public partial class TblContractor { }

    [MetadataType(typeof(ContractorStatusMetadata))]
    public partial class TblContractorStatu { }
    [MetadataType(typeof(FoodMetadata))]
    public partial class TblFood { }


    [MetadataType(typeof(FoodCategoryMetadata))]
    public partial class TblFoodCategory { }


    [MetadataType(typeof(FoodReservationMetadata))]
    public partial class TblFoodReservation
    {
       
    }

    public class FoodReservationHelp : TblFoodReservation
    {

        public string NationalCode { get; set; }
        public string PersonnelCode { get; set; }
        public string Name { get; set; }
        public string Family { get; set; }
        public string MealTitle { get; set; }
        public string FoodTitle { get; set; }
        public string ContractTitle { get; set; }
        public int ContractInitialQuota { get; set; }
        public string AccountNumberTitle { get; set; }
        public string OfficeTitle { get; set; }
        public string RestaurantTitle { get; set; }
        public decimal? Price { get; set; }
        
    }
    [MetadataType(typeof(FoodReservationLetterMetadata))]
    public partial class TblFoodReservationLetter { }

    [MetadataType(typeof(FoodReservationStatusMetadata))]
    public partial class TblFoodReservationStatu { }


    [MetadataType(typeof(MealMetadata))]
    public partial class TblMeal { }

    [MetadataType(typeof(ManagementMetadata))]
    public partial class TblManagement { }

    [MetadataType(typeof(CompanyMetadata))]
    public partial class TblCompany { }

    [MetadataType(typeof(SubsidiaryMetadata))]
    public partial class TblSubsidiary { }

    [MetadataType(typeof(OfficeMetadata))]
    public partial class TblOffice { }


    [MetadataType(typeof(OfficeDepartmentMetadata))]
    public partial class TblOfficeDepartment { }

    [MetadataType(typeof(OfficeDepartmentAccountNumberMetadata))]
    public partial class TblOfficeDepartmentAccountNumber { }

    [MetadataType(typeof(OfficeFoodQuotaMetadata))]
    public partial class TblOfficeFoodQuota { }


    [MetadataType(typeof(OfficeRestaurantMetadata))]
    public partial class TblOfficeRestaurant { }


    [MetadataType(typeof(PersonMetadata))]
    public partial class TblPerson
    {
      

        
        public string Fullname()
        {
            return Name.Trim() + " " + Family.Trim();
        }
    }


    [MetadataType(typeof(RestaurantMetadata))]
    public partial class TblRestaurant { }

    [MetadataType(typeof(RestaurantStatusMetadata))]
    public partial class TblRestaurantStatu { }

    [MetadataType(typeof(RestaurantTypeMetadata))]
    public partial class TblRestaurantType { }

    [MetadataType(typeof(RestaurantFoodProgramMetadata))]
    public partial class TblRestaurantFoodProgram { }


    [MetadataType(typeof(RoleMetadata))]
    public partial class TblRole { }


    [MetadataType(typeof(RoomMetadata))]
    public partial class TblRoom { }

    [MetadataType(typeof(RoomTypeMetadata))]
    public partial class TblRoomType { }

    [MetadataType(typeof(RoomStatusMetadata))]
    public partial class TblRoomStatu { }


    [MetadataType(typeof(UserMetadata))]
    public partial class TblUser { }


    [MetadataType(typeof(UserMessageMetadata))]
    public partial class TblUserMessage { }


    //[MetadataType(typeof(Metadata))]
    public partial class ModelFoodProgram
    {
        [Display(Name = "تاریخ", Description = " ")]
        public DateTime Date { get; set; }

        [Display(Name = "صبحانه", Description = " ")]
        public List<MealFood> Foods { get; set; }

        [Display(Name = "صبحانه", Description = " ")]
        public List<int> Breakfast { get; set; }

        [Display(Name = "نهار", Description = " ")]
        public List<int> Lunch { get; set; }

        [Display(Name = "شام", Description = " ")]
        public List<int> Dinner { get; set; }
    }

    public partial class MealFood
    {
        [Display(Name = "تاریخ", Description = " ")]
        public DateTime Date { get; set; }


        public int Food { get; set; }

        public int Meal { get; set; }

        public bool Default { get; set; }

    }


    [MetadataType(typeof(BookingCodeMetadata))]
    public partial class TblBookingCode { }


    [MetadataType(typeof(RouteMetadata))]
    public partial class TblRoute
    {
        public string Title()
        {
            return TblCity1.Title + " به " + TblCity.Title;
        }
    }

    [MetadataType(typeof(VehicleTypeMetadata))]
    public partial class TblVehicleType { }

    [MetadataType(typeof(VehicleMetadata))]
    public partial class TblVehicle { }


    [MetadataType(typeof(DriverMetadata))]
    public partial class TblDriver
    {
        public string Fullname()
        {
            return Name + " " + Family;
        }
    }


    [MetadataType(typeof(TripMetadata))]
    public partial class TblTrip
    {
        public string Title()
        {
            return Route + " " + Time;
        }
    }

    [MetadataType(typeof(BookingRequestMetadata))]
    public partial class TblBookingRequest
    {


    }


      [MetadataType(typeof(StoreMetadata))]
    public partial class TblStore
    {


    }
      [MetadataType(typeof(Category1Metadata))]
    public partial class TblCategory1
    {


    }
       [MetadataType(typeof(Category2Metadata))]
    public partial class TblCategory2
    {


    }
         [MetadataType(typeof(ProductMetadata))]
    public partial class TblProduct
    {


    }
           [MetadataType(typeof(EntryDocumentMetadata))]
    public partial class TblEntryDocument
    {


    }

    //[MetadataType(typeof(Metadata))]
    //public partial class  { }

    //[MetadataType(typeof(Metadata))]
    //public partial class  { }

    //[MetadataType(typeof(Metadata))]
    //public partial class  { }

    //[MetadataType(typeof(Metadata))]
    //public partial class  { }


    //[MetadataType(typeof(Metadata))]
    //public partial class  { }


    //[MetadataType(typeof(Metadata))]
    //public partial class  { }

}