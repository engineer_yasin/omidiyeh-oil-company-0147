//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omidiyeh_Oil_Company___0147.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TblFoodReservationGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TblFoodReservationGroup()
        {
            this.TblBeds = new HashSet<TblBed>();
            this.TblFoodReservations = new HashSet<TblFoodReservation>();
            this.TblFoodReservationShiftPersons = new HashSet<TblFoodReservationShiftPerson>();
        }
    
        public int Id { get; set; }
        public System.DateTime Date { get; set; }
        public int AccountNumber { get; set; }
        public int User { get; set; }
        public string Des { get; set; }
        public Nullable<int> Letter { get; set; }
        public bool Temp { get; set; }
        public byte Type { get; set; }
        public Nullable<int> Shift { get; set; }
        public bool Remove { get; set; }
        public bool Confirm { get; set; }
    
        public virtual TblOfficeDepartmentAccountNumber TblOfficeDepartmentAccountNumber { get; set; }
        public virtual TblUser TblUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblBed> TblBeds { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblFoodReservation> TblFoodReservations { get; set; }
        public virtual TblFoodReservationGroup TblFoodReservationGroup1 { get; set; }
        public virtual TblFoodReservationGroup TblFoodReservationGroup2 { get; set; }
        public virtual TblFoodReservationLetter TblFoodReservationLetter { get; set; }
        public virtual TblFoodReservationShift TblFoodReservationShift { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblFoodReservationShiftPerson> TblFoodReservationShiftPersons { get; set; }
    }
}
