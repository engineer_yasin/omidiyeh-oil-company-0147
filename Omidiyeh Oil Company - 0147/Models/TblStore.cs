//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omidiyeh_Oil_Company___0147.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TblStore
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TblStore()
        {
            this.TblBillOfLadings = new HashSet<TblBillOfLading>();
            this.TblEntryDocuments = new HashSet<TblEntryDocument>();
            this.TblEntryDocuments1 = new HashSet<TblEntryDocument>();
            this.TblStoreUsers = new HashSet<TblStoreUser>();
        }
    
        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string Des { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblBillOfLading> TblBillOfLadings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblEntryDocument> TblEntryDocuments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblEntryDocument> TblEntryDocuments1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblStoreUser> TblStoreUsers { get; set; }
    }
}
