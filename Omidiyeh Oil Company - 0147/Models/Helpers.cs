﻿using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Data.Entity;

namespace Omidiyeh_Oil_Company___0147.Models
{
    public class Helpers
    {
        private readonly IUnitOfWork _unitOfWork = new UnitOfWork<dbEntities>();
        private readonly dbEntities _db = new dbEntities();


        public List<Item> UserCompany(int id)
        {
            var user =  _unitOfWork.GetRepository<TblUser>().GetSingleSync(a => a.Id == id
                            , includeProperties: "TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary");
            List<Item> list;
            var q = _unitOfWork.GetRepository<TblCompany>();
            switch (user.AccessTo)
            {
                default:
                    list = null;
                    break;
                case 1:
                    list = ( q.GetSync(orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    list = ( q.GetSync(filter: a => a.Id == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company, orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                    break;

            }
            return list;
        }
        public async Task<List<Item>> UserCompanyAsync(int id)
        {
            var user = await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == id
                            , includeProperties: "TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary");
            List<Item> list;
            var q = _unitOfWork.GetRepository<TblCompany>();
            switch (user.AccessTo)
            {
                default:
                    list = null;
                    break;
                case 1:
                    list = (await q.Get(orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    list = (await q.Get(filter: a => a.Id == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company, orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                    break;

            }
            return list;
        }
        public List<Item> UserSubsidiary(int id, int company)
        {
            var user =  _unitOfWork.GetRepository<TblUser>().GetSingleSync(a => a.Id == id
                , includeProperties: "TblPerson.TblOfficeDepartment.TblOffice.TblManagement");
            List<Item> list;
            var q = _unitOfWork.GetRepository<TblSubsidiary>();
            if (company == 1)
            {
                list = ( q.GetSync(filter: a => a.Company == company, orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
            }
            else
            {
                switch (user.AccessTo)
                {
                    default:
                        list = null;
                        break;
                    case 1:
                    case 2:
                        list = ( q.GetSync(filter: a => a.Company == company, orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        list = ( q.GetSync(
                                filter: a =>
                                    a.Company == company && a.Id == user.TblPerson.TblOfficeDepartment.TblOffice
                                        .TblManagement.Subsidiary, orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;

                }
            }

            return list;
        }
        public async Task<List<Item>> UserSubsidiaryAsync(int id, int company)
        {
            var user = await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == id
                , includeProperties: "TblPerson.TblOfficeDepartment.TblOffice.TblManagement");
            List<Item> list;
            var q = _unitOfWork.GetRepository<TblSubsidiary>();
            if (company == 1)
            {
                list = (await q.Get(filter: a => a.Company == company, orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
            }
            else
            {
                switch (user.AccessTo)
                {
                    default:
                        list = null;
                        break;
                    case 1:
                    case 2:
                        list = (await q.Get(filter: a => a.Company == company, orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        list = (await q.Get(
                                filter: a =>
                                    a.Company == company && a.Id == user.TblPerson.TblOfficeDepartment.TblOffice
                                        .TblManagement.Subsidiary, orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;

                }
            }

            return list;
        }

        public List<Item> UserManagement(int id, int subsidiary)
        {
            var user =  _unitOfWork.GetRepository<TblUser>().GetSingleSync(a => a.Id == id
                , includeProperties: @"TblPerson.TblOfficeDepartment.TblOffice.TblManagement,
                                        TblPerson.TblOfficeDepartment.TblOffice");
            List<Item> list;
            var q = _unitOfWork.GetRepository<TblManagement>();
            switch (user.AccessTo)
            {
                default:
                    list = null;
                    break;
                case 1:
                case 2:
                case 3:
                    list = subsidiary == -1 ? ( q.GetSync(orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList()
                        : ( q.GetSync(filter: a => a.Subsidiary == subsidiary, orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                    break;

                case 4:
                case 5:
                case 6:
                    list = subsidiary == -1 ?
                 ( q.GetSync(filter: a => a.Subsidiary == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary && a.Id == user.TblPerson.TblOfficeDepartment.TblOffice.Management, orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList()
                 : ( q.GetSync(filter: a => a.Subsidiary == subsidiary && a.Id == user.TblPerson.TblOfficeDepartment.TblOffice.Management, orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                    break;

            }
            return list;
        }
         public async Task<List<Item>> UserManagementAsync(int id, int subsidiary)
        {
            var user = await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == id
                , includeProperties: @"TblPerson.TblOfficeDepartment.TblOffice.TblManagement,
                                        TblPerson.TblOfficeDepartment.TblOffice");
            List<Item> list;
            var q = _unitOfWork.GetRepository<TblManagement>();
            switch (user.AccessTo)
            {
                default:
                    list = null;
                    break;
                case 1:
                case 2:
                case 3:
                    list = subsidiary == -1 ? (await q.Get(orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList()
                        : (await q.Get(filter: a => a.Subsidiary == subsidiary, orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                    break;

                case 4:
                case 5:
                case 6:
                    list = subsidiary == -1 ?
                 (await q.Get(filter: a => a.Subsidiary == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary && a.Id == user.TblPerson.TblOfficeDepartment.TblOffice.Management, orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList()
                 : (await q.Get(filter: a => a.Subsidiary == subsidiary && a.Id == user.TblPerson.TblOfficeDepartment.TblOffice.Management, orderBy: o => o.OrderBy(a => a.Title))).Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                    break;

            }
            return list;
        }

        public  List<Item> UserOffice(int id, int management)
        {
            var user =  _unitOfWork.GetRepository<TblUser>().GetSingleSync(a => a.Id == id
                , includeProperties: "TblPerson.TblOfficeDepartment");
            List<Item> list;
            var q = _unitOfWork.GetRepository<TblOffice>();
            if (management == -3)
            {
                switch (user.AccessTo)
                {
                    default:
                        list = null;
                        break;
                    case 1:
                        list = ( q.GetSync(orderBy: o => o.OrderBy(a => a.Title), includeProperties: "TblManagement,TblManagement.TblSubsidiary"))
                            .Select(a => new Item { Id = a.Id, Title = a.Title, Des = a.TblManagement.TblSubsidiary.Title + " - " + a.TblManagement.Title }).ToList();
                        break;
                    case 2:
                        list = ( q.GetSync(filter: a => a.TblManagement.TblSubsidiary.Company == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company,
                                orderBy: o => o.OrderBy(a => a.Title), includeProperties: "TblManagement,TblManagement.TblSubsidiary"))
                            .Select(a => new Item { Id = a.Id, Title = a.Title, Des = a.TblManagement.TblSubsidiary.Title + " - " + a.TblManagement.Title }).ToList();
                        break;
                    case 3:
                        list = ( q.GetSync(filter: a => a.TblManagement.Subsidiary == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title, Des = a.TblManagement.Title }).ToList();
                        break;
                    case 4:
                        list = ( q.GetSync(filter: a => a.Management == user.TblPerson.TblOfficeDepartment.TblOffice.Management,
                                orderBy: o => o.OrderBy(a => a.Title), includeProperties: "TblManagement"))
                            .Select(a => new Item { Id = a.Id, Title = a.Title, Des = a.TblManagement.Title }).ToList();
                        break;
                    case 5:
                    case 6:
                        list = ( q.GetSync(filter: a => a.Id == user.TblPerson.TblOfficeDepartment.Office,
                                orderBy: o => o.OrderBy(a => a.Title), includeProperties: "TblManagement"))
                            .Select(a => new Item { Id = a.Id, Title = a.Title, Des = a.TblManagement.Title }).ToList();
                        break;


                }
            }
            else
            {
                switch (user.AccessTo)
                {
                    default:
                        list = null;
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        list = ( q.GetSync(filter: a => a.Management == management,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title })
                            .ToList();
                        break;


                    case 5:
                    case 6:
                        list = ( q.GetSync(
                                filter: a =>
                                    a.Management == management && a.Id == user.TblPerson.TblOfficeDepartment.Office,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title })
                            .ToList();
                        break;

                }
            }

            return list;
        }
        public async Task<List<Item>> UserOfficeAsunc(int id, int management)
        {
            var user = await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == id
                , includeProperties: "TblPerson.TblOfficeDepartment");
            List<Item> list;
            var q = _unitOfWork.GetRepository<TblOffice>();
            if (management == -3)
            {
                switch (user.AccessTo)
                {
                    default:
                        list = null;
                        break;
                    case 1:
                        list = (await q.Get(orderBy: o => o.OrderBy(a => a.Title), includeProperties: "TblManagement,TblManagement.TblSubsidiary"))
                            .Select(a => new Item { Id = a.Id, Title = a.Title, Des = a.TblManagement.TblSubsidiary.Title + " - " + a.TblManagement.Title }).ToList();
                        break;
                    case 2:
                        list = (await q.Get(filter: a => a.TblManagement.TblSubsidiary.Company == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company,
                                orderBy: o => o.OrderBy(a => a.Title), includeProperties: "TblManagement,TblManagement.TblSubsidiary"))
                            .Select(a => new Item { Id = a.Id, Title = a.Title, Des = a.TblManagement.TblSubsidiary.Title + " - " + a.TblManagement.Title }).ToList();
                        break;
                    case 3:
                        list = (await q.Get(filter: a => a.TblManagement.Subsidiary == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title, Des = a.TblManagement.Title }).ToList();
                        break;
                    case 4:
                        list = (await q.Get(filter: a => a.Management == user.TblPerson.TblOfficeDepartment.TblOffice.Management,
                                orderBy: o => o.OrderBy(a => a.Title), includeProperties: "TblManagement"))
                            .Select(a => new Item { Id = a.Id, Title = a.Title, Des = a.TblManagement.Title }).ToList();
                        break;
                    case 5:
                    case 6:
                        list = (await q.Get(filter: a => a.Id == user.TblPerson.TblOfficeDepartment.Office,
                                orderBy: o => o.OrderBy(a => a.Title), includeProperties: "TblManagement"))
                            .Select(a => new Item { Id = a.Id, Title = a.Title, Des = a.TblManagement.Title }).ToList();
                        break;


                }
            }
            else
            {
                switch (user.AccessTo)
                {
                    default:
                        list = null;
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        list = (await q.Get(filter: a => a.Management == management,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title })
                            .ToList();
                        break;


                    case 5:
                    case 6:
                        list = (await q.Get(
                                filter: a =>
                                    a.Management == management && a.Id == user.TblPerson.TblOfficeDepartment.Office,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title })
                            .ToList();
                        break;

                }
            }

            return list;
        }

        public async Task<List<Item>> UserOfficeDepartmentAsync(int id, int office)
        {
            var user = await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == id
                , includeProperties: @"TblPerson,
                                        TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary,
                                        TblPerson.TblOfficeDepartment.TblOffice.TblManagement,
                                        TblPerson.TblOfficeDepartment.TblOffice,
                                        TblPerson.TblOfficeDepartment");
            List<Item> list;
            var q = _unitOfWork.GetRepository<TblOfficeDepartment>();
            if (office == -3)
            {
                switch (user.AccessTo)
                {
                    default:
                        list = null;
                        break;
                    case 1:
                        list = (await q.Get(orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;
                    case 2:
                        list = (await q.Get(filter: a => a.TblOffice.TblManagement.TblSubsidiary.Company == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;
                    case 3:
                        list = (await q.Get(filter: a => a.TblOffice.TblManagement.Subsidiary == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;
                    case 4:
                        list = (await q.Get(filter: a => a.TblOffice.Management == user.TblPerson.TblOfficeDepartment.TblOffice.Management,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;
                    case 5:
                        list = (await q.Get(filter: a => a.Office == user.TblPerson.TblOfficeDepartment.Office,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;

                    case 6:
                        list = (await q.Get(filter: a => a.Office == office && a.Id == user.TblPerson.OfficeDepartment,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title })
                            .ToList();
                        break;

                }
            }
            else
            {
                switch (user.AccessTo)
                {
                    default:
                        list = null;
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        list = (await q.Get(filter: a => a.Office == office, orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;

                    case 6:
                        list = (await q.Get(filter: a => a.Office == office && a.Id == user.TblPerson.OfficeDepartment,
                                orderBy: o => o.OrderBy(a => a.Title)))
                            .Select(a => new Item { Id = a.Id, Title = a.Title })
                            .ToList();
                        break;

                }
            }

            return list;
        }

        public List<Item> UserOfficeDepartment(int id, int office)
        {
            var user1 = _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == id
                , includeProperties: @"TblPerson,
                                        TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary,
                                        TblPerson.TblOfficeDepartment.TblOffice.TblManagement,
                                        TblPerson.TblOfficeDepartment.TblOffice,
                                        TblPerson.TblOfficeDepartment");
            var user = _db.TblUsers
                .Include(a => a.TblPerson)
                .Include(a=>a.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary)
                .Include(a=>a.TblPerson.TblOfficeDepartment.TblOffice.TblManagement)
                .Include(a=>a.TblPerson.TblOfficeDepartment.TblOffice)
                .Include(a=>a.TblPerson.TblOfficeDepartment)
                .First(a=>a.Id==id)
                ;
            List<Item> list;
          
            var q = _db.TblOfficeDepartments;
            if (office == -3)
            {
                switch (user.AccessTo)
                {
                    default:
                        list = null;
                        break;
                    case 1:
                        list = ( q.OrderBy(a => a.Title))
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;
                    case 2:
                        list =  q.Where(a => a.TblOffice.TblManagement.TblSubsidiary.Company == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company)
                                .OrderBy(a => a.Title)
                                .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;
                    case 3:
                        list =  q.Where(a => a.TblOffice.TblManagement.Subsidiary == user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary)
                               .OrderBy(a => a.Title)
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;
                    case 4:
                        list =  q.Where(a => a.TblOffice.Management == user.TblPerson.TblOfficeDepartment.TblOffice.Management)
                                .OrderBy(a => a.Title)
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;
                    case 5:
                        list =  q.Where(a => a.Office == user.TblPerson.TblOfficeDepartment.Office)
                                .OrderBy(a => a.Title)
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;

                    case 6:
                        list =  q.Where( a =>/* a.Office == office && */a.Id == user.TblPerson.OfficeDepartment)
                                .OrderBy(a => a.Title)
                            .Select(a => new Item { Id = a.Id, Title = a.Title })
                            .ToList();
                        break;

                }
            }
            else
            {
                switch (user.AccessTo)
                {
                    default:
                        list = null;
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        list = q.Where( a => a.Office == office).OrderBy(a => a.Title)
                            .Select(a => new Item { Id = a.Id, Title = a.Title }).ToList();
                        break;

                    case 6:
                        list = q.Where( a => a.Office == office && a.Id == user.TblPerson.OfficeDepartment)
                                 .OrderBy(a => a.Title)
                            .Select(a => new Item { Id = a.Id, Title = a.Title })
                            .ToList();
                        break;

                }
            }

            return list;
        }
        /// <summary>
        /// لیست اداراتی که یک کاربر به آن دسترسی دارد
        /// 0:None, 1All, 2:Company, 3:Subsidiary, 4:Management, 5:Office, 6:OfficeDepartment
        /// </summary>
        /// <param name="id">شناسه کاربر</param>
        /// <returns></returns>
        public async Task<(List<int> Offices, bool OnlyOfficeDepartment, int OfficeDepartment)> UserAccess1(int id)
        {
            var onlyOfficeDepartment = false;
            var user = await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == id,
                includeProperties: "TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary,TblPerson.TblOfficeDepartment.TblOffice.TblManagement,TblPerson.TblOfficeDepartment.TblOffice,TblPerson.TblOfficeDepartment");
            List<int> list;
            var q = _unitOfWork.GetRepository<TblOffice>();
            switch (user.AccessTo)
            {
                case 0:
                    list = null;
                    break;
                case 1:
                    list = (await q.Get()).Select(a => a.Id).ToList();
                    break;
                case 2:
                    list = (await q.Get(a => a.TblManagement.TblSubsidiary.Company ==
                                           user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company))
                                           .Select(a => a.Id).ToList();
                    break;
                case 3:
                    list = (await q.Get(a => a.TblManagement.Subsidiary ==
                                             user.TblPerson.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary))
                        .Select(a => a.Id).ToList();
                    break;
                case 4:
                    list = (await q.Get(a => a.Management ==
                                             user.TblPerson.TblOfficeDepartment.TblOffice.Management))
                        .Select(a => a.Id).ToList();
                    break;
                case 5:
                    list = new List<int> { user.TblPerson.TblOfficeDepartment.Office };
                    break;
                case 6:
                    list = new List<int> { user.TblPerson.TblOfficeDepartment.Office };
                    onlyOfficeDepartment = true;
                    break;
                default:
                    list = null;
                    break;

            }

            return (list, onlyOfficeDepartment, user.TblPerson.OfficeDepartment);
        }
    }

    public static class Helper{
        public static string ToFarsi(this string value)
        {
            return value
                .Replace("ي", "ی")
                .Replace("ك", "ک")
                .Replace("ة", "ه")
                .Replace("ﯼ", "ی")
                .Replace("ى", "ی")
                .Replace("ئ", "ی")
                ;
        }
    }
}