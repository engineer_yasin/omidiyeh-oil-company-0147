﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;

namespace Omidiyeh_Oil_Company___0147.Models
{
    public static class Statics
    {
        
        private static readonly IUnitOfWork UnitOfWork = new UnitOfWork<dbEntities>();
        
        public static async Task<string> MealTitle(int id)
        {
            return (await UnitOfWork.GetRepository<TblMeal>().GetSingle(a=>a.Id==id)).Title;
        }
        
        public static  string MealTitleSync(int id)
        {
            return ( UnitOfWork.GetRepository<TblMeal>().GetSingleSync(a=>a.Id==id)).Title;
        }
        

        public static string HashedPassword(string password, string passwordSalt)
        {
 
            var saltedPassword = password + passwordSalt;
            var saltedPasswordBytes = Encoding.UTF8.GetBytes(saltedPassword);
            var hashedPassword = Convert.ToBase64String(SHA512.Create().ComputeHash(saltedPasswordBytes));
            return hashedPassword;
        }

        public static (DateTime startDate, DateTime endDate) GetMonthDate(int year, int month)
        {
            const int startDay = 1;
            var endDay = month > 6 ? 30 : 31;
            var startDate = new PersianDateTime(year, month, startDay).ToDateTime();
            if (!PersianDateTime.TryParse(year + "/" + month + "/" + endDay, out var endDateFa))
            {
                PersianDateTime.TryParse(year + "/" + month + "/" + (endDay - 1), out endDateFa);
            }

            var endDate = endDateFa.ToDateTime();
            return (startDate, endDate);
        }

        public static string MonthName(int month)
        {
            switch (month)
            {
                case 1:
                    return "فروردین";
                case 2:
                    return "اردیبهشت";
                case 3:
                    return "خرداد";
                case 4:
                    return "تیر";
                case 5:
                    return "مرداد";
                case 6:
                    return "شهریور";
                case 7:
                    return "مهر";
                case 8:
                    return "آبان";
                case 9:
                    return "آذر";
                case 10:
                    return "دی";
                case 11:
                    return "بهمن";
                case 12:
                    return "اسفند";
                default:
                    return "-";
            }
        }

        public static string ToSubstring(this string value, string search)
        {
            try
            {
                return value.Contains(search)
                    ? value.Substring(0, value.LastIndexOf(search, StringComparison.Ordinal) + search.Length)
                    : value;
            }
            catch
            {
                return value;
            }
        } 
        public static string ToRemoveAt(this string value, string search)
        {
            try
            {
                return value.Contains(search)
                    ? value.Substring(0, value.LastIndexOf(search, StringComparison.Ordinal))
                    : value;
            }
            catch
            {
                return value;
            }
        }
    }
}