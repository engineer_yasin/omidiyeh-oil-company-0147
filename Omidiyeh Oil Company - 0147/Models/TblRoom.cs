//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Omidiyeh_Oil_Company___0147.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TblRoom
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TblRoom()
        {
            this.TblBeds = new HashSet<TblBed>();
        }
    
        public int Id { get; set; }
        public int Restaurant { get; set; }
        public int Number { get; set; }
        public byte Status { get; set; }
        public int Capacity { get; set; }
        public string Des { get; set; }
        public byte Type { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TblBed> TblBeds { get; set; }
        public virtual TblRestaurant TblRestaurant { get; set; }
        public virtual TblRoomStatu TblRoomStatu { get; set; }
        public virtual TblRoomType TblRoomType { get; set; }
    }
}
