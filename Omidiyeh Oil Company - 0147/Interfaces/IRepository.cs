﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Omidiyeh_Oil_Company___0147.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<IEnumerable<TEntity>> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");
        
        IEnumerable<TEntity> GetSync(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");

        Task<TEntity> GetSingle(
            Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "",
            string includeProperties = "");

         TEntity GetSingleSync(
            Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "",
            string includeProperties = "");

        Task<TEntity> GetFirst(
            Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "",
            string includeProperties = "");

        Task<bool> Any(Expression<Func<TEntity, bool>> filter = null);
       bool AnySync(Expression<Func<TEntity, bool>> filter = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <param name="includeProperties"></param>
        /// <param name="take"></param>
        /// <param name="skip"></param>
        /// <returns>List,query count, full count</returns>
        Task<Tuple<IEnumerable<TEntity>, int,int>> GetOrdered(
            Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "",
            string includeProperties = "",int take =10, int skip=0);

        Tuple<IEnumerable<TEntity>, int,int> GetOrderedSync(
            Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "",
            string includeProperties = "",int take =10, int skip=0);
        
        Tuple<IEnumerable<TEntity>, int,int> GetOrderedSyncNoTracking(
            Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "",
            string includeProperties = "",int take =10, int skip=0);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns>Query Count, Full Count</returns>
        Tuple<int, int> Count(Expression<Func<TEntity, bool>> filter = null);

        //Task<TEntity> GetById(object id);
        Task<Tuple<bool, string>> Insert(TEntity entity,int user);

        Tuple<bool, string> InsertSync(TEntity entity,int user,bool log = false);
        Task<(bool result, string message)> InsertRange(List<TEntity> entities, int user);
        (bool result, string message) InsertRangeSync(List<TEntity> entities, int user);
        Task<Tuple<bool, string>> Delete(object id, int user);
       
        Task<Tuple<bool, string>> DeleteSoft(TEntity entity,object id, int user);
      Tuple<bool, string> DeleteSoftSync(TEntity entity,object id, int user);
        Task<(bool, int, string)> DeleteRange(Expression<Func<TEntity, bool>> filter, int user);
        //void Delete(TEntity entityToDelete);
        Task<Tuple<bool, string>> Update(TEntity entity,int user); 
        Tuple<bool, string> UpdateSync(TEntity entity, int user);

    }
}