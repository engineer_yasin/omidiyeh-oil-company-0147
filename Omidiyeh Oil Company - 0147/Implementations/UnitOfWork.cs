﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Omidiyeh_Oil_Company___0147.Interfaces;

namespace Omidiyeh_Oil_Company___0147.Implementations
{
    public class UnitOfWork<C> : IDisposable, IUnitOfWork where C : DbContext, new()
    {
        private readonly C _context = new C { Configuration = { LazyLoadingEnabled = false, ProxyCreationEnabled = true } };
        private readonly Hashtable _repositories = new Hashtable();
        public IRepository<T> GetRepository<T>() where T : class
        {
            if (!_repositories.Contains(typeof(T)))
            {
                _repositories.Add(typeof(T), new Repository<T>(_context));
            }
            return (IRepository<T>)_repositories[typeof(T)];
        }
        public void Save()
        {
            _context.SaveChanges();
        }
        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();

        }
        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}