﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using Omidiyeh_Oil_Company___0147.Interfaces;
using System.Web;
using Newtonsoft.Json;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Implementations
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        internal DbContext Context;
        internal DbSet<TEntity> DbSet;

        public Repository(DbContext context)
        {

            this.Context = context;
            this.DbSet = context.Set<TEntity>();
            Context.Configuration.LazyLoadingEnabled = false;
        }

        public virtual async Task<IEnumerable<TEntity>> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            try
            {

                IQueryable<TEntity> query = DbSet;

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }

                return orderBy != null ? await orderBy(query).ToListAsync() : await query.ToListAsync();
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public virtual IEnumerable<TEntity> GetSync(
           Expression<Func<TEntity, bool>> filter = null,
           Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
           string includeProperties = "")
        {
            try
            {

                IQueryable<TEntity> query = DbSet;

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }

                return orderBy != null ? orderBy(query) : query;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<TEntity> GetSingle(Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "", string includeProperties = "")
        {
            try
            {
                IQueryable<TEntity> query = DbSet;

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }


                var item = orderBy == "" ? await query.SingleOrDefaultAsync() : await query.OrderBy(orderBy).SingleOrDefaultAsync();

                return item;
            }
            catch (Exception e)
            {
                throw;
            }
        }
         public TEntity GetSingleSync(Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "", string includeProperties = "")
        {
            try
            {
                IQueryable<TEntity> query = DbSet;

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }


                var item = orderBy == "" ?  query.SingleOrDefault() :  query.OrderBy(orderBy).SingleOrDefault();

                return item;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<TEntity> GetFirst(Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "", string includeProperties = "")
        {
            try
            {
                IQueryable<TEntity> query = DbSet;

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }


                var item = orderBy == "" ? await query.FirstOrDefaultAsync() : await query.OrderBy(orderBy).FirstOrDefaultAsync();

                return item;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<bool> Any(Expression<Func<TEntity, bool>> filter = null)
        {

            IQueryable<TEntity> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return await query.AnyAsync();

        }
        public bool AnySync(Expression<Func<TEntity, bool>> filter = null)
        {

            IQueryable<TEntity> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return  query.Any();

        }
       
        
        public async Task<Tuple<IEnumerable<TEntity>, int, int>> GetOrdered(Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "", string includeProperties = "", int take = 10, int skip = 0)
        {
            try
            {
                IQueryable<TEntity> query = DbSet;
                var fullCount = query.Count();

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }

                var queryCount = query.Count();
                var list = await query.OrderBy(orderBy).Skip(skip).Take(take).ToListAsync();

                return new Tuple<IEnumerable<TEntity>, int, int>(list, queryCount, fullCount);
            }
            catch (Exception e)
            {
                return new Tuple<IEnumerable<TEntity>, int, int>(null, 0, 0);

            }
        }
        public Tuple<IEnumerable<TEntity>, int, int> GetOrderedSync(Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "", string includeProperties = "", int take = 10, int skip = 0)
        {
            try
            {
                IQueryable<TEntity> query = DbSet;
                var fullCount = query.Count();

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }

                var queryCount = query.Count();
                var list =  query.OrderBy(orderBy).Skip(skip).Take(take).ToList();

                return new Tuple<IEnumerable<TEntity>, int, int>(list, queryCount, fullCount);
            }
            catch (Exception e)
            {
                return new Tuple<IEnumerable<TEntity>, int, int>(null, 0, 0);

            }
        }
         public Tuple<IEnumerable<TEntity>, int, int> GetOrderedSyncNoTracking(Expression<Func<TEntity, bool>> filter = null,
            string orderBy = "", string includeProperties = "", int take = 10, int skip = 0)
        {
            try
            {
                IQueryable<TEntity> query = DbSet;
                var fullCount = query.Count();

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                // ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var includeProperty in includeProperties.Split
                    (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }

                var queryCount = query.Count();
                var list =  query.AsNoTracking().OrderBy(orderBy).Skip(skip).Take(take).ToList();

                return new Tuple<IEnumerable<TEntity>, int, int>(list, queryCount, fullCount);
            }
            catch (Exception e)
            {
                return new Tuple<IEnumerable<TEntity>, int, int>(null, 0, 0);

            }
        }

        public Tuple<int, int> Count(Expression<Func<TEntity, bool>> filter = null)
        {
            try
            {
                IQueryable<TEntity> query = DbSet;
                var fullCount = query.Count();

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                var queryCount = query.Count();


                return new Tuple<int, int>(queryCount, fullCount);
            }
            catch (Exception e)
            {
                return new Tuple<int, int>(-1, -1);
            }
        }
        //public virtual TEntity GetById(params object[] id)
        //{
        //    return DbSet.Find(id);
        //}
        //public virtual async Task<TEntity> GetById(object id)
        //{
        //    Context.Configuration.LazyLoadingEnabled = true;
        //    var result = await DbSet.FindAsync(id);

        //    return result;

        //}

        public virtual async Task<Tuple<bool, string>> Insert(TEntity entity, int user)
        {
            var message = "";
            var innerException = "";
            var validationErrors = new List<DbValidationError>();
            bool result;
            try
            {
                DbSet.Add(entity);
                await Context.SaveChangesAsync();
                result = true;
            }
            catch (DbEntityValidationException ve)
            {
                validationErrors = ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList();
                result = false;
               
                innerException = ve.InnerException?.InnerException?.Message;
                message = ve.Message ;
            }
            catch (Exception e)
            {
                result = false;
                message = e.Message;
                innerException = e.InnerException?.InnerException?.Message;

            }

            var body = new { message, innerException, validationErrors, data = entity };
            /* await*/
#pragma warning disable 4014
            Logs.Log(title: "Insert " + entity.GetType().Name, body: JsonConvert.SerializeObject(body, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects, ReferenceLoopHandling = ReferenceLoopHandling.Ignore }), type: "Insert", result, user);
#pragma warning restore 4014
            return new Tuple<bool, string>(result, message + $"<br />{string.Join(",", validationErrors.Select(a => a.PropertyName))}<br />{innerException}");

        }

        public virtual Tuple<bool, string> InsertSync(TEntity entity, int user, bool log = false)
        {
            var message = "";
            var innerException = "";
            var validationErrors = new List<DbValidationError>();
            bool result;
            try
            {
                DbSet.Add(entity);
                Context.SaveChanges();
                result = true;
            }
            catch (DbEntityValidationException ve)
            {
                validationErrors = ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList();
                result = false;
                message = ve.Message;
                innerException = ve.InnerException?.InnerException?.Message;

            }
            catch (Exception e)
            {
                result = false;
                message = e.Message;
                innerException = e.InnerException?.InnerException?.Message;

            }

            var body = new { message, innerException, validationErrors, data = entity };
            if (log)
                Logs.Log(title: "Insert " + entity.GetType().Name, body: JsonConvert.SerializeObject(body, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects, ReferenceLoopHandling = ReferenceLoopHandling.Ignore }), type: "Insert", result, user);
            return new Tuple<bool, string>(result, message + $"<br />{string.Join(",", validationErrors.Select(a => a.PropertyName))}<br />{innerException}");

        }

        public virtual async Task<(bool result, string message)> InsertRange(List<TEntity> entities, int user)
        {
            var message = "";
            var innerException = "";
            var validationErrors = new List<DbValidationError>();
            bool result;
            try
            {
                DbSet.AddRange(entities);
                await Context.SaveChangesAsync();
                result = true;
            }
            catch (DbEntityValidationException ve)
            {
                validationErrors = ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList();
                result = false;
                message = ve.Message;
                innerException = ve.InnerException?.InnerException?.Message;

            }
            catch (Exception e)
            {
                result = false;
                message = e.Message;
                innerException = e.InnerException?.InnerException?.Message;

            }

            var body = new { message, innerException, validationErrors, data = entities };
            /* await*/
            Logs.Log(title: "Insert " + entities.GetType().Name, body: JsonConvert.SerializeObject(body, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects, ReferenceLoopHandling = ReferenceLoopHandling.Ignore }), type: "Insert", result, user);
            return (result, message);
        }
        public virtual (bool result, string message) InsertRangeSync(List<TEntity> entities, int user)
        {
            var message = "";
            var innerException = "";
            var validationErrors = new List<DbValidationError>();
            bool result;
            try
            {
                DbSet.AddRange(entities);
                 Context.SaveChanges();
                result = true;
            }
            catch (DbEntityValidationException ve)
            {
                validationErrors = ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList();
                result = false;
                message = ve.Message;
                innerException = ve.InnerException?.InnerException?.Message;

            }
            catch (Exception e)
            {
                result = false;
                message = e.Message;
                innerException = e.InnerException?.InnerException?.Message;

            }

            var body = new { message, innerException, validationErrors, data = entities };
            /* await*/
            Logs.Log(title: "Insert " + entities.GetType().Name, body: JsonConvert.SerializeObject(body, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.Objects, ReferenceLoopHandling = ReferenceLoopHandling.Ignore }), type: "Insert", result, user);
            return (result, message + "\r\n"+ innerException);
        }
        public virtual async Task<(bool, int, string)> DeleteRange(
            Expression<Func<TEntity, bool>> filter, int user)
        {
            var message = "";
            var innerException = "";
            var validationErrors = new List<DbValidationError>();
            bool result;
            var count = 0;
            IQueryable<TEntity> query = DbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }


            var entities = await query.ToListAsync();
            if (!entities.Any())
            {
                result = true;
                message = "اطلاعات درخواست داده شده برای حذف یافت نشد.";
                return (false, 0, message);
            }
            try
            {
                count = entities.Count;
                // entity = DbSet.Find(id);
                DbSet.RemoveRange(entities);
                await Context.SaveChangesAsync();
                result = true;
            }

            catch (DbEntityValidationException ve)
            {
                validationErrors = ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList();
                result = false;
                message = ve.Message;
                innerException = ve.InnerException?.InnerException?.Message;

            }
            catch (Exception e)
            {
                result = false;
                message = e.Message;
                innerException = e.InnerException?.InnerException?.Message;

            }

            var body = new { message, innerException, validationErrors, data = entities };
            /*await*/
            Logs.Log(title: "Delete " + entities.GetType().BaseType?.Name, body: JsonConvert.SerializeObject(body), type: "Delete", result, user);

            return (result, count, message);

        }

        public virtual async Task<Tuple<bool, string>> Delete(object id, int user)
        {
            var message = "";
            var innerException = "";
            var validationErrors = new List<DbValidationError>();
            bool result;
            var entity = DbSet.Find(id);
            var entityDeleted = entity;
            if (entity == null)
            {
                result = false;
                message = "اطلاعات درخواست داده شده برای حذف یافت نشد.";
                return new Tuple<bool, string>(result, message);
            }
            try
            {
                // entity = DbSet.Find(id);
                DbSet.Remove(entity);


                await Context.SaveChangesAsync();
                result = true;
            }

            catch (DbEntityValidationException ve)
            {
                validationErrors = ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList();
                result = false;
                message = ve.Message;
                innerException = ve.InnerException?.InnerException?.Message;

            }
            catch (Exception e)
            {
                result = false;
                message = e.Message;
                innerException = e.InnerException?.InnerException?.Message;

            }

            var body = new { message, innerException, validationErrors, data = entityDeleted };
            /*  await*/
            Logs.Log(title: "Delete " + entity.GetType().BaseType?.Name, body: JsonConvert.SerializeObject(body), type: "Delete", result, user);

            return new Tuple<bool, string>(result, message + $"<br />{string.Join(",", validationErrors.Select(a => a.PropertyName))}<br />{innerException}");

        }
        //public virtual void Update(TEntity entityToUpdate)
        //{
        //    DbSet.Attach(entityToUpdate);
        //    Context.Entry(entityToUpdate).State = EntityState.Modified;
        //}

        public virtual async Task<Tuple<bool, string>> DeleteSoft(TEntity entity, object id, int user)
        {
            var message = "";
            
            var innerException = "";
            var validationErrors = new List<DbValidationError>();
            bool result;
            try
            {

                Context.Entry(entity).State = EntityState.Modified;

                await Context.SaveChangesAsync();
                result = true;
            }

            catch (DbEntityValidationException ve)
            {
                validationErrors = ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList();
                result = false;
                message = ve.Message;
                innerException = ve.InnerException?.InnerException?.Message;

            }
            catch (Exception e)
            {
                result = false;
                message = e.Message;
                innerException = e.InnerException?.InnerException?.Message;

            }

            var name = entity.GetType().BaseType?.Name == "Object" ? entity.GetType().Name : entity.GetType().BaseType?.Name;
            //var t2 = entity.GetType().BaseType?.Name;
            //var t = entity.GetType().Name;
            var body = new { message, innerException, validationErrors, data = Context.Entry(entity).CurrentValues.ToObject(), orginalData = Context.Entry(entity).OriginalValues.ToObject() };
            /*await*/
            Logs.Log(title: "Delete Soft " + name, body: JsonConvert.SerializeObject(body), type: "Delete Soft", result, user);

            return new Tuple<bool, string>(result, message + $"<br />{string.Join(",", validationErrors.Select(a => a.PropertyName))}<br />{innerException}");
        }
        public virtual Tuple<bool, string> DeleteSoftSync(TEntity entity, object id, int user)
        {
            var message = "";
            
            var innerException = "";
            var validationErrors = new List<DbValidationError>();
            bool result;
            try
            {

                Context.Entry(entity).State = EntityState.Modified;

                 Context.SaveChanges();
                result = true;
            }

            catch (DbEntityValidationException ve)
            {
                validationErrors = ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList();
                result = false;
                message = ve.Message;
                innerException = ve.InnerException?.InnerException?.Message;

            }
            catch (Exception e)
            {
                result = false;
                message = e.Message;
                innerException = e.InnerException?.InnerException?.Message;

            }

            var name = entity.GetType().BaseType?.Name == "Object" ? entity.GetType().Name : entity.GetType().BaseType?.Name;
            //var t2 = entity.GetType().BaseType?.Name;
            //var t = entity.GetType().Name;
            var body = new { message, innerException, validationErrors, data = Context.Entry(entity).CurrentValues.ToObject(), orginalData = Context.Entry(entity).OriginalValues.ToObject() };
            /*await*/
            Logs.Log(title: "Delete Soft " + name, body: JsonConvert.SerializeObject(body), type: "Delete Soft", result, user);

            return new Tuple<bool, string>(result, message + $"<br />{string.Join(",", validationErrors.Select(a => a.PropertyName))}<br />{innerException}");
        }
        
        public virtual async Task<Tuple<bool, string>> Update(TEntity entity, int user)
        {
            var message = "";
            var innerException = "";
            var validationErrors = new List<DbValidationError>();
            bool result;
            try
            {

                Context.Entry(entity).State = EntityState.Modified;

                await Context.SaveChangesAsync();
                result = true;
            }

            catch (DbEntityValidationException ve)
            {
                validationErrors = ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList();
                result = false;
                message = ve.Message;
                innerException = ve.InnerException?.InnerException?.Message;

            }
            catch (Exception e)
            {
                result = false;
                message = e.Message;
                innerException = e.InnerException?.InnerException?.Message;

            }

            var name = entity.GetType().BaseType?.Name == "Object" ? entity.GetType().Name : entity.GetType().BaseType?.Name;
            //var t2 = entity.GetType().BaseType?.Name;
            //var t = entity.GetType().Name;
            var body = new { message, innerException, validationErrors, data = Context.Entry(entity).CurrentValues.ToObject(), orginalData = Context.Entry(entity).OriginalValues.ToObject() };
            /*await*/
            Logs.Log(title: "Update " + name, body: JsonConvert.SerializeObject(body), type: "Update", result, user);

          //  return new Tuple<bool, string>(result, message);
            return new Tuple<bool, string>(result, message + $"<br />{string.Join(",", validationErrors.Select(a => a.PropertyName))}<br />{innerException}");
        }
         public virtual Tuple<bool, string> UpdateSync(TEntity entity, int user)
        {
            var message = "";
            var innerException = "";
            var validationErrors = new List<DbValidationError>();
            bool result;
            try
            {

                Context.Entry(entity).State = EntityState.Modified;

                 Context.SaveChanges();
                result = true;
            }

            catch (DbEntityValidationException ve)
            {
                validationErrors = ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList();
                result = false;
                message = ve.Message;
                innerException = ve.InnerException?.InnerException?.Message;

            }
            catch (Exception e)
            {
                result = false;
                message = e.Message;
                innerException = e.InnerException?.InnerException?.Message;

            }

            var name = entity.GetType().BaseType?.Name == "Object" ? entity.GetType().Name : entity.GetType().BaseType?.Name;
            //var t2 = entity.GetType().BaseType?.Name;
            //var t = entity.GetType().Name;
            var body = new { message, innerException, validationErrors, data = Context.Entry(entity).CurrentValues.ToObject(), orginalData = Context.Entry(entity).OriginalValues.ToObject() };
            /*await*/
            Logs.Log(title: "Update " + name, body: JsonConvert.SerializeObject(body), type: "Update", result, user);

          //  return new Tuple<bool, string>(result, message);
            return new Tuple<bool, string>(result, message + $"<br />{string.Join(",", validationErrors.Select(a => a.PropertyName))}<br />{innerException}");
        }

       
    }
}