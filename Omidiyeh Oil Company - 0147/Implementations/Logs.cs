﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Implementations
{
    public static class Logs
    {
       
        public static async Task<bool> Log(string title, string body, string type, bool result, int user)
        {
            try
            {
                var db = new logEntities();
                var device = HttpContext.Current?.Request.UserHostAddress ?? "--";
                var l = new Log
                {
                    Title = title,
                    Body = body,
                    Type = type,
                    Result = result,
                    User = user,
                    Time = DateTime.Now,
                    Device = device,
                    Id = Guid.NewGuid()
                };
                db.Logs.Add(l);
                await db.SaveChangesAsync();
                return true;
            }
            catch //(Exception e)
            {

                return false;
            }
        }

        public static async Task<string> CatchLog(Exception e, int user, HttpRequestBase request)
        {
           
            var eMessage = e.Message;
            var eInnerException = e.InnerException?.Message ?? e.InnerException?.InnerException?.Message;
            var data = request.Form.AllKeys.Aggregate("", (current, item) => current + (item + ":" + request.Form[item] + ";"));
            var queryString = request.QueryString;
            var body = new { eMessage, eInnerException, data, queryString };

            await Logs.Log(title: "Error " + request.Url, body: JsonConvert.SerializeObject(body), type: "Error", false, user);
            return string.Format(Resources.Public.ErrorMessage, e.Message);
        }

        public static async Task<string> ErrorLog(HandleErrorInfo e,int user)
        {

            var eMessage = e.Exception.Message;
            var eInnerException = e.Exception.InnerException?.Message ?? e.Exception.InnerException?.InnerException?.Message;
            var action = e.ActionName;
            var controller = e.ControllerName;
            var source = e.Exception.Source;
            var data = e.Exception.Data;
            var body = new { eMessage, eInnerException, action,controller,source, data };

            await Logs.Log(title: "Error " + controller +"/"+action, body: JsonConvert.SerializeObject(body), type: "Error", false, user);
            return string.Format(Resources.Public.ErrorMessage, eMessage);
        }
    }
}