//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Public {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Public() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Public", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to غذای انتخابی قرارداد فعالی ندارد، لطفا با پشتیبانی تماس بگیرید..
        /// </summary>
        internal static string ActiveContract {
            get {
                return ResourceManager.GetString("ActiveContract", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to اتاق شماره {0} و تخت شماره {1} برای آقا/خانم {2} با موفقیت ذخیره شد..
        /// </summary>
        internal static string AddBedToReservationSuccess {
            get {
                return ResourceManager.GetString("AddBedToReservationSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to هنگام ثبت &lt;strong&gt;{0}&lt;/strong&gt; خطایی رخ داده است.
        ///    &lt;br /&gt; &lt;small&gt;{1}&lt;/small&gt;.
        /// </summary>
        internal static string AddErrorMessage {
            get {
                return ResourceManager.GetString("AddErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;{0}&lt;/strong&gt; با موفقیت انجام شد..
        /// </summary>
        internal static string AddSuccessMessage {
            get {
                return ResourceManager.GetString("AddSuccessMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to هیچ آیتمی برای ثبت انتخاب نشده است..
        /// </summary>
        internal static string AddWarning {
            get {
                return ResourceManager.GetString("AddWarning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to قبلا برای تاریخ {0} و وعده غذایی {1} غذا رزرو شده است، لطفا به مدیریت درخواست غذا مراجعه کنید..
        /// </summary>
        internal static string AlreadyBooked {
            get {
                return ResourceManager.GetString("AlreadyBooked", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to از قبل غذا رزرو شده است، لطفا به مدیریت درخواست ها مراجعه کنید. درخواست شما {0}.
        /// </summary>
        internal static string AlreadyBooked2 {
            get {
                return ResourceManager.GetString("AlreadyBooked2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to از قبل برای این وسیله نقلیه غذا رزرو شده است، لطفا به مدیریت درخواست ها مراجعه کنید..
        /// </summary>
        internal static string AlreadyBookedVehicle {
            get {
                return ResourceManager.GetString("AlreadyBookedVehicle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to کاربر گرامی: گذرواژه  شما با موفقیت تغییر یافت..
        /// </summary>
        internal static string ChangePasswordSuccess {
            get {
                return ResourceManager.GetString("ChangePasswordSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to هنگام تایید درخواست &lt;strong&gt;{0}&lt;/strong&gt; خطایی رخ داده است.
        ///    &lt;br /&gt; &lt;small&gt;{1}&lt;/small&gt;.
        /// </summary>
        internal static string ConfirmBookingRequestError {
            get {
                return ResourceManager.GetString("ConfirmBookingRequestError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تایید درخواست&lt;strong&gt;{0}&lt;/strong&gt; با موفقیت انجام شد..
        /// </summary>
        internal static string ConfirmBookingRequestSuccess {
            get {
                return ResourceManager.GetString("ConfirmBookingRequestSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to هنگام ایجاد &lt;strong&gt;{0}&lt;/strong&gt; خطایی رخ داده است.&lt;br /&gt; &lt;small&gt;{1}&lt;/small&gt;.
        /// </summary>
        internal static string CreateErrorMessage {
            get {
                return ResourceManager.GetString("CreateErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ایجاد &lt;strong&gt;{0}&lt;/strong&gt; با موفقیت انجام شد..
        /// </summary>
        internal static string CreateSuccessMessage {
            get {
                return ResourceManager.GetString("CreateSuccessMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to هنگام حذف اطلاعات درخواستی خطایی رخ داده است &lt;br /&gt; &lt;small&gt;{0}&lt;/small&gt;.
        /// </summary>
        internal static string DeleteErrorMessage {
            get {
                return ResourceManager.GetString("DeleteErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to اطلاعات درخواستی با موفقیت حذف شد..
        /// </summary>
        internal static string DeleteSuccessMessage {
            get {
                return ResourceManager.GetString("DeleteSuccessMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to هنگام ویرایش &lt;strong&gt;{0}&lt;/strong&gt; خطایی رخ داده است.
        ///    &lt;br /&gt; &lt;small&gt;{1}&lt;/small&gt;.
        /// </summary>
        internal static string EditErrorMessage {
            get {
                return ResourceManager.GetString("EditErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ویرایش &lt;strong&gt;{0}&lt;/strong&gt; با موفقیت انجام شد..
        /// </summary>
        internal static string EditSuccessMessage {
            get {
                return ResourceManager.GetString("EditSuccessMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to هنگام انجام درخواست شما  خطایی رخ داده است.
        ///    &lt;br /&gt; &lt;small&gt;{0}&lt;/small&gt;.
        /// </summary>
        internal static string ErrorMessage {
            get {
                return ResourceManager.GetString("ErrorMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to برای روز {0} وعده {1} برنامه غذایی ثبت نشده است.
        /// </summary>
        internal static string FoodProgramForMealInNull {
            get {
                return ResourceManager.GetString("FoodProgramForMealInNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to برای این روز برنامه غذایی ثبت نشده است.
        /// </summary>
        internal static string FoodProgramIsNull {
            get {
                return ResourceManager.GetString("FoodProgramIsNull", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to برنامه غذایی انتخاب شده صحیح نمی باشد..
        /// </summary>
        internal static string FoodProgramIsTrue {
            get {
                return ResourceManager.GetString("FoodProgramIsTrue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to سهمیه غذای انتخابی به پایان رسیده است.
        /// </summary>
        internal static string FoodQuotaRemains {
            get {
                return ResourceManager.GetString("FoodQuotaRemains", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to حضور شخص ثبت شد.
        /// </summary>
        internal static string FoodServing {
            get {
                return ResourceManager.GetString("FoodServing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to عدم حضور شخص ثبت شد.
        /// </summary>
        internal static string FoodServingRemove {
            get {
                return ResourceManager.GetString("FoodServingRemove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to کاربر گرامی {0}: شما با موفقیت وارد حساب کاربری خود شدید..
        /// </summary>
        internal static string LoginSuccessMessage {
            get {
                return ResourceManager.GetString("LoginSuccessMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to کاربر گرامی: نام کاربری یا گذرواژه ورودی شما اشتباه می باشد..
        /// </summary>
        internal static string LoginUserPasswordWrong {
            get {
                return ResourceManager.GetString("LoginUserPasswordWrong", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to سهمیه درخواست غذای اداره شما به اتمام رسیده است.
        /// </summary>
        internal static string OfficeQuotaRemains {
            get {
                return ResourceManager.GetString("OfficeQuotaRemains", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to زمان درخواست غذا برای تاریخ و وعده غذایی انتخاب شده به پایان رسیده است..
        /// </summary>
        internal static string TimelyReview {
            get {
                return ResourceManager.GetString("TimelyReview", resourceCulture);
            }
        }
    }
}
