﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Omidiyeh_Oil_Company___0147
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        //protected void Application_AuthorizeRequest()
        //{
        //    var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
        //    if (authCookie != null)
        //    {
        //        FormsAuthenticationTicket ticket = null;
        //        try
        //        {
        //            ticket = FormsAuthentication.Decrypt(authCookie.Value);
        //        }
        //        catch
        //        {
        //            // ignored
        //        }

        //        if (ticket != null)
        //        {
        //            var roles = new List<string>();
        //            if (!string.IsNullOrEmpty(ticket.UserData))
        //            {
        //                roles.AddRange(ticket.UserData.Split(','));
        //            }
        //            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(ticket.Name), roles.ToArray());
        //            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(ticket.Name), roles.ToArray());
        //        }
        //    }
        //}

        protected void Application_AuthorizeRequest()
        {
            //var req = HttpContext.Current.Request.Url;
            //var a=  HttpContext.Current.Request.IsAuthenticated;
            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket ticket = null;
                try
                {
                    ticket = FormsAuthentication.Decrypt(authCookie.Value);
                }
                catch
                {
                    // ignored
                }

                if (ticket != null)
                {
                    var roles = new List<string>();
                    if (!string.IsNullOrEmpty(ticket.UserData))
                    {
                        roles.AddRange(ticket.UserData.Split(','));
                    }
                    Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(ticket.Name), roles.ToArray());
                    HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(ticket.Name), roles.ToArray());
                }
            }
        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            switch (custom)
            {
                case "byUser":
                    return User.Identity.Name;
                
            }
            return base.GetVaryByCustomString(context, custom);
        }
    }
}
