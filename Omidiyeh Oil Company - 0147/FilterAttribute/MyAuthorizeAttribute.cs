﻿using Omidiyeh_Oil_Company___0147.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Omidiyeh_Oil_Company___0147.FilterAttribute
{
    public class MyAuthorizeAttribute :AuthorizeAttribute
    {
        private readonly dbEntities _db = new dbEntities() {Configuration = {LazyLoadingEnabled = false}};

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            try
            {
                var user = Int64.Parse(filterContext.HttpContext.User.Identity.Name);
                var us = _db.TblUsers.Single(a => a.Id == user);
                if (! _db.TblUserGroupRoles.Any(a => Roles.Contains(a.Role.ToString()) && a.Group == us.Group))
                {
                    filterContext.Result = new RedirectToRouteResult(new
                        RouteValueDictionary(new { controller = "Account", action = "login", returnUrl = filterContext.HttpContext.Request.RawUrl,msg=-1 }));
                }


            }
            catch (Exception e)
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Account", action = "login", returnUrl = filterContext.HttpContext.Request.RawUrl,msg=e.Message }));
                Console.WriteLine(e);
            }
        }
    }

   

}