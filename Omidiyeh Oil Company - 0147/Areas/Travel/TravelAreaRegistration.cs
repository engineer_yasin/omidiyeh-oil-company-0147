﻿using System.Web.Mvc;

namespace Omidiyeh_Oil_Company___0147.Areas.Travel
{
    public class TravelAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Travel";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            context.Routes.MapMvcAttributeRoutes();
            context.MapRoute(
                "Travel_default",
                "Travel/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Omidiyeh_Oil_Company___0147.Areas.Travel.Controllers" }
            );
        }
    }
}