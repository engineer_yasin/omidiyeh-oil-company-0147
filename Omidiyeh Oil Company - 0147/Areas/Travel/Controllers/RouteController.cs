﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;

using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Travel.Controllers
{
    
    public class RouteController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public RouteController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }


        //[OutputCache(Duration = 86400, VaryByCustom = "byUser", Location = System.Web.UI.OutputCacheLocation.Client)]
        public async Task<JsonResult> Items()
        {
            var list = (await _unitOfWork.GetRepository<TblRoute>().Get(includeProperties:"TblCity1,TblCity")).Select(a=> new {a.Id, Title= a.Title()/*, a.Origin, a.Destination, a.LongDistance, a.Type*/});
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "106")]
        // GET: Travel/Route
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "106")]
       [HttpPost]
        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblRoute>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.TblCity.Title.Contains(searchValue) || a.TblCity1.Title.Contains(searchValue),
                    orderBy: order, take: length, skip: start, includeProperties: "TblCity,TblCity1,TblRouteType") :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start, includeProperties: "TblCity,TblCity1,TblRouteType");
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
               Origin= a.TblCity.Title, Destination=a.TblCity1.Title, a.LongDistance, Type= a.TblRouteType.Title,
               DT_RowId = a.Id
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Travel/Route/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblRoute>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }


        // GET: Travel/Route/Create
        [Authorize(Roles = "107")]
        public async Task<ActionResult> Create()
        {
            var (city, _, _) = await _unitOfWork.GetRepository<TblCity>().GetOrdered(orderBy: "TblState.Id,Title", take: 1000);
            ViewBag.Origin = new SelectList(city, "Id", "Title",1);
            ViewBag.Destination = new SelectList(city, "Id", "Title",2);
            ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<TblRouteType>().Get(), "Id", "Title");
            return View();
        }

        // POST: Travel/Route/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "107")]
        public async Task<ActionResult> Create([Bind(Include = "Id,Origin,Destination,Type,LongDistance")] TblRoute entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblRoute>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, "مسیر");
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, "مسیر", message);
                    return RedirectToAction("Index");
                }

                var (city, _, _) = await _unitOfWork.GetRepository<TblCity>().GetOrdered(orderBy: "TblState.Id,Title", take: 1000);
                ViewBag.Origin = new SelectList(city, "Id", "Title", 1);
                ViewBag.Destination = new SelectList(city, "Id", "Title", 2);
                ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<TblRouteType>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Travel/Route/Edit/5
        [Authorize(Roles = "108")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblRoute>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }

            var (city, _, _) = await _unitOfWork.GetRepository<TblCity>().GetOrdered(orderBy: "TblState.Id,Title", take: 1000);
            ViewBag.Origin = new SelectList(city, "Id", "Title", result.Origin);
            ViewBag.Destination = new SelectList(city, "Id", "Title", result.Destination);
            ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<TblRouteType>().Get(), "Id", "Title",result.Type);
            return View(result);
        }

        // POST: Travel/Route/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "108")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Origin,Destination,Type,LongDistance")] TblRoute entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblRoute>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, "مسیر");
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, "مسیر", message);
                    return RedirectToAction("Index");
                }
                var (city, _, _) = await _unitOfWork.GetRepository<TblCity>().GetOrdered(orderBy: "TblState.Id,Title", take: 1000);
                ViewBag.Origin = new SelectList(city, "Id", "Title", entity.Origin);
                ViewBag.Destination = new SelectList(city, "Id", "Title", entity.Destination);
                ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<TblRouteType>().Get(), "Id", "Title", entity.Type);
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Travel/Route/Delete/5
        [Authorize(Roles = "109")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblRoute>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Travel/Route/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "109")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblRoute>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
