﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Travel.Controllers
{
    [Authorize()]
    public class TripController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db;

        public TripController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            _db = new dbEntities();
        }

        // GET: Travel/Trip/JsonList/?office=5
        //[OutputCache(Duration = 600)]
        public async Task<JsonResult> JsonList(DateTime date, int vehicleType)
        {
            Expression<Func<TblTrip, bool>> filter = (a) =>
                (a.Date == date && a.VehicleType == vehicleType);
            var list = await _unitOfWork.GetRepository<TblTrip>().Get(filter
                , orderBy: b => b.OrderByDescending(c => c.Time).ThenBy(c=>c.TblRoute.Origin),
                includeProperties: "TblRoute,TblRoute.TblCity,TblRoute.TblCity1");
            var data = list.Select(a => new
            {
                a.Id,
             Title=   a.Title(),
                OriginId = a.TblRoute.Origin,
                DestinationId = a.TblRoute.Destination,
                Origin = a.TblRoute.TblCity.Title,
                Destination = a.TblRoute.TblCity1.Title,
                Time = a.Time.ToString(@"hh\:mm"),
                Date = new PersianDateTime(a.Date).ToLongDateString(),
                a.VehicleType,
                a.Route,
                a.Items
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        // GET: Travel/Trip
        public ActionResult Index()
        {
            return View();
        }
        //[HttpPost]
        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblTrip>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Title().Contains(searchValue) ,
                    orderBy: order, take: length, skip: start,includeProperties: "TblRoute.TblCity,TblRoute.TblCity1,TblVehicleType") :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start, includeProperties: "TblRoute.TblCity,TblRoute.TblCity1,TblVehicleType");
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                Title =a.Title(),
                Origin= a.TblRoute.TblCity1.Title,
                Destination= a.TblRoute.TblCity.Title,
                Time =a.Time.ToString(),
                DateFa=   new PersianDateTime(a.Date).ToLongDateString(),
                //a.Date,

                VehicleType=   a.TblVehicleType.Title,
                a.Capacity,
                a.Items,
                edit = $"<a href='/Travel/Trip/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> " +
                      $"<a href='/Travel/Trip/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Travel/Trip/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblTrip>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }


        // GET: Travel/Trip/Create
        public ActionResult Create()
        {
            var entity = _unitOfWork.GetRepository<TblFlightDefaultTariff>().GetSingleSync(a => a.Id == 1);
            return View(entity);
        }

        // POST: Travel/Trip/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create( TblTrip entity, DateTime endDate, DateTime startDate, string[] items,TblFlightDefaultTariff tariff)
        {
            try
            {
                entity.Date = startDate;
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var trips = new List<TblTrip>();
                    for (var i = startDate; i <= endDate; i = i.AddDays(1))
                    {

                        var t = new TblTrip()
                        {
                            Capacity = entity.Capacity,
                            Date = i,
                            Route = entity.Route,
                            Time = entity.Time,
                            VehicleType = entity.VehicleType,
                            Items = items == null ? "" : string.Join(";", items)
                        };

                        trips.Add(t);
                    }

                    var (result, message) = await _unitOfWork.GetRepository<TblTrip>().InsertRange(trips, user);
                    if (result)
                    {
                        //اگر هواپیما باشد
                        if (entity.VehicleType == 4)
                        {
                            var fts = new List<TblFlightTripTariff>();

                            foreach (var item in trips)
                            {
                                var ft = new TblFlightTripTariff()
                                {
                                    Trip = item.Id,
                                    CancellationFee24HoursAgo = tariff.CancellationFee24HoursAgo,
                                    CancellationFee3DaysAgo = tariff.CancellationFee3DaysAgo,
                                    CancellationFeeLessThan24HoursAgo = tariff.CancellationFeeLessThan24HoursAgo,
                                    Price = tariff.Price,
                                    Under12 = tariff.Under12,
                                    Under2 = tariff.Under2
                                };
                                fts.Add(ft);
                            }
                            var (resultFlightTripTariff, messageFlightTripTariff) = await _unitOfWork.GetRepository<TblFlightTripTariff>().InsertRange(fts, user);
                            if (resultFlightTripTariff)
                            {
                                TempData["successToast"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title());
                                return RedirectToAction("Index");
                            }
                            else
                            {
                                TempData["errorToast"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title(), message);
                                return RedirectToAction("Index");
                            }
                        }

                    }
                    TempData["successToast"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title());
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["errorToast"] = string.Format(Resources.Public.CreateErrorMessage, ModelState.IsValid.ToString());

                    return RedirectToAction("Create");
                }


            }
            catch (Exception e)
            {
               TempData["errorToast"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return RedirectToAction("Create");
        }

        // GET: Travel/Trip/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblTrip>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.Driver = new SelectList(await _unitOfWork.GetRepository<TblDriver>().Get(), "Id", "Name");
            ViewBag.Route = new SelectList(await _unitOfWork.GetRepository<TblRoute>().Get(), "Id", "Title");
            ViewBag.Vehicle = new SelectList(await _unitOfWork.GetRepository<TblVehicle>().Get(), "Id", "Title");
            ViewBag.VehicleType = new SelectList(await _unitOfWork.GetRepository<TblVehicleType>().Get(), "Id", "Title");
            return View(result);
        }

        // POST: Travel/Trip/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Route,Time,Date,Vehicle,VehicleType,Capacity,Driver")] TblTrip entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblTrip>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title());
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title(), message);
                    return RedirectToAction("Index");
                }
                ViewBag.Driver = new SelectList(await _unitOfWork.GetRepository<TblDriver>().Get(), "Id", "Name");
                ViewBag.Route = new SelectList(await _unitOfWork.GetRepository<TblRoute>().Get(), "Id", "Title");
                ViewBag.Vehicle = new SelectList(await _unitOfWork.GetRepository<TblVehicle>().Get(), "Id", "Title");
                ViewBag.VehicleType = new SelectList(await _unitOfWork.GetRepository<TblVehicleType>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Travel/Trip/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblTrip>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Travel/Trip/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblTrip>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

        #region FlightTariff
        public ActionResult DefaultFlightTariff()
        {
            var entity = _unitOfWork.GetRepository<TblFlightDefaultTariff>().GetSingleSync(a => a.Id == 1);
            return View(entity);
        }
        [HttpPost]
        public async Task<ActionResult> DefaultFlightTariff(TblFlightDefaultTariff entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblFlightDefaultTariff>().Update(entity, user);
                    if (result)
                    {
                        TempData["successToast"] = string.Format(Resources.Public.EditSuccessMessage, "تعرفه سفر هواپیما");
                        return View(entity);
                    }
                    TempData["errorToast"] = string.Format(Resources.Public.EditErrorMessage, "تعرفه سفر هواپیما", message);
                    return View(entity);
                }
                
            }
            catch (Exception e)
            {
               TempData["errorToast"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }
        #endregion

    }
}
