﻿using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Omidiyeh_Oil_Company___0147.Areas.Travel.Controllers
{
    [Authorize()]
    public class BookingRequestsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public BookingRequestsController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        // GET: Travel/BookingRequests
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblBookingRequest>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a =>  a.Des.Contains(searchValue),
                    orderBy: order, take: length, skip: start) :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start);
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                a.Person,
                // a.User,

                a.OfficeDepartment,
                a.Trip,
                a.FellowTraveler,
                //a.RequestNumber,
                //a.BookingCode,
                a.Mobile,
                a.FoodDinner,
                a.FoodLunch,
                a.Residence,
                //a.RequestTime,
                a.Des,
                a.Status,
                a.TblOfficeDepartment,
                a.TblPerson,
                // a.TblUser,
                // a.TblBookingCode,
                a.TblBookingRequestStatu,
             //   a.Trip,
                a.TblBookingRequestStatusHistories,
                edit = $"<span id='edit-booking-{a.Id}' class='text-success ml-2 editBooking' ><i class='fas fa-edit fa-2x'></i></span> " +
                      $"<a href='/Travel/BookingRequests/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult List2(SearchAdvancedBookingRequest model)
        {
            var user = Convert.ToInt32(User.Identity.Name);
            Expression<Func<TblBookingRequest, bool>> filter = (a) =>
            (model.AccountNumber.Contains(0) || model.AccountNumber.Contains(a.AccountNumber))// &&
            //(model.BookingCode.Contains(0) || model.BookingCode.Contains(a.TblBookingRequestGroup.BookingCode)) &&
            ////(model.VehicleType == 0 || model.VehicleType == a.VehicleType) &&
            ////(model.Trip == 0 || model.Trip == a.Trip) &&
            //(model.StartDate.Year == 1 || a.Date >= model.StartDate && a.Date <= model.EndDate)

            ;
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            // var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";

            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = @"TblPerson,
                                        TblCity1,
                                        TblCity,
                                        TblVehicleType,
                                        TblOfficeDepartmentAccountNumber,TblBookingRequestGroup,TblBookingRequestGroup.TblUser.TblPerson"
                ;
            var iResult = _unitOfWork.GetRepository<TblBookingRequest>();

            var (items, count, total) = iResult.GetOrderedSync(/*filter: filter,*/
                    orderBy: order, take: length, skip: start, includeProperties: includeProperties);


            // ReSharper disable once PossibleMultipleEnumeration
            var data = items.Select(a => new
            {
                id = " ",
                a.TblPerson.Name,
                a.TblPerson.Family,
                a.Mobile,
                a.TblPerson.NationalCode,
                a.TblPerson.PersonnelCode,
                Date = new PersianDateTime(a.Date).ToString("yyyy/MM/dd"),
                Time= a.Time.ToString(@"hh\:mm"),
                Origin = a.TblCity.Title,
                Destination = a.TblCity1.Title,
                Vehicle = a.TblVehicleType.Title,
                a.TblOfficeDepartmentAccountNumber.AccountNumber,
                a.ExtraChair,
                a.FoodBreakfast,
                a.FoodDinner,
                a.FoodLunch,
                a.Group,
                a.Residence,
                RequestDate = new PersianDateTime(a.TblBookingRequestGroup.Date).ToString("yyyy/MM/dd hh:mm"),
                
                a.TblBookingRequestGroup.Des,
                a.TblBookingRequestGroup.BookingCode,
               User= a.TblBookingRequestGroup.TblUser.TblPerson.Fullname(),
                a.TblBookingRequestGroup.Text1,
                a.TblBookingRequestGroup.Text2,
                a.TblBookingRequestGroup.Text3,
                a.TblBookingRequestGroup.Text4,
                a.TblBookingRequestGroup.Text5,
                Day = new PersianDateTime(a.Date).ToString("dddd"),
                DT_RowId = a.Id,


            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            //return Json(res, JsonRequestBehavior.AllowGet);

            var jsonResult = Json(res, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
      
        public ActionResult AssigningBuses()
        {
            return View();
        }

    }
}
