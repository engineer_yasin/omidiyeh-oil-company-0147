﻿using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using Omidiyeh_Oil_Company___0147.Implementations;
using MD.PersianDateTime;
using System.Web.Script.Serialization;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;

namespace Omidiyeh_Oil_Company___0147.Areas.Travel.Controllers
{
    public class ReportController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db;

        public ReportController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            _db = new dbEntities();
            var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/Reports/license.key");
            Stimulsoft.Base.StiLicense.LoadFromFile(path);
        }

        #region Report9
        public ActionResult Report1()
        {
            //ViewBag.Office = new SelectList((await _unitOfWork.GetRepository<TblOffice>().Get()).OrderBy(a => a.Title), "Id", "Title");
            return View();
        }

        // [HttpPost]
        public ActionResult Report1Items(int id)
        {
            var list = _db.TblBookingRequests
                .Where(a => a.TblBookingRequestGroup.Id == id)
                .AsNoTracking()
                 .Include(a => a.TblBookingRequestGroup)
                 .Include(a => a.TblCity)
                 .Include(a => a.TblCity1)
                 .Include(a => a.TblPerson)
                 .Include(a => a.TblOfficeDepartmentAccountNumber)
                 .Include(a => a.TblVehicleType)

                 .ToList();
            if (!list.Any())
            {
                TempData["errorToast"] = "درخواستی با این کد وجود ندارد.";
                //return null;
                return PartialView("_Message");
            }
            var data2 = list.Select(a => new
            {
                a.TblPerson.PersonnelCode,
                a.TblPerson.NationalCode,
                Fullname = a.TblPerson.Fullname() +( a.FellowTraveler ? " (همراه)" : ""),
                BirthDate = new PersianDateTime(a.TblPerson.BirthDate).ToString("yyyy/MM/dd"),
                a.TblOfficeDepartmentAccountNumber.AccountNumber,
                a.TblBookingRequestGroup.BookingCode,
                a.TblVehicleType.Title,
                Date = new PersianDateTime(a.Date).ToString("yyyy/MM/dd"),
                Time = a.Time.ToString(@"hh\:mm"),

                Origin = a.TblCity.Title,
                Destination = a.TblCity1.Title,
                a.Residence,
                a.FoodBreakfast,
                a.FoodDinner,
                a.FoodLunch,
                a.ExtraChair,
                a.Mobile,
                RequestDate = new PersianDateTime(a.TblBookingRequestGroup.Date).ToString("yyyy/MM/dd"),
                a.Des,
                a.TblBookingRequestGroup.Text1,
                a.TblBookingRequestGroup.Text2,
                a.TblBookingRequestGroup.Text3,
                a.TblBookingRequestGroup.Text4,
                a.TblBookingRequestGroup.Text5,
                a.TblBookingRequestGroup.Id,


            });


            var header = new
            {
                Title = $"درخواست بوکینگ شماره {id} ",

            };


            var res = new
            {
                data2,
                header
            };
            TempData["res"] = new JavaScriptSerializer().Serialize(res);

            //return Json(res, JsonRequestBehavior.AllowGet);
            return PartialView();
        }

        public ActionResult GetReport1()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/TReport1.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }


        #endregion

        public ActionResult ViewerEvent()
        {
            StiOptions.Viewer.RightToLeft = StiRightToLeftType.Yes;
            return StiMvcViewer.ViewerEventResult();
        }

        public ActionResult DesignReport()

        {
            StiReport report = StiMvcViewer.GetReportObject();
            ViewBag.ReportName = report.ReportName;
            return View("Designer");

        }
    }
}