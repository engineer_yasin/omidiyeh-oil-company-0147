﻿using System.Web.Mvc;

namespace Omidiyeh_Oil_Company___0147.Areas.Reservation
{
    public class ReservationAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Reservation";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            context.Routes.MapMvcAttributeRoutes();
            context.MapRoute(
                "Reservation_default",
                "Reservation/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Omidiyeh_Oil_Company___0147.Areas.Reservation.Controllers" }
            );
        }
    }
}