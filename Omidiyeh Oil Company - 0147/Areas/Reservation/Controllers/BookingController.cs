﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using Stimulsoft.Report.Dictionary;

namespace Omidiyeh_Oil_Company___0147.Areas.Reservation.Controllers
{
    [Authorize()]
    public class BookingController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db;

        public BookingController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            _db = new dbEntities();
        }

        #region Limitations
        /// <summary>
        /// بررسی محدودیت زمانی ثبت بوکینگ
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool TimeReview(TblBookingRequest entity)
        {
            var result = true;
            var code = _db.TblBookingCodes.AsNoTracking().Single(a => a.Id == entity.TblBookingRequestGroup.BookingCode);

            var tempDate = entity.Date.AddDays(code.RequestEndDay * -1);
            tempDate = tempDate.Add(code.RequestEndTime);
            if (tempDate < DateTime.Now)
                result = false;

            return result;
        }

        public bool RouteReview(TblBookingRequest entity)
        {
            var result = !_db.TblBookingRequests.Any(a =>
                a.Person == entity.Person && a.Route == entity.Route && a.Date == entity.Date);
            return result;
        }

        #endregion
        // GET: Reservation/Trip
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> List(SearchAdvancedBookingRequest2 model)
        {
            try
            {
                var from = model.DateFrom();
                var to = model.DateTo();
                var rFrom = model.RequestDateFrom();
                var rTo = model.RequestDateTo();
                Expression<Func<TblBookingRequest, bool>> filter = (a) =>
                    (string.IsNullOrEmpty(model.SearchPersonalCode) || a.TblPerson.PersonnelCode == model.SearchPersonalCode) &&
                    (string.IsNullOrEmpty(model.SearchNationalCode) || a.TblPerson.NationalCode == model.SearchNationalCode) &&
                    (string.IsNullOrEmpty(model.SearchName) || a.TblPerson.Name.Contains(model.SearchName)) &&
                    (string.IsNullOrEmpty(model.SearchFamily) || a.TblPerson.Family.Contains(model.SearchFamily)) &&
                    (string.IsNullOrEmpty(model.SearchMobile) || a.Mobile.Contains(model.SearchMobile)) &&
                    (string.IsNullOrEmpty(model.SearchDes) || a.Des.Contains(model.SearchDes)) &&
                    (model.SearchVehicleType.Contains(0) || model.SearchVehicleType.Contains(a.VehicleType)) &&
                    (model.SearchOrigin.Contains(0) || model.SearchOrigin.Contains(a.Origin)) &&
                    (model.SearchDestination.Contains(0) || model.SearchDestination.Contains(a.Destination)) &&
                    (model.SearchStatus.Contains(0) || model.SearchStatus.Contains(a.Status)) &&
                    (model.SearchManagement.Contains(0) || model.SearchManagement.Contains(a.TblOfficeDepartment.TblOffice.Management)) &&
                    (model.SearchOffice.Contains(0) || model.SearchManagement.Contains(a.TblOfficeDepartment.Office)) &&
                    (model.SearchOfficeDepartment.Contains(0) || model.SearchOfficeDepartment.Contains(a.OfficeDepartment)) //&&
                    //(model.SearchRequestNumber == null || model.SearchRequestNumber== a.RequestNumber)&&
                    //(string.IsNullOrEmpty(model.SearchRequestDate) || (a.RequestTime >= rFrom && a.RequestTime <=rTo)) //&&
                    //(model.SearchTimeGo == null || a.Time == model.SearchTimeGo) &&
                    //(a.Date >= from && a.Date <= to &&  model.SearchBookingCode.Contains(a.BookingCode))
                    ;



                //ToDo: نمایش فقط مربوط به اداره همان کاربر
                //Server Side Parameter
                var start = Convert.ToInt32(Request["start"]);
                var length = Convert.ToInt32(Request["length"]);
                var searchValue = Request["search[value]"];
                var draw = Request["draw"];
                var order = "";
                foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
                {
                    if (item.EndsWith("[column]"))
                        order += Request["columns[" + Request[item] + "][name]"] + " ";
                    else if (item.EndsWith("[dir]"))
                        order += Request[item] + ",";
                }

                length = length == 0 ? 10 : length;
                order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
                var iResult = _unitOfWork.GetRepository<TblBookingRequest>();
                var result = iResult.GetOrdered(filter: filter,
                        orderBy: order, take: length, skip: start,
                        includeProperties:
                        "TblPerson,TblTrip,TblPerson.TblFamilyRelationship,TblPerson.TblOfficeDepartment.TblOffice,TblPerson.TblOfficeDepartment,TblVehicleType,TblCity1,TblCity,TblPerson.TblPersonGroup");
                var (items, count, total) = await result;

                var data = items.Select(a => new
                {
                    id = " ",
                    DT_RowId = a.Id,
                    Time = a.Time.ToString(),
                    Date = new PersianDateTime(a.Date).ToLongDateString(),
                    //RequestTime = new PersianDateTime(a.RequestTime).ToLongDateString(),
                    Trip = a.Trip,
                    a.TblPerson.PersonnelCode,
                    Fullname = a.TblPerson.Fullname(),
                    a.TblPerson.NationalCode,
                    a.TblPerson.Tel,
                    a.Mobile,
                    FamilyRelationship = a.TblPerson.TblFamilyRelationship.Title,
                    Office = a.TblPerson.TblOfficeDepartment.TblOffice.Title,
                    OfficeDepartment = a.TblPerson.TblOfficeDepartment.Title,
                    a.Residence,
                    a.FoodLunch,
                    a.FoodDinner,
                    a.SpecialServices,
                    a.ExtraChair,
                    a.FellowTraveler,
                    VehicleType = a.TblVehicleType.Title,
                    //a.RequestNumber,
                    Origin = a.TblCity.Title,
                    Destination = a.TblCity1.Title,
                    Group = a.TblPerson.TblPersonGroup.Title,
                    a.Des,
                    //edit = $"<a id='edit-booking-{a.Id}' class='text-success ml-2 editBooking' ><i class='fas fa-edit fa-2x'></i></a> " +

                    //      $"<a href='/Travel/BookingRequests/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
                }).ToList();
                var res = new
                {
                    draw,
                    recordsTotal = total.ToString(),
                    recordsFiltered = count.ToString(),
                    data
                };
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Reservation/Booking/AdvancedSearch
        public async Task<PartialViewResult> AdvancedSearch()
        {
            var (vehicleType, _, _) = await _unitOfWork.GetRepository<TblVehicleType>().GetOrdered(orderBy: "Title", take: 1000);
            var (city, _, _) = await _unitOfWork.GetRepository<TblCity>().GetOrdered(orderBy: "Title", take: 1000);
            var (status, _, _) = await _unitOfWork.GetRepository<TblBookingRequestStatu>().GetOrdered(orderBy: "Title", take: 1000);
            var (management, _, _) = await _unitOfWork.GetRepository<TblManagement>().GetOrdered(orderBy: "Title", take: 1000);
            var (office, _, _) = await _unitOfWork.GetRepository<TblOffice>().GetOrdered(orderBy: "Title", take: 1000);
            var (officeDepartment, _, _) = await _unitOfWork.GetRepository<TblOfficeDepartment>().GetOrdered(orderBy: "Title", take: 1000);

            ViewBag.searchVehicleType = new SelectList(vehicleType, "Id", "Title");
            ViewBag.searchOrigin = new SelectList(city, "Id", "Title");
            ViewBag.searchDestination = new SelectList(city, "Id", "Title");
            ViewBag.searchStatus = new SelectList(status, "Id", "Title");
            ViewBag.searchManagement = new SelectList(management, "Id", "Title");
            ViewBag.searchOffice = new SelectList(office, "Id", "Title");
            ViewBag.searchOfficeDepartment = new SelectList(officeDepartment, "Id", "Title");
            // ViewBag.searchFood = new SelectList(vehicleType, "Id", "Title");
            return PartialView();
        }

        public ActionResult Mission()
        {
            return View();
        }
        //[OutputCache(Duration = 6)]
        public async Task<ActionResult> Mission3()
        {
            var date = DateTime.Now.AddDays(1);
           // var (route, _, _) = await _unitOfWork.GetRepository<TblRoute>().GetOrdered(orderBy: "Title", take: 1000);
           // var (vehicleType, _, _) = await _unitOfWork.GetRepository<TblVehicleType>().GetOrdered(orderBy: "Title", take: 1000);
           // var (claim, _, _) = await _unitOfWork.GetRepository<TblClaim>().GetOrdered(orderBy: "Title", take: 1000);
            // var (accountNumber, _, _) = await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>().GetOrdered(orderBy: "Title", take: 1000);

           // var r = route.Select(a=> new ite)
          //  ViewBag.Route = new SelectList(route, "Id", "Title");
           // ViewBag.VehicleType = new SelectList(vehicleType, "Id", "Title");
           // ViewBag.Claim = new SelectList(claim, "Id", "Title");
            ViewBag.Date = date;
            //ViewBag.AccountNumber = new SelectList(accountNumber, "Id", "Title"); 

            return View();
        }

        public async Task<ActionResult> Mission2()
        {
            return View();
        }

        
        public ActionResult Sport()
        {
          
            return View();
        }

        public ActionResult Hospital()
        {
            return View();
        }

        public ActionResult Normal()
        {
          
            return View();
        }

        [HttpPost]
        public JsonResult AddBooking(List<BookingRequest> items, string des, byte bookingCode,
            string text1,string text2,string text3,string text4,string text5)
        {
            var user = Convert.ToInt32(User.Identity.Name);
           // var list = new List<Message>();
           var m = new Message();
           // var requestNumber = 100;
            var group = new TblBookingRequestGroup
            {
                BookingCode = bookingCode, Date = DateTime.Now, Des = des, Text1 = text1, Text2 = text2, Text3 = text3,
                Text4 = text4, Text5 = text5, User = user
            };
            _db.TblBookingRequestGroups.Add(group);
            _db.SaveChanges();
            var bookings = new List<TblBookingRequest>();
            foreach (var item in items)
            {
                

                var person = _db.TblPersons.AsNoTracking().Single(a => a.Id == item.Person);
                var accountNumber = _db.TblOfficeDepartmentAccountNumbers
                    .AsNoTracking()
                    .Include(a => a.TblOfficeDepartment)
                    .Single(a => a.Id == item.AccountNumber);
              
               // var route = await _unitOfWork.GetRepository<TblRoute>().GetSingle(a => a.Id == item.Route);
                if (person != null)
                {
                    var booking = new TblBookingRequest
                    {
                        Group = group.Id,
                        Date = item.Date,
                        Des = item.Des,
                        Destination = item.Destination,
                        FellowTraveler = item.FellowTraveler,
                        FoodDinner = item.FoodDinner,
                        FoodLunch = item.FoodLunch,
                        Mobile = item.Mobile,
                        OfficeDepartment = accountNumber.OfficeDepartment,
                        Origin = item.Origin,
                        Person = person.Id,
                        Relationship = person.FamilyRelationship,
                        Residence = item.Residence,
                        Status = 1,
                        Tel = person.Tel,
                        Time = item.Time,
                        Trip = item.Trip,
                        VehicleType = item.VehicleType,
                        Returned = item.IsReturn,
                        ExtraChair = item.SpecialServices,
                        SpecialServices = item.ExtraChair,
                        Route = item.Route,
                        WentId = null,
                        AccountNumber = item.AccountNumber,
                        Cancel = false,
                        Payment = false,
                        FoodBreakfast = item.FoodBreakfast,
                        TblBookingRequestGroup = group,
                        
                    };

                    var timeResult = TimeReview(booking);
                   if (timeResult)
                   {
                       var routeResult = RouteReview(booking);
                       if (routeResult)
                       {
                           bookings.Add(booking);
                       }
                       else
                       {
                           var r = _db.TblRoutes.Include(a=>a.TblCity).Include(a=>a.TblCity1).Single(a => a.Id == booking.Route);
                           m.Msg = "برای شخص " + person.Fullname() + " در تاریخ " +
                                   new PersianDateTime(booking.Date).ToLongDateString()
                                   + " مسیر " + r.TblCity.Title + " - " + r.TblCity1.Title + " درخواست دیگری ثبت شده است.";
                           m.Result = false;
                           m.Type = "error";
                           goto result;
                       }
                   }
                   else
                   {

                     
                       m.Msg = "زمان ثبت درخواست برای تاریخ " +
                               new PersianDateTime(booking.Date).ToLongDateString()
                               + " گذشته است.";
                       m.Result = false;
                       m.Type = "error";
                       goto result;
                    }

                   bookings.Add(booking);
                 


                }
                else
                {
                    m.Result = false;
                    m.Msg = "شخصی با این کد ایجاد نشده است";
                    m.Type = "warning";
                }

                
            }

            var (res,msg) = _unitOfWork.GetRepository<TblBookingRequest>().InsertRangeSync(bookings, user);
            if (res)
            {
                m.Result = true;
                m.Msg = string.Format(Resources.Public.AddSuccessMessage, "درخواست با شناسه " + group.Id);
                m.Type = "success";
            }
            else
            {
                m.Result = false;
                m.Msg = string.Format(Resources.Public.AddErrorMessage, "درخواست بوکینگ", msg);
                m.Type = "error";
            }
            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }



    }
}