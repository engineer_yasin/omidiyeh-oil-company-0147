﻿using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.FilterAttribute;

namespace Omidiyeh_Oil_Company___0147.Areas.Reservation.Controllers
{
    [Authorize()]
    public class FoodController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<TblFoodReservation> _e;
        private readonly dbEntities _db;

        public FoodController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            _db = new dbEntities();
            _e = _unitOfWork.GetRepository<TblFoodReservation>();
        }

        #region Helpers

        /// <summary>
        /// بررسی وجود رزرو غذا از قبل برای تاریخ و وعده انتخابی
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>در صورتی که قبلا رزرو شده باشد، مقدار false برگشت می دهد</returns>
        private async Task<(bool result, int id)> AlreadyBooked(TblFoodReservation entity)
        {
            var group = 0;
            var result = await _e.Any(a =>
                a.Person == entity.Person && a.Meal == entity.Meal && a.Date == entity.Date &&
                a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false && a.Food != -1);
            if (result)
            {
                group = (await _e.GetFirst(a =>
                   a.Person == entity.Person && a.Meal == entity.Meal && a.Date == entity.Date &&
                   a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false && a.Food != -1)).Group;
            }
            return (result, group);
        }

        /// <summary>
        /// بررسی تسویه حساب پیمان و رستوران
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>در صورتی که قبلا رزرو شده باشد، مقدار false برگشت می دهد</returns>
        private bool  AlreadyCheckout(TblFoodReservation entity)
        {
            var result = _db.TblContractRestaurantCheckouts.Any(
                a => a.Restaurant == entity.Restaurant && a.FromDate <= entity.Date && a.ToDate >= entity.Date && a.Contract == entity.Contract);
            
            return result;
        }

        /// <summary>
        /// بررسی وجود رزرو غذا از قبل برای تاریخ و وعده انتخابی
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>در صورتی که قبلا رزرو شده باشد، مقدار false برگشت می دهد</returns>
        private async Task<bool> AlreadyBookedVehicle(TblFoodReservation entity)
        {
            var result = await _e.Any(a => a.Vehicle == entity.Vehicle && a.Meal == entity.Meal && a.Date == entity.Date && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false);
            return !result;
        }

        /// <summary>
        /// بررسی سهمیه غذایی اداره
        /// </summary>
        /// <param name="office">اداره</param>
        /// <param name="date">تاریخ رزرو</param>
        /// <param name="count">تعداد در درخواست</param>
        /// <returns></returns>
        public async Task<bool> OfficeQuotaRemains(int office, DateTime date, int count)
        {
            var quota = await _unitOfWork.GetRepository<TblOfficeFoodQuota>().GetFirst(a => a.Office == office/* && a.StartDate <= date && a.EndDate >= date*/);
            var result = quota?.RemainingQuota > count;
            return result;
        }
        /// <summary>
        /// بررسی اینکه ایا غذای انتخاب شده دارای قرارداد فعال برای این رستوران است یا خیر
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> ActiveContract(TblFoodReservation entity)
        {
            if (entity.Meal == 0)
                entity.Food = 0;
            var result = await _unitOfWork.GetRepository<TblContractFood>().Any(
                a => a.TblContract.TblContractRestaurants.Any(b => b.Restaurant == entity.Restaurant)
                     && a.TblContract.StartDate <= entity.Date && a.TblContract.EndDate >= entity.Date
                     && a.TblContract.Status == 1 && a.Food == entity.Food);
            return result;
        }

        /// <summary>
        /// بررسی داشتن ظرفیت غذا
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> FoodQuotaRemains(TblFoodReservation entity)
        {
            var food = await _unitOfWork.GetRepository<TblContractFood>().GetSingle(
                a => a.TblContract.TblContractRestaurants.Any(b => b.Restaurant == entity.Restaurant)
                     && a.TblContract.StartDate <= entity.Date && a.TblContract.EndDate >= entity.Date
                     && a.TblContract.Status == 1 && a.Food == entity.Food);
            var result = food.RemainingQuota > 1;
            return result;
        }


        /// <summary>
        /// بررسی اینکه برنامه غذایی انتخاب شده صحیح می باشد یا خیر
        /// </summary>
        /// <param name="entity"></param>

        /// <returns></returns>
        public async Task<bool> FoodProgramIsTrue(TblFoodReservation entity)
        {
            var result = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().Any(
                a => a.Food == entity.Food && a.Date == entity.Date && a.Restaurant == entity.Restaurant && a.Meal == entity.Meal);

            return result;
        }


        /// <summary>
        /// بررسی زمانی درخواست غذا
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> TimelyReview(TblFoodReservation entity)
        {

            var result = true;
            var meal = await _unitOfWork.GetRepository<TblMeal>().GetSingle(a => a.Id == entity.Meal);
            var pFood = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().GetFirst(
                a => a.Food == entity.Food && a.Date == entity.Date && a.Restaurant == entity.Restaurant && a.Meal == entity.Meal);
            //بررسی زمانی درخواست غذا
            //اعمال محدودیت های زمانی
            var tempDate = entity.Date.AddDays(meal.ReservationEndDay * -1);
            tempDate = tempDate.Add(meal.ReservationEndTime);
            if (tempDate < DateTime.Now && !pFood.Default)
                result = false;

            //بررسی زمان برای انتخاب غذای پیشفرض
            tempDate = entity.Date.AddDays(meal.ReservationDefaultFoodDay * -1);
            tempDate = tempDate.Add(meal.ReservationDefaultFoodTime);
          
            if (tempDate < DateTime.Now && pFood.Default)
                result = false;


            return result;
        }

        /// <summary>
        /// بررسی تمامی محدودیت ها
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="office"></param>
        /// <param name="count"></param>
        /// <param name="type"></param>
        /// <param name="edit"></param>
        /// <returns></returns>
        private async Task<(bool resutl, string message)> ReviewRestrictions(TblFoodReservation entity, int office, int count, int type = 0,bool edit = false)
        {
            if (edit == false)
            {


                if (entity.Person != -2)
                {
                    var (alreadyBooked, group) = await AlreadyBooked(entity);
                    if (alreadyBooked)
                        return (false, string.Format(Resources.Public.AlreadyBooked2, group));
                }

                if (entity.Vehicle != null)
                {
                    var alreadyBookedVehicle = await AlreadyBookedVehicle(entity);
                    if (!alreadyBookedVehicle)
                        return (false, Resources.Public.AlreadyBookedVehicle);
                }
            }

            if (entity.Meal != 0)
            {
                var foodProgramIsTrue = await FoodProgramIsTrue(entity);
                if (!foodProgramIsTrue)
                    return (false, Resources.Public.FoodProgramIsTrue);
            }

            //var user = await _unitOfWork.GetRepository<TblUser>()
            //  .GetFirst(a => a.Id == entity.TblFoodReservationGroup.User);
            var userId = Convert.ToInt32(User.Identity.Name);
            var user = _db.TblUsers.First(a => a.Id == userId);
            //if (user.Group != 1)
            //{
                var officeQuotaRemains = await OfficeQuotaRemains(office, entity.Date, count);
                if (!officeQuotaRemains)
                    return (false, Resources.Public.OfficeQuotaRemains);
            //}

            var activeContract = await ActiveContract(entity);
            if (!activeContract)
                return (false, Resources.Public.ActiveContract);

            //if (user.Group != 1)
            //{
                var foodQuotaRemains = await FoodQuotaRemains(entity);
                if (!foodQuotaRemains)
                    return (false, Resources.Public.FoodQuotaRemains);
            //}


            if (type != 3 && type != 5 && (user.Group != 1 && user.Group != 21 && user.Group != 12&& user.Group != 10 && user.Group != 14))
            {
                var timelyReview = await TimelyReview(entity);
                if (!timelyReview)
                    return (false, Resources.Public.TimelyReview);
            }

            var alreadyCheckout =  AlreadyCheckout(entity);
            if (alreadyCheckout)
                return (false, string.Format("برای این پیمان و رستوران در تاریخ انتخاب شده تسویه حساب صورت گرفته است."));

            return (true, "");
        }

        #endregion

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home", new { area = "" });
        }

        // GET: Reservation/Food
        public ActionResult Manage()
        {
            return View();
        }


        public ActionResult Bed(int bed)
        {
            return PartialView();
        }
        /// <summary>
        /// ابتدا انتخاب تاریخ توسط کاربر
        /// سپس انتخاب وعده غذایی
        /// با توجه به انتخاب وعده غذایی، و رستوران دسترسی دار کاربر و اداره، لیست غذاها نمایش داده می شود
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "160")]
        public async Task<ActionResult> Add()
        {
            ViewBag.Meal = await _unitOfWork.GetRepository<TblMeal>().Get(a => a.Status);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "160")]
        public async Task<ActionResult> Add(DateTime startDate, DateTime endDate, bool? addAnother)
        {
            var m = new Message();
            try
            {
                if (ModelState.IsValid)
                {

                    var list = new List<TblFoodReservation>();
                    var listStatus = new List<TblFoodReservationStatusHistory>();
                    var userId = Convert.ToInt32(User.Identity.Name);
                    var user = await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == userId,
                        includeProperties: "TblPerson,TblPerson.TblOfficeDepartment");
                    var person = user.Person;
                    var accountNumber = await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>()
                        .GetFirst(a => a.OfficeDepartment == user.TblPerson.OfficeDepartment);
                    foreach (var item in Request.Form.AllKeys.Where(a => a.Contains("Restaurant")))
                    {
                        var meal = item.Split('-')[1];
                        var t = Request.Form["sMeal" + meal];
                        if (Request.Form["sMeal" + meal] == "true" &&
                            !string.IsNullOrEmpty(Request.Form[item]) &&
                            !string.IsNullOrEmpty(Request.Form["Food-" + meal]))
                        {
                            for (var i = startDate; i <= endDate; i = i.AddDays(1))
                            {
                                var entity = new TblFoodReservation
                                {
                                    Date = i,
                                    Food = Convert.ToInt32(Request.Form["Food-" + meal]),
                                    Meal = Convert.ToInt32(meal),
                                    Restaurant = Convert.ToInt32(Request.Form[item]),
                                    Person = person,

                                };
                                if (meal == "0")
                                {
                                    var contract = await _unitOfWork.GetRepository<TblContract>().GetFirst(a =>
                                        a.TblContractRestaurants.Any(b => b.Restaurant == entity.Restaurant)
                                        && a.StartDate <= i && a.EndDate >= i.Date
                                        && a.Status == 1);

                                    entity.Contract = contract.Id;
                                    list.Add(entity);
                                }
                                else
                                {
                                    var foodProgram = await FoodProgramIsTrue(entity);
                                    //اگر برنامه غذایی صحیح نبود، برای آن روز غذای پیش فرض انتخاب شود
                                    if (!foodProgram)
                                    {
                                        var food = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().GetFirst(
                                            a => a.Date == entity.Date && a.Restaurant == entity.Restaurant &&
                                                 a.Meal == entity.Meal);
                                        if (food != null)
                                            entity.Food = food.Food;
                                        else
                                        {
                                            var msg = string.Format(Resources.Public.AddErrorMessage,
                                                "درخواست غذا " +
                                                new PersianDateTime(entity.Date)
                                                    .ToLongDateString() +
                                                " وعده " + MealTitle(entity.Meal),
                                                Resources.Public.FoodProgramIsNull);
                                            m.Msg = msg;
                                            m.Result = false;
                                            m.Type = "error";
                                            return Json(m, JsonRequestBehavior.AllowGet);
                                        }

                                    }

                                    //بررسی شرط های رزرو غذا
                                    var (res, ms) = await ReviewRestrictions(entity,
                                        user.TblPerson.TblOfficeDepartment.Office, 1);
                                    if (!res)
                                    {
                                        var msg = string.Format(Resources.Public.AddErrorMessage,
                                            "درخواست غذا " +
                                            new PersianDateTime(entity.Date)
                                                .ToLongDateString() +
                                            " وعده " + MealTitle(entity.Meal),
                                            ms);
                                        m.Msg = msg;
                                        m.Result = false;
                                        m.Type = "error";
                                        return Json(m, JsonRequestBehavior.AllowGet);

                                    }


                                    var  (contractResult,contractFood, contractMessage) = await CalculateQuota(entity,
                                        user.TblPerson.TblOfficeDepartment.Office);
                                    if (contractResult)
                                    {
                                        //مقدار دهی فیلد های مورد نیاز
                                        entity.Contract = contractFood.Contract;
                                        list.Add(entity);
                                    }
                                    else {
                                        m.Msg = contractMessage;
                                        m.Result = false;
                                        m.Type = "error";
                                        return Json(m, JsonRequestBehavior.AllowGet);
                                    }

                                }
                            }
                        }
                    }

                    if (list.Count > 0)
                    {
                        var foodGroup = new TblFoodReservationGroup
                        {
                            User = userId,
                            AccountNumber = accountNumber.Id,
                            Date = DateTime.Now
                        };
                        var (resultGroup, messageGroup) = await _unitOfWork.GetRepository<TblFoodReservationGroup>().Insert(foodGroup, userId);
                        if (resultGroup)
                        {
                            foreach (var item in list)
                            {
                                item.Group = foodGroup.Id;
                            }
                            var (result, message) = await _e.InsertRange(list, userId);
                            if (result)
                            {
                                foreach (var item in list)
                                {
                                    //ثبت وضعیت درخواست غذا
                                    var statusHistory = new TblFoodReservationStatusHistory
                                    {
                                        Reservation = item.Id,
                                        Id = Guid.NewGuid(),
                                        Time = DateTime.Now,
                                        Status = 1,
                                        User = userId
                                    };
                                    listStatus.Add(statusHistory);
                                }
                                await _unitOfWork.GetRepository<TblFoodReservationStatusHistory>().InsertRange(listStatus, userId);
                                await _unitOfWork.SaveAsync();

                                m.Msg = string.Format(Resources.Public.AddSuccessMessage, "درخواست رزرو غذا با شناسه " + foodGroup.Id);

                            }
                            else
                            {
                                m.Msg = string.Format(Resources.Public.AddErrorMessage, "درخواست رزرو غذا", message);
                            }
                            m.Result = result;
                            m.Type = result ? "success" : "error";
                        }
                        else
                        {
                            m.Msg = string.Format(Resources.Public.AddErrorMessage, "درخواست رزرو غذا", messageGroup);
                            m.Result = false;
                            m.Type = "error";
                        }
                    }
                    else
                    {
                        m.Result = false;
                        m.Type = "warning";
                        m.Msg = Resources.Public.AddWarning;
                    }

                }
            }
            catch (Exception e)
            {
                Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        //کسر کردن سهمیه اداره ، پیمان و  غذا پیمان
        private async Task<(bool result, TblContractFood contract,string message)> CalculateQuota(TblFoodReservation entity, int office)
        {
            var result = true;
            var message = "";
            //کسر کردن سهمیه اداره ، پیمان و  غذا پیمان
            var contractFood = await _unitOfWork.GetRepository<TblContractFood>().GetSingle(a =>
                a.TblContract.TblContractRestaurants.Any(b => b.Restaurant == entity.Restaurant)
                && a.TblContract.StartDate <= entity.Date && a.TblContract.EndDate >= entity.Date
                && a.TblContract.Status == 1 && a.Food == entity.Food);

            if (contractFood == null)
            {
                result = false;
                message = Resources.Public.ActiveContract;
                goto res;
            }
            var officeFoodQuota = await _unitOfWork.GetRepository<TblOfficeFoodQuota>()
                .GetFirst(a => a.Office == office && DateTime.Now >= a.StartDate &&
                               DateTime.Now <= a.EndDate);
            if(officeFoodQuota == null)
            {
                result = false;
                message = "شماره حساب (اداره) انتخابی دارای سهمیه غذایی فعال نمی باشد.";
                goto res;
            }
            var contract = await _unitOfWork.GetRepository<TblContract>().GetSingle(a => a.Id == contractFood.Contract);

            //سهمیه پیمان
            contract.RemainingQuota--;
            contract.UsedQuota++;

            //سهمیه غذا پیمان
            contractFood.RemainingQuota--;
            contractFood.UsedQuota++;

            //سهمیه اداره
            officeFoodQuota.RemainingQuota--;
            officeFoodQuota.UsedQuota++;
            res:
            return (result, contractFood, message);
        }

        //اضافه کردن سهمیه اداره ، پیمان و  غذا پیمان
        //هنگامی که از جدول موقت حذف شود یا درخواست تایید نشود
        private async Task<TblContractFood> AddQuota(TblFoodReservation entity, int office)
        {
            //اضافه کردن سهمیه اداره ، پیمان و  غذا پیمان
            var contractFood = await _unitOfWork.GetRepository<TblContractFood>().GetSingle(a =>
                a.TblContract.TblContractRestaurants.Any(b => b.Restaurant == entity.Restaurant)
                && a.TblContract.StartDate <= entity.Date && a.TblContract.EndDate >= entity.Date
                && a.TblContract.Status == 1 && a.Food == entity.Food);

            var officeFoodQuota = await _unitOfWork.GetRepository<TblOfficeFoodQuota>()
                .GetFirst(a => a.Office == office && DateTime.Now >= a.StartDate &&
                               DateTime.Now <= a.EndDate);
            var contract = await _unitOfWork.GetRepository<TblContract>().GetSingle(a => a.Id == contractFood.Contract);

            //سهمیه پیمان
            contract.RemainingQuota++;
            contract.UsedQuota--;

            //سهمیه غذا پیمان
            contractFood.RemainingQuota++;
            contractFood.UsedQuota--;

            //سهمیه اداره
            officeFoodQuota.RemainingQuota++;
            officeFoodQuota.UsedQuota--;
            return contractFood;
        }

        public class TempModel
        {
            public DateTime DateEn { get; set; }
            public string Date { get; set; }
            public string Name { get; set; }
            public string Family { get; set; }
            public string PersonnelCode { get; set; }
            public string NationalCode { get; set; }
            public int Person { get; set; }
            public int? ItemNumber { get; set; }
            public int? Vehicle { get; set; }

            public string VehicleTitle { get; set; }
            public class MealsModel
            {
                public int Id { get; set; }
                public int Meal { get; set; }
                public int Food { get; set; }
                public List<Item> Programs { get; set; }
                public bool Serving { get; set; } = false;
            }
            public List<MealsModel> Meals { get; set; }

        }

        /// <summary>
        /// 20/08/31
        /// </summary>
        /// <param name="accountNumber"></param>
        /// <param name="officeDepartment"></param>
        /// <returns></returns>
        [Authorize(Roles = "161")]
        public ActionResult Shift()
        {
            var user = Convert.ToInt32(User.Identity.Name);
            var us = _db.TblUsers.Find(user);
            //if (us.FoodReservationLock)
            //{
            //    TempData["errorToast"] = "حساب کاربری شما قفل می باشد لطفا به پیام ها مراجعه نمایید.";
            //    return RedirectToAction("Index", "Home",new { area = "" });
            //}
            ViewBag.UserGroup = us?.Group;



            if (_db.TblFoodReservationGroups.Any(a => a.User == user && a.Temp && a.Type == 4 && a.Remove == false))
            {
                var groups = _db.TblFoodReservationGroups.Where(a => a.User == user && a.Temp && a.Type == 4 && a.Remove == false).ToList();
                foreach (var group in groups)
                {
                    group.Remove = true;
                    group.TblFoodReservationShift.Remove = false;

                }

                _db.SaveChanges();

            }




            return View();
        }

        /// <summary>
        /// 20/08/31
        /// </summary>
        /// <param name="code"></param>
        /// <param name="endDate"></param>
        /// <param name="startDate"></param>
        /// <param name="accountNumber"></param>
        /// <param name="officeDepartment"></param>
        /// <param name="restCount"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddPersonToShiftTempList(string code, DateTime endDate, DateTime startDate, int accountNumber, int officeDepartment, int restCount)
        {

            var groups = new List<int> { 1, 4, 5, 6, 8 };
            var m = new Message();
            try
            {
                var user = Convert.ToInt32(User.Identity.Name);
                var list = new List<TblFoodReservation>();

                var person = _db.TblPersons.FirstOrDefault(a =>
                   (a.PersonnelCode == code || a.NationalCode == code) && groups.Contains(a.Group) &&
                  /* a.ShiftWorker == true &&*/ a.OfficeDepartment == officeDepartment );
                if (person == null)
                {
                    m.Msg = "شخصی با این مشخصات وجود ندارد یا دسترسی رزرو غذا در این بخش را ندارد";
                    m.Result = false;
                    m.Type = "error";
                   // goto result;
                }
                else if (person.Disable == true)
                {
                    m.Msg = $"وضعیت شخص  ({person.Fullname()}) غیرفعال می باشد";
                    m.Result = false;
                    m.Type = "error";
                    //goto result;
                }
                else if (person.ShiftWorker == false)
                {
                    m.Msg = $"شخص ({person.Fullname()}) نوبتکار نمی باشد";
                    m.Result = false;
                    m.Type = "error";
                    //goto result;
                }
                else
                {
                    //بررسی اینکه درخواست موقتی وجود دارد یا خیر
                    //اگر وجود داشت برای درخواست قبلی نفر اضافه می شود
                    var group = _db.TblFoodReservationGroups.Include(a => a.TblFoodReservationShift)
                        .FirstOrDefault(a => a.User == user && a.Temp && a.Type == 4 && a.Remove == false);
                    if (group == null)
                    {
                        var shift = new TblFoodReservationShift
                        {
                            Department = officeDepartment,
                            EndDate = endDate,
                            StartDate = startDate,
                            RestCount = restCount
                        };
                        //var (resultShift, messageShift) =
                        //    await _unitOfWork.GetRepository<TblFoodReservationShift>().Insert(shift, user);
                        _db.TblFoodReservationShifts.Add(shift);
                        var resultShift = _db.SaveChanges();
                        if (resultShift > 0)
                        {
                            var g = new TblFoodReservationGroup
                            {
                                Temp = true,
                                User = user,
                                AccountNumber = accountNumber,
                                Date = DateTime.Now,
                                Type = 4,
                                Shift = shift.Id
                               
                            };

                            var groupAgain = _db.TblFoodReservationGroups.Include(a => a.TblFoodReservationShift)
                                .FirstOrDefault(a => a.User == user && a.Temp && a.Type == 4 && a.Remove == false);
                            if (groupAgain == null)
                            {
                                //var (resultGroup, messageGroup) = _unitOfWork.GetRepository<TblFoodReservationGroup>().Add(g, user,false);
                                _db.TblFoodReservationGroups.Add(g);
                                _db.SaveChanges();
                                group = g;
                            }
                            else
                            {
                                group = groupAgain;
                            }
                        }
                        else
                        {
                            m.Msg = string.Format(Resources.Public.ErrorMessage, "");
                            m.Result = false;
                            m.Type = "error";
                            goto result;
                        }

                    }

                    //اصلاح تعداد نفرات استراحت در صورت نیاز
                    if (group.TblFoodReservationShift.RestCount != restCount)
                    {
                        group.TblFoodReservationShift.RestCount = restCount;
                    }

                    //اگر برای شخص در تاریخ مذکور در درخواست دیگری غذا رزرو نشده باشد
                    var isFoodReservation = _db.TblFoodReservationShiftPersons.Any(a =>
                       a.Person == person.Id &&
                       a.TblFoodReservationGroup.TblFoodReservationShift.StartDate >= startDate &&
                       a.TblFoodReservationGroup.TblFoodReservationShift.EndDate <= endDate &&
                       a.TblFoodReservationGroup.Temp == false &&
                       a.TblFoodReservationGroup.Remove == false);

                    if (isFoodReservation)
                    {
                        m.Msg = "برای این شخص در تاریخ انتخابی غذا درخواست داده شده است";
                        m.Result = false;
                        m.Type = "warning";
                        goto result;
                    }


                    //اگر قبلا کاربر در لیست موقت اضافه شده باشد
                    var p = _db.TblFoodReservationShiftPersons.Any(a =>
                        a.Person == person.Id &&
                        a.TblFoodReservationGroup.User == user &&
                        a.TblFoodReservationGroup.Temp &&
                        a.TblFoodReservationGroup.Type == 4 &&
                        a.TblFoodReservationGroup.Remove == false);
                    if (!p)
                    {
                        var entity = new TblFoodReservationShiftPerson
                        {
                            Group = group.Id,
                            Person = person.Id
                        };

                        _db.TblFoodReservationShiftPersons.Add(entity);
                        _db.SaveChanges();
                        m.Msg = person.Fullname() + "به لیست نفرات این درخواست افزوده شد.;" + group.Id;
                        m.Result = true;
                        m.Type = "success";
                        goto result;

                    }
                    else
                    {
                        m.Msg = "مورد جدیدی برای اضافه شدن به جدول موقت وجود نداشت.";
                        m.Result = false;
                        m.Type = "warning";
                        goto result;
                    }



                }
               
            }
            catch (Exception e)
            {
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
                goto result;
            }

            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddPersonToShiftTempList2(string code, int group)
        {

            var groups = new List<int> { 1, 4, 5, 6, 8 };
            var m = new Message();
            try
            {
                var user = Convert.ToInt32(User.Identity.Name);
              

                var person = _db.TblPersons.FirstOrDefault(a =>
                   (a.PersonnelCode == code || a.NationalCode == code) && groups.Contains(a.Group) 
                   //&& a.OfficeDepartment == officeDepartment
                  /* a.ShiftWorker == true &&*/  
                   );
                if (person == null)
                {
                    m.Msg = "شخصی با این مشخصات وجود ندارد یا دسترسی رزرو غذا در این بخش را ندارد";
                    m.Result = false;
                    m.Type = "error";
                   // goto result;
                }
                else if (person.Disable == true)
                {
                    m.Msg = $"وضعیت شخص  ({person.Fullname()}) غیرفعال می باشد";
                    m.Result = false;
                    m.Type = "error";
                    //goto result;
                }
                else if (person.ShiftWorker == false)
                {
                    m.Msg = $"شخص ({person.Fullname()}) نوبتکار نمی باشد";
                    m.Result = false;
                    m.Type = "error";
                    //goto result;
                }
                else
                {
                    

                    //اگر برای شخص در تاریخ مذکور در درخواست دیگری غذا رزرو نشده باشد
                    //var isFoodReservation = _db.TblFoodReservationShiftPersons.Any(a =>
                    //   a.Person == person.Id &&
                    //   a.TblFoodReservationGroup.TblFoodReservationShift.StartDate >= startDate &&
                    //   a.TblFoodReservationGroup.TblFoodReservationShift.EndDate <= endDate &&
                    //   a.TblFoodReservationGroup.Temp == false &&
                    //   a.TblFoodReservationGroup.Remove == false);

                    //if (isFoodReservation)
                    //{
                    //    m.Msg = "برای این شخص در تاریخ انتخابی غذا درخواست داده شده است";
                    //    m.Result = false;
                    //    m.Type = "warning";
                    //    goto result;
                    //}


                    //اگر قبلا کاربر در لیست موقت اضافه شده باشد
                    var p = _db.TblFoodReservationShiftPersons.Any(a =>
                        a.Person == person.Id &&
                    a.Group == group);
                    if (!p)
                    {
                        var entity = new TblFoodReservationShiftPerson
                        {
                            Group = group,
                            Person = person.Id
                        };

                        _db.TblFoodReservationShiftPersons.Add(entity);
                        _db.SaveChanges();
                        m.Msg = person.Fullname() + "به لیست نفرات این درخواست افزوده شد.;" + group;
                        m.Result = true;
                        m.Type = "success";
                        goto result;

                    }
                    else
                    {
                        m.Msg = "مورد جدیدی برای اضافه شدن به جدول موقت وجود نداشت.";
                        m.Result = false;
                        m.Type = "warning";
                        goto result;
                    }



                }
               
            }
            catch (Exception e)
            {
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
                goto result;
            }

            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddGroupToShiftTempList(int oldGroup, DateTime endDate, DateTime startDate, int accountNumber, int officeDepartment, int restCount)
        {

            var groups = new List<int> { 1, 4, 5, 6, 8 };
            var m = new Message();
            try
            {
                var user = Convert.ToInt32(User.Identity.Name);

                var persons = _db.TblFoodReservationShiftPersons.Where(a => a.Group == oldGroup).ToList();
                //بررسی اینکه درخواست موقتی وجود دارد یا خیر
                //اگر وجود داشت برای درخواست قبلی نفر اضافه می شود
                var group = _db.TblFoodReservationGroups.Include(a => a.TblFoodReservationShift)
                    .FirstOrDefault(a => a.User == user && a.Temp && a.Type == 4 && a.Remove == false);
                if (group == null)
                {
                    var shift = new TblFoodReservationShift
                    {
                        Department = officeDepartment,
                        EndDate = endDate,
                        StartDate = startDate,
                        RestCount = restCount
                    };

                    _db.TblFoodReservationShifts.Add(shift);
                    var resultShift = _db.SaveChanges();
                    if (resultShift > 0)
                    {
                        var g = new TblFoodReservationGroup
                        {
                            Temp = true,
                            User = user,
                            AccountNumber = accountNumber,
                            Date = DateTime.Now,
                            Type = 4,
                            Shift = shift.Id
                        };

                        var groupAgain = _db.TblFoodReservationGroups.Include(a => a.TblFoodReservationShift)
                            .FirstOrDefault(a => a.User == user && a.Temp && a.Type == 4 && a.Remove == false);
                        if (groupAgain == null)
                        {
                            //var (resultGroup, messageGroup) = _unitOfWork.GetRepository<TblFoodReservationGroup>().Add(g, user,false);
                            _db.TblFoodReservationGroups.Add(g);
                            _db.SaveChanges();
                            group = g;
                        }
                        else
                        {
                            group = groupAgain;
                        }

                        group.TblFoodReservationShift.RestCount = restCount;
                    }
                    else
                    {
                        m.Msg = string.Format(Resources.Public.ErrorMessage, "");
                        m.Result = false;
                        m.Type = "error";
                        m.Id = group.Id;
                        goto result;
                    }

                }


                var count = 0;
                foreach (var item in persons)
                {
                    var person = _db.TblPersons.FirstOrDefault(a =>
                        a.Id == item.Person &&
                        groups.Contains(a.Group) &&
                        a.ShiftWorker == true && a.OfficeDepartment == officeDepartment);
                    if (person == null) continue;

                    //اگر برای شخص در تاریخ مذکور در درخواست دیگری غذا رزرو نشده باشد
                    var isFoodReservation = _db.TblFoodReservationShiftPersons.Any(a =>
                        a.Person == person.Id &&
                        a.TblFoodReservationGroup.TblFoodReservationShift.StartDate >= startDate &&
                        a.TblFoodReservationGroup.TblFoodReservationShift.EndDate <= endDate &&
                        a.TblFoodReservationGroup.Temp == false &&
                        a.TblFoodReservationGroup.Remove == false
                        );

                    if (isFoodReservation)
                    {
                        continue;
                    }


                    //اگر قبلا کاربر در لیست موقت اضافه شده باشد
                    var p = _db.TblFoodReservationShiftPersons.Any(a =>
                        a.Person == person.Id &&
                        a.Group == group.Id);
                    if (p) continue;
                    var entity = new TblFoodReservationShiftPerson
                    {
                        Group = group.Id,
                        Person = person.Id
                    };

                    _db.TblFoodReservationShiftPersons.Add(entity);
                    _db.SaveChanges();
                    count++;



                }

                if (count > 0)
                {
                    m.Msg = count + " نفر به لیست نفرات این درخواست افزوده شد.;" + group.Id;
                    m.Result = true;
                    m.Type = "success";
                    m.Id = group.Id;
                }
                else
                {
                    m.Msg = "نفر جدیدی برای اضافه شدن به این درخواست وجود نداشت یا قبلا در این تاریخ غذا رزرو شده است.";
                    m.Result = false;
                    m.Type = "error";
                    m.Id = group.Id;
                }
            }
            catch (Exception e)
            {
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
             //   m.Id = group.Id;

            }

            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 20/08/31
        /// /</summary>
        /// <param name="group"></param>
        /// <returns></returns>
        public ActionResult ShiftPerson(int? group)
        {
            var user = Convert.ToInt32(User.Identity.Name);
            List<TblFoodReservationShiftPerson> list;
            if (group != null)

                list = _db.TblFoodReservationShiftPersons.Where(a => a.Group == group)
                    .AsNoTracking().Include(a => a.TblPerson)
                    .Include(a => a.TblPerson.TblPersonGroup)
                    .Include(a => a.TblPerson.TblOfficeDepartment)
                    .Include(a => a.TblPerson.TblOfficeDepartment.TblOffice)
                    .ToList();



            else
            {
                list = _db.TblFoodReservationShiftPersons
                    .AsNoTracking()
                    .Include(a => a.TblPerson)
                    .Include(a => a.TblPerson.TblPersonGroup)
                    .Include(a => a.TblPerson.TblOfficeDepartment)
                    .Include(a => a.TblPerson.TblOfficeDepartment.TblOffice)
                    .Where(a =>
                    a.TblFoodReservationGroup.Type == 4 &&
                    a.TblFoodReservationGroup.Temp &&
                    a.TblFoodReservationGroup.User == user &&
                    a.TblFoodReservationGroup.Remove == false)
                    .ToList();
                list = list.Where(a => a.Group == list.First().Group).ToList();
            }
            return PartialView(list);
        }

        /// <summary>
        /// 20/08/31
        /// </summary>
        /// <returns></returns>
        // [HttpPost]
        public async Task<ActionResult> AddToShiftTempList(int id)
        {
            try
            {
                //var groups = new List<int> { 1, 4, 5, 6 };
                var m = new Message();
                var user = Convert.ToInt32(User.Identity.Name);
                var list = new List<TblFoodReservation>();
                var endDate = Convert.ToDateTime(Request.Form["EndDate"]);
                var startDate = Convert.ToDateTime(Request.Form["StartDate"]);
                var restaurant = Convert.ToInt32(Request.Form["Restaurants"]);
                var restCount = Convert.ToInt32(Request.Form["RestCount"]);
                var restaurantInfo = _db.TblRestaurants.First(a => a.Id == restaurant);

                //بررسی اینکه درخواست موقتی وجود دارد یا خیر
                //اگر وجود داشت برای درخواست قبلی نفر اضافه می شود

                var group = _db.TblFoodReservationGroups
                    .Include(a => a.TblFoodReservationShift)
                    .Include(a => a.TblFoodReservationShiftPersons)
                    .First(a =>
                    a.User == user && a.Temp && a.Type == 4 && a.Remove == false && a.Id == id);

                //بررسی تعداد غذای درخواستی با تعداد استراحت
                var rTCount = Request.Form.AllKeys
                    .Where(a => a.StartsWith("MealT") && !a.Equals("MealT-0") && !a.Equals("MealT-5"))
                    .Sum(item => Convert.ToInt32(Request.Form[item]));
                var rFCount = Request.Form.AllKeys
                    .Where(a => a.StartsWith("MealF") && !a.Equals("MealF-0") && !a.Equals("MealF-5"))
                    .Sum(item => Convert.ToInt32(Request.Form[item]));
                var rNCount = Request.Form.AllKeys
                    .Where(a => a.StartsWith("MealN") && !a.Equals("MealN-0") && !a.Equals("MealN-5"))
                    .Sum(item => Convert.ToInt32(Request.Form[item]));

                var rTDCount = Convert.ToInt32(Request.Form["MealT-3"]);
                var rFDCount = Convert.ToInt32(Request.Form["MealF-3"]);
                var rNDCount = Convert.ToInt32(Request.Form["MealN-3"]);

                var rTADCount = Convert.ToInt32(Request.Form["MealT-5"]);
                var rFADCount = Convert.ToInt32(Request.Form["MealF-5"]);
                var rNADCount = Convert.ToInt32(Request.Form["MealN-5"]);
                if (rNDCount < rNADCount)
                {
                    m.Result = false;
                    m.Type = "error";
                    m.Msg = "تعداد درخواستی وعده پس شام در روزهای عادی بیش از تعداد مجاز می باشد"
                            + "<br /><small>تعداد درخواستی: " + rNADCount + "<br /> تعداد مجاز: " + rNDCount +
                            "</small>";
                    goto result;
                }

                if (rTDCount < rTADCount)
                {
                    m.Result = false;
                    m.Type = "error";
                    m.Msg = "تعداد درخواستی وعده پس شام در روزهای پنجشنبه بیش از تعداد مجاز می باشد"
                            + "<br /><small>تعداد درخواستی: " + rTADCount + "<br /> تعداد مجاز: " + rTDCount +
                            "</small>";
                    goto result;
                }

                if (rFDCount < rFADCount)
                {
                    m.Result = false;
                    m.Type = "error";
                    m.Msg = "تعداد درخواستی وعده پس شام در روزهای جمعه بیش از تعداد مجاز می باشد"
                            + "<br /><small>تعداد درخواستی: " + rFADCount + "<br /> تعداد مجاز: " + rFDCount +
                            "</small>";
                    goto result;
                }

                group.TblFoodReservationShift.RestCount = restCount;
                group.Confirm = !restaurantInfo.ConfirmReservation;
                var personCount = group.TblFoodReservationShiftPersons.Count();
                var rCount = personCount - restCount;
                //if (rCount < rNCount)
                //{
                //    m.Result = false;
                //    m.Type = "error";
                //    m.Msg = "تعداد غذای درخواستی در روزهای عادی بیش از تعداد مجاز می باشد"
                //            + "<br /><small>تعداد درخواستی: " + rNCount + "<br /> تعداد مجاز: " + rCount +
                //            "<br />تعداد نفرات:" + personCount + "<br /> تعداد استراحت:" + restCount + "</small>";
                //    goto result;
                //}

                //if (rCount < rTCount)
                //{
                //    m.Result = false;
                //    m.Type = "error";
                //    m.Msg = "تعداد غذای درخواستی در روزهای پنجشنبه بیش از تعداد مجاز می باشد"
                //            + "<br /><small>تعداد درخواستی: " + rTCount + "<br /> تعداد مجاز: " + rCount +
                //            "<br />تعداد نفرات:" + personCount + "<br /> تعداد استراحت:" + restCount + "</small>";

                //    goto result;
                //}

                //if (rCount < rFCount)
                //{
                //    m.Result = false;
                //    m.Type = "error";
                //    m.Msg = "تعداد غذای درخواستی در روزهای جمعه و تعطیل بیش از تعداد مجاز می باشد"
                //            + "<br /><small>تعداد درخواستی: " + rFCount + "<br /> تعداد مجاز: " + rCount +
                //            "<br />تعداد نفرات:" + personCount + "<br /> تعداد استراحت:" + restCount + "</small>";

                //    goto result;
                //}

                for (var i = startDate; i <= endDate; i = i.AddDays(1))
                {

                    //جمعه
                    if (i.DayOfWeek == DayOfWeek.Friday || _db.TblHolidays.Any(a => a.Date == i))
                    {

                        foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("MealF")))
                        {
                            var meal = Convert.ToInt32(item.Split('-')[1]);
                            var count = Convert.ToInt32(Request.Form[item]);
                            if (count > 0)
                            {
                                var defaultFood = _db.TblRestaurantFoodPrograms
                                    .FirstOrDefault(a => a.Restaurant == restaurant && a.Meal == meal && a.Date == i &&
                                                         a.Default);
                                for (var j = 0; j < count; j++)
                                {
                                    

                                    if (meal == 0 || defaultFood != null)
                                    {

                                        var entity = new TblFoodReservation
                                        {
                                            Date = i,
                                            Food = meal == 0 ? 0 : defaultFood.Food,
                                            Meal = meal,
                                            Restaurant = restaurant,
                                            Person = -2,
                                            Group = group.Id,
                                            Contract = -1,
                                            ItemNumber = j,
                                           // Confirm = !restaurantInfo.ConfirmReservation
                                        };
                                        list.Add(entity);
                                    }
                                    else
                                    {
                                      //  var mealTitle = (_db.TblMeals.First(a => a.Id == meal)).Title;
                                        var mealTitle = MealTitle(meal);
                                        m.Msg = string.Format(Resources.Public.FoodProgramForMealInNull,
                                            new PersianDateTime(i).ToString("yyyy/MM/dd"), mealTitle);
                                        m.Result = false;
                                        m.Type = "error";
                                        goto result;
                                    }
                                }

                            }
                        }

                    }
                    //پنجشنبه
                    else if (i.DayOfWeek == DayOfWeek.Thursday)
                    {


                        foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("MealT")))
                        {
                            var meal = Convert.ToInt32(item.Split('-')[1]);
                            var count = Convert.ToInt32(Request.Form[item]);
                            if (count > 0)
                            {
                                var defaultFood = _db.TblRestaurantFoodPrograms
                                    .FirstOrDefault(a => a.Restaurant == restaurant && a.Meal == meal && a.Date == i && a.Default);
                                for (var j = 0; j < count; j++)
                                {
                                   
                                    if (meal == 0 || defaultFood != null)
                                    {

                                        var entity = new TblFoodReservation
                                        {
                                            Date = i,
                                            Food = meal == 0 ? 0 : defaultFood.Food,
                                            Meal = meal,
                                            Restaurant = restaurant,
                                            Person = -2,
                                            Group = group.Id,
                                            Contract = -1,
                                            ItemNumber = j
                                        };
                                        list.Add(entity);
                                    }
                                    else
                                    {
                                       // var mealTitle = (_db.TblMeals.First(a => a.Id == meal)).Title;
                                        var mealTitle = MealTitle(meal);
                                        m.Msg = string.Format(Resources.Public.FoodProgramForMealInNull,
                                            new PersianDateTime(i).ToString("yyyy/MM/dd"), mealTitle);
                                        m.Result = false;
                                        m.Type = "error";
                                        goto result;
                                    }
                                }

                            }
                        }

                    }

                    //روزهای عادی
                    else
                    {

                        foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("MealN")))
                        {
                            var meal = Convert.ToInt32(item.Split('-')[1]);
                            var count = Convert.ToInt32(Request.Form[item]);
                            if (count > 0)
                            {
                                var defaultFood = _db.TblRestaurantFoodPrograms
                                    .FirstOrDefault(a => a.Restaurant == restaurant && a.Meal == meal && a.Date == i && a.Default);
                                for (var j = 0; j < count; j++)
                                {
                                  

                                    if (meal == 0 || defaultFood != null)
                                    {
                                      
                                            var entity = new TblFoodReservation
                                            {
                                                Date = i,
                                                Food = meal == 0 ? 0 : defaultFood.Food,
                                                Meal = meal,
                                                Restaurant = restaurant,
                                                Person = -2,
                                                Group = group.Id,
                                                Contract = -1,
                                                ItemNumber = j
                                            };
                                            list.Add(entity);
                                        
                                    }
                                    else
                                    {
                                        //var mealTitle = (_db.TblMeals.First(a => a.Id == meal)).Title;
                                        var mealTitle = MealTitle(meal);
                                        m.Msg = string.Format(Resources.Public.FoodProgramForMealInNull,
                                            new PersianDateTime(i).ToString("yyyy/MM/dd"), mealTitle);
                                        m.Result = false;
                                        m.Type = "error";
                                        goto result;
                                    }
                                }

                            }
                        }

                    }



                }

                if (list.Any())
                {
                    group.TblFoodReservationShift.F0 = Convert.ToInt32(Request.Form["MealF-0"]);
                    group.TblFoodReservationShift.F1 = Convert.ToInt32(Request.Form["MealF-1"]);
                    group.TblFoodReservationShift.F2 = Convert.ToInt32(Request.Form["MealF-2"]);
                    group.TblFoodReservationShift.F3 = Convert.ToInt32(Request.Form["MealF-3"]);
                    group.TblFoodReservationShift.F4 = Convert.ToInt32(Request.Form["MealF-4"]);
                    group.TblFoodReservationShift.F5 = Convert.ToInt32(Request.Form["MealF-5"]);
                    
                    group.TblFoodReservationShift.N0 = Convert.ToInt32(Request.Form["MealN-0"]);
                    group.TblFoodReservationShift.N1 = Convert.ToInt32(Request.Form["MealN-1"]);
                    group.TblFoodReservationShift.N2 = Convert.ToInt32(Request.Form["MealN-2"]);
                    group.TblFoodReservationShift.N3 = Convert.ToInt32(Request.Form["MealN-3"]);
                    group.TblFoodReservationShift.N4 = Convert.ToInt32(Request.Form["MealN-4"]);
                    group.TblFoodReservationShift.N5 = Convert.ToInt32(Request.Form["MealN-5"]);
                    
                    group.TblFoodReservationShift.T0 = Convert.ToInt32(Request.Form["MealT-0"]);
                    group.TblFoodReservationShift.T1 = Convert.ToInt32(Request.Form["MealT-1"]);
                    group.TblFoodReservationShift.T2 = Convert.ToInt32(Request.Form["MealT-2"]);
                    group.TblFoodReservationShift.T3 = Convert.ToInt32(Request.Form["MealT-3"]);
                    group.TblFoodReservationShift.T4 = Convert.ToInt32(Request.Form["MealT-4"]);
                    group.TblFoodReservationShift.T5 = Convert.ToInt32(Request.Form["MealT-5"]);
                    _db.SaveChanges();
                    // var count = ;
                    var (resultFood, messageFood) = await _e.InsertRange(list, user);
                    var fromDate = list.OrderBy(a => a.Date).First().Date.ToString("M/dd/yyyy");
                    var toDate = list.OrderByDescending(a => a.Date).First().Date.ToString("M/dd/yyyy");
                    m.Msg = resultFood
                        ? "تعداد " + list.Count() + " درخواست با غذای پیش فرض به لیست افزوده شد. (" +
                          list.First().Group + ");" + list.First().Group + ";" + fromDate + ";" + toDate
                        : messageFood;
                    m.Result = resultFood;
                    m.Type = resultFood ? "success" : "error";
                    m.Id = group.Id;

                }
                else
                {
                    m.Msg = "مورد جدیدی برای اضافه شدن به جدول موقت وجود نداشت.";
                    m.Result = false;
                    m.Type = "warning";
                    m.Id = group.Id;
                }

                result:
                return Json(m, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                // Get stack trace for the exception with source file information
                var st = new StackTrace(e, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                var m = new Message { Result = false, Msg = e.Message + ", Line:" + line, Type = "error" };
                Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                return Json(m, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public JsonResult ShiftTempList()
        {
            try
            {
                const string includeProperties = @"TblRestaurant.TblRestaurantFoodPrograms";

                var user = Convert.ToInt32(User.Identity.Name);
                var list =  _unitOfWork.GetRepository<TblFoodReservation>().GetSync(
                    a => a.TblFoodReservationGroup.Temp && a.TblFoodReservationGroup.User == user &&
                         a.TblFoodReservationGroup.Type == 4, includeProperties: includeProperties);
                var count = list.Count();
                //  var res = list.First().Restaurant;
                var meals = list.Select(s => s.Meal).Distinct().OrderBy(a => a).ToList();
                var listMeals = new List<Item>();
                foreach (var item in meals)
                {
                    var i = new Item
                    {
                        Id = item,
                        Title = MealTitle(item)
                    };
                    listMeals.Add(i);
                }

                ViewBag.Meals = listMeals;
                var temp = list.GroupBy(g => new { g.Date, g.ItemNumber }).Select(a => new TempModel
                {

                    DateEn = a.First().Date,
                    Person = a.First().Person,
                    Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd dddd"),
                    Name = "-",
                    Family = "-",
                    NationalCode = a.First().TblPerson.NationalCode,
                    PersonnelCode = a.First().TblPerson.PersonnelCode,
                    Meals = a.GroupBy(g => g.Meal).Select(b => new TempModel.MealsModel
                    {
                        Id = b.First().Id,
                        Meal = b.First().Meal,
                        Food = b.First().Food,
                        Programs = RestaurantFoodItems(a.First().Date, b.First().Meal, restaurant: a.First().Restaurant)
                    }).ToList()
                }).ToList();
                var countPerson = 0;
                try
                {
                    countPerson = temp.Count() / list.GroupBy(g => new { g.Date }).Count();
                }
                catch
                {

                }
                ViewBag.CountPerson = countPerson;
                return Json(new { PartialView = ConvertViewToString("ShiftTempList", temp), countPerson, tempCount = temp.Count() },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { PartialView = ex.Message + "11-3", countPerson = "11-2", tempCount = 0 },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Authorize(Roles = "161")]
        public async Task<ActionResult> ShiftSave(int id,string des)
        {
            var m = new Message();
            try
            {
                var user = Convert.ToInt32(User.Identity.Name);

                if (_db.TblFoodReservationGroups.Any(a => a.User == user && a.Temp && a.Type == 4 && a.Remove == false && a.Id == id))
                {

                    var group = _db.TblFoodReservationGroups
                        .Include(a => a.TblOfficeDepartmentAccountNumber.TblOfficeDepartment).First(a => a.Id == id);
                    group.Des = des;
                    var items = _db.TblFoodReservations.Where(a => a.Group == group.Id).ToList();
                    var count = items.Count();
                    foreach (var item in items)
                    {
                        if (item.Food == -1)
                        {
                            continue;
                        }
                        var (res, ms) = await ReviewRestrictions(item, group.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office, 1);
                        if (!res)
                        {
                            var msg = string.Format(Resources.Public.AddErrorMessage,
                                "درخواست غذا برای تاریخ " +
                                new PersianDateTime(item.Date).ToLongDateString() +
                                " وعده " + MealTitle(item.Meal),
                                ms);
                            m.Msg = msg;
                            m.Result = false;
                            m.Type = "error";
                            goto result;

                        }

                        var (contractResult, contractFood, contractMessage) = await CalculateQuota(item, group.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office);
                        if (contractResult)
                        {
                            //مقدار دهی فیلد های مورد نیاز
                            item.Contract = contractFood.Contract;
                            
                        }
                        else
                        {
                            m.Msg = contractMessage;
                            m.Result = false;
                            m.Type = "error";
                            return Json(m, JsonRequestBehavior.AllowGet);
                        }
                        //var contractFood = await CalculateQuota(item, group.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office);

                        ////مقدار دهی فیلد های مورد نیاز

                        //item.Contract = contractFood.Contract;
                    }

                    group.Temp = false;
                    var result = _db.SaveChanges();
                    //var (result, message) = await _unitOfWork.GetRepository<TblFoodReservationGroup>().Update(group, user);
                    if (result > 0)
                    {
                        var delList = _db.TblFoodReservations.Where(a => a.Food == -1 && a.Group == group.Id);
                        _db.TblFoodReservations.RemoveRange(delList);
                        _db.SaveChanges();

                        m.Msg = string.Format(Resources.Public.AddSuccessMessage, "درخواست رزرو غذا با شناسه " + group.Id);
                        m.Result = true;
                        m.Type = "success";
                    }
                    else
                    {
                        m.Msg = string.Format(Resources.Public.AddErrorMessage, "درخواست رزرو غذا", " ");
                        m.Result = false;
                        m.Type = "error";
                    }
                }
            }
            catch (Exception e)
            {
                Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }
            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }


        [Authorize(Roles = "216")]
        public async Task<ActionResult> Delayed2()
        {
            var user = Convert.ToInt32(User.Identity.Name);
            var us = _db.TblUsers.Find(user);
            //if (us.FoodReservationLock)
            //{
            //    TempData["errorToast"] = "حساب کاربری شما قفل می باشد لطفا به پیام ها مراجعه نمایید.";
            //    return RedirectToAction("Index", "Home",new { area = "" });
            //}
            if (await _unitOfWork.GetRepository<TblFoodReservationGroup>().Any(a => a.User == user && a.Temp && a.Type == 5))
            {
                var group = await _unitOfWork.GetRepository<TblFoodReservationGroup>()
                    .GetFirst(a => a.User == user && a.Temp && a.Type == 5);

                await _unitOfWork.GetRepository<TblFoodReservation>().DeleteRange(a => a.Group == group.Id, user);
                await _unitOfWork.GetRepository<TblFoodReservationGroup>().Delete(group.Id, user);
            }
            return View();
        }
        //[AllowAnonymous]
          [Authorize(Roles = "162")]
        public async Task<ActionResult> Office()
        {
            var user = Convert.ToInt32(User.Identity.Name);
            var us = _db.TblUsers.Find(user);
            //if (us.FoodReservationLock)
            //{
            //    TempData["errorToast"] = "حساب کاربری شما قفل می باشد لطفا به پیام ها مراجعه نمایید.";
            //    return RedirectToAction("Index", "Home",new { area = "" });
            //}
            if (await _unitOfWork.GetRepository<TblFoodReservationGroup>().Any(a => a.User == user && a.Temp && a.Type == 1))
            {
                var group = await _unitOfWork.GetRepository<TblFoodReservationGroup>()
                    .GetFirst(a => a.User == user && a.Temp && a.Type == 1);

                await _unitOfWork.GetRepository<TblFoodReservation>().DeleteRange(a => a.Group == group.Id, user);
                await _unitOfWork.GetRepository<TblFoodReservationGroup>().Delete(group.Id, user);
            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddPersonToTempList(string code, DateTime endDate, DateTime startDate, int accountNumber, int restaurant, List<int> meals, int? vehicle, byte type =1,bool confirm = true)
        {
            var groups = new List<int> { 1, 4, 5, 6 };
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            var list = new List<TblFoodReservation>();
            var person = await _unitOfWork.GetRepository<TblPerson>().GetFirst(a =>
                (a.PersonnelCode == code || a.NationalCode == code) && (vehicle != null || groups.Contains(a.Group)));
            var restaurantInfo = await _unitOfWork.GetRepository<TblRestaurant>().GetFirst(a => a.Id == restaurant);

            if (person == null)
            {
                m.Msg = "شخصی با این مشخصات وجود ندارد یا دسترسی رزرو غذا در این بخش را ندارد";
                m.Result = false;
                m.Type = "error";
                // goto result;
            }
            else if (person.Disable == true)
            {
                m.Msg = $"وضعیت شخص  ({person.Fullname()}) غیرفعال می باشد";
                m.Result = false;
                m.Type = "error";
                //goto result;
            } 
            else if (person.ShiftWorker == true)
            {
                m.Msg = $" شخص  ({person.Fullname()}) نوبتکار می باشد";
                m.Result = false;
                m.Type = "error";
                //goto result;
            }
            else
            {
                //بررسی اینکه درخواست موقتی وجود دارد یا خیر
                //اگر وجود داشت برای درخواست قبلی نفر اضافه می شود
                var group = await _unitOfWork.GetRepository<TblFoodReservationGroup>()
                    .GetFirst(a => a.User == user && a.Temp && a.Type == type);
                if (group == null)
                {
                    var g = new TblFoodReservationGroup
                    {
                        Temp = true,
                        User = user,
                        AccountNumber = accountNumber,
                        Date = DateTime.Now,
                        Type = type,
                        Confirm = confirm
                    };
                    var (resultGroup, messageGroup) = await _unitOfWork.GetRepository<TblFoodReservationGroup>().Insert(g, user);

                    group = g;
                }
                if(type == 1)
                group.Confirm = !restaurantInfo.ConfirmReservation;
                for (var i = startDate; i <= endDate; i = i.AddDays(1))
                {
                    foreach (var meal in meals)
                    {
                        //اگر قبلا کاربر در لیست موقت اضافه شده باشد
                        var p = await _unitOfWork.GetRepository<TblFoodReservation>().Any(a =>
                            a.Person == person.Id && a.Meal == meal && a.Date == i &&
                            a.TblFoodReservationGroup.User == user && a.TblFoodReservationGroup.Temp && a.TblFoodReservationGroup.Remove == false && a.TblFoodReservationGroup.Type == type);
                        if (p) continue;

                        //اگر قبلا وسیله نقلیه در لیست موقت اضافه شده باشد
                        if (vehicle != null)
                        {
                            var v = await _unitOfWork.GetRepository<TblFoodReservation>().Any(a =>
                                a.Vehicle == vehicle && a.Meal == meal && a.Date == i &&
                                a.TblFoodReservationGroup.User == user && a.TblFoodReservationGroup.Temp && a.TblFoodReservationGroup.Remove == false && a.TblFoodReservationGroup.Type == type);
                            if (v) continue;
                        }

                        var defaultFood = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().GetFirst(a =>
                            a.Restaurant == restaurant && a.Meal == meal && a.Date == i && a.Default);

                        if (meal == 0 || defaultFood != null)
                        {

                            var entity = new TblFoodReservation
                            {
                                Date = i,
                                Food = meal == 0 ? 0 : defaultFood.Food,
                                Meal = meal,
                                Restaurant = restaurant,
                                Person = person.Id,
                                Group = group.Id,
                                Contract = -1,
                                Vehicle = vehicle,
                                //Confirm = !restaurantInfo.ConfirmReservation
                            };
                            list.Add(entity);
                        }
                        else
                        {
                            m.Msg = Resources.Public.FoodProgramIsNull;
                            m.Result = false;
                            m.Type = "error";
                            goto result;
                        }
                        //  }

                    }
                }


                if (list.Any())
                {
                    var (resultFood, messageFood) = await _e.InsertRange(list, user);
                    m.Msg = resultFood ? person.Fullname() + " با غذای پیش فرض به لیست افزوده شد." : messageFood;
                    m.Result = resultFood;
                    m.Type = resultFood ? "success" : "error";
                    goto result;

                }
                else
                {
                    m.Msg = "مورد جدیدی برای اضافه شدن به جدول موقت وجود نداشت.";
                    m.Result = false;
                    m.Type = "warning";
                    goto result;
                }
            }
           

            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        // [HttpPost]
        public async Task<JsonResult> OfficeTempList(byte type=1)
        {
            const string includeProperties = @"TblPerson,TblRestaurant.TblRestaurantFoodPrograms";

            var user = Convert.ToInt32(User.Identity.Name);
            var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
                a => a.TblFoodReservationGroup.Temp && a.TblFoodReservationGroup.User == user && a.TblFoodReservationGroup.Type == type, includeProperties: includeProperties);
            //  var res = list.First().Restaurant;
            var meals = list.Select(s => s.Meal).Distinct().OrderBy(a => a).ToList();
            var listMeals = new List<Item>();
            foreach (var item in meals)
            {
                var i = new Item
                {
                    Id = item,
                    Title = MealTitle(item)
                };
                listMeals.Add(i);
            }
            ViewBag.Meals = listMeals;
            var temp = list.GroupBy(g => new { g.Person, g.Date }).Select(a => new TempModel
            {
                DateEn = a.First().Date,
                Person = a.First().Person,
                Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd"),
                Name = a.First().TblPerson.Name,
                Family = a.First().TblPerson.Family,
                NationalCode = a.First().TblPerson.NationalCode,
                PersonnelCode = a.First().TblPerson.PersonnelCode,
                Vehicle = a.First().Vehicle,
                Meals = a.GroupBy(g => g.Meal).Select(b => new TempModel.MealsModel
                {
                    Id = b.First().Id,
                    Meal = b.First().Meal,
                    Food = b.First().Food,
                    Programs = RestaurantFoodItems(a.First().Date, b.First().Meal, restaurant: a.First().Restaurant)
                }).ToList()
            }).ToList();
            var countPerson = temp.Select(a => a.NationalCode).Distinct().Count();
            ViewBag.CountPerson = countPerson;
            return Json(new { PartialView = ConvertViewToString("OfficeTempList", temp), countPerson }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public async Task<bool> SaveOneFoodFromTempList(int reservationId, int food)
        {
            var user = Convert.ToInt32(User.Identity.Name);
            var r = await _unitOfWork.GetRepository<TblFoodReservation>().GetFirst(a => a.Id == reservationId);

            r.Food = food;
            var (result, message) = await _unitOfWork.GetRepository<TblFoodReservation>().Update(r, user);
            return result;
        } 
        [HttpPost]
        public async Task<ActionResult> EditOneFoodFromTempList(int reservationId, int food)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            var r =  _unitOfWork.GetRepository<TblFoodReservation>().GetSingleSync(a => a.Id == reservationId
                     ,includeProperties: "TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,TblMeal");
            //var r = _db.TblFoodReservations
            //    .Include(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment)
            //    .Include(a => a.TblMeal)
            //    .First(a => a.Id == reservationId);
            var (res, ms) = await ReviewRestrictions(r, r.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office, 1,edit:true);
            if (!res)
            {
                var msg = string.Format(Resources.Public.EditErrorMessage,
                    " غذا برای تاریخ " +
                    new PersianDateTime(r.Date).ToLongDateString() +
                    " وعده " + r.TblMeal.Title,
                    ms);
                m.Msg = msg;
                m.Result = false;
                m.Type = "error";
               

            }
            else
            {
                r.Food = food;
                var (result, message) = await _unitOfWork.GetRepository<TblFoodReservation>().Update(r, user);
                if (result)
                {
                    m.Msg = string.Format(Resources.Public.EditSuccessMessage,
                        "ویرایش غذا برای تاریخ " +
                        new PersianDateTime(r.Date).ToLongDateString() +
                        " وعده " + r.TblMeal.Title);
                    m.Result = true;
                    m.Type = "success";
                }
                else
                {
                    m.Msg = string.Format(Resources.Public.EditErrorMessage,
                        "ویرایش غذا برای تاریخ " +
                        new PersianDateTime(r.Date).ToLongDateString() +
                        " وعده " + r.TblMeal.Title,
                        message);
                    m.Result = false;
                    m.Type = "error";
                }
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public async Task<bool> DeleteOneDayPersonFromTemp(int person, DateTime date)
        {
            var user = Convert.ToInt32(User.Identity.Name);


            var (result, count, message) = await _unitOfWork.GetRepository<TblFoodReservation>().DeleteRange(a =>
                a.Person == person && a.Date == date && a.TblFoodReservationGroup.User == user && a.TblFoodReservationGroup.Temp, user);
            return result;
        }
        [HttpPost]
        public async Task<bool> DeleteOnePersonFromTemp(int person,byte type=1)
        {
            var user = Convert.ToInt32(User.Identity.Name);

            var (result, count, message) = await _unitOfWork.GetRepository<TblFoodReservation>().DeleteRange(a =>
                a.Person == person && a.TblFoodReservationGroup.User == user && a.TblFoodReservationGroup.Temp && a.TblFoodReservationGroup.Type == type, user);
            return result;
        }
        //[HttpPost]
        //public async Task<bool> DeleteOnePersonFromTempShift(int person)
        //{
        //    var user = Convert.ToInt32(User.Identity.Name);

        //    var (result, count, message) = await _unitOfWork.GetRepository<TblFoodReservationShiftPerson>().DeleteRange(a =>
        //        a.Person == person && a.TblFoodReservationGroup.User == user && a.TblFoodReservationGroup.Temp && a.TblFoodReservationGroup.Type == 4, user);
        //    return result;
        //}
        [HttpPost]
        public async Task<bool> DeleteOnePersonFromTempShift(int person,int group)
        {
            var user = Convert.ToInt32(User.Identity.Name);

            var (result, count, message) = await _unitOfWork.GetRepository<TblFoodReservationShiftPerson>().DeleteRange(a =>
                a.Person == person && a.Group == group, user);
            return result;
        }

        private string ConvertViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var writer = new StringWriter())
            {
                var vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }

        [HttpPost]
        [Authorize(Roles = "162")]
        public async Task<ActionResult> OfficeSave(string des,byte type=1)
        {
            var m = new Message();
            try
            {
                var user = Convert.ToInt32(User.Identity.Name);

                if (await _unitOfWork.GetRepository<TblFoodReservationGroup>().Any(a => a.User == user && a.Temp))
                {
                    var group = await _unitOfWork.GetRepository<TblFoodReservationGroup>()
                        .GetFirst(a => a.User == user && a.Temp && a.Type == type, includeProperties: "TblOfficeDepartmentAccountNumber.TblOfficeDepartment");
                    group.Des = des;
                    var foods =  _unitOfWork.GetRepository<TblFoodReservation>()
                        .GetSync(a => a.Group == group.Id, includeProperties: "TblPerson").ToList();
                    foreach (var item in foods )
                    {
                        if (item.Food == -1)
                        {
                            continue;
                        }
                        var (contractResult, contractFood, contractMessage) = await CalculateQuota(item, group.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office);
                        if (contractResult)
                        {
                            //مقدار دهی فیلد های مورد نیاز
                            item.Contract = contractFood.Contract;

                        }
                        else
                        {
                            var msg = string.Format(Resources.Public.AddErrorMessage,
                               "درخواست غذا برای " + item.TblPerson.Fullname() + " تاریخ " +
                               new PersianDateTime(item.Date)
                                   .ToLongDateString() +
                               " وعده " + MealTitle(item.Meal),
                               contractMessage);
                            m.Msg = msg;
                            m.Result = false;
                            m.Type = "error";
                            goto result;
                          
                        }

                        var (res, ms) = await ReviewRestrictions(item, group.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office, 1,type);
                        if (!res)
                        {
                            var msg = string.Format(Resources.Public.AddErrorMessage,
                                "درخواست غذا برای " + item.TblPerson.Fullname() + " تاریخ " +
                                new PersianDateTime(item.Date)
                                    .ToLongDateString() +
                                " وعده " + MealTitle(item.Meal),
                                ms);
                            m.Msg = msg;
                            m.Result = false;
                            m.Type = "error";
                            goto result;

                        }

                       
                        //var contractFood = await CalculateQuota(item, group.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office);

                        ////مقدار دهی فیلد های مورد نیاز

                        //item.Contract = contractFood.Contract;
                    }

                    group.Temp = false;
                    var (result, message) = await _unitOfWork.GetRepository<TblFoodReservationGroup>().Update(group, user);
                    if (result)
                    {
                        var (resultDelete, countDelete, messageDelete) = await _unitOfWork
                            .GetRepository<TblFoodReservation>()
                            .DeleteRange(a => a.Food == -1 && a.Group == group.Id, user);

                        m.Msg = string.Format(Resources.Public.AddSuccessMessage, "درخواست رزرو غذا با شناسه " + group.Id);
                        m.Result = true;
                        m.Type = "success";
                    }
                    else
                    {
                        m.Msg = string.Format(Resources.Public.AddErrorMessage, "درخواست رزرو غذا", message);
                        m.Result = false;
                        m.Type = "error";
                    }
                }
            }
            catch (Exception e)
            {
                Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }
            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DelayedMessage()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> DelayedMessage(string username, DateTime startDate, DateTime endDate,
            int restaurants, int count, string message, bool lockUser = false)
        {
            try
            {
                var cUser = Convert.ToInt32(User.Identity.Name);
                var user = await _unitOfWork.GetRepository<TblUser>().GetFirst(a =>
                    a.TblPerson.PersonnelCode == username || a.TblPerson.NationalCode == username);
                var userMessage = new TblUserMessage
                {
                    Body = message,
                    CreateDate = DateTime.Now,
                    CreateUser = cUser,
                    Status = 0,
                    Title = "ثبت درخواست معوقه",
                    User = user.Id
                };
                if (lockUser)
                    user.FoodReservationLock = true;

                var (result, msg) = await _unitOfWork.GetRepository<TblUserMessage>().Insert(userMessage, cUser);
                if (result)
                {
                    // _unitOfWork.SaveAsync();
                    TempData["successToast"] = "پیام ثبت درخواست معوقه برای کاربر با موفقیت ارسال شد.";
                    return RedirectToAction("");
                }
                else
                {
                    TempData["errorToast"] = string.Format(Resources.Public.ErrorMessage, msg);

                }
            }
            catch (Exception e)
            {
                Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                TempData["errorToast"] = string.Format(Resources.Public.ErrorMessage, e.Message);

            }

            return View();
        }

        [Authorize(Roles = "164")]
        public ActionResult Delayed()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddDelayedItemsToTempList(int count, DateTime startDate, int restaurant, List<int> meals)
        {
            // var groups = new List<int> { 1, 4, 5, 6 };
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            var list = new List<TblFoodReservation>();

            //بررسی اینکه درخواست موقتی وجود دارد یا خیر
            //اگر وجود داشت  درخواست قبلی حذف می شود
            if (await _unitOfWork.GetRepository<TblFoodReservationGroup>().Any(a => a.User == user && a.Temp && a.Type == 3))
            {
                var gd = await _unitOfWork.GetRepository<TblFoodReservationGroup>()
                    .GetFirst(a => a.User == user && a.Temp && a.Type == 3);

                await _unitOfWork.GetRepository<TblFoodReservation>().DeleteRange(a => a.Group == gd.Id, user);
                await _unitOfWork.GetRepository<TblFoodReservationGroup>().Delete(gd.Id, user);

            }

            var restaurantInfo = await _unitOfWork.GetRepository<TblRestaurant>().GetFirst(a => a.Id == restaurant);

            var group = new TblFoodReservationGroup
            {
                
                Temp = true,
                User = user,
                AccountNumber = 0,
                Date = DateTime.Now,
                Type = 3 //Type Delayed

            };
            group.Confirm = !restaurantInfo.ConfirmReservation;
            var (resultGroup, messageGroup) = await _unitOfWork.GetRepository<TblFoodReservationGroup>().Insert(group, user);
           
            for (var j = 0; j < count; j++)
            {
                var i = startDate;

                foreach (var meal in meals)
                {
                    var defaultFood = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().GetFirst(a =>
                        a.Restaurant == restaurant && a.Meal == meal && a.Date == i && a.Default);

                    if (meal == 0 || defaultFood != null)
                    {

                        var entity = new TblFoodReservation
                        {
                            Date = i,
                            Food = meal == 0 ? 0 : defaultFood.Food,
                            Meal = meal,
                            Restaurant = restaurant,
                            Person = -2,
                            Group = group.Id,
                            Contract = -1,
                            ItemNumber = j,
                            //Confirm = !restaurantInfo.ConfirmReservation
                        };
                        list.Add(entity);
                    }
                    else
                    {
                        m.Msg = Resources.Public.FoodProgramIsNull;
                        m.Result = false;
                        m.Type = "error";
                        goto result;
                    }
                    //  }


                }

            }

            if (list.Any())
            {
                var (resultFood, messageFood) = await _e.InsertRange(list, user);
                m.Msg = resultFood ? "تعداد " + count + " نفر با غذای پیش فرض به لیست افزوده شد." : messageFood;
                m.Result = resultFood;
                m.Type = resultFood ? "success" : "error";
            }
            else
            {
                m.Msg = "مورد جدیدی برای اضافه شدن به جدول موقت وجود نداشت.";
                m.Result = false;
                m.Type = "warning";
            }


            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> __DelayedTempList()
        {
            const string includeProperties = @"TblPerson,TblRestaurant.TblRestaurantFoodPrograms";

            var user = Convert.ToInt32(User.Identity.Name);
            var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
                a => a.TblFoodReservationGroup.Temp && a.TblFoodReservationGroup.User == user && a.TblFoodReservationGroup.Type == 3, includeProperties: includeProperties);
            //  var res = list.First().Restaurant;
            var meals = list.Select(s => s.Meal).Distinct().OrderBy(a => a).ToList();
            var listMeals = new List<Item>();
            foreach (var item in meals)
            {
                var i = new Item
                {
                    Id = item,
                    Title = MealTitle(item)
                };
                listMeals.Add(i);
            }
            ViewBag.Meals = listMeals;
            var temp = list.GroupBy(g => new { g.Date, g.ItemNumber }).Select(a => new TempModel
            {

                DateEn = a.First().Date,
                Person = a.First().Person,
                Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd"),
                Name = a.First().TblPerson.Name,
                Family = a.First().TblPerson.Family,
                NationalCode = a.First().TblPerson.NationalCode,
                PersonnelCode = a.First().TblPerson.PersonnelCode,
                Meals = a.GroupBy(g => g.Meal).Select(b => new TempModel.MealsModel
                {
                    Id = b.First().Id,
                    Meal = b.First().Meal,
                    Food = b.First().Food,
                    Programs = RestaurantFoodItems(a.First().Date, b.First().Meal, restaurant: a.First().Restaurant)
                }).ToList()
            }).ToList();
            var countPerson = temp.Count() / list.GroupBy(g => new { g.Date }).Count();
            ViewBag.CountPerson = countPerson;
            return Json(new { PartialView = ConvertViewToString("LetterTempList", temp), countPerson }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [Authorize(Roles = "164")]
        public async Task<ActionResult> DelayedSave(int officeDepartment, int office, string message,string des, bool lockUserOfficeDepartment = false, bool lockUserOffice = false)
        {
            var m = new Message();
            try
            {
                var user = Convert.ToInt32(User.Identity.Name);

                if (await _unitOfWork.GetRepository<TblFoodReservationGroup>().Any(a => a.User == user && a.Temp && a.Type == 3))
                {
                    var group = await _unitOfWork.GetRepository<TblFoodReservationGroup>()
                        .GetFirst(a => a.User == user && a.Temp && a.Type == 3, includeProperties: "TblOfficeDepartmentAccountNumber.TblOfficeDepartment");
                    group.Des = des;
                    foreach (var item in await _unitOfWork.GetRepository<TblFoodReservation>()
                        .Get(a => a.Group == group.Id && a.Food != -1, includeProperties: "TblPerson"))
                    {

                        var (res, ms) = await ReviewRestrictions(item, office, 1, group.Type);
                        if (!res)
                        {
                            var msg = string.Format(Resources.Public.AddErrorMessage,
                                "درخواست غذا برای " + item.TblPerson.Fullname() + " تاریخ " +
                                new PersianDateTime(item.Date)
                                    .ToLongDateString() +
                                " وعده " + MealTitle(item.Meal),
                                ms);
                            m.Msg = msg;
                            m.Result = false;
                            m.Type = "error";
                            goto result;

                        }
                        var (contractResult, contractFood, contractMessage) = await CalculateQuota(item, office);
                        if (contractResult)
                        {
                            //مقدار دهی فیلد های مورد نیاز
                            item.Contract = contractFood.Contract;

                        }
                        else
                        {
                            m.Msg = contractMessage;
                            m.Result = false;
                            m.Type = "error";
                            return Json(m, JsonRequestBehavior.AllowGet);
                        }
                        //var contractFood = await CalculateQuota(item, office);

                        ////مقدار دهی فیلد های مورد نیاز

                        //item.Contract = contractFood.Contract;
                    }

                    if (await _unitOfWork.GetRepository<TblFoodReservation>()
                        .Any(a => a.Group == group.Id && a.Food != -1))
                    {
                        var an = await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>()
                       .GetFirst(a => a.OfficeDepartment == officeDepartment, includeProperties: "TblOfficeDepartment");
                        if(an != null)
                        {
                            group.AccountNumber = an.Id;
                            group.Des = "AC;"+officeDepartment+";"+office +";"+des;
                        }
                        else
                        {
                            m.Msg = string.Format(Resources.Public.AddErrorMessage, "درخواست رزرو غذا", "واحد انتخابی شماره حساب در سیستم ندارد.");
                            m.Result = false;
                            m.Type = "error";
                            goto result;
                        }
                        group.Temp = false;
                        var (resultGroup, messageGroup) =
                            await _unitOfWork.GetRepository<TblFoodReservationGroup>().Update(group, user);
                        if (resultGroup)
                        {
                            var listUsers = new List<TblUser>();
                            if (lockUserOffice)
                            {
                                listUsers = (await _unitOfWork.GetRepository<TblUser>()
                                    .Get(a => a.TblPerson.TblOfficeDepartment.Office == office && (a.Group == 7 || a.Group ==8))).ToList();
                            }
                            else if (lockUserOfficeDepartment)
                            {
                                listUsers = (await _unitOfWork.GetRepository<TblUser>()
                                    .Get(a => a.TblPerson.OfficeDepartment == officeDepartment && (a.Group == 7 || a.Group == 8))).ToList();
                            }

                            if (listUsers.Any())
                            {
                                foreach (var item in listUsers)
                                {
                                    var userMessage = new TblUserMessage
                                    {
                                        Body = message,
                                        CreateDate = DateTime.Now,
                                        CreateUser = user,
                                        Status = 0,
                                        Title = "ثبت درخواست معوقه",
                                        User = item.Id
                                    };
                                    item.FoodReservationLock = true;
                                    item.ImportantMessage = true;
                                    var (resultMessage, msg) = await _unitOfWork.GetRepository<TblUserMessage>()
                                        .Insert(userMessage, user);
                                }
                            }

                            var (resultDelete, countDelete, messageDelete) = await _unitOfWork
                                    .GetRepository<TblFoodReservation>()
                                    .DeleteRange(a => a.Food == -1 && a.Group == group.Id, user);
                            m.Msg = string.Format(Resources.Public.AddSuccessMessage, "درخواست رزرو غذا با شناسه " + group.Id);
                            m.Result = true;
                            m.Type = "success";
                        }
                        else
                        {
                            m.Msg = string.Format(Resources.Public.AddErrorMessage, "درخواست رزرو غذا", messageGroup);
                            m.Result = false;
                            m.Type = "error";
                        }


                    }
                    else
                    {
                        m.Result = false;
                        m.Type = "warning";
                        m.Msg = Resources.Public.AddWarning;
                    }
                }
            }
            catch (Exception e)
            {
#pragma warning disable 4014
                Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
#pragma warning restore 4014
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }
            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Not Use
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="restaurants"></param>
        /// <param name="officeDepartment"></param>
        /// <param name="lockUserOfficeDepartment"></param>
        /// <param name="lockUserOffice"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Delayed(DateTime startDate, DateTime endDate, int restaurants, int officeDepartment,
            bool lockUserOfficeDepartment, bool lockUserOffice)
        {
            var m = new Message();
            try
            {
                if (ModelState.IsValid)
                {
                    var an = await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>()
                        .GetFirst(a => a.OfficeDepartment == officeDepartment, includeProperties: "TblOfficeDepartment");
                    var list = new List<TblFoodReservation>();
                    var listStatus = new List<TblFoodReservationStatusHistory>();
                    var userId = Convert.ToInt32(User.Identity.Name);
                    var user = await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == userId,
                        includeProperties: "TblPerson,TblPerson.TblOfficeDepartment");
                    // var person = user.Person;
                    foreach (var item in Request.Form.AllKeys.Where(a => a.Contains("Count-")))
                    {
                        var f = item.Split('-');
                        if (!string.IsNullOrEmpty(Request.Form[item]))
                        {
                            var num = Convert.ToInt32(Request.Form[item]);
                            var meal = Convert.ToInt32(f[1]);

                            if (num > 0)
                            {
                                for (var i = startDate; i <= endDate; i = i.AddDays(1))
                                {
                                    if (meal == 0)
                                    {
                                        var contract = await _unitOfWork.GetRepository<TblContract>().GetFirst(a =>
                                            a.TblContractRestaurants.Any(b => b.Restaurant == restaurants)
                                            && a.StartDate <= i && a.EndDate >= i.Date
                                            && a.Status == 1);
                                        var entity = new TblFoodReservation
                                        {
                                            Date = i,
                                            Food = 0,
                                            Meal = 0,
                                            Restaurant = restaurants,
                                            Person = -2,
                                            Contract = contract.Id
                                        };
                                        list.Add(entity);
                                    }
                                    else
                                    {
                                        var officeQuotaRemains = await OfficeQuotaRemains(an.TblOfficeDepartment.Office, i, num);
                                        if (!officeQuotaRemains)
                                        {
                                            var msg = string.Format(Resources.Public.AddErrorMessage,
                                                "درخواست غذا " +
                                                new PersianDateTime(i)
                                                    .ToLongDateString() +
                                                " وعده " + MealTitle(meal),
                                                Resources.Public.OfficeQuotaRemains);
                                            m.Msg = msg;
                                            m.Result = false;
                                            m.Type = "error";
                                            return Json(m, JsonRequestBehavior.AllowGet);

                                        }
                                        var food = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().GetFirst(
                                            a => a.Date == i && a.Restaurant == restaurants &&
                                                 a.Meal == meal);
                                        for (var j = 0; j < num; j++)
                                        {
                                            var entity = new TblFoodReservation
                                            {
                                                Date = i,
                                                Meal = meal,
                                                Restaurant = restaurants,
                                                Person = -2
                                            };

                                            if (food != null)
                                                entity.Food = food.Food;
                                            else
                                            {
                                                var msg = string.Format(Resources.Public.AddErrorMessage,
                                                    "درخواست غذا " +
                                                    new PersianDateTime(entity.Date)
                                                        .ToLongDateString() +
                                                    " وعده " + MealTitle(entity.Meal),
                                                    Resources.Public.FoodProgramIsNull);
                                                m.Msg = msg;
                                                m.Result = false;
                                                m.Type = "error";
                                                return Json(m, JsonRequestBehavior.AllowGet);
                                            }
                                            var (contractResult, contractFood, contractMessage) = await CalculateQuota(entity, an.TblOfficeDepartment.Office);
                                            if (contractResult)
                                            {
                                                //مقدار دهی فیلد های مورد نیاز
                                                entity.Contract = contractFood.Contract;
                                                list.Add(entity);
                                            }
                                            else
                                            {
                                                m.Msg = contractMessage;
                                                m.Result = false;
                                                m.Type = "error";
                                                return Json(m, JsonRequestBehavior.AllowGet);
                                            }
                                            //var contractFood =
                                            //    await CalculateQuota(entity, an.TblOfficeDepartment.Office);

                                            ////مقدار دهی فیلد های مورد نیاز
                                            //entity.Contract = contractFood.Contract;
                                            //list.Add(entity);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (list.Count > 0)
                    {
                        //var accountNumber = await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>()
                        //    .GetFirst(a => a.OfficeDepartment == officeDepartment);
                        var foodGroup = new TblFoodReservationGroup
                        {
                            User = userId,
                            AccountNumber = an.Id,
                            Date = DateTime.Now
                        };
                        var (resultGroup, messageGroup) = await _unitOfWork.GetRepository<TblFoodReservationGroup>().Insert(foodGroup, userId);
                        if (resultGroup)
                        {
                            foreach (var item in list)
                            {
                                item.Group = foodGroup.Id;
                            }
                            var (result, message) = await _e.InsertRange(list, userId);
                            if (result)
                            {
                                foreach (var item in list)
                                {
                                    //ثبت وضعیت درخواست غذا
                                    var statusHistory = new TblFoodReservationStatusHistory
                                    {
                                        Reservation = item.Id,
                                        Id = Guid.NewGuid(),
                                        Time = DateTime.Now,
                                        Status = 6,
                                        User = userId
                                    };
                                    listStatus.Add(statusHistory);
                                }


                                await _unitOfWork.GetRepository<TblFoodReservationStatusHistory>().InsertRange(listStatus, userId);
                                await _unitOfWork.SaveAsync();
                                m.Msg = string.Format(Resources.Public.AddSuccessMessage, "درخواست رزرو غذا");

                                ////قفل کردن کاربران
                                //if (lockUserOffice)
                                //{
                                //    var user = await _unitOfWork.GetRepository<TblUser>().Get(a=>a.TblPerson.TblUsers = off)
                                //}
                                //else if (lockUserOfficeDepartment)
                                //{

                                //}
                            }
                            else
                            {
                                m.Msg = string.Format(Resources.Public.AddErrorMessage, "درخواست رزرو غذا", message);
                            }
                            m.Result = result;
                            m.Type = result ? "success" : "error";
                        }
                        else
                        {
                            m.Msg = string.Format(Resources.Public.AddErrorMessage, "درخواست رزرو غذا", messageGroup);
                            m.Result = false;
                            m.Type = "error";
                        }
                    }
                    else
                    {
                        m.Result = false;
                        m.Type = "warning";
                        m.Msg = Resources.Public.AddWarning;
                    }


                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public async Task<ActionResult> Delayed1(int office, int officeDepartment, DateTime date, int count, List<int> person, List<int> meal, int restaurant, int food)
        {
            var listOfficeRestaurant =
                (await _unitOfWork.GetRepository<TblRestaurant>().Get(
                    a => !a.TblOfficeRestaurants.Where(b => b.OfficeDepartment == officeDepartment && a.Status == 1)
                        .Select(b => b.Restaurant).Contains(a.Id), includeProperties: "TblOfficeRestaurants")).ToList();
            ViewBag.OfficeTitle = (await _unitOfWork.GetRepository<TblOffice>().GetSingle(a => a.Id == office)).Title;
            ViewBag.OfficeDepartmentTitle = (await _unitOfWork.GetRepository<TblOfficeDepartment>().GetSingle(a => a.Id == officeDepartment)).Title;
            ViewBag.Office = office;
            ViewBag.OfficeDepartment = officeDepartment;
            ViewBag.DateTitle = new PersianDateTime(date).ToLongDateString();
            ViewBag.Date = date;
            ViewBag.Restaurant = new SelectList(listOfficeRestaurant, "Id", "Title", restaurant);
            ViewBag.RestaurantSelected = restaurant;
            ViewBag.Meal = await _unitOfWork.GetRepository<TblMeal>().Get(a => meal.Contains(a.Id));
            ViewBag.Foods = new List<TblFood>();
            ViewBag.Food = food;
            ViewBag.Count = count;
            return View("Delayed_Post");
        }

        private static List<DateTime> GetListDate(DateTime startDate, DateTime endDate)
        {
            var list = new List<DateTime>();
            for (var i = startDate; i <= endDate; i = i.AddDays(1))
            {
                list.Add(i);
            }

            return list;
        }

        [Authorize(Roles = "163")]
        public async Task<ActionResult> Letter()
        {
            var user = Convert.ToInt32(User.Identity.Name);
            var us = _db.TblUsers.Find(user);
            ViewBag.UserGroup = us.Group;
            //if (us.FoodReservationLock)
            //{
            //    TempData["errorToast"] = "حساب کاربری شما قفل می باشد لطفا به پیام ها مراجعه نمایید.";
            //    return RedirectToAction("Index", "Home",new { area = "" });
            //}
            if ( _unitOfWork.GetRepository<TblFoodReservationGroup>().AnySync(a => a.User == user && a.Temp && a.Type == 2))
            {
                var group = await _unitOfWork.GetRepository<TblFoodReservationGroup>()
                    .GetFirst(a => a.User == user && a.Temp && a.Type == 2);

               await  _unitOfWork.GetRepository<TblFoodReservation>().DeleteRange(a => a.Group == group.Id, user);
               await  _unitOfWork.GetRepository<TblFoodReservationGroup>().Delete(group.Id, user);
            }
            return View();
        }

        [HttpPost]
        public ActionResult AddLetterItemsToTempList(int count, DateTime endDate, DateTime startDate, int accountNumber, int restaurant, List<int> meals)
        {
            
            var groups = new List<int> { 1, 4, 5, 6 };
            var m = new Message();
            try
            {
                var user = Convert.ToInt32(User.Identity.Name);
                var list = new List<TblFoodReservation>();

                ////بررسی اینکه درخواست موقتی وجود دارد یا خیر
                ////اگر وجود داشت  درخواست قبلی حذف می شود
                //if ( _unitOfWork.GetRepository<TblFoodReservationGroup>().AnySync(a => a.User == user && a.Temp && a.Type == 2))
                //{
                //    var gd = await _unitOfWork.GetRepository<TblFoodReservationGroup>()
                //        .GetFirst(a => a.User == user && a.Temp && a.Type == 2);

                //    await _unitOfWork.GetRepository<TblFoodReservation>().DeleteRange(a => a.Group == gd.Id, user);
                //    await _unitOfWork.GetRepository<TblFoodReservationGroup>().Delete(gd.Id, user);

                //}

                var restaurantInfo = _unitOfWork.GetRepository<TblRestaurant>().GetSingleSync(a => a.Id == restaurant);

                var group = new TblFoodReservationGroup
                {
                    Temp = true,
                    User = user,
                    AccountNumber = accountNumber,
                    Date = DateTime.Now,
                    Type = 2
                };
                group.Confirm = !restaurantInfo.ConfirmReservation;
                var (resultGroup, messageGroup) =
                    _unitOfWork.GetRepository<TblFoodReservationGroup>().InsertSync(group, user);

                
                    for (var i = startDate; i <= endDate; i = i.AddDays(1))
                    {
                        foreach (var meal in meals)
                        {
                            var defaultFood = _unitOfWork.GetRepository<TblRestaurantFoodProgram>().GetSingleSync(a =>
                                a.Restaurant == restaurant && a.Meal == meal && a.Date == i && a.Default);
                            for (var j = 0; j < count; j++)
                            {
                                if (meal == 0 || defaultFood != null)
                                {

                                    var entity = new TblFoodReservation
                                    {
                                        Date = i,
                                        Food = meal == 0 ? 0 : defaultFood.Food,
                                        Meal = meal,
                                        Restaurant = restaurant,
                                        Person = -2,
                                        Group = group.Id,
                                        Contract = -1,
                                        ItemNumber = j,
                                        //Confirm = !restaurantInfo.ConfirmReservation
                                    };
                                    list.Add(entity);
                                }
                                else
                                {
                                    m.Msg = Resources.Public.FoodProgramIsNull;
                                    m.Result = false;
                                    m.Type = "error";
                                    goto result;
                                }
                            }
                            //  }

                        }
                    }

                

                if (list.Any())
                {
                    var (resultFood, messageFood) = _e.InsertRangeSync(list, user);
                    m.Msg = resultFood
                       // ? $" تعداد {count} نفر با غذای پیش فرض به درخواست  {group.Id} افزوده شد."
                        ? $" تعداد {count} نفر با غذای پیش فرض به درخواست افزوده شد."
                        : messageFood;
                    m.Id = group.Id;
                    m.Result = resultFood;
                    m.Type = resultFood ? "success" : "error";
                }
                else
                {
                    m.Msg = "مورد جدیدی برای اضافه شدن به جدول موقت وجود نداشت.";
                    m.Result = false;
                    m.Type = "warning";
                    m.Id = group.Id;
                }
            }
            catch (Exception e)
            {
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
                goto result;
            }

            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> LetterTempList()
        {
            const string includeProperties = @"TblPerson,TblRestaurant.TblRestaurantFoodPrograms";

            var user = Convert.ToInt32(User.Identity.Name);
            var list =  _unitOfWork.GetRepository<TblFoodReservation>().GetSync(
                a => a.TblFoodReservationGroup.Temp && a.TblFoodReservationGroup.User == user && a.TblFoodReservationGroup.Type == 2, includeProperties: includeProperties);
            //  var res = list.First().Restaurant;
            var meals = list.Select(s => s.Meal).Distinct().OrderBy(a => a).ToList();
            var listMeals = new List<Item>();
            foreach (var item in meals)
            {
                var i = new Item
                {
                    Id = item,
                    Title = MealTitle(item)
                };
                listMeals.Add(i);
            }
            ViewBag.Meals = listMeals;
            var temp = list.GroupBy(g => new { g.Date, g.ItemNumber }).Select(a => new TempModel
            {

                DateEn = a.First().Date,
                Person = a.First().Person,
                Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd"),
                Name = a.First().TblPerson.Name,
                Family = a.First().TblPerson.Family,
                NationalCode = a.First().TblPerson.NationalCode,
                PersonnelCode = a.First().TblPerson.PersonnelCode,
                Meals = a.GroupBy(g => g.Meal).Select(b => new TempModel.MealsModel
                {
                    Id = b.First().Id,
                    Meal = b.First().Meal,
                    Food = b.First().Food,
                    Programs = RestaurantFoodItems(a.First().Date, b.First().Meal, restaurant: a.First().Restaurant)
                }).ToList()
            }).ToList();
            var countPerson = temp.Count() / list.GroupBy(g => new { g.Date }).Count();
            ViewBag.CountPerson = countPerson;
            return Json(new { PartialView = ConvertViewToString("LetterTempList", temp), countPerson }, JsonRequestBehavior.AllowGet);

        }
        public async Task<JsonResult> DelayedTempList()
        {
            const string includeProperties = @"TblPerson,TblRestaurant.TblRestaurantFoodPrograms";

            var user = Convert.ToInt32(User.Identity.Name);
            var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
                a => a.TblFoodReservationGroup.Temp && a.TblFoodReservationGroup.User == user && a.TblFoodReservationGroup.Type == 3, includeProperties: includeProperties);
            //  var res = list.First().Restaurant;
            var meals = list.Select(s => s.Meal).Distinct().OrderBy(a => a).ToList();
            var listMeals = new List<Item>();
            foreach (var item in meals)
            {
                var i = new Item
                {
                    Id = item,
                    Title = MealTitle(item)
                };
                listMeals.Add(i);
            }
            ViewBag.Meals = listMeals;
            var temp = list.GroupBy(g => new { g.Date, g.ItemNumber }).Select(a => new TempModel
            {

                DateEn = a.First().Date,
                Person = a.First().Person,
                Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd"),
                Name = a.First().TblPerson.Name,
                Family = a.First().TblPerson.Family,
                NationalCode = a.First().TblPerson.NationalCode,
                PersonnelCode = a.First().TblPerson.PersonnelCode,
                Meals = a.GroupBy(g => g.Meal).Select(b => new TempModel.MealsModel
                {
                    Id = b.First().Id,
                    Meal = b.First().Meal,
                    Food = b.First().Food,
                    Programs = RestaurantFoodItems(a.First().Date, b.First().Meal, restaurant: a.First().Restaurant)
                }).ToList()
            }).ToList();
            var countPerson = temp.Count() / list.GroupBy(g => new { g.Date }).Count();
            ViewBag.CountPerson = countPerson;
            return Json(new { PartialView = ConvertViewToString("LetterTempList", temp), countPerson }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [Authorize(Roles = "163")]
        public async Task<ActionResult> LetterSave(DateTime date, string subject, int count, string number, int groupId, string des, int officeFor = 0)
        {
            var m = new Message();
            try
            {
                var user = Convert.ToInt32(User.Identity.Name);

                if (await _unitOfWork.GetRepository<TblFoodReservationGroup>().Any(a => a.User == user && a.Temp && a.Type == 2))
                {
                    var group = await _unitOfWork.GetRepository<TblFoodReservationGroup>()
                        .GetFirst(a => a.Id == groupId, includeProperties: "TblOfficeDepartmentAccountNumber.TblOfficeDepartment");
                    group.Des = des;
                    foreach (var item in await _unitOfWork.GetRepository<TblFoodReservation>()
                        .Get(a => a.Group == group.Id, includeProperties: "TblPerson"))
                    {
                        if (item.Food == -1)
                        {
                            continue;
                        }
                        var (res, ms) = await ReviewRestrictions(item, group.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office, 1);
                        if (!res)
                        {
                            var msg = string.Format(Resources.Public.AddErrorMessage,
                                "درخواست غذا برای " + item.TblPerson.Fullname() + " تاریخ " +
                                new PersianDateTime(item.Date)
                                    .ToLongDateString() +
                                " وعده " + MealTitle(item.Meal),
                                ms);
                            m.Msg = msg;
                            m.Result = false;
                            m.Type = "error";
                            goto result;

                        }
                        var (contractResult, contractFood, contractMessage) = await CalculateQuota(item, group.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office);
                        if (contractResult)
                        {
                            //مقدار دهی فیلد های مورد نیاز
                            item.Contract = contractFood.Contract;

                        }
                        else
                        {
                            m.Msg = contractMessage;
                            m.Result = false;
                            m.Type = "error";
                            goto result;
                        }
                        //var contractFood = await CalculateQuota(item, group.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office);

                        ////مقدار دهی فیلد های مورد نیاز

                        //item.Contract = contractFood.Contract;
                    }

                    if (await _unitOfWork.GetRepository<TblFoodReservation>()
                        .Any(a => a.Group == group.Id && a.Food != -1))
                    {
                        var l = new TblFoodReservationLetter
                        {
                            Count = count,
                            Date = date,
                            Number = number,
                            Office = group.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office,
                            OfficeFor = officeFor,
                            Subject = subject,
                            User = user
                        };
                        var (resultLetter, messageLetter) = await _unitOfWork.GetRepository<TblFoodReservationLetter>().Insert(l, user);
                        if (resultLetter)
                        {
                            group.Temp = false;
                            group.Letter = l.Id;

                            group.Temp = false;
                            var (resultGroup, messageGroup) =
                                await _unitOfWork.GetRepository<TblFoodReservationGroup>().Update(group, user);
                            if (resultGroup)
                            {
                                var (resultDelete, countDelete, messageDelete) = await _unitOfWork
                                    .GetRepository<TblFoodReservation>()
                                    .DeleteRange(a => a.Food == -1 && a.Group == group.Id, user);
                                m.Msg = string.Format(Resources.Public.AddSuccessMessage, "درخواست رزرو غذا با شناسه " + group.Id);
                                m.Result = true;
                                m.Type = "success";
                            }
                            else
                            {
                                m.Msg = string.Format(Resources.Public.AddErrorMessage, "درخواست رزرو غذا", messageGroup);
                                m.Result = false;
                                m.Type = "error";
                            }
                        }
                        else
                        {
                            m.Msg = string.Format(Resources.Public.AddErrorMessage, "درخواست رزرو غذا", messageLetter);
                            m.Result = false;
                            m.Type = "error";
                        }

                    }
                    else
                    {
                        m.Result = false;
                        m.Type = "warning";
                        m.Msg = Resources.Public.AddWarning;
                    }
                }
            }
            catch (Exception e)
            {
                await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }
            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        //[OutputCache(Duration = 6)]
        public async Task<JsonResult> RestaurantFood(DateTime date, int meal, int restaurant)
        {
            var userId = int.Parse(User.Identity.Name);
            var foods = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().Get(a => a.Restaurant == restaurant && a.Date == date && a.Meal == meal,
                includeProperties: "TblFood");
            var data = foods.OrderByDescending(a => a.Default).Select(a => new
            {
                Id = a.Food,
                Title = a.TblFood.Title
            });
            // var result = (await iResult.Get(a => a.Office == id)).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        [OutputCache(Duration = 1200)]
        public List<Item> RestaurantFoodItems(DateTime date, int meal, int restaurant)
        {
            var userId = int.Parse(User.Identity.Name);
            var foods = _unitOfWork.GetRepository<TblRestaurantFoodProgram>().GetSync(a => a.Restaurant == restaurant && a.Date == date && a.Meal == meal,
                includeProperties: "TblFood");
            var data = foods.OrderByDescending(a => a.Default).Select(a => new Item
            {
                Id = a.Food,
                Title = a.TblFood.Title
            });
            // var result = (await iResult.Get(a => a.Office == id)).ToList();
            return data.ToList();

        }
        //[OutputCache(Duration = 600)]
        //دریافت لیست رستوران های مجاز برای اداره - واحد
        public async Task<JsonResult> OfficeDepartmentRestaurant(int office, int department)
        {
            //لیست رستوران هایی که برای اداره یا اداره - واحد مجاز نمی باشد
            var listNot = (await _unitOfWork.GetRepository<TblOfficeRestaurant>()
                    .Get(b => ((b.Office == office && b.OfficeDepartment == null) || b.OfficeDepartment == department) && b.TblRestaurant.Status == 1))
                .Select(b => b.Restaurant);

            //لیست رستوران های مجاز
            var listOfficeRestaurant = await _unitOfWork.GetRepository<TblRestaurant>().Get(a => !listNot.Contains(a.Id), includeProperties: "TblOfficeRestaurants");



            var data = listOfficeRestaurant.Select(a => new
            {
                Id = a.Id,
                Title = a.Title
            });

            return Json(data, JsonRequestBehavior.AllowGet);

        }
        //[OutputCache(Duration = 600)]
        public async Task<JsonResult> PersonOffice(int officeDepartment)

        {
            var userId = int.Parse(User.Identity.Name);
            var (persons, _, _) = await _unitOfWork.GetRepository<TblPerson>().GetOrdered(a =>
                   a.OfficeDepartment == officeDepartment && a.Disable == false
                 , orderBy: "Family asc");


            var data = persons.Select(a => new
            {
                Id = a.Id,
                Title = a.Fullname() + " - " + a.PersonnelCode + " - " + a.NationalCode
            });
            return Json(data, JsonRequestBehavior.AllowGet);

        }

        [OutputCache(Duration = 60000,VaryByParam ="id")]
        public  string MealTitle(int id)
        {
            return _db.TblMeals.Single(a => a.Id == id).Title;
            
        }


    }
}