﻿using System.Web.Mvc;

namespace Omidiyeh_Oil_Company___0147.Areas.Store
{
    public class StoreAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Store";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Store_default",
                "Store/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Omidiyeh_Oil_Company___0147.Areas.Store.Controllers" }
            );
        }
    }
}