﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using MD.PersianDateTime;
using System.Web.WebPages;
using Microsoft.Ajax.Utilities;
using Stimulsoft.Report.Dictionary;
using System.Data.Entity.Migrations;
using System.Linq.Expressions;

namespace Omidiyeh_Oil_Company___0147.Areas.Store.Controllers
{

    public class EntryDocumentsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public EntryDocumentsController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }


        
        [AllowAnonymous]
        public bool DoesRequestIdExist(string requestId)
        {
            return _unitOfWork.GetRepository<TblEntryDocument>().AnySync(a => a.RequestId == requestId);
        }

        // GET: Store/EntryDocuments
        public ActionResult Index()
        {
            return View();
        }

        public  JsonResult List(SearchAdvancedDocument model)
        {
            model.EndDate = model.EndDate.AddDays(1);
            Expression<Func<TblEntryDocument, bool>> filter = (a) =>
                    (model.Product == null || a.TblEntryDocumentProducts.Any(p=>p.Id == model.Product) ) &&
                    (model.Store == 0 || a.Store== model.Store) &&
                    (model.StartDate.Year == 1 || a.Date >= model.StartDate && a.Date <= model.EndDate) &&
                    (string.IsNullOrEmpty(model.Des) || a.Des.Contains(model.Des)) &&
                    (string.IsNullOrEmpty(model.Des) || a.RequestReason.Contains(model.Des)) &&
                    a.Type &&
                    a.Remove == false
                ;
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblEntryDocument>();
            const string includeProperties = "TblStore,TblUser.TblPerson,TblStore1,TblPerson,TblEntryDocumentProducts";
            var result = iResult.GetOrderedSyncNoTracking(filter: filter,
                orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) =  result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                Date = new PersianDateTime(a.Date).ToString("yyyy/MM/dd"),
                User = a.TblUser.TblPerson.Fullname(),
                a.Des,
                a.Count,
                Store = a.TblStore1.Title,
                a.RequestId,
                RequestDate = new PersianDateTime(a.RequestDate).ToString("yyyy/MM/dd"),
                Sum = a.TblEntryDocumentProducts.Where(w=>w.Remove == false).Sum(s=>s.Count),

                a.RequestReason,
                edit = $"<a href='/Store/EntryDocuments/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-lg'></i></a> " +
                      $"<a href='/Store/EntryDocuments/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-lg'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListProduct(long id)
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = 10000; //Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 1000 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblEntryDocumentProduct>();
            var result = 
                    iResult.GetOrderedSyncNoTracking(a => a.EntryDocument == id && a.Remove == false, orderBy: order, take: length, skip: start, includeProperties: "TblProduct,TblProduct.TblProductUnit");
            var (items, count, total) =  result;

           
            
           var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                ProductId = a.TblProduct.Id,
                a.TblProduct.Title,
                Unit= a.TblProduct.TblProductUnit.Title,
              Code =  a.Code,
                Count = a.Count

            }).ToList();
            
         
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = "10000",
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }




        // GET: Store/EntryDocuments/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddProduct(long product, int count, long id, string code, string name)
        {
            
            var user = int.Parse(User.Identity.Name);
            //var exist = _unitOfWork.GetRepository<TblEntryDocumentProduct>()
            //    .AnySync(a => a.EntryDocument == id && a.Product == product && a.Code.IsNullOrWhiteSpace());
            //if (exist)
            //{

            //}
            var m = new Message();
            var list = new List<TblEntryDocumentProduct>();
            if (code.IsNullOrWhiteSpace())
            {

                    var p = new TblEntryDocumentProduct
                    {
                        Code = "",
                        EntryDocument = id,
                        Product = product,
                        Remove = false,
                        Count = count,
                        Date = DateTime.Now
                    };
                    list.Add(p);
                
            }
            else
            {
                var ramz2 = code.Split('\n');
                for (var i = 0; i < count; i++)
                {
                    var c = string.Empty;
                    if (ramz2.Length > i)
                        c = ramz2[i];
                    var p = new TblEntryDocumentProduct
                    {
                        Code = c,
                        EntryDocument = id,
                        Product = product,
                        Remove = false,Count =1
                    };
                    list.Add(p);
                }
            }
         

            var (result,message) = _unitOfWork.GetRepository<TblEntryDocumentProduct>().InsertRangeSync(list, user);
            m.Result = result;
            m.Msg = result ? "کالای " + name + " با موفقیت به لیست اضافه شد": string.Format(Resources.Public.AddErrorMessage, name, message); ;
            m.Type =result ? "success": "error";
           // result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        // POST: Store/EntryDocuments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TblEntryDocument entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (DoesRequestIdExist(entity.RequestId))
                    {
                        TempData["errorToast"] = string.Format(Resources.Public.CreateErrorMessage, "سند ورود", "شماره درخواست تکراری می باشد");
                        return View(entity);
                    }
                    var user = int.Parse(User.Identity.Name);
                    entity.Temp = false;
                    entity.Type = true;
                    entity.Date = DateTime.Now;
                    entity.User = user;
                    entity.Count = 0;
                    var (result, message) = await _unitOfWork.GetRepository<TblEntryDocument>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successToast"] = string.Format(Resources.Public.AddSuccessMessage, "ایجاد سند ورود");
                        return RedirectToAction("Edit",new {id = entity.Id});
                    }
                    TempData["errorToast"] = string.Format(Resources.Public.CreateErrorMessage, entity.Id, message);
                    return RedirectToAction("Index");
                }


            }
            catch (Exception e)
            {
                TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Store/EntryDocuments/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
         
            var result = _unitOfWork.GetRepository<TblEntryDocument>().GetSingleSync(a => a.Id == id , includeProperties: "TblStore1");
            if (result == null)
            {
                return HttpNotFound();
            }


            return View(result);
        }

        // POST: Store/EntryDocuments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit( TblEntryDocument entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var iResult = _unitOfWork.GetRepository<TblEntryDocument>();
                    var ed = await iResult.GetSingle(a => a.Id == entity.Id);
                    ed.RequestDate = entity.RequestDate;
                    ed.RequestId = entity.RequestId;
                    ed.RequestReason = entity.RequestReason;
                    ed.Des = entity.Des;
                   // ed.Store = entity.Store;
                    var (result, message) = await _unitOfWork.GetRepository<TblEntryDocument>().Update(ed, user);
                    if (result)
                    {
                        TempData["successToast"] = string.Format(Resources.Public.EditSuccessMessage, entity.Id);
                        return RedirectToAction("Index");
                    }
                    TempData["errorToast"] = string.Format(Resources.Public.EditErrorMessage, entity.Id, message);
                    return RedirectToAction("Index");
                }
                
            }
            catch (Exception e)
            {
               TempData["errorToast"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }
        [HttpPost]
        public JsonResult DeleteProduct(int id)
        {
            var m = new Message();

            var item = _unitOfWork.GetRepository<TblEntryDocumentProduct>().GetSingleSync(a => a.Id == id);
            item.Remove = true;

            var user = int.Parse(User.Identity.Name);
            var (result, message) = _unitOfWork.GetRepository<TblEntryDocumentProduct>().DeleteSoftSync(item, id, user);
            if (result)
            {
                m.Msg = Resources.Public.DeleteSuccessMessage;
                m.Result = true;
                m.Type = "success";
            }
            else
            {
                m.Msg = string.Format(Resources.Public.DeleteErrorMessage, message);
                m.Result = false;
                m.Type = "error";
            }


            return Json(m, JsonRequestBehavior.AllowGet);

        } 
        [HttpPost]
        public JsonResult Delete(int id)
        {
            var m = new Message();

            var item = _unitOfWork.GetRepository<TblEntryDocument>().GetSingleSync(a => a.Id == id);
            item.Remove = true;

            var user = int.Parse(User.Identity.Name);
            var (result, message) = _unitOfWork.GetRepository<TblEntryDocument>().DeleteSoftSync(item, id, user);
            if (result)
            {
                m.Msg = Resources.Public.DeleteSuccessMessage;
                m.Result = true;
                m.Type = "success";
            }
            else
            {
                m.Msg = string.Format(Resources.Public.DeleteErrorMessage, message);
                m.Result = false;
                m.Type = "error";
            }


            return Json(m, JsonRequestBehavior.AllowGet);

        }
        // GET: Store/EntryDocuments/Delete/5
        public async Task<ActionResult> _Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblEntryDocument>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Store/EntryDocuments/Delete/5
      
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> _DeleteConfirmed(long id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblEntryDocument>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }
        //1
        public string OldImportDocument()
        {
            var msg = "";
            try
            {
                var ndb = new negahdashtEntities();
                var old = ndb.tblInDocs
                    .ToList();
                var list = new List<TblEntryDocument>();
                
                foreach (var item in old)
                {
                    var doc = new TblEntryDocument
                    {
                        OldId = item.ID,
                        RequestDate = item.inDate ?? DateTime.Now,
                        OldUserId = item.userID,
                        Des = item.description,
                        Date = item.cDate ?? DateTime.Now,
                        Remove = item.deleted ?? false,
                        DocumentType = Convert.ToByte(item.doceTypeId ?? 0),
                        RequestId = item.DocNumber.ToString(),
                        Store = item.DocStore ?? 0,
                        Temp = false,
                        Type = true,
                        User = -1,
                        Count = 0
                    };


                    list.Add(doc);
                }

                var db = new dbEntities();
                db.TblEntryDocuments.AddRange(list);
                db.SaveChanges();
                return list.Count.ToString();
            }
            catch (DbEntityValidationException ve)
            {
                msg += ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList() + "--";


                msg += ve.InnerException?.InnerException?.Message + "--";
                msg += ve.Message;
            }
            catch (Exception e)
            {

                msg += e.InnerException?.InnerException?.Message + "--";
                msg += e.Message;

            }
            return msg;
        } 
        
       
        //2
        public string OldImportDocumentProduct()
        {
            var msg = "";
            try
            {
               
                var ndb = new negahdashtEntities();
                var old = ndb.tblInGoods
                    .Where(a=>a.tblGood.show == true)
                    .ToList();
                var list = new List<TblEntryDocumentProduct>();
                var db = new dbEntities();
                foreach (var item in old)
                {
                    var p = new TblEntryDocumentProduct
                    {
                        OldId = item.ID,
                      Product = item.goodID ?? 0,
                      EntryDocument = db.TblEntryDocuments.AsNoTracking().FirstOrDefault(a=> a.Type && a.OldId == item.docID)?.Id ?? 0  ,
                      Count = Convert.ToInt32(item.gCount),
                      Remove = item.deleted ?? false,
                      Date = item.cDate ?? DateTime.Now,

                    };
                    list.Add(p);
                }

                
                db.TblEntryDocumentProducts.AddRange(list);
                db.SaveChanges();
                return list.Count.ToString();
            }
            catch (DbEntityValidationException ve)
            {
                msg += ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList() + "--";


                msg += ve.InnerException?.InnerException?.Message +"--";
                msg +=  ve.Message;
            }
            catch (Exception e)
            {
              
                msg += e.InnerException?.InnerException?.Message + "--";
                msg += e.Message;

            }
            return msg;
        }
        //2-1
        public string OldImportDocumentDeleteEmpty()
        {
            var msg = "";
            try
            {
                var db = new dbEntities();
                var list = db.TblEntryDocuments.Where(a => !a.TblEntryDocumentProducts.Any());
                var count = list.Count();
                foreach (var item in list)
                {
                    item.Remove = true;
                }
               // db.TblEntryDocuments.RemoveRange(list);
                db.SaveChanges();

                return count.ToString();
            }
            catch (DbEntityValidationException ve)
            {
                msg += ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList() + "--";


                msg += ve.InnerException?.InnerException?.Message + "--";
                msg += ve.Message;
            }
            catch (Exception e)
            {

                msg += e.InnerException?.InnerException?.Message + "--";
                msg += e.Message;

            }
            return msg;
        }
        //4
        public string OldImportDocumentRequestProduct(int id)
        {
            var msg = "";
            try
            {
               
                var ndb = new negahdashtEntities();
                var old = ndb.tblReqGoods.OrderBy(a=>a.ID)
                    .Where(a => a.tblGood.show == true)
                   // .Skip((id - 1) * 10000)
                 // .Take(10000)
                    .ToList();
                var list = new List<TblEntryDocumentProduct>();
                var db = new dbEntities();
                foreach (var item in old)
                {
                    var p = new TblEntryDocumentProduct
                    {
                        OldId = item.ID,
                      Product = item.goodID ?? 0,
                      EntryDocument = db.TblEntryDocuments.AsNoTracking().FirstOrDefault(a=> a.Type==false && a.OldId == item.docID)?.Id ?? 0  ,
                      Count = item.assignNr ?? 0,
                      Remove = item.deleted ?? false,
                      Date = item.cDate ?? DateTime.Now,
                      Code = item.goodCode,
                      OldGoodDate = item.goodDate,
                      OldBarname = item.barname,
                      Des = item.GoodDescription,
                      OldStatus = item.status
                    };
                    //db.TblEntryDocumentProducts.Add(p);
                   // db.SaveChanges();
                    list.Add(p);
                }


                db.TblEntryDocumentProducts.AddRange(list
                );
                db.SaveChanges();
                return list.Count.ToString();
            }
            catch (DbEntityValidationException ve)
            {
                msg += ve.EntityValidationErrors.ToList()[0].ValidationErrors.First().ErrorMessage + "--"+
                       ve.EntityValidationErrors.ToList()[0].ValidationErrors.First().PropertyName + "--";


                msg += ve.InnerException?.InnerException?.Message +"--";
                msg +=  ve.Message;
            }
            catch (Exception e)
            {
              
                msg += e.InnerException?.InnerException?.Message + "--";
                msg += e.Message;

            }
            return msg;
        }

     

        //3
        public string OldImportDocumentRequest()
        {
            var msg = "";
            try
            {
                var ndb = new negahdashtEntities();
                var old = ndb.tblRequests.ToList();
                var list = new List<TblEntryDocument>();

                foreach (var item in old)
                {
                    var doc = new TblEntryDocument
                    {
                        OldId = item.ID,
                        RequestDate = item.docDate ?? DateTime.Now,
                        //OldUserId = item.userID,
                        Des = item.Description,
                        Date = item.cDate ?? DateTime.Now,
                        Remove = item.deleted ?? false,
                        DocumentType = 0,
                        RequestId = "0",
                        Store = 0,
                        Temp = false,
                        Type = false,
                        User = -1,
                        Count = 0,
                        OldOfficeId = item.officeID,
                        OldApplicatorID = item.applicatorID,
                        OldEmployeeId = item.employeeID,
                    };


                    list.Add(doc);
                }

                var db = new dbEntities();
                db.TblEntryDocuments.AddRange(list);
                db.SaveChanges();
                return list.Count.ToString();
            }
            catch (DbEntityValidationException ve)
            {
                msg += ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList() + "--";


                msg += ve.InnerException?.InnerException?.Message + "--";
                msg += ve.Message;
            }
            catch (Exception e)
            {

                msg += e.InnerException?.InnerException?.Message + "--";
                msg += e.Message;

            }
            return msg;
        }
        public string OldImportDocumentRequest3()
        {
            var msg = "";
            try
            {
                var ndb = new negahdashtEntities();
                var old = ndb.tblRequests.ToList();
              //  var list = new List<TblEntryDocument>();
              var count = 0;
                var db = new dbEntities();
                foreach (var item in old.Where(a=>a.employeeID != null))
                {
                    var d = db.TblEntryDocuments.First(a => a.OldId == item.ID && a.Type == false);
                    var p = db.TblPersons.FirstOrDefault(a => a.PersonnelCode == item.employeeID.Trim() && a.FamilyRelationship ==0);
                    if (p != null)
                    {
                        d.ToPerson = p.Id;
                        d.ToOffice = p.TblOfficeDepartment.Office;
                        d.ToOfficeDepartment = p.OfficeDepartment;
                    }
                    else
                    {
                        var p2 = db.TblPersons.FirstOrDefault(a => a.NationalCode == item.employeeID.Trim() );
                        if(p2 != null)
                        {
                            d.ToPerson = p2.Id;
                            d.ToOffice = p2.TblOfficeDepartment.Office;
                            d.ToOfficeDepartment = p2.OfficeDepartment;
                        }
                        else
                        {
                            msg += ";" + item.employeeID;

                        }
                    }

                    count++;

                }

              
                //db.TblEntryDocuments.AddOrUpdate(a=> new {a.OldId,a.Type} , list);
                db.SaveChanges();
                return count.ToString() +"---" +msg;
            }
            catch (DbEntityValidationException ve)
            {
                msg += ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList() + "--";


                msg += ve.InnerException?.InnerException?.Message + "--";
                msg += ve.Message;
            }
            catch (Exception e)
            {

                msg += e.InnerException?.InnerException?.Message + "--";
                msg += e.Message;

            }
            return msg;
        }

        public string OldImportDocumentRequest2()
        {
            var msg = "";
            try
            {
                var ndb = new negahdashtEntities();
                var old = ndb.tblRequests.ToList();
                var list = new List<TblEntryDocument>();
                var db = new dbEntities();
                foreach (var item in old)
                {
                    var d = db.TblEntryDocuments.First(a => a.OldId == item.ID && a.Type == false);

                    d.FromStore = item.tblReqGoods.FirstOrDefault()?.tblGood.store ?? 0;


                }


                //db.TblEntryDocuments.AddOrUpdate(a=> new {a.OldId,a.Type} , list);
                db.SaveChanges();
                return list.Count.ToString();
            }
            catch (DbEntityValidationException ve)
            {
                msg += ve.EntityValidationErrors.ToList()[0].ValidationErrors.ToList() + "--";


                msg += ve.InnerException?.InnerException?.Message + "--";
                msg += ve.Message;
            }
            catch (Exception e)
            {

                msg += e.InnerException?.InnerException?.Message + "--";
                msg += e.Message;

            }
            return msg;
        }
        
        public string pp()
        {
            var ndb = new negahdashtEntities();

            var old = ndb.tblReqGoods.OrderBy(a => a.ID)
                //.Skip((5 - 1) * 10000)
                //.Take(10000)
                .Select(a=> (long)( a.goodID ?? 0))
                .ToList();
            var db = new dbEntities();

            var p = db.TblProducts.Select(a => a.Id).ToList();

            var list = old.Except(p);
            //var list2 = p.Except(old);
            //return string.Join(",", list2);
            return string.Join(",", list);
        }
    }
}
