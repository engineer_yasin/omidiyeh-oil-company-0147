﻿using MD.PersianDateTime;
using Microsoft.Ajax.Utilities;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.WebPages;

namespace Omidiyeh_Oil_Company___0147.Areas.Store.Controllers
{
    public class ExitDocumentsController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;

        public ExitDocumentsController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/Reports/license.key");
            Stimulsoft.Base.StiLicense.LoadFromFile(path);
        }
        // GET: Store/ExitDocuments
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult List(SearchAdvancedDocument model)
        {

            Expression<Func<TblEntryDocument, bool>> filter = (a) =>
                    (model.Product == null || a.TblEntryDocumentProducts.Any(p => p.Id == model.Product)) &&
                    (model.Store == 0 || a.Store == model.Store) &&
                    (model.FromStore == 0 || a.FromStore == model.FromStore) &&
                    (model.StartDate.Year == 1 || a.Date >= model.StartDate && a.Date <= model.EndDate) &&
                    (string.IsNullOrEmpty(model.Des) || a.Des.Contains(model.Des)) &&
                    (string.IsNullOrEmpty(model.Des) || a.RequestReason.Contains(model.Des)) &&
                    a.Type == false &&
                    a.Remove == false
                ;
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblEntryDocument>();
            const string includeProperties = "TblStore,TblUser.TblPerson,TblStore1,TblPerson,TblOffice,TblOfficeDepartment,TblEntryDocumentProducts";
            var result = iResult.GetOrderedSyncNoTracking(filter: filter,
                orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                Date = new PersianDateTime(a.Date).ToString("yyyy/MM/dd"),
                User = a.TblUser.TblPerson.Fullname(),
                Des = a.Des.Trim(),
                a.Count,
                Store = a.TblStore1.Title,
                a.RequestId,
                RequestDate = new PersianDateTime(a.RequestDate).ToString("yyyy/MM/dd"),
                FromStore = a.TblStore?.Title ?? "-",
                //FromStore = "",
                Person = a.TblPerson?.Fullname(),
                PersonnelCode=  a.TblPerson?.PersonnelCode,
                To = a.TblPerson?.Fullname() ?? (a.ToOfficeDepartment != null ? a.TblOffice.Title +" - " + a.TblOfficeDepartment.Title :  a.TblOffice?.Title),
               // To ="",
                a.RequestReason,
                Sum = a.TblEntryDocumentProducts.Sum(b=>b.Count)
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListProduct(long id)
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = 10000; //Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 100000 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblEntryDocumentProduct>();
            var result =
                    iResult.GetOrderedSync(a => a.EntryDocument == id && a.Remove == false, orderBy: order, take: length, skip: start, includeProperties: "TblProduct,TblProduct.TblProductUnit");
            var (items, count, total) =  result;

            var data = items.GroupBy(g => new { g.Product }).Select(a => new
            {
                id = " ",
                DT_RowId = a.First().Id,
                a.First().Id,
                ProductId = a.First().TblProduct.Id,
                a.First().TblProduct.Title,
                Unit = a.First().TblProduct.TblProductUnit.Title,
                Code = string.Join(",", a.Select(b => b.Code).Where(c => !c.IsEmpty()).ToList()),
                Count = a.Count()

            }).ToList();


            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = "100000",
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TblEntryDocument entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    entity.Temp = false;
                    entity.Type = false;
                    entity.Date = DateTime.Now;
                    entity.User = user;
                    entity.Count = 0;
                    entity.RequestId = "0";
                    var (result, message) =  _unitOfWork.GetRepository<TblEntryDocument>().InsertSync(entity, user);
                    if (result)
                    {
                        entity.RequestId = new PersianDateTime(entity.RequestDate).ToString("yyyyMM") + entity.Id;
                        var (resultU, messageU) = await _unitOfWork.GetRepository<TblEntryDocument>().Update(entity, user);
                        TempData["successToast"] = string.Format(Resources.Public.AddSuccessMessage, "ایجاد سند خروج");
                        return RedirectToAction("Edit", new { id = entity.Id });
                    }
                    TempData["errorToast"] = string.Format(Resources.Public.CreateErrorMessage, entity.Id, message);
                   // return RedirectToAction("Index");
                }


            }
            catch (Exception e)
            {
                TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Store/EntryDocuments/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblEntryDocument>();
            var result = await iResult.GetSingle(a => a.Id == id,
                includeProperties: "TblStore,TblStore1,TblPerson,TblOfficeDepartment,TblOffice,TblOffice.TblManagement,TblOffice.TblManagement.TblSubsidiary");
            if (result == null)
            {
                return HttpNotFound();
            }


            return View(result);
        }

        [HttpPost]
        public ActionResult AddProduct(long product, int count, long id, string code, string name)
        {
            var user = int.Parse(User.Identity.Name);
            var m = new Message();
            var list = new List<TblEntryDocumentProduct>();
            if (code.IsNullOrWhiteSpace())
            {

                var p = new TblEntryDocumentProduct
                {
                    Code = "",
                    EntryDocument = id,
                    Product = product,
                    Remove = false,
                    Count = count,
                    Exit = true,
                    Date = DateTime.Now
                };
                list.Add(p);

            }
            else
            {
                var ramz2 = code.Split('\n');
                for (var i = 0; i < count; i++)
                {
                    var c = string.Empty;
                    if (ramz2.Length > i)
                        c = ramz2[i];

                    var p = new TblEntryDocumentProduct
                    {
                        Code = c,
                        EntryDocument = id,
                        Product = product,
                        Remove = false,
                        Exit = true,
                        Count = 1
                    };
                    list.Add(p);

                    ////اگر رمز اموال خالی نباشد
                    //if (c != string.Empty)
                    //{
                    //    //اگر رمز اموال قبلا در سند ورود ثبت شده باشد
                    //    if (_unitOfWork.GetRepository<TblEntryDocumentProduct>()
                    //        .AnySync(a => a.Code == c && a.Exit == false))
                    //    {

                    //    }
                    //}
                }
            }

            var (result, message) = _unitOfWork.GetRepository<TblEntryDocumentProduct>().InsertRangeSync(list, user);
            m.Result = result;
            m.Msg = result ? "کالای " + name + " با موفقیت به لیست اضافه شد" : string.Format(Resources.Public.AddErrorMessage, name, message); ;
            m.Type = result ? "success" : "error";
            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        // POST: Store/EntryDocuments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(TblEntryDocument entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var iResult = _unitOfWork.GetRepository<TblEntryDocument>();
                    var ed = await iResult.GetSingle(a => a.Id == entity.Id);
                    ed.RequestDate = entity.RequestDate;
                    ed.RequestId = entity.RequestId;
                    ed.RequestReason = entity.RequestReason;
                    ed.Des = entity.Des;
                  //  ed.Store = entity.Store;
                    var (result, message) = await _unitOfWork.GetRepository<TblEntryDocument>().Update(ed, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Id);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Id, message);
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        public ActionResult ReportItems(int id)
        {


            var start = Convert.ToInt32(Request["start"]);
            var length = 10000; //Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
           
            length = length == 0 ? 100000 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblEntryDocumentProduct>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrderedSync(a => a.EntryDocument == id && a.TblProduct.Title.Contains(searchValue),
                    orderBy: order, take: length, skip: start, includeProperties: "TblProduct,TblProduct.TblProductUnit") :
                    iResult.GetOrderedSync(a => a.EntryDocument == id, orderBy: order, take: length, skip: start, includeProperties: "TblProduct,TblProduct.TblProductUnit");
            var (items, count, total) =  result;

            var item = items.GroupBy(g => new { g.Product }).Select(a => new
            {
                id = " ",
                DT_RowId = a.First().Id,
                a.First().Id,
                ProductId = a.First().TblProduct.Id,
                a.First().TblProduct.Title,
                Unit = a.First().TblProduct.TblProductUnit.Title,
                Code = string.Join(",", a.Select(b => b.Code).Where(c => !c.IsEmpty()).ToList()),
                Count = a.Count(),
                Date = "",
                Des = ""

            }).ToList();


            var header = new
            {
                StartDate = "",
                EndDate = "",

                Contract = "",
                Contractor = "",
                //Restaurant = re.Title
            };
            var data = new
            {
                header = header,
                item
            };
            TempData["res"] = new JavaScriptSerializer().Serialize(data);

            // return Json(data, JsonRequestBehavior.AllowGet);
            return View();
        }

        public ActionResult GetReport1()
        {
           
            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/NReport1.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        public ActionResult ViewerEvent()
        {
            StiOptions.Viewer.RightToLeft = StiRightToLeftType.Yes;
            return StiMvcViewer.ViewerEventResult();
        }

        public ActionResult DesignReport()

        {
            StiReport report = StiMvcViewer.GetReportObject();
            ViewBag.ReportName = report.ReportName;
            return View("Designer");

        }
    }
}