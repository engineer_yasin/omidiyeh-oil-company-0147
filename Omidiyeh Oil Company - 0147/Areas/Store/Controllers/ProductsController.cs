﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using System.Linq.Expressions;
using Microsoft.Ajax.Utilities;

namespace Omidiyeh_Oil_Company___0147.Areas.Store.Controllers
{
   
    public class ProductsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db;

        public ProductsController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            _db = new dbEntities();
        }

        public ActionResult ModalItems()
        {
            return PartialView();
        } 
        
        public ActionResult ModalItems2()
        {
            return PartialView();
        }
        public JsonResult Items()
        {
            var list =  _unitOfWork.GetRepository<TblProduct>().GetSync(a=>a.Remove == false,includeProperties: "TblCategory1,TblCategory2") ;
            return Json(list.Select(a => new { a.Id, a.Title,Category1  = a.TblCategory1.Title, Category2 = a.TblCategory2.Title }), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "219")]
        // GET: Store/Products
        public ActionResult Index()
        {
            return View();
        }
        
        public JsonResult List(SearchAdvancedProduct model)
        {
            model.Title = model.Title?.ToFarsi();
            Expression<Func<TblProduct, bool>> filter = (a) =>
                  (model.Requestable == null || a.Requestable == model.Requestable) &&
                  (model.Repairable == null || a.Repairable == model.Repairable) &&
                  (model.Remove == null || a.Remove == model.Remove) &&
                  (model.Category1 == 0 || a.Category1 == model.Category1) &&
                  (model.Category2 == 0 || a.Category2 == model.Category2) &&
                  (model.Store == 0 || a.TblEntryDocumentProducts.Any(b => b.TblEntryDocument.Store == model.Store)) &&
                  (model.Lading == 0 || a.TblEntryDocumentProducts.Any(b => b.Lading == model.Lading)) &&
                
                  (string.IsNullOrEmpty(model.Des) || a.Des.Contains(model.Des)) &&
                  (string.IsNullOrEmpty(model.Title) || a.Title.Contains(model.Title)) //&&
                  //a.TblEntryDocumentProducts.Any(b=>b.Exit == false)
                //(model.Category1.Contains(-5) || model.Category1.Contains(a.Category1)) &&
                 // (model.Category2.Contains(-5) || model.Category2.Contains(a.Category2)) //&&
                  //(model.Unit.Contains(-5) || model.Unit.Contains(a.Unit)) 
          ;

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 20 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties= "TblCategory1,TblCategory2,TblProductUnit,TblEntryDocumentProducts";
            var iResult = _unitOfWork.GetRepository<TblProduct>();
            var result = iResult.GetOrderedSyncNoTracking(filter: filter,
                    orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                a.Title,
                a.Des,
                a.Category1,
                a.Category2,
                a.Shelflife,
                a.Threshold,
                a.Requestable,
                a.Repairable,
                a.Remove,
                a.Show,
                Unit = a.TblProductUnit.Title,
                Category1Title = a.TblCategory1.Title,
                Category2Title = a.TblCategory2.Title,
                Count = a.TblEntryDocumentProducts.Where(b => b.Remove == false && b.Exit == false).Sum(b=>b.Count)
                - a.TblEntryDocumentProducts.Where(b => b.Remove == false && b.Exit).Sum(b => b.Count),
               // a.TblProductUnit,
                edit = $"<a href='/Store/Products/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-lg'></i></a> " +
                      $"<i class='fas fa-trash-alt fa-lg red-text cursor-pointer ml-2' onclick='deleteProduct({a.Id},\"{a.Title}\")'></i>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            var jsonResult = Json(res, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        /// <summary>
        /// لیست برای افزودن بارنامه
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
          public JsonResult List2(SearchAdvancedEntryDocumentProduct model)
        {
           // model.TblProduct.Title = model.TblProduct.Title?.ToFarsi();
            Expression<Func<TblEntryDocumentProduct, bool>> filter = (a) =>
                    
           
                  (model.DocumentId == null || a.TblEntryDocument.Id == model.DocumentId) &&
                  (model.ProductCode == null || a.Code.Contains(model.ProductCode)) &&
                   (model.ToOffice == 0 || a.TblEntryDocument.ToOffice == model.ToOffice) &&
                   (model.ToOfficeDepartment == 0 || a.TblEntryDocument.ToOfficeDepartment == model.ToOfficeDepartment) &&
                  a.Lading == null &&
                   (a.OldBarname == false || a.OldBarname == null) &&
                   a.TblEntryDocument.Remove == false &&
            a.Remove == false


          ;

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 20 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties= "TblEntryDocument,TblProduct,TblProduct.TblCategory1,TblProduct.TblCategory2,TblProduct.TblProductUnit";
            var iResult = _unitOfWork.GetRepository<TblEntryDocumentProduct>();
            var result = iResult.GetOrderedSync(
                filter,
                orderBy: order, take: length, skip: start , includeProperties: includeProperties
            );
            //var result2 = _db.TblEntryDocumentProducts.Where(a =>
            //    a.TblEntryDocument.ToOffice == model.TblEntryDocument.ToOffice
            //    && a.TblEntryDocument.ToOfficeDepartment == model.TblEntryDocument.ToOfficeDepartment).ToList();
            var (items, count, total) = result;

            var data = items?.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                a.TblProduct.Title,
                a.Des,
                a.Code,
                a.TblProduct.Category1,
                a.TblProduct.Category2,
                a.TblProduct.Shelflife,
                a.TblProduct.Threshold,
                a.TblProduct.Requestable,
                a.TblProduct.Repairable,
                a.Remove,
                a.TblProduct.Show,
                Unit = a.TblProduct.TblProductUnit.Title,
                Category1Title = a.TblProduct.TblCategory1.Title,
                Category2Title = a.TblProduct.TblCategory2.Title,
                EntryDocument=  a.TblEntryDocument.Id,
                Count = a.Count,
               // a.TblProductUnit,
               
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            var jsonResult = Json(res, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }




        [Authorize(Roles = "222")]
        // GET: Store/Products/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Unit = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblProductUnit>().Get(), "Id", "Title");
            return View();
        }

        // POST: Store/Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "222")]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Des,Category1,Category2,Shelflife,Threshold,Requestable,Repairable,Remove,Show,Unit")] TblProduct entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblProduct>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }

                ViewBag.Category1 = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblCategory1>().Get(), "Id", "Title");
                ViewBag.Category2 = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblCategory2>().Get(), "Id", "Title");
                ViewBag.Unit = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblProductUnit>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }
        [Authorize(Roles = "223")]
        // GET: Store/Products/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblProduct>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.Category1 = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblCategory1>().Get(), "Id", "Title");
            ViewBag.Category2 = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblCategory2>().Get(), "Id", "Title");
            ViewBag.Unit = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblProductUnit>().Get(), "Id", "Title");
            return View(result);
        }

        // POST: Store/Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "223")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Des,Category1,Category2,Shelflife,Threshold,Requestable,Repairable,Remove,Show,Unit")] TblProduct entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblProduct>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }

                ViewBag.Unit = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblProductUnit>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [HttpPost]
        [Authorize(Roles = "224")]
        public async Task<JsonResult> Delete(int id)
        {
            var m = new Message();
            try
            {
                var user = int.Parse(User.Identity.Name);
                var entity =  _unitOfWork.GetRepository<TblProduct>().GetSingleSync(a => a.Id == id);
                entity.Remove = true;
                var (result, message) = await _unitOfWork.GetRepository<TblProduct>().DeleteSoft(entity,id, user);
                    if (result)
                    {
                        m.Msg = Resources.Public.DeleteSuccessMessage;
                        m.Result = true;
                        m.Type = "success";
                    }
                    else
                    {
                        m.Msg = string.Format(Resources.Public.DeleteErrorMessage, message);
                        m.Result = false;
                        m.Type = "error";
                    }


            }
            catch (Exception e)
            {
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);

        }

       
    }
}
