﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Store.Controllers
{
	//[Authorize(Roles ="1")]
    public class Category1Controller : Controller
    {
		private readonly IUnitOfWork _unitOfWork;

		public Category1Controller()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }

        public JsonResult JsonList()
        {
            //Expression<Func<TblCategory1, bool>> filter = (a) =>
            //    (a.Category1 == category1);
            var list = _unitOfWork.GetRepository<TblCategory1>().GetSync( orderBy: b => b.OrderByDescending(c => c.Title));
            var data = list.Select(a => new
            {
                a.Id,
                a.Title,
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "225")]
        // GET: Store/Category1
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
			var iResult = _unitOfWork.GetRepository<TblCategory1>();
            var result = !string.IsNullOrEmpty(searchValue) ? 
                iResult.GetOrdered(a => a.Title.Contains(searchValue) || a.Des.Contains(searchValue),
                    orderBy: order,take:length, skip:start) :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start);
            var (items, count,total) = await result;

            var data = items.Select(a => new { id = " ", 
                 DT_RowId = a.Id,
                                    a.Id,
                                    a.Title,
                                    a.Des,
                                    a.TblCategory2,
                                    a.TblProducts,
                                edit= $"<a href='/Store/Category1/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> "+
                      $"<a href='/Store/Category1/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }


        [Authorize(Roles = "226")]
        // GET: Store/Category1/Create
        public async Task<ActionResult> Create()
        {
            return View();
        }

        // POST: Store/Category1/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "226")]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Des")] TblCategory1 entity)
        {
			try
            {
            if (ModelState.IsValid)
            {
				var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblCategory1>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                return RedirectToAction("Index");
            }

			}
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }
        [Authorize(Roles = "227")]
        // GET: Store/Category1/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblCategory1>();
            var result = await iResult.GetSingle(a=>a.Id==id);
			if (result == null)
            {
                return HttpNotFound();
            }
            
            return View(result);
        }

        // POST: Store/Category1/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "227")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Des")] TblCategory1 entity)

        {
			try
            {
            if (ModelState.IsValid)
            {
					var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblCategory1>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
					return RedirectToAction("Index");
            }
			}
			catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [Authorize(Roles = "228")]
        // GET: Store/Category1/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblCategory1>();
            var result = await iResult.GetSingle(a=>a.Id==id);
			if (result == null)
            {
                return HttpNotFound();
            }      
            return View(result);
        }

        // POST: Store/Category1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "228")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
			try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblCategory1>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new {id});
        }

    }
}
