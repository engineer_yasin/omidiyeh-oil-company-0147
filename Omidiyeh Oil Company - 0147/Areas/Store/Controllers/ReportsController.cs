﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Store.Controllers
{
    public class ReportsController : Controller
    {
        private readonly dbEntities _db = new dbEntities();

        // GET: Store/Reports
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Code()
        {
            return View();
            
        }

        public ActionResult  CodeResult(string code)
        {
            var item = _db.TblEntryDocumentProducts.AsNoTracking()
                .OrderByDescending(a => a.Date)
                .Include(i => i.TblEntryDocument.TblPerson)
                .Include(i => i.TblEntryDocument.TblOffice)
                .Include(i => i.TblEntryDocument.TblOfficeDepartment)
                .Include(i => i.TblProduct)
                .Include(i => i.TblProduct.TblProductUnit)
                .FirstOrDefault(a => a.Code == code && a.TblEntryDocument.Type == false && a.Remove == false);
            
            
            return PartialView(item);
        }
        
        public ActionResult Cardex()
        {
            return View();
            
        }

        public ActionResult CardexResult(DateTime startDate,DateTime endDate, long product)
        {
            var item = _db.TblEntryDocumentProducts.AsNoTracking()
                .OrderByDescending(a => a.Date)
                .Include(i => i.TblEntryDocument.TblPerson)
                .Include(i => i.TblEntryDocument.TblOffice)
                .Include(i => i.TblEntryDocument.TblOfficeDepartment)
                .Include(i => i.TblProduct)
                .Include(i => i.TblProduct.TblProductUnit)
                .Where(a => a.Date >= startDate && a.Date<= endDate /*&&  a.TblEntryDocument.Type == false*/ && a.Remove == false && a.Product == product);


            return PartialView(item);
        }
        
        public ActionResult ProductPersons()
        {
            return View();
            
        }

        public ActionResult ProductPersonsResult(DateTime startDate,DateTime endDate, long product)
        {
            var item = _db.TblEntryDocumentProducts
                .AsNoTracking()
                .OrderByDescending(a => a.TblEntryDocument.Date)
                .Include(i => i.TblEntryDocument.TblPerson)
                .Include(i => i.TblEntryDocument.TblOffice)
                .Include(i => i.TblEntryDocument.TblOfficeDepartment)
                .Include(i => i.TblProduct)
                .Include(i => i.TblProduct.TblProductUnit)
                
                .Where(a => a.TblEntryDocument.Date >= startDate && a.TblEntryDocument.Date<= endDate && a.TblEntryDocument.Type == false && a.Remove == false && a.Product == product);


            return PartialView(item);
        }
        
        public ActionResult ProductStore()
        {
            return View();
            
        }

        public ActionResult ProductStoreResult( long product)
        {
            var item = _db.TblEntryDocumentProducts
                .AsNoTracking()
                .Include(i => i.TblProduct)
                .Include(i => i.TblProduct.TblProductUnit)
                .Include(i => i.TblEntryDocument)
                .Where(a => a.Remove == false && a.Product == product);
               


            return PartialView(item);
        } 
        
        public ActionResult ProductOffice()
        {
            return View();
            
        }

        public ActionResult ProductOfficeResult(DateTime startDate, DateTime endDate, long? product, int? store)
        {
            var item = _db.TblEntryDocumentProducts
                .AsNoTracking()
                .OrderByDescending(a => a.TblEntryDocument.Date)
                .Include(i => i.TblProduct)
                .Include(i => i.TblProduct.TblProductUnit)
                .Include(i => i.TblEntryDocument)
                .Where(a => a.TblEntryDocument.Date >= startDate && a.TblEntryDocument.Date <= endDate && a.TblEntryDocument.Type == false && a.Remove == false)
                .Where(a=> product == null || a.Product == product)
                .Where(a=> store == null || a.TblEntryDocument.Store == store)
               ;

               


            return PartialView(item);
        }

        public ActionResult Inventory()
        {
            return View();

        }

        public ActionResult InventoryResult(int store)
        {
            var item = _db.TblEntryDocumentProducts
                    .AsNoTracking()
                    .OrderByDescending(a => a.TblEntryDocument.Date)
                    .Include(i => i.TblProduct)
                    .Include(i => i.TblProduct.TblProductUnit)
                    .Include(i => i.TblEntryDocument)
                    .Where(a =>  a.TblEntryDocument.Store == store)
                ;




            return PartialView(item);
        }
    }
}