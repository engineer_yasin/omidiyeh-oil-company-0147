﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using System.Linq.Expressions;

namespace Omidiyeh_Oil_Company___0147.Areas.Store.Controllers
{
	//[Authorize(Roles ="1")]
    public class Category2Controller : Controller
    {
		private readonly IUnitOfWork _unitOfWork;

		public Category2Controller()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }

        public  JsonResult JsonList( int category1)
        {
            Expression<Func<TblCategory2, bool>> filter = (a) =>
                (a.Category1 == category1);
            var list =  _unitOfWork.GetRepository<TblCategory2>().GetSync(filter
                , orderBy: b => b.OrderByDescending(c => c.Title));
            var data = list.Select(a => new
            {
                a.Id,
                a.Title,
                a.Repairable,
                a.Requestable,
                a.Threshold,
               
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "229")]
        // GET: Store/Category2
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
			var iResult = _unitOfWork.GetRepository<TblCategory2>();
            var result = !string.IsNullOrEmpty(searchValue) ? 
                iResult.GetOrderedSync(a => a.Title.Contains(searchValue) || a.Des.Contains(searchValue),
                    orderBy: order,take:length, skip:start, includeProperties: "TblCategory1") :
                    iResult.GetOrderedSync(orderBy: order, take: length, skip: start, includeProperties: "TblCategory1");
            var (items, count,total) = result;

            var data = items.Select(a => new { id = " ", 
                 DT_RowId = a.Id,
                                    a.Id,
                                    a.Title,
                                    
                                    a.Threshold,
                                    a.Requestable,
                                    a.Repairable,
                                    a.Des,
                Category1=   a.TblCategory1.Title,
                                  
                                edit= $"<a href='/Store/Category2/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> "+
                      $"<a href='/Store/Category2/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }



        [Authorize(Roles = "230")]
        // GET: Store/Category2/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Category1 = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblCategory1>().Get(), "Id", "Title");
            return View();
        }

        // POST: Store/Category2/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "230")]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Category1,Threshold,Requestable,Repairable,Des")] TblCategory2 entity)
        {
			try
            {
            if (ModelState.IsValid)
            {
				var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblCategory2>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                return RedirectToAction("Index");
            }

            ViewBag.Category1 = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblCategory1>().Get(), "Id", "Title");
			}
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }
        [Authorize(Roles = "231")]
        // GET: Store/Category2/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblCategory2>();
            var result = await iResult.GetSingle(a=>a.Id==id);
			if (result == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.Category1 = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblCategory1>().Get(), "Id", "Title");
            return View(result);
        }

        // POST: Store/Category2/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "231")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Category1,Threshold,Requestable,Repairable,Des")] TblCategory2 entity)

        {
			try
            {
            if (ModelState.IsValid)
            {
					var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblCategory2>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
					return RedirectToAction("Index");
            }
            ViewBag.Category1 = new SelectList(await _unitOfWork.GetRepository<Omidiyeh_Oil_Company___0147.Models.TblCategory1>().Get(), "Id", "Title");
			}
			catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }
        [Authorize(Roles = "232")]
        // GET: Store/Category2/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblCategory2>();
            var result = await iResult.GetSingle(a=>a.Id==id);
			if (result == null)
            {
                return HttpNotFound();
            }      
            return View(result);
        }

        // POST: Store/Category2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "232")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
			try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblCategory2>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new {id});
        }

    }
}
