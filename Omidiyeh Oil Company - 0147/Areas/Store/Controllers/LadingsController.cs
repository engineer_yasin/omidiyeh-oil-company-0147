﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using System.Web.Script.Serialization;

using System.Web.WebPages;
using Microsoft.Ajax.Utilities;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;

namespace Omidiyeh_Oil_Company___0147.Areas.Store.Controllers
{
    //[Authorize(Roles ="1")]
    public class LadingsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public LadingsController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        // GET: Store/Ladings
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult List()
        {
            Expression<Func<TblBillOfLading, bool>> filter = (a) =>
                    
                    a.Remove == false
                ;
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblBillOfLading>();
            const string includeProperties = "TblStore,TblUser.TblPerson,TblPerson,TblOffice,TblOfficeDepartment";

            var result = iResult.GetOrderedSyncNoTracking(filter: filter,
                orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                Date = new PersianDateTime(a.Date).ToString("yyyy/MM/dd"),
                Person = a.TblPerson.Fullname(),
                Office = a.TblOffice.Title + " - " + a.TblOfficeDepartment?.Title,
                Store = a.TblStore.Title,
                a.ReqeustId,
                User = a.TblUser.TblPerson.Fullname(),

                edit = $"<a href='/Store/Ladings/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> " +
                      $"<a href='/Store/Ladings/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ListProduct(long id)
        {
           

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = 10000; //Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 1000 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblEntryDocumentProduct>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Lading == id && a.TblProduct.Title.Contains(searchValue),
                    orderBy: order, take: length, skip: start, includeProperties: "TblProduct,TblProduct.TblProductUnit") :
                    iResult.GetOrdered(a => a.Lading == id, orderBy: order, take: length, skip: start, includeProperties: "TblProduct,TblProduct.TblProductUnit");
            var (items, count, total) = await result;

            //var data = items.GroupBy(g => new { g.Product }).Select(a => new
            //{
            //    id = " ",
            //    DT_RowId = a.First().Id,
            //    a.First().Id,
            //    ProductId = a.First().TblProduct.Id,
            //    a.First().TblProduct.Title,
            //    Unit= a.First().TblProduct.TblProductUnit.Title,
            //  Code =   string.Join(",", a.Select(b => b.Code).Where(c=> !c.IsEmpty()).ToList()),
            //    Count = a.Sum(b=>b.Count)

            //}).ToList();

            var data = items.Select(a => new
            {
                id = a.Id,
                DT_RowId = a.Id,
                a.Id,
                ProductId = a.TblProduct.Id,
                a.TblProduct.Title,
                Unit = a.TblProduct.TblProductUnit.Title,
                Code = a.Code,
                Count = a.Count

            }).ToList();


            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = "10000",
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        // GET: Store/Ladings/Details/5
        public async Task<ActionResult> Details(long id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblBillOfLading>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }


        // GET: Store/Ladings/Create
        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TblBillOfLading entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    entity.User = user;
                    var (result, message) = await _unitOfWork.GetRepository<TblBillOfLading>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successToast"] = string.Format(Resources.Public.AddSuccessMessage, "ایجاد بارنامه");
                        return RedirectToAction("Edit", new { id = entity.Id });
                    }
                    TempData["errorToast"] = string.Format(Resources.Public.CreateErrorMessage, entity.Id, message);

                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                 TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [HttpPost]
        public ActionResult AddProduct(long entryProductId, int ladings)
        {
            var user = int.Parse(User.Identity.Name);
            var m = new Message();
            var list = new List<TblEntryDocumentProduct>();
            var item = _unitOfWork.GetRepository<TblEntryDocumentProduct>().GetSingleSync(a => a.Id == entryProductId, includeProperties: "TblProduct");
            item.Lading = ladings;
            
            var (result, message) = _unitOfWork.GetRepository<TblEntryDocumentProduct>().UpdateSync(item, user);

            m.Result = result;
            m.Msg = result ? "کالای " + item.TblProduct.Title + " با موفقیت به بارنامه اضافه شد" : string.Format(Resources.Public.AddErrorMessage, item.TblProduct.Title, message); ;
            m.Type = result ? "success" : "error";
          
            return Json(m, JsonRequestBehavior.AllowGet);
        }

           [HttpPost]
        public ActionResult RemoveProduct(long entryProductId)
        {
            var user = int.Parse(User.Identity.Name);
            var m = new Message();
            var list = new List<TblEntryDocumentProduct>();
            var item = _unitOfWork.GetRepository<TblEntryDocumentProduct>().GetSingleSync(a => a.Id == entryProductId, includeProperties: "TblProduct");
            item.Lading = null;
            
            var (result, message) = _unitOfWork.GetRepository<TblEntryDocumentProduct>().UpdateSync(item, user);

            m.Result = result;
            m.Msg = result ? "کالای " + item.TblProduct.Title + " با موفقیت از بارنامه حذف شد" : string.Format(Resources.Public.DeleteErrorMessage, message); ;
            m.Type = result ? "success" : "error";
          
            return Json(m, JsonRequestBehavior.AllowGet);
        }


        // GET: Store/Ladings/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblBillOfLading>();
            var result =  iResult.GetSingleSync(a => a.Id == id,
            includeProperties: "TblStore,TblPerson,TblOfficeDepartment,TblOffice,TblOffice.TblManagement,TblOffice.TblManagement.TblSubsidiary");
            if (result == null)
            {
                return HttpNotFound();
            }

          return View(result);
        }

        // POST: Store/Ladings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(TblBillOfLading entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblBillOfLading>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Id);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Id, message);
                    return RedirectToAction("Index");
                }
               
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            var m = new Message();

                var item =  _unitOfWork.GetRepository<TblBillOfLading>().GetSingleSync(a => a.Id == id);
                item.Remove = true;

                    var user = int.Parse(User.Identity.Name);
                    var (result, message) =  _unitOfWork.GetRepository<TblBillOfLading>().DeleteSoftSync(item, id, user);
                    if (result)
                    {
                        m.Msg = Resources.Public.DeleteSuccessMessage;
                        m.Result = true;
                        m.Type = "success";
                    }
                    else
                    {
                        m.Msg = string.Format(Resources.Public.DeleteErrorMessage, message);
                        m.Result = false;
                        m.Type = "error";
                    }
               

            return Json(m, JsonRequestBehavior.AllowGet);

        }
        // GET: Store/Ladings/Delete/5
        public async Task<ActionResult> _Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblBillOfLading>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Store/Ladings/Delete/5
      
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblBillOfLading>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

        public ActionResult ReportItems(int id)
        {


            var start = Convert.ToInt32(Request["start"]);
            var length = 10000; //Convert.ToInt32(Request["length"]);
           
            var order = "";

            length = length == 0 ? 100000 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblEntryDocumentProduct>();
            var includeProperties = "TblProduct,TblProduct.TblProductUnit";
           
            var result = iResult.GetOrderedSyncNoTracking(a=>a.Lading == id,
                orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = result;
            var item = items.GroupBy(g => new { g.Product }).Select(a => new
            {
                id = " ",
                DT_RowId = a.First().Id,
                a.First().Id,
                ProductId = a.First().TblProduct.Id,
                a.First().TblProduct.Title,
                Unit = a.First().TblProduct.TblProductUnit.Title,
                Code = string.Join("\n", a.Select(b => b.Code).Where(c => !c.IsEmpty()).ToList()),
                Count = a.Sum(b=>b.Count),
                Date = "",
                Des = ""

            }).ToList();

            var l = _unitOfWork.GetRepository<TblBillOfLading>().GetSingleSync(a => a.Id == id,
                includeProperties: "TblOffice,TblOfficeDepartment,TblPerson");
            var header = new
            {
                Office = l.TblOffice.Title + " - " + l.TblOfficeDepartment.Title,
                Person = l.TblPerson.Fullname() + " / " + l.TblPerson.PersonnelCode.IfNullOrWhiteSpace(l.TblPerson.NationalCode) ,
                Id = l.Id,
                l.RequestId,
                l.Des,
                RequestDate = new PersianDateTime(l.RequestDate).ToString("yyyy/MM/dd"),
                Date = new PersianDateTime(l.Date).ToString("yyyy/MM/dd"),

            };
            var data = new
            {
                header = header,
                item
            };
            TempData["res"] = new JavaScriptSerializer().Serialize(data);

            // return Json(data, JsonRequestBehavior.AllowGet);
            return View();
        }

        public ActionResult GetReport1()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/NReport2.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        public ActionResult ViewerEvent()
        {
            StiOptions.Viewer.RightToLeft = StiRightToLeftType.Yes;
            return StiMvcViewer.ViewerEventResult();
        }

        public ActionResult DesignReport()

        {
            StiReport report = StiMvcViewer.GetReportObject();
            ViewBag.ReportName = report.ReportName;
            return View("Designer");

        }

    }
}
