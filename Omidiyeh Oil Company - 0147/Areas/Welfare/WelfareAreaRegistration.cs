﻿using System.Web.Mvc;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare
{
    public class WelfareAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "Welfare";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            context.Routes.MapMvcAttributeRoutes();
            context.MapRoute(
                "Welfare_default",
                "Welfare/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                namespaces:new [] { "Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers" }
            );
        }
    }
}