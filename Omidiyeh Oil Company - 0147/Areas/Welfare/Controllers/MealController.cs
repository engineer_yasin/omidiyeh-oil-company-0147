﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    [Authorize()]
    public class MealController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public MealController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        /// <summary>
        /// لیست وعده های غذایی
        /// هفت روز کش می شود بر روی کلاینت کاربر
        /// </summary>
        /// <returns></returns>
        //[OutputCache(Duration = 86400,VaryByParam ="all")]
        public JsonResult Items(bool all = false, bool shift = false)
        {
            var list = all ?  _unitOfWork.GetRepository<TblMeal>().GetSync() :  _unitOfWork.GetRepository<TblMeal>().GetSync(a => a.Status);
            if (shift)
            {
                var mShift = new List<int>() {0, 1, 2, 3, 4, 5};
                list = list.Where(a => mShift.Contains(a.Id));
            }
            return Json(list.Select(a=> new {a.Id, a.Title}), JsonRequestBehavior.AllowGet);
        }
         /// <summary>
        /// لیست وعده های غذایی
        /// هفت روز کش می شود بر روی کلاینت کاربر
        /// </summary>
        /// <returns></returns>
        //[OutputCache(Duration = 86400, VaryByCustom = "byUser"/*, Location = System.Web.UI.OutputCacheLocation.Client*/)]
        //public async Task<JsonResult> Items(DateTime startDate, DateTime endDate)
        //{
        //    var list =  await _unitOfWork.GetRepository<TblMeal>().Get(a=>a.ActivationDate ==null || a.ActivationDate >= startDate && a.DeactivationDate) 
        //    return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        //}

        /// <summary>
        /// لیست وعده های غذایی
        /// هفت روز کش می شود بر روی کلاینت کاربر
        /// </summary>
        /// <returns></returns>
        //[OutputCache(Duration = 86400, VaryByCustom = "byUser"/*, Location = System.Web.UI.OutputCacheLocation.Client*/)]
        public async Task<PartialViewResult> ItemsSwitch(bool all = false)
        {
            var list = all ? await _unitOfWork.GetRepository<TblMeal>().Get() : await _unitOfWork.GetRepository<TblMeal>().Get(a => a.Status);
            return PartialView(list);
        }
        //[OutputCache(Duration = 86400)]
        public async Task<string> GetTitle(int id)
        {
            var iResult = _unitOfWork.GetRepository<TblMeal>();
            var result = await iResult.GetSingle(a => a.Id == id);
            return result.Title;
        }
        // GET: Welfare/Meal
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblMeal>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Title.Contains(searchValue),
                    orderBy: order, take: length, skip: start) :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start);
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                DT_RowId = a.Id,
                a.Id,
                a.Title,
                StartTime= a.StartTime.ToString(@"hh\:mm"),
                EndTime=  a.EndTime.ToString(@"hh\:mm"),
                ReservationEndTime=  a.ReservationEndTime.ToString(@"hh\:mm"),
                a.Status,
                a.ReservationEndDay,
                ReservationDefaultFoodTime= a.ReservationDefaultFoodTime.ToString(@"hh\:mm"),
                a.ReservationDefaultFoodDay,
                ActivationDate = a.ActivationDate == null? "": new PersianDateTime(a.ActivationDate).ToString("dddd yyyy/MM/dd"),
                DeactivationDate = a.DeactivationDate == null? "": new PersianDateTime(a.DeactivationDate).ToString("dddd yyyy/MM/dd"),
                edit = $"<a href='/Welfare/Meal/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-lg'></i></a> "
                       //+$"<a href='/Welfare/Meal/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-lg'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Welfare/Meal/Details/5
        public async Task<ActionResult> Details(int id)
        {
            var iResult = _unitOfWork.GetRepository<TblMeal>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }


        // GET: Welfare/Meal/Create
        public async Task<ActionResult> Create()
        {
            return View();
        }

        // POST: Welfare/Meal/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,StartTime,EndTime,ReservationEndTime,Status,ReservationEndDay,ReservationDefaultFoodTime,ReservationDefaultFoodDay,ActivationDate,DeactivationDate")] TblMeal entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblMeal>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/Meal/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblMeal>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }

            return View(result);
        }

        // POST: Welfare/Meal/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,StartTime,EndTime,ReservationEndTime,Status,ReservationEndDay,ReservationDefaultFoodTime,ReservationDefaultFoodDay,ActivationDate,DeactivationDate")] TblMeal entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblMeal>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/Meal/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblMeal>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Welfare/Meal/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblMeal>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
