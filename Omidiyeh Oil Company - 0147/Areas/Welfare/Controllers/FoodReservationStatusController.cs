﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    public class FoodReservationStatusController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public FoodReservationStatusController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        //[OutputCache(Duration = 86400, VaryByCustom = "byUser"/*, Location = System.Web.UI.OutputCacheLocation.Client*/)]
        public async Task<JsonResult> Items()
        {
            var list = await _unitOfWork.GetRepository<TblFoodReservationStatu>().Get();
            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }
        
    }
}