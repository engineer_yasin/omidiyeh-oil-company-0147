﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;

using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    [Authorize()]
    public class FoodController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db;
        public FoodController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            _db = new dbEntities();
        }
        //[OutputCache(Duration = 86400, VaryByCustom = "byUser"/*, Location = System.Web.UI.OutputCacheLocation.Client*/)]
        public JsonResult Items()
        {
            var list = _unitOfWork.GetRepository<TblFood>().GetSync(a=>a.Id != -1) ;
            return Json(list.ToList(), JsonRequestBehavior.AllowGet);
        }
        [OutputCache(Duration = 86400,VaryByParam = "id")]
        public async Task<string> Name(int id)
        {
            var food = await _unitOfWork.GetRepository<TblFood>().GetFirst(a=>a.Id==id) ;
            return food.Title;
        }
        // GET: Welfare/Food
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            var sortDirection = Request["order[0][dir]"];
            var draw = Request["draw"];
            length = length == 0 ? 10 : length;
            if (string.IsNullOrEmpty(sortColumnName))
            {
                sortColumnName = "Id";
                sortDirection = "desc";
            }
            var iResult = _unitOfWork.GetRepository<TblFood>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Title.Contains(searchValue) || a.Des.Contains(searchValue),
                    orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                    includeProperties: "TblFoodCategory") :
                    iResult.GetOrdered(orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                    includeProperties: "TblFoodCategory");
            var (items, count, total) = await result;

            var data = items.Where(a => a.Id > 5).Select(a => new
            {
                id = " ",
                a.Title,
                a.Des,
                a.Quota,
                Category = a.TblFoodCategory.Title,
                DT_RowId = a.Id,
                edit = $"<a href='/Welfare/Food/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> " +
                      $"<a href='/Welfare/Food/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();

            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Welfare/Food/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblFood>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }


        // GET: Welfare/Food/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Category = new SelectList((await _unitOfWork.GetRepository<TblFoodCategory>().Get()).OrderBy(a=>a.Title), "Id", "Title");
            return View();
        }

        // POST: Welfare/Food/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Des,Quota,Category")] TblFood entity, bool? addAnother)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblFood>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return addAnother==true ? RedirectToAction("Create") : RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Create");
                }

                ViewBag.Category = new SelectList(await _unitOfWork.GetRepository<TblFoodCategory>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/Food/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblFood>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.Category = new SelectList(await _unitOfWork.GetRepository<TblFoodCategory>().Get(), "Id", "Title",result.Category);
            return View(result);
        }

        // POST: Welfare/Food/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Des,Quota,Category")] TblFood entity, bool? addAnother)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblFood>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return addAnother == true ? RedirectToAction("Create") : RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }
                ViewBag.Category = new SelectList(await _unitOfWork.GetRepository<TblFoodCategory>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/Food/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblFood>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Welfare/Food/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblFood>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
