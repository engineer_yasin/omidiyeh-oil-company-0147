﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    [Authorize()]
    public class OfficeFoodQuotaController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public OfficeFoodQuotaController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        [Authorize(Roles = "268")]
        // GET: Welfare/OfficeFoodQuota
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            var sortDirection = Request["order[0][dir]"];
            var draw = Request["draw"];
            length = length == 0 ? 10 : length;
            if (string.IsNullOrEmpty(sortColumnName))
            {
                sortColumnName = "Id";
                sortDirection = "desc";
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeFoodQuota>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.TblOffice.Title.Contains(searchValue) ,
                    orderBy: sortColumnName + " " + sortDirection, take: length, skip: start
                    , includeProperties: "TblOffice") :
                    iResult.GetOrdered(orderBy: sortColumnName + " " + sortDirection, take: length, skip: start
                    , includeProperties: "TblOffice");
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                Office = a.TblOffice.Title,
               
                a.InitialQuota,
                a.UsedQuota,
                a.RemainingQuota,
                StartDate = new PersianDateTime(a.StartDate).ToLongDateString(),
                EndDate = new PersianDateTime(a.EndDate).ToLongDateString(),
               // a.EndDate,
                DT_RowId = a.Id,
                edit = $"<a href='/Welfare/OfficeFoodQuota/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> " +
                      $"<a href='/Welfare/OfficeFoodQuota/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Welfare/OfficeFoodQuota/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeFoodQuota>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        [Authorize(Roles = "269")]
        // GET: Welfare/OfficeFoodQuota/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Office = new SelectList(await _unitOfWork.GetRepository<TblOffice>().Get(), "Id", "Title");
            return View();
        }

        // POST: Welfare/OfficeFoodQuota/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "269")]
        public async Task<ActionResult> Create( TblOfficeFoodQuota entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    entity.RemainingQuota = entity.InitialQuota - entity.UsedQuota;
                    var user = int.Parse(User.Identity.Name);
                    if (await _unitOfWork.GetRepository<TblOfficeFoodQuota>().Any(a =>
                        (a.StartDate <= entity.StartDate || (a.StartDate <= entity.StartDate && a.EndDate >= entity.StartDate ))&&
                        a.Office == entity.Office))
                    {
                         TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, "برای تاریخ مذکور سهمیه غذایی تعریف شده است");
                         return View(entity);
                    }
                    
                
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeFoodQuota>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage,"سهمیه غذایی");
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, "سهمیه غذایی", message);
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }
        [Authorize(Roles = "270")]
        // GET: Welfare/OfficeFoodQuota/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeFoodQuota>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }

            await OfficeDetails(result.Office);

            return View(result);
        }

        private async Task OfficeDetails(int id)
        {
            var office = await _unitOfWork.GetRepository<TblOffice>()
                .GetSingle(a => a.Id == id,
                    includeProperties:
                    "TblManagement.TblSubsidiary.TblCompany,TblManagement.TblSubsidiary,TblManagement");
            ViewBag.Company = office.TblManagement.TblSubsidiary.TblCompany.Title;
            ViewBag.Subsidiary = office.TblManagement.TblSubsidiary.Title;
            ViewBag.Management = office.TblManagement.Title;
            ViewBag.Office = office.Title;
        }

        // POST: Welfare/OfficeFoodQuota/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "270")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Office,InitialQuota,UsedQuota,RemainingQuota,StartDate,EndDate")] TblOfficeFoodQuota entity)

        {
          
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    entity.RemainingQuota = entity.InitialQuota - entity.UsedQuota;
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeFoodQuota>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, "سهمیه غذایی");
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, "سهمیه غذایی", message);
                    return RedirectToAction("Index");
                }
                
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            
            await OfficeDetails(entity.Office);
            return View(entity);
        }

        [Authorize(Roles = "271")]
        // GET: Welfare/OfficeFoodQuota/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeFoodQuota>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Welfare/OfficeFoodQuota/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "271")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeFoodQuota>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

        /// <summary>
        /// حساب کردن مجدد سهمیه باقی مانده ادارات
        ///
        /// </summary>
        /// <returns></returns>
        public async Task<string> CalculateQuota()
        {
            try
            {
                var quota = await _unitOfWork.GetRepository<TblOfficeFoodQuota>()
                    .Get(/*a => a.EndDate >= DateTime.Now.AddDays(1),*/);

                foreach (var item in quota)
                {
                    //شماره حساب هایی که از این سهمیه استفاده می کنند
                    var accounts = (await _unitOfWork.GetRepository<TblOfficeDepartmentAccountNumber>()
                        .Get(a => a.TblOfficeDepartment.Office == item.Office)).Select(a=>a.Id).ToList();

                    var (used, full) = _unitOfWork.GetRepository<TblFoodReservation>().Count(a =>
                        a.Date >= item.EndDate && a.Date <=item.EndDate &&
                        accounts.Contains(a.TblFoodReservationGroup.AccountNumber));
                    if (used != -1)
                    {
                        item.UsedQuota = used + 1;
                        item.RemainingQuota = item.InitialQuota - item.UsedQuota;
                    }
                }

                await _unitOfWork.SaveAsync();
                return "Ok";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
