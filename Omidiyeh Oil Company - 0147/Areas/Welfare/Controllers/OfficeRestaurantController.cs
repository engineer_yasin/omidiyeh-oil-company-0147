﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;

using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    [Authorize()]
    public class OfficeRestaurantController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public OfficeRestaurantController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        // GET: Welfare/OfficeRestaurant
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            var sortDirection = Request["order[0][dir]"];
            var draw = Request["draw"];
            length = length == 0 ? 10 : length;
            if (string.IsNullOrEmpty(sortColumnName))
            {
                sortColumnName = "Id";
                sortDirection = "desc";
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeRestaurant>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.TblOffice.Title.Contains(searchValue) || a.TblOfficeDepartment.Title.Contains(searchValue) || a.TblRestaurant.Title.Contains(searchValue),
                    orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                    includeProperties: "TblRestaurant,TblOffice,TblOfficeDepartment") :
                    iResult.GetOrdered(orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                        includeProperties: "TblRestaurant,TblOffice,TblOfficeDepartment");
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                Restaurant= a.TblRestaurant.Title,
                Office = a.TblOffice.Title,
                OfficeDepartment= a.TblOfficeDepartment?.Title,
                DT_RowId = a.Id,
                edit = $"<a href='/Welfare/OfficeRestaurant/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> " +
                      $"<a href='/Welfare/OfficeRestaurant/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Welfare/OfficeRestaurant/Details/5
        public async Task<ActionResult> Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeRestaurant>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }


        // GET: Welfare/OfficeRestaurant/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Office = new SelectList(await _unitOfWork.GetRepository<TblOffice>().Get(), "Id", "Title");
            ViewBag.OfficeDepartment = new SelectList(new List<TblOfficeDepartment>(), "Id", "Title");
            ViewBag.Restaurant = new SelectList(await _unitOfWork.GetRepository<TblRestaurant>().Get(), "Id", "Title");
            return View();
        }

        // POST: Welfare/OfficeRestaurant/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Office,OfficeDepartment,Restaurant")] TblOfficeRestaurant entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeRestaurant>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Id);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Id, message);
                    return RedirectToAction("Index");
                }

                ViewBag.Office = new SelectList(await _unitOfWork.GetRepository<TblOffice>().Get(), "Id", "Title");
                ViewBag.OfficeDepartment = new SelectList(await _unitOfWork.GetRepository<TblOfficeDepartment>().Get(), "Id", "Title");
                ViewBag.Restaurant = new SelectList(await _unitOfWork.GetRepository<TblRestaurant>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/OfficeRestaurant/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeRestaurant>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.Office = new SelectList(await _unitOfWork.GetRepository<TblOffice>().Get(), "Id", "Title");
            ViewBag.OfficeDepartment = new SelectList(await _unitOfWork.GetRepository<TblOfficeDepartment>().Get(), "Id", "Title");
            ViewBag.Restaurant = new SelectList(await _unitOfWork.GetRepository<TblRestaurant>().Get(), "Id", "Title");
            return View(result);
        }

        // POST: Welfare/OfficeRestaurant/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Office,OfficeDepartment,Restaurant")] TblOfficeRestaurant entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeRestaurant>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Id);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Id, message);
                    return RedirectToAction("Index");
                }
                ViewBag.Office = new SelectList(await _unitOfWork.GetRepository<TblOffice>().Get(), "Id", "Title");
                ViewBag.OfficeDepartment = new SelectList(await _unitOfWork.GetRepository<TblOfficeDepartment>().Get(), "Id", "Title");
                ViewBag.Restaurant = new SelectList(await _unitOfWork.GetRepository<TblRestaurant>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/OfficeRestaurant/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblOfficeRestaurant>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Welfare/OfficeRestaurant/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblOfficeRestaurant>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
