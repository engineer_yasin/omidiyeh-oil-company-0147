﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;

using Omidiyeh_Oil_Company___0147.Models;
using Antlr.Runtime.Tree;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    [Authorize()]
    public class BedStatusController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public BedStatusController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }

        #region BedStatusHistory
        public PartialViewResult History(int bed)
        {
            var list = _unitOfWork.GetRepository<TblBedStatusHistory>().GetOrderedSync
                (a => a.Bed == bed,take:5,orderBy:"Date Desc",includeProperties: "TblBedStatu,TblUser.TblPerson");
            return PartialView(list.Item1);
        }
        [Authorize]
        public async Task<JsonResult> AddStatus(int bed, byte status, string des)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var b = new TblBedStatusHistory { Bed = bed, Date = DateTime.Now, Des = des, User = user, Status = status };
                    var (result, msg) = await _unitOfWork.GetRepository<TblBedStatusHistory>().Insert(b, user);
                    if (result)
                    {
                        var be = _unitOfWork.GetRepository<TblBed>().GetSingleSync(a => a.Id == bed);
                        be.Status = status;
                        _unitOfWork.Save();
                    }
                    m.Msg = result ? string.Format(Resources.Public.AddSuccessMessage, "تغییر وضعیت تخت") :
                        string.Format(Resources.Public.AddErrorMessage, "تغییر وضعیت تخت", msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                #pragma warning disable 4014
                Logs.CatchLog(e, user, Request);
                #pragma warning restore 4014
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public PartialViewResult Lists(int id)
        {
            var beds = _unitOfWork.GetRepository<TblBed>().GetSync(a => a.TblRoom.Restaurant == id);
            var list = _unitOfWork.GetRepository<TblBedStatu>().GetSync();
            ViewBag.Beds = beds.ToList();
            return PartialView(list);
        }
        // GET: Welfare/BedStatus
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            var sortDirection = Request["order[0][dir]"];
            var draw = Request["draw"];
            length = length == 0 ? 10 : length;
            if (string.IsNullOrEmpty(sortColumnName))
            {
                sortColumnName = "Id";
                sortDirection = "desc";
            }
            var iResult = _unitOfWork.GetRepository<TblBedStatu>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Title.Contains(searchValue) || a.Des.Contains(searchValue),
                    orderBy: sortColumnName + " " + sortDirection, take: length, skip: start) :
                    iResult.GetOrdered(orderBy: sortColumnName + " " + sortDirection, take: length, skip: start);
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                a.Title,
                a.Des,
                DT_RowId = a.Id,
                edit = $"<a href='/Welfare/BedStatus/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> " +
                      $"<a href='/Welfare/BedStatus/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Welfare/BedStatus/Details/5
        public async Task<ActionResult> Details(byte id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblBedStatu>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }


        // GET: Welfare/BedStatus/Create
        public async Task<ActionResult> Create()
        {
            return View();
        }

        // POST: Welfare/BedStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Des")] TblBedStatu entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblBedStatu>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/BedStatus/Edit/5
        public async Task<ActionResult> Edit(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblBedStatu>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }

            return View(result);
        }

        // POST: Welfare/BedStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Des")] TblBedStatu entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblBedStatu>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/BedStatus/Delete/5
        public async Task<ActionResult> Delete(byte? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblBedStatu>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Welfare/BedStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(byte id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblBedStatu>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
