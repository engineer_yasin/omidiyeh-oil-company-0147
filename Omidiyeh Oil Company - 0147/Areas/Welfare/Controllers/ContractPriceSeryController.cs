﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    public class ContractPriceSeryController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ContractPriceSeryController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }

        // GET: Welfare/ContractPriceSery
        public string Index()
        {
            return "Not Found";
        }

        public async Task<ActionResult> Add(int contract)
        {
            var contractPriceSery = (await _unitOfWork.GetRepository<TblContractPriceSery>().Get(a => a.Contract == contract));
            ViewBag.ContractPriceSery = "";
            ViewBag.Contract = contract;
          
            var foods = (await _unitOfWork.GetRepository<TblContractFood>().Get(a => a.Contract == contract
                    ,includeProperties: "TblFood,TblContractPriceSeriesFoods"))
               ;
            return PartialView(foods);
        }

        public ActionResult Edit(int id)
        {
            var entity = _unitOfWork.GetRepository<TblContractPriceSery>().GetSingleSync(a => a.Id == id);
            return PartialView(entity);
        }

        [HttpPost]

        public async Task<ActionResult> Edit(int id, DateTime fromDate,DateTime toDate, string title)

        {
            var m = new Message();
            try
            {

                var user = int.Parse(User.Identity.Name);
                var iResult = _unitOfWork.GetRepository<TblContractPriceSery>();
                var entity = await iResult.GetSingle(a => a.Id == id);
                entity.Title = title;
                entity.FromDate = fromDate;
                entity.ToDate = toDate;
                var (result, msg) = await _unitOfWork.GetRepository<TblContractPriceSery>().Update(entity, user);
                m.Msg = result ? string.Format(Resources.Public.EditSuccessMessage, "ویرایش سری قیمت ") :
                    string.Format(Resources.Public.EditErrorMessage, "ویرایش سری قیمت ", msg);
                m.Result = result;
                m.Type = result ? "success" : "error";

            }
            catch (Exception e)
            {
                Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> EditFood(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblContractPriceSeriesFood>();
            var result = await iResult.GetSingle(a => a.Id == id, includeProperties: "TblContractPriceSery,TblContractFood.TblFood");
            if (result == null)
            {
                return HttpNotFound();
            }

            return PartialView(result);
        }

        // POST: Welfare/ContractFood/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]

        public async Task<ActionResult> EditFood(int id, int price, int freePrice)

        {
            var m = new Message();
            try
            {

                var user = int.Parse(User.Identity.Name);
                var iResult = _unitOfWork.GetRepository<TblContractPriceSeriesFood>();
                var entity = await iResult.GetSingle(a => a.Id == id);
                entity.Price = price;
                entity.FreePrice = freePrice;
                var (result, msg) = await _unitOfWork.GetRepository<TblContractPriceSeriesFood>().Update(entity, user);
                m.Msg = result ? string.Format(Resources.Public.EditSuccessMessage, "ویرایش قیمت غذا") :
                    string.Format(Resources.Public.EditErrorMessage, "ویرایش قیمت غذا", msg);
                m.Result = result;
                m.Type = result ? "success" : "error";

            }
            catch (Exception e)
            {
                Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }

    }
}