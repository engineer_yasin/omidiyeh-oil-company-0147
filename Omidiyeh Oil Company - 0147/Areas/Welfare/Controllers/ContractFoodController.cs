﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;

using Omidiyeh_Oil_Company___0147.Models;
using System.Security.Cryptography.Xml;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    [Authorize()]
    public class ContractFoodController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ContractFoodController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        // GET: Welfare/ContractFood
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                {
                    if (Request["columns[" + Request[item] + "][name]"] == "Restaurant")
                        order += "TblContract.TblRestaurant.Title" + " ";
                    else
                        order += Request["columns[" + Request[item] + "][name]"] + " ";

                }
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var iResult = _unitOfWork.GetRepository<TblContractFood>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.TblFood.Title.Contains(searchValue) || a.TblContract.Title.Contains(searchValue),
                    orderBy: order, take: length, skip: start,
                    includeProperties: "TblFood,TblContract,TblContract.TblContractRestaurants,TblContract.TblContractRestaurants.TblRestaurant") :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start,
                        includeProperties: "TblFood,TblContract,TblContract.TblContractRestaurants,TblContract.TblContractRestaurants.TblRestaurant");
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                Food = a.TblFood.Title,
                Contract = a.TblContract.Title,
                Restaurant = string.Join("، ", a.TblContract.TblContractRestaurants.Select(b => b.TblRestaurant.Title)),
                Price = 0,
                FreePrice = 0,
                InitialQuota = a.InitialQuota.ToString("#,0"),
                UsedQuota = a.UsedQuota.ToString("#,0"),
                RemainingQuota = a.RemainingQuota.ToString("#,0"),

                DT_RowId = a.Id,
                edit = $"<a href='/Welfare/ContractFoodPriceAdjustment/Create/{a.Id}' title='تعدیل قیمت' class='text-info ml-2' ><i class='fas fa-exchange-alt fa-2x'></i></a> " +
                       $"<a href='/Welfare/ContractFood/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> " +
                      $"<a href='/Welfare/ContractFood/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Add(int contract, string title)
        {
            var contractFood = (await _unitOfWork.GetRepository<TblContractFood>().Get(a => a.Contract == contract)).Select(s => s.Food);
            ViewBag.Food = new SelectList((await _unitOfWork.GetRepository<TblFood>().Get(
                a => !contractFood.Contains(a.Id) && a.Id > -1)).OrderBy(a => a.Title), "Id", "Title");
            ViewBag.Contract = contract;
            ViewBag.Title = title;
            return PartialView();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> Add([Bind(Include = "Food,Contract,InitialQuota,UsedQuota")] TblContractFood entity, int price, int freePrice)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    entity.RemainingQuota = entity.InitialQuota - entity.UsedQuota;
                    var (result, msg) = await _unitOfWork.GetRepository<TblContractFood>().Insert(entity, user);
                    var firstPriceSery = await _unitOfWork.GetRepository<TblContractPriceSery>()
                        .GetFirst(a => a.Contract == entity.Contract);
                    var foodPrice = new TblContractPriceSeriesFood
                    {
                        ContractFood = entity.Id,
                        FreePrice = freePrice,
                        Price = price,
                        ContractPriceSeries = firstPriceSery.Id
                    };
                    var (resultPriceSery, msgPriceSery) = await _unitOfWork.GetRepository<TblContractPriceSeriesFood>().Insert(foodPrice, user);

                    m.Msg = result ? string.Format(Resources.Public.AddSuccessMessage, "ایجاد غذا") :
                        string.Format(Resources.Public.AddErrorMessage, "ایجاد غذا", msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> Delete(int id)
        {
            var m = new Message();
            try
            {
                var contractFood = await _unitOfWork.GetRepository<TblContractFood>().GetFirst(a => a.Id == id);
                if (!await _unitOfWork.GetRepository<TblFoodReservation>().Any(
                    a => a.Food == contractFood.Food && a.Contract == contractFood.Contract))
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblContractFood>().Delete(id, user);
                    if (result)
                    {
                        m.Msg = Resources.Public.DeleteSuccessMessage;
                        m.Result = true;
                        m.Type = "success";
                    }
                    else
                    {
                        m.Msg = string.Format(Resources.Public.DeleteErrorMessage, message);
                        m.Result = false;
                        m.Type = "error";
                    }
                }
                else
                {
                    m.Msg = string.Format(Resources.Public.DeleteErrorMessage,
                        "این غذا در لیست درخواست های غذا وجود دارد");
                    m.Result = false;
                    m.Type = "error";
                }

            }
            catch (Exception e)
            {
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);

        }

        // GET: Welfare/ContractFood/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblContractFood>();
            var result = await iResult.GetSingle(a => a.Id == id,includeProperties:"TblContract,TblFood");
            if (result == null)
            {
                return HttpNotFound();
            }

            return PartialView(result);
        }

        // POST: Welfare/ContractFood/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
       
        public async Task<ActionResult> Edit(int id, int initialQuota)

        {
            var m = new Message();
            try
            {
                  
                    var user = int.Parse(User.Identity.Name);
                    var iResult = _unitOfWork.GetRepository<TblContractFood>();
                    var entity = await iResult.GetSingle(a => a.Id == id);
                    entity.InitialQuota = initialQuota;
                    entity.RemainingQuota = entity.InitialQuota - entity.UsedQuota;
                    var (result, msg) = await _unitOfWork.GetRepository<TblContractFood>().Update(entity, user);
                m.Msg = result ? string.Format(Resources.Public.EditSuccessMessage, "ویرایش غذا") :
                    string.Format(Resources.Public.EditErrorMessage, "ویرایش غذا", msg);
                m.Result = result;
                m.Type = result ? "success" : "error";

            }
            catch (Exception e)
            {
                Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }


    }
}
