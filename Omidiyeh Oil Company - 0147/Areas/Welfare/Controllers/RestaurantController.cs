﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;

using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    
    public class RestaurantController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public RestaurantController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }

        #region Reception

        [Authorize]
        public async Task<ActionResult> Reception(int id =0)
        {
            if (id == 0)
            {
                var user = Convert.ToInt32(User.Identity.Name);
                id = _unitOfWork.GetRepository<TblRestaurantUser>().GetSync(a => a.User == user).First().Restaurant;
            }
            ViewBag.Statues = _unitOfWork.GetRepository<TblBedStatu>().GetSync().ToList();
            var iResult = _unitOfWork.GetRepository<TblRestaurant>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        public  ActionResult ReceptionRooms(int id)
        {
            var rooms = _unitOfWork.GetRepository<TblRoom>().GetSync(a => a.Restaurant == id,
                includeProperties: "TblBeds,TblBeds.TblBedStatu,TblRoomStatu");
            return PartialView(rooms.OrderBy(a => a.Number));
        }
        public  ActionResult ReceptionList(int id)
        {
            var rooms = _unitOfWork.GetRepository<TblBed>().GetSync(a => a.TblRoom.Restaurant == id,
                includeProperties: "TblBedStatu,TblRoom");
            return PartialView(rooms.OrderBy(a => a.Number));
        }
        #endregion
        //[OutputCache(Duration = 86400, VaryByCustom = "byUser"/*, Location = System.Web.UI.OutputCacheLocation.Client*/)]

        public JsonResult Items()
        {
            var user = Convert.ToInt32(User.Identity.Name);
            if ( _unitOfWork.GetRepository<TblRestaurantUser>().AnySync(a => a.User == user))
            {
                return  JsonListUser(user);
            }
            var office =  UserOfficeDepartment(user);
            return  JsonList(office);
        }

        public async Task<JsonResult> Contract(int id)
        {
            var cr = (await _unitOfWork.GetRepository<TblContractRestaurant>().Get(a => a.Contract == id,includeProperties: "TblRestaurant"))
                .Select(a=>new {a.TblRestaurant.Title, a.TblRestaurant.Id});
            
            return Json(cr, JsonRequestBehavior.AllowGet);
        }
        private int UserOfficeDepartment(int user)
        {
            return  _unitOfWork.GetRepository<TblUser>().GetSingleSync(a => a.Id == user, includeProperties: "TblPerson").TblPerson.OfficeDepartment;
        }
        // GET: Welfare/Restaurant/JsonList/?office=5
        //[OutputCache(Duration = 600)]
        public JsonResult JsonList(int office = 0)
        {
            Expression<Func<TblOfficeRestaurant, bool>> filter = (a) =>
                (office == 0 || (a.OfficeDepartment == office ));
            var listNot = ( _unitOfWork.GetRepository<TblOfficeRestaurant>().GetSync(filter))
                .Select(b => b.Restaurant);
            var listOfficeRestaurant = ( _unitOfWork.GetRepository<TblRestaurant>().GetSync(a => !listNot.Contains(a.Id) && a.Status == 1)).OrderBy(a=>a.Title).ToList();
            return Json(listOfficeRestaurant, JsonRequestBehavior.AllowGet);
        }

        //[OutputCache(Duration = 600)]
        public JsonResult JsonListUser(int user = 0)
        {
            var res = ( _unitOfWork.GetRepository<TblRestaurantUser>().GetSync(a => a.User == user)).Select(b => b.Restaurant).ToList();
            var listUserRestaurant = ( _unitOfWork.GetRepository<TblRestaurant>().GetSync(a => res.Contains(a.Id)))
                .Select(a=> new { a.Title, a.Id})
                .OrderBy(a => a.Title).ToList();
            return Json(listUserRestaurant, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "102")]
        // GET: Welfare/Restaurant
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            var sortDirection = Request["order[0][dir]"];
            var draw = Request["draw"];
            length = length == 0 ? 10 : length;
            if (string.IsNullOrEmpty(sortColumnName))
            {
                sortColumnName = "Id";
                sortDirection = "desc";
            }
            var iResult = _unitOfWork.GetRepository<TblRestaurant>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Title.Contains(searchValue) || a.Address.Contains(searchValue),
                    orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                    includeProperties: "TblRestaurantType,TblRestaurantStatu") :
                    iResult.GetOrdered(orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                        includeProperties: "TblRestaurantType,TblRestaurantStatu");
            var (items, count, total) = await result;

            var data = items
                .Where(a => a.Id != 0)
                .Select(a => new
            {
                id = " ",
                a.Title,
                a.Tel,
                a.NumberOfRooms,
                ConfirmReservation=  a.ConfirmReservation ? "بلی": "خیر",
                Type = a.TblRestaurantType.Title,
                Status = a.TblRestaurantStatu.Title,
                DT_RowId = a.Id,
                edit = $"<a href='/Welfare/Restaurant/reception/{a.Id}' class='text-primary ml-2' ><i class='fas fa-eye fa-lg'></i></a> " +
                 $"<a href='/Welfare/Restaurant/details/{a.Id}' class='text-primary ml-2' ><i class='fas fa-cogs fa-lg'></i></a> " +
                      $"<a href='/Welfare/Restaurant/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-lg'></i></a>" +
                      $"<a href='/Welfare/Restaurant/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-lg'></i></a>" 
            })
                .ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Welfare/Restaurant/Details/5
        public async Task<ActionResult> Details(int id)
        {
            var iResult = _unitOfWork.GetRepository<TblRestaurant>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        public async Task<ActionResult> Rooms(int id,bool edit = true)
        {
            var rooms = await _unitOfWork.GetRepository<TblRoom>().Get(a => a.Restaurant == id,
                includeProperties: "TblBeds,TblBeds.TblBedStatu,TblRoomStatu");
            ViewBag.Edit = edit;
            return PartialView(rooms.OrderBy(a=>a.Number));
        }
        
        [Authorize(Roles = "103")]

        // GET: Welfare/Restaurant/Create
        public async Task<ActionResult> Create()
        {
            
            var list = new List<SelectListItem>
            {
                new SelectListItem {Value = "false", Text = @"خیر"},
                new SelectListItem {Value = "true", Text = @"بلی"}
            };
            ViewBag.ConfirmReservation = new SelectList(list,"Value","Text");
            ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblRestaurantStatu>().Get(), "Id", "Title");
            ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<TblRestaurantType>().Get(), "Id", "Title");
            
            return View();
        }

        // POST: Welfare/Restaurant/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "103")]

        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Address,Tel,NumberOfRooms,Status,Type,ConfirmReservation")] TblRestaurant entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblRestaurant>().Insert(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }
                var list = new List<SelectListItem>
                {
                    new SelectListItem {Value = "false", Text = @"خیر"},
                    new SelectListItem {Value = "true", Text = @"بلی"}
                };
                ViewBag.ConfirmReservation = new SelectList(list, "Value", "Text");
                ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblRestaurantStatu>().Get(), "Id", "Title");
                ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<TblRestaurantType>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/Restaurant/Edit/5
        [Authorize(Roles = "104")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblRestaurant>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            var list = new List<SelectListItem>
            {
                new SelectListItem {Value = "false", Text = @"خیر"},
                new SelectListItem {Value = "true", Text = @"بلی"}
            };
            ViewBag.ConfirmReservation = new SelectList(list, "Value", "Text",result.ConfirmReservation);
            ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblRestaurantStatu>().Get(), "Id", "Title",result.Status);
            ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<TblRestaurantType>().Get(), "Id", "Title",result.Type);
            return View(result);
        }

        // POST: Welfare/Restaurant/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "104")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Address,Tel,NumberOfRooms,Status,Type,ConfirmReservation")] TblRestaurant entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblRestaurant>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }
                var list = new List<SelectListItem>
                {
                    new SelectListItem {Value = "false", Text = @"خیر"},
                    new SelectListItem {Value = "true", Text = @"بلی"}
                };
                ViewBag.ConfirmReservation = new SelectList(list, "Value", "Text", entity.ConfirmReservation);
                ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblRestaurantStatu>().Get(), "Id", "Title",entity.Status);
                ViewBag.Type = new SelectList(await _unitOfWork.GetRepository<TblRestaurantType>().Get(), "Id", "Title",entity.Type);
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/Restaurant/Delete/5
        [Authorize(Roles = "105")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblRestaurant>();
            var result = await iResult.GetSingle(a=>a.Id==id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Welfare/Restaurant/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "105")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblRestaurant>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
