﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using MD.PersianDateTime;
using Newtonsoft.Json;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    public class ReportController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db;

        public ReportController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            _db = new dbEntities();
            var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/Reports/license.key");
            Stimulsoft.Base.StiLicense.LoadFromFile(path);
        }

        #region گزارش تعداد فروش پیمان
        public async Task<ActionResult> Report1()
        {
            ViewBag.Contract = new SelectList(await _unitOfWork.GetRepository<TblContract>().Get(a => a.Id > 1 && a.Status == 1 ), "Id", "Title");
            return View();
        }

        public class Foods
        {
            public int Id { get; set; }
            public string Date { get; set; }
            public string Food { get; set; }
            public string MealTitle { get; set; }
            public int Count { get; set; }
            public int Meal { get; set; }
            public int Row { get; set; }
        }
        // [HttpPost]
        public async Task<ActionResult> Report1Items(DateTime startDate, DateTime endDate, int contract)
        {


            ;

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            // var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 1000 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = @"TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice,
                                        TblRestaurant,
                                        TblFood,
                                        TblMeal,
                                        TblFoodReservationGroup.TblUser.TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber"
                ;
            var list = _unitOfWork.GetRepository<TblFoodReservation>().GetSync(
                a => a.Contract == contract &&
                a.Date >= startDate && a.Date <= endDate &&
                a.Food != -1 &&
                a.TblFoodReservationGroup.Temp == false &&
                a.TblFoodReservationGroup.Remove == false
                , includeProperties: includeProperties);

            var items = list.ToList();
            if (!items.Any())
            {
                TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                return PartialView("_Message");
                // return null;
            }


            if (items.Any())
            {


                var foods = items.GroupBy(g => new { g.Date, g.Meal, g.Food }).Select(a => new Foods
                {
                    Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd"),
                    Meal = a.First().Meal,
                    MealTitle = a.First().TblMeal.Title,
                    Food = a.First().TblFood.Title,
                    Count = a.Count(),

                }).ToList();

                var listGroup = foods.GroupBy(g => new { g.Meal, g.Date }).Select(a => new { a.First().Meal, a.First().Date, Count = a.Count() }).ToList()
                    .OrderBy(a => a.Meal);

                foreach (var item in listGroup)
                {
                    var j = item.Count;
                    var l = foods.Where(a => a.Date == item.Date && a.Meal == item.Meal);
                    foreach (var f in l)
                    {
                        f.Row = j--;
                    }
                }

                var co = await _unitOfWork.GetRepository<TblContract>().GetFirst(a => a.Id == contract, includeProperties: "TblContractor");
                // var re = await _unitOfWork.GetRepository<TblRestaurant>().GetFirst(a => a.Id == restaurant);
                var header = new
                {
                    StartDate = new PersianDateTime(startDate).ToString("yyyy/MM/dd"),
                    EndDate = new PersianDateTime(endDate).ToString("yyyy/MM/dd"),

                    Contract = co.Title,
                    Contractor = co.TblContractor.Title,
                    //Restaurant = re.Title
                };
                var data = new
                {
                    header = header,
                    foods = foods.OrderBy(a => a.Meal)
                };
                TempData["res"] = new JavaScriptSerializer().Serialize(data);


                return PartialView();
                // return Json(data, JsonRequestBehavior.AllowGet);
            }

            return null;
        }

        public ActionResult GetReport1()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/Report1.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }
        #endregion

        #region Report2
        public async Task<ActionResult> Report2()
        {
            ViewBag.Contract = new SelectList(await _unitOfWork.GetRepository<TblContract>().Get(), "Id", "Title");
            return View();
        }
        public ActionResult Report2Checkout(int year, int month, int contract, List<int> restaurant)
        {
            var m = new Message();

            var user = Convert.ToInt32(User.Identity.Name);
            var (startDate, endDate) = Statics.GetMonthDate(year, month);
            foreach (var item in restaurant)
            {
                var entity = new TblContractRestaurantCheckout
                {
                    Restaurant = item,
                    Contract = contract,
                    Date = DateTime.Now,
                    FromDate = startDate,
                    ToDate = endDate,
                    User = user
                };
                if (_db.TblContractRestaurantCheckouts.Any(a => a.Restaurant == item && a.Contract == contract && a.FromDate == startDate && a.ToDate == endDate))
                {
                    m.Msg = string.Format(Resources.Public.ErrorMessage, "تسویه این پیمان و رستوران در تاریخ انتخاب شده قبلا ثبت شده است.");
                    m.Result = true;
                    m.Type = "success";
                }
                else
                {
                    var (result, message) = _unitOfWork.GetRepository<TblContractRestaurantCheckout>().InsertSync(entity, user);
                    if (result)
                    {
                        m.Msg = string.Format(Resources.Public.AddSuccessMessage, "ثبت تسویه حساب");
                        m.Result = true;
                        m.Type = "success";
                    }
                    else
                    {
                        var msg = string.Format(Resources.Public.ErrorMessage
                            , message);
                        m.Msg = msg;
                        m.Result = false;
                        m.Type = "error";
                    }
                }
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]
        public async Task<ActionResult> Report2Items(int year, int month, int contract, List<int> restaurant)
        {
            var (startDate, endDate) = Statics.GetMonthDate(year, month);
            const string includeProperties = @"
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                        TblFoodReservationGroup,TblMeal,
                                        TblContract.TblContractPriceSeries,
                                        TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods,
                                        TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods.TblContractFood
                                        "
                ;
            //var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
            //    a => a.Contract == contract && a.Restaurant == restaurant && a.Date >= startDate && a.Date <= endDate
            //         && a.Food != -1 && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false, includeProperties: includeProperties);

            var list = _db.TblFoodReservations
                .Include(a => a.TblFoodReservationGroup)
                .Include(a => a.TblMeal)
                .Include(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber)
                .Include(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment)
                .Include(a => a.TblContract.TblContractPriceSeries)
                .Include(a => a.TblContract)

                .Where(
                 a => a.Contract == contract &&
                   restaurant.Contains(a.Restaurant) &&
                 a.Date >= startDate && a.Date <= endDate
                     && a.Food != -1 && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false
                ).ToList();

            if (!list.Any())
            {
                TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                return PartialView("_Message");
            }

            var data = list.Select(a => new
            {

                Office = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Title,
                AccountNumber = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber,
                Meal = a.Meal,
                MealTitle = a.TblMeal.Title,
                Price = a.TblContract.TblContractPriceSeries.Where(b => b.FromDate <= a.Date && b.ToDate >= a.Date && b.TblContractPriceSeriesFoods.Any(c => c.TblContractFood.Food == a.Food))//.Select(s => s.Title)
                .Select(ss => ss.TblContractPriceSeriesFoods.Where(w => w.TblContractFood.Food == a.Food).Sum(s => s.Price)).Sum()

            })
                .OrderBy(a => a.AccountNumber).ToList();

            var co = await _unitOfWork.GetRepository<TblContract>().GetFirst(a => a.Id == contract, includeProperties: "TblContractor");
            var re = _unitOfWork.GetRepository<TblRestaurant>().GetSync(a => restaurant.Contains(a.Id)).ToList();
            var rest = string.Join("،", re.Select(a => a.Title));
            var header = new
            {
                Title = $"فرم تسویه حساب {Statics.MonthName(month)} ماه سال {year} پیمان {co.Title} {co.TblContractor.Title} در رستوران {rest}",
                Month = Statics.MonthName(month),
                Year = year,
                Contract = co.Title,
                Contractor = co.TblContractor.Title,
                Restaurant = ""
            };

            //TempData["data"] = data;
            //TempData["header"] = header;

            var res = new
            {
                data,
                header
            };
            var JsonSerializer = new JavaScriptSerializer();
            JsonSerializer.MaxJsonLength = Int32.MaxValue;
            TempData["res"] = JsonSerializer.Serialize(res);


            //return Json(res, JsonRequestBehavior.AllowGet);
            return PartialView();
        }
        public ActionResult GetReport2()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/Report2.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        #endregion
        #region Report2_2
        public async Task<ActionResult> Report2_2()
        {
            ViewBag.Contract = new SelectList(await _unitOfWork.GetRepository<TblContract>().Get(), "Id", "Title");
            return View();
        }
        [Authorize(Roles = "286")]
        public ActionResult Report2_2Checkout(DateTime startDate, DateTime endDate, int year, int month, int contract, List<int> restaurant)
        {
            var m = new Message();

            var user = Convert.ToInt32(User.Identity.Name);
            // var (startDate, endDate) = Statics.GetMonthDate(year, month);
            foreach (var item in restaurant)
            {
                var entity = new TblContractRestaurantCheckout
                {
                    Restaurant = item,
                    Contract = contract,
                    Date = DateTime.Now,
                    FromDate = startDate,
                    ToDate = endDate,
                    User = user
                };
                if (_db.TblContractRestaurantCheckouts.Any(a => a.Restaurant == item && a.Contract == contract && a.FromDate == startDate && a.ToDate == endDate))
                {
                    m.Msg = string.Format(Resources.Public.ErrorMessage, "تسویه این پیمان و رستوران در تاریخ انتخاب شده قبلا ثبت شده است.");
                    m.Result = true;
                    m.Type = "success";
                }
                else
                {
                    var (result, message) = _unitOfWork.GetRepository<TblContractRestaurantCheckout>().InsertSync(entity, user);
                    if (result)
                    {
                        m.Msg = string.Format(Resources.Public.AddSuccessMessage, "ثبت تسویه حساب");
                        m.Result = true;
                        m.Type = "success";
                    }
                    else
                    {
                        var msg = string.Format(Resources.Public.ErrorMessage
                            , message);
                        m.Msg = msg;
                        m.Result = false;
                        m.Type = "error";
                    }
                }
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]
        public async Task<ActionResult> Report2_2Items(DateTime startDate, DateTime endDate, int year, int month, int contract, List<int> restaurant)
        {
            // var (startDate, endDate) = Statics.GetMonthDate(year, month);
            //const string includeProperties = @"
            //                            TblFoodReservationGroup.TblOfficeDepartmentAccountNumber,
            //                            TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
            //                            TblFoodReservationGroup,TblMeal,
            //                            TblContract.TblContractPriceSeries,
            //                            TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods,
            //                            TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods.TblContractFood
            //                            "
            //    ;
            //var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
            //    a => a.Contract == contract && a.Restaurant == restaurant && a.Date >= startDate && a.Date <= endDate
            //         && a.Food != -1 && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false, includeProperties: includeProperties);

            var list = _db.TblFoodReservations
                .Include(a => a.TblFoodReservationGroup)
                .Include(a => a.TblMeal)
                .Include(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber)
                .Include(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment)
                // .Include(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement)
                .Include(a => a.TblContract.TblContractPriceSeries)
                .Include(a => a.TblContract)
                .Where(
                 a => a.Contract == contract &&
                     restaurant.Contains(a.Restaurant) &&
                     a.Date >= startDate && a.Date <= endDate
                     && a.Food != -1 && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false
                )
                .OrderBy(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary)
                .ThenBy(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber)
                .ToList();

            if (!list.Any())
            {
                TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                return PartialView("_Message");
            }

            var data = list.Select(a => new
            {

                Office = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Title
                // + " - " + a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary
                ,
                AccountNumber = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber,
                Meal = a.Meal,
                MealTitle = a.TblMeal.Title,
                Price = a.TblContract.TblContractPriceSeries.Where(b => b.FromDate <= a.Date && b.ToDate >= a.Date && b.TblContractPriceSeriesFoods.Any(c => c.TblContractFood.Food == a.Food))//.Select(s => s.Title)
                .Select(ss => ss.TblContractPriceSeriesFoods.Where(w => w.TblContractFood.Food == a.Food).Sum(s => s.Price)).Sum()

            })
                //.OrderBy(a => a.AccountNumber)
                .ToList();

            var co = await _unitOfWork.GetRepository<TblContract>().GetFirst(a => a.Id == contract, includeProperties: "TblContractor");
            var re = _unitOfWork.GetRepository<TblRestaurant>().GetSync(a => restaurant.Contains(a.Id)).ToList();
            var rest = string.Join("،", re.Select(a => a.Title));
            var header = new
            {
                Title = $"فرم تسویه حساب {Statics.MonthName(month)} ماه سال {year}  از تاریخ {new PersianDateTime(startDate).ToString("yyyy/MM/dd")} تا {new PersianDateTime(endDate).ToString("yyyy/MM/dd")} پیمان {co.Title} {co.TblContractor.Title} در رستوران {rest}",
                Month = Statics.MonthName(month),
                Year = year,
                Contract = co.Title,
                Contractor = co.TblContractor.Title,
                Restaurant = ""
            };

            //TempData["data"] = data;
            //TempData["header"] = header;

            var res = new
            {
                data,
                header
            };
            var JsonSerializer = new JavaScriptSerializer();
            JsonSerializer.MaxJsonLength = Int32.MaxValue;
            TempData["res"] = JsonSerializer.Serialize(res);


            //return Json(res, JsonRequestBehavior.AllowGet);
            return PartialView();
        }
        //public ActionResult GetReport2()
        //{

        //    var report = StiReport.CreateNewReport();
        //    var path = Server.MapPath("~/Content/Reports/Report2.mrt");
        //    report.Load(path);
        //    var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

        //    report.Dictionary.Databases.Clear();
        //    report.Dictionary.DataSources.Clear();
        //    report.RegData(data);
        //    report.Dictionary.Synchronize();

        //    return StiMvcViewer.GetReportResult(report);
        //}

        #endregion

        #region Report3
        public async Task<ActionResult> Report3()
        {
            ViewBag.Office = new SelectList((await _unitOfWork.GetRepository<TblOffice>().Get()).OrderBy(a => a.Title), "Id", "Title");
            return View();
        }

        // [HttpPost]
        public async Task<ActionResult> Report3Items(DateTime startDate, DateTime endDate, List<int> office/*,int officeDepartment, int personType*/)
        {
            //var (startDate, endDate) = Statics.GetMonthDate(year, month);
            const string includeProperties = @"
                                        TblPerson,TblMeal,
                                        TblContract.TblContractPriceSeries,
                                        TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods,
                                        TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods.TblContractFood
                                        "
                ;
            var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
                a => office.Contains(a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office) && a.Date >= startDate && a.Date <= endDate
                     && a.Food != -1 && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false, includeProperties: includeProperties);

            if (!list.Any())
            {
                TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                //return null;
                return PartialView("_Message");
            }
            var data = list.GroupBy(g => new { g.Person, g.Meal }).Select(a => new
            {
                a.First().TblPerson.Id,
                a.First().TblPerson.NationalCode,
                a.First().TblPerson.PersonnelCode,
                a.First().TblPerson.Name,
                a.First().TblPerson.Family,
                Meal = a.First().Meal,
                MealTitle = a.First().TblMeal.Title,
                Count = a.Count(),
                //Sum = 0 //(int)a.Sum(s => s.Price) Todo Price
                Sum = a.Sum(b1 => b1.TblContract.TblContractPriceSeries.Where(b => b.FromDate <= b1.Date && b.ToDate >= b1.Date && b.TblContractPriceSeriesFoods.Any(c => c.TblContractFood.Food == a.First().Food))
                .Select(ss => ss.TblContractPriceSeriesFoods.Where(w => w.TblContractFood.Food == a.First().Food).Sum(s => s.Price)).Sum())
            });

            foreach (var item in data)
            {
                var t = new
                {
                    item.Id,
                    item.NationalCode,
                    item.PersonnelCode,
                    item.Name,
                    item.Family,
                    Meal = 100000,
                    MealTitle = "مبلغ",
                    Count = item.Sum,
                    Sum = 0
                };
                data = data.Append(t);
            }

            data = data.OrderBy(a => a.Meal);

            var o = await _unitOfWork.GetRepository<TblOffice>().Get(a => office.Contains(a.Id));
            //  var d = await _unitOfWork.GetRepository<TblOfficeDepartment>().GetFirst(a => a.Id == officeDepartment);
            var header = new
            {
                StartDate = new PersianDateTime(startDate).ToString("yyyy/MM/dd"),
                EndDate = new PersianDateTime(endDate).ToString("yyyy/MM/dd"),

                Office = string.Join("، ", o.Select(a => a.Title)),

                //Restaurant = re.Title
            };

            //TempData["data"] = data;
            //TempData["header"] = header;

            var res = new
            {
                data,
                header
            };
            TempData["res"] = new JavaScriptSerializer().Serialize(res);

            //return Json( res , JsonRequestBehavior.AllowGet);
            return PartialView();
        }

        public ActionResult GetReport3()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/Report3.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        #endregion

        #region Report3_2

        //public class PricesModel
        //{
        //    public List<int> 
        //}
        public class Report3_2Model
        {
            public int Id { get; set; }
            public string NationalCode { get; set; }
            public string PersonnelCode { get; set; }
            public string Name { get; set; }
            public string Family { get; set; }
            public int Meal { get; set; }
            public string MealTitle { get; set; }
            public decimal? Count { get; set; }
            public string AccountNumber { get; set; }
            public string Office { get; set; }
            public string Restaurant { get; set; }
            //public List<List<List<int>>> Prices { get; set; }
            public decimal? Sum { get; set; }
        }

        public async Task<ActionResult> Report3_2()
        {
            ViewBag.Office = new SelectList((await _unitOfWork.GetRepository<TblOffice>().Get()).OrderBy(a => a.Title), "Id", "Title");
            return View();
        }

        // [HttpPost]
        public ActionResult Report3_2Items(DateTime startDate, DateTime endDate, List<int> office, List<int> restaurant)
        {
            try
            {
                //var (startDate, endDate) = Statics.GetMonthDate(year, month);
                //const string includeProperties = @"
                //                            TblPerson,TblMeal,TblRestaurant,
                //                            TblContract.TblContractPriceSeries,
                //                            TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods,
                //                            TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods.TblContractFood,
                //                            TblFoodReservationGroup.TblOfficeDepartmentAccountNumber,
                //                            TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice
                //                            "
                //    ;
                //var list2 = _unitOfWork.GetRepository<TblFoodReservation>().GetSync(
                //    a =>
                //    office.Contains(a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office) &&
                //    restaurant.Contains(a.Restaurant) &&
                //    a.Date >= startDate && a.Date <= endDate
                //         && a.Food != -1 && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false, includeProperties: includeProperties);
                //var list = _db.TblFoodReservations
                //    .Include(a => a.TblPerson)
                //    .Include(a => a.TblMeal)
                //    .Include(a => a.TblRestaurant)
                //    .Include(a => a.TblContract.TblContractPriceSeries)
                //    .Include(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber)
                //    .Include(a =>
                //        a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice)
                //    .Include(a => a.TblContract.TblContractFoods)
                //    .Include(a => a.TblContract)
                //    .Where(
                //        a =>
                //            office.Contains(a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber
                //                .TblOfficeDepartment.Office) &&
                //            restaurant.Contains(a.Restaurant) &&
                //            a.Date >= startDate && a.Date <= endDate &&
                //            a.Food != -1 &&
                //            a.TblFoodReservationGroup.Temp == false &&
                //            a.TblFoodReservationGroup.Remove == false
                //    ).ToList();
               
                var list = _db.TblFoodReservations
                        .AsNoTracking()
                    .Where(
                        a =>
                            office.Contains(a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber
                                .TblOfficeDepartment.Office) &&
                            restaurant.Contains(a.Restaurant) &&
                            a.Date >= startDate && a.Date <= endDate &&
                            a.Food != -1 &&
                            a.TblFoodReservationGroup.Temp == false &&
                            a.TblFoodReservationGroup.Remove == false
                           // && a.Person ==-2
                    )
                    .Select(a => new FoodReservationHelp
                    {
                        Person = a.Person,
                        NationalCode = a.TblPerson.NationalCode,
                        PersonnelCode = a.TblPerson.PersonnelCode,
                        Restaurant = a.Restaurant,
                        Name = a.TblPerson.Name,
                        Family = a.TblPerson.Family,
                        Meal = a.Meal,
                        MealTitle = a.TblMeal.Title,
                        AccountNumberTitle = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber,
                        OfficeTitle = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment
                            .TblOffice.Title,
                        RestaurantTitle = a.TblRestaurant.Title,
                        Price = a.TblContract.TblContractPriceSeries
                            .Where(w => w.Contract == a.Contract && a.Date >= w.FromDate && a.Date <= w.ToDate).Sum(
                            b => b.TblContractPriceSeriesFoods
                                .Where(w => w.TblContractFood.Food == a.Food)
                                .Sum(s => s.Price)
                            ),
                        //Price =0
                    })

                    .ToList();
                if (!list.Any())
                {
                    TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                    //return null;
                    return PartialView("_Message");
                }

                var data = list.GroupBy(g => new { g.Person, g.Meal,g.Restaurant }).Select(a => new Report3_2Model
                {
                    Id = a.First().Person,
                    NationalCode = a.First().NationalCode,
                    PersonnelCode = a.First().PersonnelCode,
                    Name = a.First().Name,
                    Family = a.First().Family,
                    Meal = a.First().Meal,
                    MealTitle = a.First().MealTitle,
                    Count = (decimal)a.Count(),
                    AccountNumber = a.First().AccountNumberTitle,
                    Office = a.First().OfficeTitle,
                    Restaurant = a.First().RestaurantTitle,
                    Sum = a.Sum(s => s.Price)

                }).ToList();
                

                //var cc = data.Count();
                var data2 = new List<Report3_2Model>();
                foreach (var item in data.ToList())
                {

                    var t = new Report3_2Model
                    {
                        Id = item.Id,
                        NationalCode = item.NationalCode,
                        PersonnelCode = item.PersonnelCode,
                        Name = item.Name,
                        Family = item.Family,
                        Meal = 100000,
                        MealTitle = "مبلغ",
                        Count = item.Sum,
                        AccountNumber = item.AccountNumber,
                        Office = item.Office,
                        Restaurant = item.Restaurant,
                        Sum = 0m
                    };
                    data2.Add(t);
                }

                data2.AddRange(data);
                data2 = data2.OrderBy(a => a.Meal).ToList();

                //var o = await _unitOfWork.GetRepository<TblOffice>().Get(a => office.Contains(a.Id));
                //  var d = await _unitOfWork.GetRepository<TblOfficeDepartment>().GetFirst(a => a.Id == officeDepartment);
                var header = new
                {
                    StartDate = new PersianDateTime(startDate).ToString("yyyy/MM/dd"),
                    EndDate = new PersianDateTime(endDate).ToString("yyyy/MM/dd"),

                    Office = "", //string.Join("، ", o.Select(a => a.Title)),

                    Title = "گزارش افراد رزرو شده اداره "
                            //+ string.Join("، ", o.Select(a => a.Title))
                            + " از تاریخ " + new PersianDateTime(startDate).ToString("yyyy/MM/dd") + " تا تاریخ "
                            + new PersianDateTime(endDate).ToString("yyyy/MM/dd")
                    //Restaurant = re.Title
                };

                //TempData["data"] = data;
                //TempData["header"] = header;

                var res = new
                {
                    data = data2,
                    header
                };
                // TempData["res"] = new JavaScriptSerializer().Serialize(res);

                var jsonSerializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };


                TempData["res"] = jsonSerializer.Serialize(res);
                // TempData["res"] = res;
                //  Stimulsoft.Base.Json.Linq.JToken j = new Stimulsoft.Base.Json.Linq.JToken();

                //return Json( res , JsonRequestBehavior.AllowGet);
                return PartialView();
            }
            catch (Exception e)
            {
                TempData["errorToast"] = e.Message;

                //return null;
                return PartialView("_Message");
            }
        }

        public ActionResult GetReport3_2()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/Report3-2.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        #endregion

        #region Report9
        public async Task<ActionResult> Report9()
        {
            ViewBag.Office = new SelectList((await _unitOfWork.GetRepository<TblOffice>().Get()).OrderBy(a => a.Title), "Id", "Title");
            return View();
        }

        // [HttpPost]
        public ActionResult Report9Items(DateTime startDate, DateTime endDate, List<int> office, List<int> restaurant)
        {
            var list = _db.TblFoodReservations.AsNoTracking()
                .Where(a => a.TblFoodReservationGroup.Type == 4 &&
                          a.Date >= startDate && a.Date <= endDate &&
                          restaurant.Contains(a.Restaurant) &&
                          a.Food != -1 &&
                          a.TblFoodReservationGroup.Temp == false &&
                          a.TblFoodReservationGroup.Remove == false)
                 .Include(a => a.TblFoodReservationGroup)
                 .Include(a => a.TblRestaurant)
                 .Include(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber)
                 .Include(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice)
                 .Include(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment)
                 //.Take(20000000)
                 .Where(a => office.Contains(a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office))
                 .ToList();
            if (!list.Any())
            {
                TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                //return null;
                return PartialView("_Message");
            }
            var data = list.OrderBy(a => a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Title)
                .GroupBy(g => new { g.TblFoodReservationGroup.AccountNumber }).Select(a => new
                {
                    OfficeDepartment = a.FirstOrDefault().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Title,
                    Office = a.FirstOrDefault().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Title,
                    AccountNumber = a.FirstOrDefault().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber,
                    Bed = a.Count(b => b.Meal == 0),
                    Restaurant=  string.Join("،",a.GroupBy(b => b.Restaurant).Select(b=>b.First().TblRestaurant.Title)),
                    BN = a.Count(b => b.Meal == 1 && b.Date.DayOfWeek != DayOfWeek.Thursday && b.Date.DayOfWeek != DayOfWeek.Friday),
                    BT = a.Count(b => b.Meal == 1 && b.Date.DayOfWeek == DayOfWeek.Thursday),
                    BF = a.Count(b => b.Meal == 1 && b.Date.DayOfWeek == DayOfWeek.Friday),
                    LN = a.Count(b => b.Meal == 2 && b.Date.DayOfWeek != DayOfWeek.Thursday && b.Date.DayOfWeek != DayOfWeek.Friday),
                    LT = a.Count(b => b.Meal == 2 && b.Date.DayOfWeek == DayOfWeek.Thursday),
                    LF = a.Count(b => b.Meal == 2 && b.Date.DayOfWeek == DayOfWeek.Friday),
                    DN = a.Count(b => b.Meal == 3 && b.Date.DayOfWeek != DayOfWeek.Thursday && b.Date.DayOfWeek != DayOfWeek.Friday),
                    DT = a.Count(b => b.Meal == 3 && b.Date.DayOfWeek == DayOfWeek.Thursday),
                    DF = a.Count(b => b.Meal == 3 && b.Date.DayOfWeek == DayOfWeek.Friday),
                    PDN = a.Count(b => b.Meal == 5 && b.Date.DayOfWeek != DayOfWeek.Thursday && b.Date.DayOfWeek != DayOfWeek.Friday),
                    PDT = a.Count(b => b.Meal == 5 && b.Date.DayOfWeek == DayOfWeek.Thursday),
                    PDF = a.Count(b => b.Meal == 5 && b.Date.DayOfWeek == DayOfWeek.Friday),

                    Count = a.Count(),

                });


            var header = new
            {
                Title = $"گزارش نوبتکار از تاریخ {new PersianDateTime(startDate).ToString("yyyy/MM/dd")} تا تاریخ {new PersianDateTime(endDate).ToString("yyyy/MM/dd")} ",

            };


            var res = new
            {
                data,
                header
            };
            TempData["res"] = new JavaScriptSerializer().Serialize(res);

            // return Json(res, JsonRequestBehavior.AllowGet);
            return PartialView();
        }

        public ActionResult GetReport9()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/Report9.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        #endregion
        #region Report4
        public async Task<ActionResult> Report4()
        {
            ViewBag.Contract = new SelectList((await _unitOfWork.GetRepository<TblContract>().Get()).OrderBy(a => a.Title), "Id", "Title");
            return View();
        }

        // [HttpPost]
        public ActionResult Report4Items(DateTime startDate, DateTime endDate, int contract/*,int officeDepartment, int personType*/)
        {
            try
            {
                //var (startDate, endDate) = Statics.GetMonthDate(year, month);
                //const string includeProperties = @"TblFood,TblContract.TblContractFoods,
                //                                TblContract.TblContractPriceSeries,
                //                                TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods,
                //                                TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods.TblContractFood
                //                                "
                //    ;
                //var list2 = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
                //    a => a.Contract == contract && a.Date >= startDate && a.Date <= endDate
                //         && a.Food != -1 && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false, includeProperties: includeProperties);

                var list = _db.TblFoodReservations
                .Where(
                    a => a.Contract == contract &&
                        a.Date >= startDate && a.Date <= endDate &&
                        a.Food != -1 &&
                        a.TblFoodReservationGroup.Temp == false &&
                        a.TblFoodReservationGroup.Remove == false
                )
                .Include(a => a.TblContract.TblContractFoods)
                .Select(a => new FoodReservationHelp
                {
                    Food = a.Food,
                    FoodTitle = a.TblFood.Title,
                    ContractTitle = a.TblContract.Title,
                    ContractInitialQuota = a.TblContract.TblContractFoods.FirstOrDefault(b => b.Food == a.Food).InitialQuota,
                    Person = a.Person,
                    NationalCode = a.TblPerson.NationalCode,
                    PersonnelCode = a.TblPerson.PersonnelCode,

                    Name = a.TblPerson.Name,
                    Family = a.TblPerson.Family,
                    Meal = a.Meal,
                    MealTitle = a.TblMeal.Title,
                    AccountNumberTitle = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber,
                    OfficeTitle = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment
                        .TblOffice.Title,
                    RestaurantTitle = a.TblRestaurant.Title,
                    Price = a.TblContract.TblContractPriceSeries
                        .Where(w => w.Contract == a.Contract && a.Date >= w.FromDate && a.Date <= w.ToDate).Sum(
                            b => b.TblContractPriceSeriesFoods
                                .Where(w => w.TblContractFood.Food == a.Food)
                                .Sum(s => s.Price)
                        ),
                    //Price =0
                })

                .ToList();
                if (!list.Any())
                {
                    TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                    //return null;
                    return PartialView("_Message");
                }
                var data = list.Where(a => a.Meal != 0).GroupBy(g => new { g.Food }).Select(a => new
                {
                    Title = a.First().FoodTitle,
                    InitialQuota = a.First().ContractInitialQuota,
                    // a.First().TblContract.TblContractFoods.FirstOrDefault(b => b.Food == a.First().Food)?.InitialQuota,
                    Contract = a.First().ContractTitle,
                    UsedQuota = a.Count(),
                    // a.First().TblContract.TblContractFoods.FirstOrDefault(b => b.Food == a.First().Food)?.UsedQuota,
                    Price = a.Sum(s => s.Price)
                    //Price = a.First().TblContract.TblContractPriceSeries.Where(b => b.FromDate <= startDate && b.ToDate >= endDate && b.TblContractPriceSeriesFoods.Any(c1 => c1.TblContractFood.Food == a.First().Food))
                    //.Select(ss => ss.TblContractPriceSeriesFoods.Where(w => w.TblContractFood.Food == a.First().Food).Sum(s => s.Price)).Sum()
                    //  a.First().TblContract.TblContractFoods.FirstOrDefault(b => b.Food == a.First().Food)?.TblContractPriceSeriesFoods.First().Price,
                });

                var c = _unitOfWork.GetRepository<TblContract>().GetSingleSync(a => a.Id == contract);
                //  var d = await _unitOfWork.GetRepository<TblOfficeDepartment>().GetFirst(a => a.Id == officeDepartment);
                var header = new
                {
                    StartDate = new PersianDateTime(startDate).ToString("yyyy/MM/dd"),
                    EndDate = new PersianDateTime(endDate).ToString("yyyy/MM/dd"),

                    Contract = c.Title,

                    //Restaurant = re.Title
                };

                //TempData["data"] = data;
                //TempData["header"] = header;

                var res = new
                {
                    data,
                    header
                };
                TempData["res"] = new JavaScriptSerializer().Serialize(res);

                //return Json( res , JsonRequestBehavior.AllowGet);
                return PartialView();
            }
            catch (Exception e)
            {
                TempData["errorToast"] = e.Message;

                //return null;
                return PartialView("_Message");
            }
        }
        public ActionResult GetReport4()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/Report4.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        #endregion
        #region Report5
        public async Task<ActionResult> Report5()
        {
            ViewBag.Office = new SelectList((await _unitOfWork.GetRepository<TblOffice>().Get()).OrderBy(a => a.Title), "Id", "Title");
            ViewBag.OfficeFor = new SelectList(await _unitOfWork.GetRepository<TblSubsidiary>().Get(a => a.Company == 1), "Id", "Title");

            return View();
        }

        // [HttpPost]
        public async Task<ActionResult> Report5Items(DateTime startDate, DateTime endDate, int? office, int? officeFor/*,int officeDepartment, int personType*/)
        {
            //var (startDate, endDate) = Statics.GetMonthDate(year, month);
            const string includeProperties = @"TblFood,TblMeal"
                ;

            var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
          a =>
            (officeFor == null || a.TblFoodReservationGroup.TblFoodReservationLetter.OfficeFor == officeFor) &&
            (office == null || a.TblFoodReservationGroup.TblFoodReservationLetter.Office == office) &&
               a.Date >= startDate && a.Date <= endDate
               && a.Food != -1 && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false, includeProperties: includeProperties)
          ;


            if (!list.Any())
            {
                TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                //return null;
                return PartialView("_Message");
            }


            var data = list.GroupBy(g => new { g.Date, g.Meal }).Select(a => new
            {
                Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd"),
                Meal = a.First().Meal,
                MealTitle = a.First().TblMeal.Title,
                Count = a.Count(),
                Des = ""

            });

            //foreach (var item in data)
            //{
            //    var d = new
            //    {
            //        item.Date,
            //        item.Meal,
            //        item.MealTitle,
            //        item.Count
            //    }
            //}

            var c = await _unitOfWork.GetRepository<TblOffice>().GetFirst(a => a.Id == office);
            var f = await _unitOfWork.GetRepository<TblSubsidiary>().GetFirst(a => a.Id == officeFor);
            //  var d = await _unitOfWork.GetRepository<TblOfficeDepartment>().GetFirst(a => a.Id == officeDepartment);
            var header = new
            {
                StartDate = new PersianDateTime(startDate).ToString("yyyy/MM/dd"),
                EndDate = new PersianDateTime(endDate).ToString("yyyy/MM/dd"),

                Office = c?.Title,
                OfficeFor = f?.Title,
                //Restaurant = re.Title
            };

            //TempData["data"] = data;
            //TempData["header"] = header;

            var res = new
            {
                data,
                header
            };
            TempData["res"] = new JavaScriptSerializer().Serialize(res);

            //return Json(res, JsonRequestBehavior.AllowGet);
            return PartialView();
        }
        public ActionResult GetReport5()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/Report5.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        #endregion
        #region Report7
        public async Task<ActionResult> Report7()
        {
            //  ViewBag.Office = new SelectList((await _unitOfWork.GetRepository<TblOffice>().Get()).OrderBy(a=>a.Title), "Id", "Title");
            ViewBag.OfficeFor = new SelectList(await _unitOfWork.GetRepository<TblSubsidiary>().Get(a => a.Company == 1), "Id", "Title");

            return View();
        }

        // [HttpPost]
        public async Task<ActionResult> Report7Items(DateTime startDate, DateTime endDate, int? office, int? officeFor/*,int officeDepartment, int personType*/)
        {
            //var (startDate, endDate) = Statics.GetMonthDate(year, month);
            const string includeProperties = @"TblFood,TblMeal,
                                                TblFoodReservationGroup.TblFoodReservationLetter.TblOffice,
                                                TblRestaurant,
                                                TblFoodReservationGroup.TblFoodReservationLetter,
                                                TblFoodReservationGroup,
                                                TblContract.TblContractPriceSeries,
                                                TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods,
                                                TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods.TblContractFood
                                                "
                ;

            var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
          a =>
            (officeFor == null || a.TblFoodReservationGroup.TblFoodReservationLetter.OfficeFor == officeFor) &&
            (office == null || a.TblFoodReservationGroup.TblFoodReservationLetter.Office == office) &&
               a.Date >= startDate && a.Date <= endDate
               && a.Food != -1 && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false, includeProperties: includeProperties)
          ;


            if (!list.Any())
            {
                TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                //return null;
                return PartialView("_Message");
            }


            var data = list.Where(a => a.TblFoodReservationGroup.Letter != null)
                .GroupBy(g => new { g.Meal, g.TblFoodReservationGroup.Id }).Select(a => new
                {
                    Date = new PersianDateTime(a.First().TblFoodReservationGroup.Date).ToString("yyyy/MM/dd"),
                    Meal = a.First().Meal,
                    MealTitle = a.First().TblMeal.Title,
                    Count = a.Count(),
                    Office = a.First().TblFoodReservationGroup.TblFoodReservationLetter.TblOffice.Title,
                    RequestId = a.First().TblFoodReservationGroup.Id,
                    Letter = a.First().TblFoodReservationGroup.TblFoodReservationLetter.Number,
                    Restaurant = a.First().TblRestaurant.Title,
                    //Sum = 0
                    Sum = a.Sum(b1 => b1.TblContract.TblContractPriceSeries.Where(b => b.FromDate <= b1.Date && b.ToDate >= b1.Date && b.TblContractPriceSeriesFoods.Any(c1 => c1.TblContractFood.Food == a.First().Food))
                        .Select(ss => ss.TblContractPriceSeriesFoods.Where(w => w.TblContractFood.Food == a.First().Food).Sum(s => s.Price)).Sum())
                });

            //foreach (var item in data)
            //{
            //    var d = new
            //    {
            //        item.Date,
            //        item.Meal,
            //        item.MealTitle,
            //        item.Count
            //    }
            //}

            var c = await _unitOfWork.GetRepository<TblOffice>().GetFirst(a => a.Id == office);
            var f = await _unitOfWork.GetRepository<TblSubsidiary>().GetFirst(a => a.Id == officeFor);
            //  var d = await _unitOfWork.GetRepository<TblOfficeDepartment>().GetFirst(a => a.Id == officeDepartment);
            var header = new
            {
                StartDate = new PersianDateTime(startDate).ToString("yyyy/MM/dd"),
                EndDate = new PersianDateTime(endDate).ToString("yyyy/MM/dd"),

                Office = c?.Title,
                OfficeFor = f?.Title,
                //Restaurant = re.Title
            };

            //TempData["data"] = data;
            //TempData["header"] = header;

            var res = new
            {
                data,
                header
            };
            TempData["res"] = new JavaScriptSerializer().Serialize(res);

            //return Json(res, JsonRequestBehavior.AllowGet);
            return PartialView();
        }
        public ActionResult GetReport7()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/Report7.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        #endregion

        #region Report6
        [Authorize(Roles = "205")]
        public ActionResult Report6()
        {
            ViewBag.Contract = new SelectList(_unitOfWork.GetRepository<TblContract>().GetSync(a => a.Id > 1), "Id", "Title");

            return View();
        }

        private class FoodsReport6
        {
            public int Line { get; set; }
            public string Title { get; set; }
            public int Price { get; set; }
            public string MealTitle { get; set; }
            public int Count { get; set; }
            public int Meal { get; set; }
            public Int64 Sum { get; set; }
        }
        // [HttpPost]
        public async Task<ActionResult> Report6Items(DateTime startDate, DateTime endDate, int year, int month, int contract, List<int> restaurant)
        {
            try
            {
               


                var list = _db.TblFoodReservations
                    .AsNoTracking()
                    .Include(a => a.TblFood)
                    .Include(a => a.TblMeal)
                    .Include(a => a.TblContract)
                    .Include(a => a.TblContract.TblContractPriceSeries)
                    .Where(a =>
                        a.Contract == contract && 
                        restaurant.Contains(a.Restaurant) &&
                        a.Date >= startDate && a.Date <= endDate &&
                        a.Food != -1 &&
                        a.TblFoodReservationGroup.Temp == false &&
                        a.TblFoodReservationGroup.Remove == false
                    ).ToList();

                //var list22 = _unitOfWork.GetRepository<TblFoodReservation>().GetSync(
                //     a => a.Contract == contract &&
                //     a.Date >= startDate && a.Date <= endDate &&
                //     a.Food != -1 &&
                //     a.TblFoodReservationGroup.Temp == false &&
                //     a.TblFoodReservationGroup.Remove == false
                //     , includeProperties: includeProperties);

                if (!list.Any())
                {
                    TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                    //return null;
                    return PartialView("_Message");
                }


                var data = list.GroupBy(g => new { g.Food, g.Meal }).Select(a => new FoodsReport6
                {
                    Line = 0,
                    Title = a.First().TblFood.Title,

                    Price = a.First().TblContract.TblContractPriceSeries.Where(b =>
                            b.FromDate <= startDate && b.ToDate >= endDate &&
                            b.TblContractPriceSeriesFoods.Any(c => c.TblContractFood.Food == a.First().Food))
                          .Select(ss =>
                              ss.TblContractPriceSeriesFoods.Where(w => w.TblContractFood.Food == a.First().Food)
                                  .Sum(s => s.Price)).Sum(),

                    //Price = 0,
                    Count = a.Count(),

                    //Sum = a.Sum(b1 => b1.TblContract.TblContractPriceSeries.Where(b =>
                    //        b.FromDate <= b1.Date && b.ToDate >= b1.Date &&
                    //        b.TblContractPriceSeriesFoods.Any(c => c.TblContractFood.Food == a.First().Food))
                    //    .Select(ss =>
                    //        ss.TblContractPriceSeriesFoods.Where(w => w.TblContractFood.Food == a.First().Food)
                    //            .Sum(s => s.Price)).Sum()),
                    Sum = 0,

                    MealTitle = a.First().TblMeal.Title,
                    Meal = a.First().Meal,

                }).ToList();

                var data2 = new List<FoodsReport6>();
                foreach (var meal in data.Select(a => a.Meal).Distinct())
                {
                    var l = 1;

                    foreach (var item in data.Where(a => a.Meal == meal))
                    {
                        item.Line = l++;
                        item.Sum = item.Price * item.Count;
                        data2.Add(item);
                    }
                }

                var total = data.Sum(a => a.Sum);
                var co = _unitOfWork.GetRepository<TblContract>()
                    .GetSingleSync(a => a.Id == contract, includeProperties: "TblContractor");
                // var re = _unitOfWork.GetRepository<TblRestaurant>().GetSingleSync(a => a.Id == restaurant);
                //  var d = await _unitOfWork.GetRepository<TblOfficeDepartment>().GetFirst(a => a.Id == officeDepartment);
                var header = new
                {
                    Title =
                        $"گزارش صورت وضعیت {Statics.MonthName(month)} ماه  سال {year} از تاریخ {new PersianDateTime(startDate).ToString("yyyy/MM/dd")} تا تاریخ {new PersianDateTime(endDate).ToString("yyyy/MM/dd")} ",
                    Title2 =
                        $"پیمان {co.Title} شماره {co.Number}  پیمانکار {co.TblContractor.Title}  ", // رستوران {re.Title}
                    StartDate = new PersianDateTime(startDate).ToString("yyyy/MM/dd"),
                    EndDate = new PersianDateTime(endDate).ToString("yyyy/MM/dd"),

                    Month = Statics.MonthName(month),
                    Year = year,

                    Contract = co.Title,
                    Contractor = co.TblContractor.Title,
                    Restaurant = "",
                    Total = total.ToString("N0")
                };

                //TempData["data"] = data;
                //TempData["header"] = header;

                var res = new
                {
                    data2,
                    header
                };
                TempData["res"] = new JavaScriptSerializer().Serialize(res);

                // return Json(res, JsonRequestBehavior.AllowGet);
                return PartialView();
            }
            catch (Exception e)
            {
                TempData["errorToast"] = e.Message + e.InnerException?.InnerException.Message;
                //return null;
                return PartialView("_Message");
            }
        }
        public ActionResult GetReport6()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/Report6.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        #endregion

        #region Report6-2
        public ActionResult Report6_2()
        {
            ViewBag.Contract = new SelectList(_unitOfWork.GetRepository<TblContract>().GetSync(a => a.Id > 1), "Id", "Title");

            return View();
        }

        private class FoodsReport6_2
        {
            public int Line { get; set; }
            public string Title { get; set; }
            public int Price { get; set; }
            public string MealTitle { get; set; }
            public int Count { get; set; }
            public int Meal { get; set; }
            public int Sum { get; set; }
        }
        // [HttpPost]
        public async Task<ActionResult> Report6_2Items(DateTime startDate, DateTime endDate, int year, int month, int contract, int restaurant)
        {
            //var (startDate, endDate) = Statics.GetMonthDate(year, month);
            const string includeProperties = @"TblFood,TblMeal,
                                            TblContract.TblContractPriceSeries,
                                            TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods,
                                            TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods.TblContractFood
                                            "
                ;

            var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
          a => a.Contract == contract && a.Restaurant == restaurant &&
               a.Date >= startDate && a.Date <= endDate && a.Food != -1 && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false, includeProperties: includeProperties)
          ;


            if (!list.Any())
            {
                TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                //return null;
                return PartialView("_Message");
            }


            var data = list.GroupBy(g => new { g.Food, g.Meal }).Select(a => new FoodsReport6
            {
                Line = 0,
                Title = a.First().TblFood.Title,
                //  Price = (int)a.First().Price, Todo Price
                Price = a.First().TblContract.TblContractPriceSeries.Where(b => b.FromDate <= startDate && b.ToDate >= endDate && b.TblContractPriceSeriesFoods.Any(c => c.TblContractFood.Food == a.First().Food))
                    .Select(ss => ss.TblContractPriceSeriesFoods.Where(w => w.TblContractFood.Food == a.First().Food).Sum(s => s.Price)).Sum(),
                Count = a.Count(),
                // Sum = (int)a.Sum(s => s.Price), Todo Price
                Sum = a.Sum(b1 => b1.TblContract.TblContractPriceSeries.Where(b => b.FromDate <= b1.Date && b.ToDate >= b1.Date && b.TblContractPriceSeriesFoods.Any(c => c.TblContractFood.Food == a.First().Food))
                    .Select(ss => ss.TblContractPriceSeriesFoods.Where(w => w.TblContractFood.Food == a.First().Food).Sum(s => s.Price)).Sum()),
                MealTitle = a.First().TblMeal.Title,
                Meal = a.First().Meal,

            });

            var data2 = new List<FoodsReport6>();
            foreach (var meal in data.Select(a => a.Meal).Distinct())
            {
                var l = 1;

                foreach (var item in data.Where(a => a.Meal == meal))
                {
                    item.Line = l++;
                    data2.Add(item);
                }
            }

            var co = await _unitOfWork.GetRepository<TblContract>().GetFirst(a => a.Id == contract, includeProperties: "TblContractor");
            var re = await _unitOfWork.GetRepository<TblRestaurant>().GetFirst(a => a.Id == restaurant);
            //  var d = await _unitOfWork.GetRepository<TblOfficeDepartment>().GetFirst(a => a.Id == officeDepartment);
            var header = new
            {
                StartDate = new PersianDateTime(startDate).ToString("yyyy/MM/dd"),
                EndDate = new PersianDateTime(endDate).ToString("yyyy/MM/dd"),

                Month = Statics.MonthName(month),
                Year = year,

                Contract = co.Title,
                Contractor = co.TblContractor.Title,
                Restaurant = re.Title
            };

            //TempData["data"] = data;
            //TempData["header"] = header;

            var res = new
            {
                data2,
                header
            };
            TempData["res"] = new JavaScriptSerializer().Serialize(res);

            // return Json(res, JsonRequestBehavior.AllowGet);
            return PartialView();
        }
        public ActionResult GetReport6_2()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/Report6.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        #endregion

        #region Report8
        public async Task<ActionResult> Report8()
        {
            ViewBag.Contract = new SelectList(await _unitOfWork.GetRepository<TblContract>().Get(), "Id", "Title");

            return View();
        }

        private class FoodsReport8
        {
            public int Line { get; set; }
            public string Title { get; set; }
            public int Price { get; set; }
            public string MealTitle { get; set; }
            public int Count { get; set; }
            public int Meal { get; set; }
            public int Sum { get; set; }
        }
        // [HttpPost]
        public async Task<ActionResult> Report8Items(int year, int month, int contract, int restaurant)
        {
            var (startDate, endDate) = Statics.GetMonthDate(year, month);
            const string includeProperties = @"TblFood,TblMeal,
                                            TblContract.TblContractPriceSeries,
                                            TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods,
                                            TblContract.TblContractPriceSeries.TblContractPriceSeriesFoods.TblContractFood
                                            "
                ;

            var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
          a => a.Contract == contract && a.Restaurant == restaurant &&
               a.Date >= startDate && a.Date <= endDate && a.Food != -1 && a.TblFoodReservationGroup.Temp == false && a.TblFoodReservationGroup.Remove == false, includeProperties: includeProperties)
          ;


            if (!list.Any())
            {
                TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
                //return null;
                return PartialView("_Message");
            }


            var data = list.GroupBy(g => new { g.Food, g.Meal }).Select(a => new FoodsReport6
            {
                Line = 0,
                Title = a.First().TblFood.Title,
                //  Price = (int)a.First().Price, Todo Price
                Price = a.First().TblContract.TblContractPriceSeries.Where(b => b.FromDate <= startDate && b.ToDate >= endDate && b.TblContractPriceSeriesFoods.Any(c => c.TblContractFood.Food == a.First().Food))
                    .Select(ss => ss.TblContractPriceSeriesFoods.Where(w => w.TblContractFood.Food == a.First().Food).Sum(s => s.Price)).Sum(),
                Count = a.Count(),
                // Sum = (int)a.Sum(s => s.Price), Todo Price
                Sum = a.Sum(b1 => b1.TblContract.TblContractPriceSeries.Where(b => b.FromDate <= b1.Date && b.ToDate >= b1.Date && b.TblContractPriceSeriesFoods.Any(c => c.TblContractFood.Food == a.First().Food))
                    .Select(ss => ss.TblContractPriceSeriesFoods.Where(w => w.TblContractFood.Food == a.First().Food).Sum(s => s.Price)).Sum()),
                MealTitle = a.First().TblMeal.Title,
                Meal = a.First().Meal,

            });

            var data2 = new List<FoodsReport6>();
            foreach (var meal in data.Select(a => a.Meal).Distinct())
            {
                var l = 1;

                foreach (var item in data.Where(a => a.Meal == meal))
                {
                    item.Line = l++;
                    data2.Add(item);
                }
            }

            var co = await _unitOfWork.GetRepository<TblContract>().GetFirst(a => a.Id == contract, includeProperties: "TblContractor");
            var re = await _unitOfWork.GetRepository<TblRestaurant>().GetFirst(a => a.Id == restaurant);
            //  var d = await _unitOfWork.GetRepository<TblOfficeDepartment>().GetFirst(a => a.Id == officeDepartment);
            var header = new
            {
                Month = Statics.MonthName(month),
                Year = year,

                Contract = co.Title,
                Contractor = co.TblContractor.Title,
                Restaurant = re.Title
            };

            //TempData["data"] = data;
            //TempData["header"] = header;

            var res = new
            {
                data2,
                header
            };
            TempData["res"] = new JavaScriptSerializer().Serialize(res);

            // return Json(res, JsonRequestBehavior.AllowGet);
            return PartialView();
        }
        public ActionResult GetReport8()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/Report6.mrt");
            report.Load(path);
            var data = Stimulsoft.Base.StiJsonToDataSetConverterV2.GetDataSet(TempData["res"].ToString());

            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources.Clear();
            report.RegData(data);
            report.Dictionary.Synchronize();

            return StiMvcViewer.GetReportResult(report);
        }

        #endregion


        // GET: Welfare/Report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Test2()
        {
            return View();
        }
        public ActionResult Test()
        {
            return View();
        }

        public async Task<ActionResult> GetReport(int? id)
        {
            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/test.mrt");
            report.Load(path);
            report.RegBusinessObject("dt",
                (await _unitOfWork.GetRepository<TblUser>().Get()).Select(a =>
                    new { Name = a.UserName, Person = a.Person }));
            return StiMvcViewer.GetReportResult(report);
        }
        public ActionResult ViewerEvent()
        {
            StiOptions.Viewer.RightToLeft = StiRightToLeftType.Yes;
            return StiMvcViewer.ViewerEventResult();
        }

        public ActionResult DesignReport()

        {
            StiReport report = StiMvcViewer.GetReportObject();
            ViewBag.ReportName = report.ReportName;
            return View("Designer");

        }

        public async Task<JsonResult> ContractRestaurantFoodMonthlyPony(/*int year, int month, int contract, int restaurant*/)
        {
            // var (startDate, endDate) = Statics.GetMonthDate(year, month);
            const string includeProperties = @"
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                        TblFoodReservationGroup,TblMeal,TblFood"
                ;
            var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
                /*a => a.Contract == contract && a.Restaurant == restaurant && a.Date >= startDate && a.Date <= endDate,*/ includeProperties: includeProperties);

            //if (!list.Any())
            //{
            //    TempData["errorToast"] = "در بازه زمانی انتخاب شده هیچ داده ای وجود ندارد.";
            //    return PartialView("_Message");
            //}
            var data = list.GroupBy(a => new { a.Food, a.Meal }).Select(a => new
            {


                Meal = a.First().Meal,
                MealTitle = a.First().TblMeal.Title,
                //Price = a.First().Price,  Todo Price
                //Food = a.First().Food,
                FoodTitle = a.First().TblFood.Title,
                // Amount = a.Sum(b => b.Price), Todo Price
                Count = a.Count()

            });
            var meal = data.Select(a => new { a.Meal, a.MealTitle }).Distinct();
            //var co = await _unitOfWork.GetRepository<TblContract>().GetFirst(a => a.Id == contract, includeProperties: "TblContractor");
            //var re = await _unitOfWork.GetRepository<TblRestaurant>().GetFirst(a => a.Id == restaurant);
            //var header = new
            //{
            //    Month = Statics.MonthName(month),
            //    Year = year,
            //    Contract = co.Title,
            //    Contractor = co.TblContractor.Title,
            //    Restaurant = re.Title
            //};

            ////TempData["data"] = data;
            ////TempData["header"] = header;

            //var res = new
            //{
            //    data,
            //    header
            //};
            return Json(new { data, meal }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> ContractorRestaurantMonthlyPony(int year, int month, int contract, int restaurant)
        {
            var (startDate, endDate) = Statics.GetMonthDate(year, month);
            const string includeProperties = @"
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                        TblFoodReservationGroup"
                ;
            var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
                a => a.Contract == contract && a.Restaurant == restaurant && a.Date >= startDate && a.Date <= endDate, includeProperties: includeProperties);

            var data = list.GroupBy(a => a.TblFoodReservationGroup.AccountNumber).Select(a => new
            {

                Office = a.First().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Title,
                AccountNumber = a.First().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber,
                //  Breakfast = a.Where(c => c.Meal == 1).Sum(item => item.Price),
                // Lunch = a.Where(c => c.Meal == 2).Sum(item => item.Price),
                // Dinner = a.Where(c => c.Meal == 3).Sum(item => item.Price),
                //  Dawn = a.Where(c => c.Meal == 4).Sum(item => item.Price),

                Room = 0,
                //   Sum = a.Sum(item => item.Price),

            });
            var co = await _unitOfWork.GetRepository<TblContract>().GetFirst(a => a.Id == contract, includeProperties: "TblContractor");
            var re = await _unitOfWork.GetRepository<TblRestaurant>().GetFirst(a => a.Id == restaurant);
            var header = new
            {
                Month = Statics.MonthName(month),
                Year = year,
                Contract = co.Title,
                Contractor = co.TblContractor.Title,
                Restaurant = re.Title
            };

            TempData["data"] = data;
            TempData["header"] = header;

            return PartialView();
        }

        public async Task<ActionResult> ContractorRestaurantMonthlyPonyReport()
        {

            var report = StiReport.CreateNewReport();
            var path = Server.MapPath("~/Content/Reports/ContractorRestaurantMonthlyPony.mrt");
            report.Load(path);
            report.RegBusinessObject("dt", TempData["data"]);
            report.RegBusinessObject("he", TempData["header"]);
            // report.LoadFromJson()
            return StiMvcViewer.GetReportResult(report);
        }

        #region Test
        public async Task<ActionResult> ContractRestaurant2()
        {
            ViewBag.Meals = await _unitOfWork.GetRepository<TblMeal>().Get();
            ViewBag.Contract = new SelectList(await _unitOfWork.GetRepository<TblContract>().Get(), "Id", "Title");
            return View();
        }

        //  [HttpPost]
        public async Task<JsonResult> ContractRestaurantList()
        {

            //var from = model.DateFrom();
            //var to = model.DateTo();
            //Expression<Func<TblFoodReservation, bool>> filter = (a) =>
            //        (string.IsNullOrEmpty(model.TblPerson.Name) || a.TblPerson.Family.Contains(model.TblPerson.Name)) &&
            //        (string.IsNullOrEmpty(model.TblPerson.Family) || a.TblPerson.Name.Contains(model.TblPerson.Family)) &&
            //        (string.IsNullOrEmpty(model.TblPerson.PersonnelCode) || a.TblPerson.PersonnelCode.Contains(model.TblPerson.PersonnelCode)) &&
            //        (string.IsNullOrEmpty(model.TblPerson.NationalCode) || a.TblPerson.NationalCode.Contains(model.TblPerson.NationalCode)) &&
            //        (string.IsNullOrEmpty(model.Des) || a.Des.Contains(model.Des)) &&
            //        //(model.Date.Year == 1 || a.Date == model.Date) &&
            //        (model.Food.Contains(0) || model.Food.Contains(a.Food)) &&
            //        (model.Meal.Contains(0) || model.Meal.Contains(a.Meal)) &&
            //        (model.Restaurant.Contains(0) || model.Restaurant.Contains(a.Restaurant)) &&
            //        (model.AccountNumber.Contains(0) || model.AccountNumber.Contains(a.TblFoodReservationGroup.AccountNumber)) &&
            //        (model.Status.Contains(0) || model.Status.Contains(a.Status)) &&
            //        (model.Price == 0 || a.Price == model.Price) &&


            //        (model.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == 0 || a.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == model.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company) &&
            //        (model.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == 0 || a.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == model.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary) &&
            //        (model.TblOfficeDepartment.TblOffice.Management == 0 || a.TblOfficeDepartment.TblOffice.Management == model.TblOfficeDepartment.TblOffice.Management) &&
            //        (model.TblOfficeDepartment.Office == 0 || a.TblOfficeDepartment.Office == model.TblOfficeDepartment.Office) &&
            //        (model.OfficeDepartment == 0 || a.OfficeDepartment == model.OfficeDepartment) &&
            //        (from.Year == 1 || a.Date >= from && a.Date < to)
            //;

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            // var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = @"TblPerson,
                                        TblOfficeDepartment,
                                        TblOfficeDepartment.TblOffice,
                                        TblRestaurant,
                                        TblFoodReservationStatu,
                                        TblFood,
                                        TblMeal,
                                        TblFoodReservationGroup.TblUser.TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                        TblFoodReservationGroup"
                ;
            var iResult = _unitOfWork.GetRepository<TblFoodReservation>();
            var result = iResult.GetOrdered(/*filter: filter,*/
                    orderBy: order, take: 100000000, skip: start, includeProperties: includeProperties);
            var (items, count, total) = await result;
            var i = 1;
            var data = items?.GroupBy(a => a.Id)
                .Select(a => new
                {
                    row = i++,
                    id = "",

                    Meal = a.OrderBy(item => item.Meal).GroupBy(item => item.Meal)
                        .Select(c => new
                        {
                            Price = 0 //c.Sum(item => item.Price) Todo Price
                            ,
                            Meal = c.First().Meal,
                            MealTitle = c.First().TblMeal.Title
                        }).ToArray(),
                    // Sum = a.Sum(item => item.Price), Todo Price
                    Count = a.Count(),
                    //Office2 = a.First().TblOfficeDepartment.TblOffice.Title,
                    Office = a.First().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Title,
                    AccountNumber = a.First().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber
                }).ToList();

            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ContractRestaurantList2()
        {

            //var from = model.DateFrom();
            //var to = model.DateTo();
            //Expression<Func<TblFoodReservation, bool>> filter = (a) =>
            //        (string.IsNullOrEmpty(model.TblPerson.Name) || a.TblPerson.Family.Contains(model.TblPerson.Name)) &&
            //        (string.IsNullOrEmpty(model.TblPerson.Family) || a.TblPerson.Name.Contains(model.TblPerson.Family)) &&
            //        (string.IsNullOrEmpty(model.TblPerson.PersonnelCode) || a.TblPerson.PersonnelCode.Contains(model.TblPerson.PersonnelCode)) &&
            //        (string.IsNullOrEmpty(model.TblPerson.NationalCode) || a.TblPerson.NationalCode.Contains(model.TblPerson.NationalCode)) &&
            //        (string.IsNullOrEmpty(model.Des) || a.Des.Contains(model.Des)) &&
            //        //(model.Date.Year == 1 || a.Date == model.Date) &&
            //        (model.Food.Contains(0) || model.Food.Contains(a.Food)) &&
            //        (model.Meal.Contains(0) || model.Meal.Contains(a.Meal)) &&
            //        (model.Restaurant.Contains(0) || model.Restaurant.Contains(a.Restaurant)) &&
            //        (model.AccountNumber.Contains(0) || model.AccountNumber.Contains(a.TblFoodReservationGroup.AccountNumber)) &&
            //        (model.Status.Contains(0) || model.Status.Contains(a.Status)) &&
            //        (model.Price == 0 || a.Price == model.Price) &&


            //        (model.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == 0 || a.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == model.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company) &&
            //        (model.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == 0 || a.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == model.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary) &&
            //        (model.TblOfficeDepartment.TblOffice.Management == 0 || a.TblOfficeDepartment.TblOffice.Management == model.TblOfficeDepartment.TblOffice.Management) &&
            //        (model.TblOfficeDepartment.Office == 0 || a.TblOfficeDepartment.Office == model.TblOfficeDepartment.Office) &&
            //        (model.OfficeDepartment == 0 || a.OfficeDepartment == model.OfficeDepartment) &&
            //        (from.Year == 1 || a.Date >= from && a.Date < to)
            //;

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            // var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = @"TblPerson,
                                        TblOfficeDepartment,
                                        TblOfficeDepartment.TblOffice,
                                        TblRestaurant,
                                        TblFoodReservationStatu,
                                        TblFood,
                                        TblMeal,
                                        TblFoodReservationGroup.TblUser.TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                        TblFoodReservationGroup"
                ;
            var iResult = _unitOfWork.GetRepository<TblFoodReservation>();
            var result = iResult.GetOrdered(/*filter: filter,*/
                    orderBy: order, take: 100000000, skip: start, includeProperties: includeProperties);
            var (items, count, total) = await result;
            var i = 1;
            var data = items
                .Select(a => new
                {
                    row = i++,
                    id = "",

                    Meal = a.Meal,
                    MealTitle = a.TblMeal.Title,
                    //Office2 = a.First().TblOfficeDepartment.TblOffice.Title,
                    Office = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Title,
                    AccountNumber = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber
                }).ToList();

            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}