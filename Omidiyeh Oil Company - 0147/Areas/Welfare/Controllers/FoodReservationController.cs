﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;
using Microsoft.Ajax.Utilities;
using System.Text.RegularExpressions;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{

    public class FoodReservationController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db = new dbEntities();

        public FoodReservationController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();

        }

        public async Task<ActionResult> CheckReservation(int count)
        {
            var list = await _unitOfWork.GetRepository<TblFoodReservation>()
                .Get(a => a.Date < DateTime.Now && a.Serving == false);

            return null;
        }
        [Authorize(Roles = "168")]
        // GET: Welfare/FoodReservation 2
        public ActionResult Index()
        {
            var user = Convert.ToInt32(User.Identity.Name);
            var us = _db.TblUsers.Find(user);
            ViewBag.UserGroup = us?.Group;

            ViewBag.UserConfirm = us.TblUserGroup.TblUserGroupRoles.Any(a => a.Role == 211);
            return View();
        }
        //[HttpPost]
        public async Task<JsonResult> List(SearchAdvancedFoodReservation model)
        {
            Expression<Func<TblFoodReservation, bool>> filter = (a) =>
                    (model.Group == 0 || a.Group == model.Group) &&
                    (string.IsNullOrEmpty(model.TblPerson.Name) || a.TblPerson.Family.Contains(model.TblPerson.Name)) &&
                    (string.IsNullOrEmpty(model.TblPerson.Family) || a.TblPerson.Name.Contains(model.TblPerson.Family)) &&
                    (string.IsNullOrEmpty(model.TblPerson.PersonnelCode) || a.TblPerson.PersonnelCode.Contains(model.TblPerson.PersonnelCode)) &&
                    (string.IsNullOrEmpty(model.TblPerson.NationalCode) || a.TblPerson.NationalCode.Contains(model.TblPerson.NationalCode)) &&
                    //(string.IsNullOrEmpty(model.Des) || a.Des.Contains(model.Des)) &&
                    (model.Food.Contains(-5) || model.Food.Contains(a.Food)) &&
                    (model.Meal.Contains(-5) || model.Meal.Contains(a.Meal)) &&
                    (model.Restaurant.Contains(-5) || model.Restaurant.Contains(a.Restaurant)) &&
                    (model.StartDate.Year == 1 || a.Date >= model.StartDate && a.Date <= model.EndDate) &&

                    (model.AccountNumber.Contains(0) || model.AccountNumber.Contains(a.TblFoodReservationGroup.AccountNumber)) &&
                    (model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == 0 || a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company) &&
                    (model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == 0 || a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary) &&
                    (model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Management == 0 || a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Management == model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Management) &&
                    (model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office == 0 || a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office == model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office) &&
                    a.TblFoodReservationGroup.Temp == false &&
                    a.Food != -1
            ;

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            // var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = @"TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice,
                                        TblRestaurant,
                                        TblFood,
                                        TblMeal,
                                        TblFoodReservationGroup.TblUser.TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber"
                ;

            var iResult = _unitOfWork.GetRepository<TblFoodReservation>();
            var result = iResult.GetOrdered(filter: filter,
                    orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = await result;

            var data = items?.Select(a => new
            {
                id = " ",
                Person = a.TblPerson.Name + " " + a.TblPerson.Family,
                User = a.TblFoodReservationGroup.TblUser.TblPerson.Fullname(),
                Office = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Title,
                OfficeDepartment = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Title,
                Food = a.TblFood.Title,
                Restaurant = a.TblRestaurant.Title,
                Meal = a.TblMeal.Title,
                Date = new PersianDateTime(a.Date).ToString("yyyy/MM/dd"),
                Day = new PersianDateTime(a.Date).ToString("dddd"),
                DT_RowId = a.Id,
                a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber,
                a.Group,
                MealId = a.Meal,
                DateEn = a.Date.ToString("MM/dd/yyyy"),
                DateReg = new PersianDateTime(a.TblFoodReservationGroup.Date).ToString("yyyy/MM/dd"),
                TimeReg = new PersianDateTime(a.TblFoodReservationGroup.Date).ToString("HH:mm"),
                Survey = a.Survey
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult List2(SearchAdvancedFoodReservation model)
        {
            var user = Convert.ToInt32(User.Identity.Name);
            if (model.Restaurant.Contains(-5))
            {
                model.Restaurant = _db.TblRestaurantUsers.Where(a => a.User == user).Select(a => a.Restaurant).ToList();
            }
            Expression<Func<TblFoodReservation, bool>> filter = (a) =>
                    (model.Serving == null || a.Serving == model.Serving) &&
                    (model.Type == null || a.TblFoodReservationGroup.Type == model.Type) &&
                    (model.Confirm == null || a.TblFoodReservationGroup.Confirm == model.Confirm) &&
                    (model.Group == 0 || a.Group == model.Group) &&
                    (string.IsNullOrEmpty(model.UserName) || a.TblFoodReservationGroup.TblUser.UserName == model.UserName) &&
                    (string.IsNullOrEmpty(model.Des) || a.TblFoodReservationGroup.Des.Contains(model.Des)) &&
                    (string.IsNullOrEmpty(model.TblPerson.Name) || a.TblPerson.Name.Contains(model.TblPerson.Name)) &&
                    (string.IsNullOrEmpty(model.TblPerson.Family) || a.TblPerson.Family.Contains(model.TblPerson.Family)) &&
                    (string.IsNullOrEmpty(model.TblPerson.PersonnelCode) || a.TblPerson.PersonnelCode.Contains(model.TblPerson.PersonnelCode)) &&
                    (string.IsNullOrEmpty(model.TblPerson.NationalCode) || a.TblPerson.NationalCode.Contains(model.TblPerson.NationalCode)) &&
                    //(string.IsNullOrEmpty(model.Des) || a.Des.Contains(model.Des)) &&
                    (model.Food.Contains(-5) || model.Food.Contains(a.Food)) &&
                    (model.Meal.Contains(-5) || model.Meal.Contains(a.Meal)) &&
                    (model.Restaurant.Contains(-5) || model.Restaurant.Contains(a.Restaurant)) &&
                    (model.StartDate.Year == 1 || a.Date >= model.StartDate && a.Date <= model.EndDate) &&

                    (model.AccountNumber.Contains(0) || model.AccountNumber.Contains(a.TblFoodReservationGroup.AccountNumber)) &&
                    (model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == 0 || a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company) &&
                    (model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == 0 || a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary) &&
                    (model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Management == 0 || a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Management == model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Management) &&
                    (model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office == 0 || a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office == model.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office) &&
                    a.TblFoodReservationGroup.Temp == false &&
                    a.TblFoodReservationGroup.Remove == false &&
                    a.Food != -1
            ;

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            // var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = @"TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice,
                                        TblRestaurant,
                                        TblFood,
                                        TblMeal,
                                        TblFoodReservationGroup.TblUser.TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber,
                                        TblFoodReservationGroup.TblFoodReservationLetter.TblSubsidiary"
                ;
            var iResult = _unitOfWork.GetRepository<TblFoodReservation>();

            var result = iResult.GetOrderedSyncNoTracking(filter: filter,
                    orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = result;

            // ReSharper disable once PossibleMultipleEnumeration
            var data = items?.GroupBy(g => new { g.Group, g.Person, g.Date }).Select(a => new
            {
                id = " ",
                a.First().TblFoodReservationGroup.Des,
                Person = a.First().TblPerson.Name + " " + a.First().TblPerson.Family,
                PersonId = a.First().Person,
                User = a.First().TblFoodReservationGroup.TblUser.TblPerson.Fullname(),
                Office = a.First().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Title,
                OfficeDepartment = a.First().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Title,
                Organ = a.First().TblFoodReservationGroup.TblFoodReservationLetter?.TblSubsidiary?.Title,
                // Count = items.Count(c => c.Group == a.First().Group),
                CountMeal = string.Join("<span class='ml-3'></span>", items.Where(w => w.Group == a.First().Group).OrderBy(o => o.Meal).GroupBy(gr => new { gr.Group, gr.Meal })
                    .Select(b => $"<span class='text-black-50'>{b.First().TblMeal.Title}: </span><span class='text-bold'>{b.Count()}</span>")),
                //CountMeal = string.Join("<span class='ml-3'></span>", items.Where(w => w.Group == a.First().Group).Count() +" " + a.First().Group),
                Count = "<span class='text-black-50'>جمع:</span><span class='text-bold'>" + items.Count(c => c.Group == a.First().Group) + "</span>",
                CountFood = string.Join("<span class='ml-3'></span>", items.Where(w => w.Group == a.First().Group).OrderBy(o => o.Meal).ThenBy(o => o.Food).GroupBy(gr => new { gr.Group, gr.Food })
                            .Select(b => $"<span class='text-black-50 lh2'>{b.First().TblFood.Title}: </span><span class='text-bold'>{b.Count()}</span>"))
                        ,
                Restaurant = a.First().TblRestaurant.Title,
                Meal = string.Join("<span class='ml-3'></span>", a.GroupBy(gr => gr.Meal)
                       .Select(b => $"<span class='text-black-50'>{b.First().TblMeal.Title}: </span><span class='text-bold'>{b.Count()}</span>")),
                Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd"),
                Day = new PersianDateTime(a.First().Date).ToString("dddd"),
                DT_RowId = a.First().Id,
                a.First().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber,
                a.First().Group,
                MealId = a.First().Meal,
                DateEn = a.First().Date.ToString("MM/dd/yyyy"),
                DateReg = new PersianDateTime(a.First().TblFoodReservationGroup.Date).ToString("yyyy/MM/dd"),
                TimeReg = new PersianDateTime(a.First().TblFoodReservationGroup.Date).ToString("HH:mm"),
                a.First().TblFoodReservationGroup.Type,
                a.First().TblFoodReservationGroup.Confirm,

            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            //return Json(res, JsonRequestBehavior.AllowGet);

            var jsonResult = Json(res, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult Edit(int id)
        {
            const string includeProperties = @"TblOfficeDepartmentAccountNumber,
                                                TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice,
                                                TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                                TblFoodReservations.TblRestaurant,
                                                TblFoodReservationLetter,
                                                TblFoodReservationShift,
                                                TblFoodReservationLetter.TblOffice.TblManagement,
                                                TblFoodReservationLetter.TblOffice,
                                                TblFoodReservationLetter.TblSubsidiary
                                                ";
            var group = _unitOfWork.GetRepository<TblFoodReservationGroup>().GetSingleSync(
                a => a.Id == id, includeProperties: includeProperties);
            ViewBag.FromDate = _db.TblFoodReservations.OrderBy(a => a.Date).First(a => a.Group == id).Date.ToString("M/dd/yyyy");
            ViewBag.ToDate = _db.TblFoodReservations.OrderByDescending(a => a.Date).First(a => a.Group == id).Date.ToString("M/dd/yyyy");
            ViewBag.Id = id;
            var user = Convert.ToInt32(User.Identity.Name);

            var us = _db.TblUsers.Find(user);

            ViewBag.UserGroup = us?.Group;
            return View(group);
        }


        public ActionResult CountMeal()
        {
            var list = _db.TblFoodReservationGroups
                .Include(a => a.TblFoodReservations)
                .Include(a => a.TblFoodReservationShift)
                .Where(a => a.Type == 4 && a.Temp == false
                            );

            foreach (var item in list)
            {
                if (!item.TblFoodReservations.Any())
                    continue;
                var firstDateN = item.TblFoodReservations.OrderBy(a => a.Date).FirstOrDefault(a => a.Date.DayOfWeek != DayOfWeek.Friday && a.Date.DayOfWeek != DayOfWeek.Thursday)?.Date;
                var firstDateT = item.TblFoodReservations.OrderBy(a => a.Date).FirstOrDefault(a => a.Date.DayOfWeek == DayOfWeek.Thursday)?.Date;
                var firstDateF = item.TblFoodReservations.OrderBy(a => a.Date).FirstOrDefault(a => a.Date.DayOfWeek == DayOfWeek.Friday)?.Date;

                item.TblFoodReservationShift.N0 = item.TblFoodReservations.Count(a => a.Meal == 0 && a.Food != -1 && a.Date == firstDateN);
                item.TblFoodReservationShift.N1 = item.TblFoodReservations.Count(a => a.Meal == 1 && a.Food != -1 && a.Date == firstDateN);
                item.TblFoodReservationShift.N2 = item.TblFoodReservations.Count(a => a.Meal == 2 && a.Food != -1 && a.Date == firstDateN);
                item.TblFoodReservationShift.N3 = item.TblFoodReservations.Count(a => a.Meal == 3 && a.Food != -1 && a.Date == firstDateN);
                item.TblFoodReservationShift.N4 = item.TblFoodReservations.Count(a => a.Meal == 4 && a.Food != -1 && a.Date == firstDateN);
                item.TblFoodReservationShift.N5 = item.TblFoodReservations.Count(a => a.Meal == 5 && a.Food != -1 && a.Date == firstDateN);

                item.TblFoodReservationShift.T0 = item.TblFoodReservations.Count(a => a.Meal == 0 && a.Food != -1 && a.Date == firstDateT);
                item.TblFoodReservationShift.T1 = item.TblFoodReservations.Count(a => a.Meal == 1 && a.Food != -1 && a.Date == firstDateT);
                item.TblFoodReservationShift.T2 = item.TblFoodReservations.Count(a => a.Meal == 2 && a.Food != -1 && a.Date == firstDateT);
                item.TblFoodReservationShift.T3 = item.TblFoodReservations.Count(a => a.Meal == 3 && a.Food != -1 && a.Date == firstDateT);
                item.TblFoodReservationShift.T4 = item.TblFoodReservations.Count(a => a.Meal == 4 && a.Food != -1 && a.Date == firstDateT);
                item.TblFoodReservationShift.T5 = item.TblFoodReservations.Count(a => a.Meal == 5 && a.Food != -1 && a.Date == firstDateT);

                item.TblFoodReservationShift.F0 = item.TblFoodReservations.Count(a => a.Meal == 0 && a.Food != -1 && a.Date == firstDateF);
                item.TblFoodReservationShift.F1 = item.TblFoodReservations.Count(a => a.Meal == 1 && a.Food != -1 && a.Date == firstDateF);
                item.TblFoodReservationShift.F2 = item.TblFoodReservations.Count(a => a.Meal == 2 && a.Food != -1 && a.Date == firstDateF);
                item.TblFoodReservationShift.F3 = item.TblFoodReservations.Count(a => a.Meal == 3 && a.Food != -1 && a.Date == firstDateF);
                item.TblFoodReservationShift.F4 = item.TblFoodReservations.Count(a => a.Meal == 4 && a.Food != -1 && a.Date == firstDateF);
                item.TblFoodReservationShift.F5 = item.TblFoodReservations.Count(a => a.Meal == 5 && a.Food != -1 && a.Date == firstDateF);

            }

            _db.SaveChanges();
            return null;
        }

        public ActionResult EditCount(int id, DateTime endDate, DateTime startDate)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            var group = _db.TblFoodReservationGroups.First(a => a.Id == id);
            var restaurant = _db.TblFoodReservations.First(a => a.Group == id).Restaurant;
            var contract = _db.TblFoodReservations.First(a => a.Group == id).Contract;
            var list = new List<TblFoodReservation>();
            var delCount = 0;
            //var endDate = Convert.ToDateTime(Request.Form["EndDate"]);
            //var startDate = Convert.ToDateTime(Request.Form["StartDate"]);

            var rTdCount = Convert.ToInt32(Request.Form["MealT-3"]);
            var rFdCount = Convert.ToInt32(Request.Form["MealF-3"]);
            var rNdCount = Convert.ToInt32(Request.Form["MealN-3"]);

            var rTadCount = Convert.ToInt32(Request.Form["MealT-5"]);
            var rFadCount = Convert.ToInt32(Request.Form["MealF-5"]);
            var rNadCount = Convert.ToInt32(Request.Form["MealN-5"]);
            if (rNdCount < rNadCount)
            {
                m.Result = false;
                m.Type = "error";
                m.Msg = "تعداد درخواستی وعده پس شام در روزهای عادی بیش از تعداد مجاز می باشد"
                        + "<br /><small>تعداد درخواستی: " + rNadCount + "<br /> تعداد مجاز: " + rNdCount +
                        "</small>";
                goto result;
            }

            if (rTdCount < rTadCount)
            {
                m.Result = false;
                m.Type = "error";
                m.Msg = "تعداد درخواستی وعده پس شام در روزهای پنجشنبه بیش از تعداد مجاز می باشد"
                        + "<br /><small>تعداد درخواستی: " + rTadCount + "<br /> تعداد مجاز: " + rTdCount +
                        "</small>";
                goto result;
            }

            if (rFdCount < rFadCount)
            {
                m.Result = false;
                m.Type = "error";
                m.Msg = "تعداد درخواستی وعده پس شام در روزهای جمعه بیش از تعداد مجاز می باشد"
                        + "<br /><small>تعداد درخواستی: " + rFadCount + "<br /> تعداد مجاز: " + rFdCount +
                        "</small>";
                goto result;
            }

            for (var i = startDate; i <= endDate; i = i.AddDays(1))
            {

                //جمعه
                if (i.DayOfWeek == DayOfWeek.Friday || _db.TblHolidays.Any(a => a.Date == i))
                {

                    foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("MealF")))
                    {
                        var meal = Convert.ToInt32(item.Split('-')[1]);
                        var count = Convert.ToInt32(Request.Form[item]);
                        if (count >= 0)
                        {
                            var dayCount = _db.TblFoodReservations.Count(a => a.Group == id && a.Date == i && a.Meal == meal);
                            //اگر تعداد بیشتر شده باشد
                            if (count > dayCount)
                            {
                                var defaultFood = _db.TblRestaurantFoodPrograms
                                    .FirstOrDefault(a => a.Restaurant == restaurant && a.Meal == meal && a.Date == i &&
                                                         a.Default);
                                for (var j = dayCount; j < count; j++)
                                {
                                   

                                    if (meal == 0 || defaultFood != null)
                                    {

                                        var entity = new TblFoodReservation
                                        {
                                            Date = i,
                                            Food = meal == 0 ? 0 : defaultFood.Food,
                                            Meal = meal,
                                            Restaurant = restaurant,
                                            Person = -2,
                                            Group = group.Id,
                                            Contract = contract,
                                            ItemNumber = j,
                                            // Confirm = !restaurantInfo.ConfirmReservation
                                        };
                                        list.Add(entity);
                                    }
                                    else
                                    {
                                        var mealTitle = MealTitle(meal);
                                        m.Msg = string.Format(Resources.Public.FoodProgramForMealInNull,
                                            new PersianDateTime(i).ToString("yyyy/MM/dd"), mealTitle);
                                        m.Result = false;
                                        m.Type = "error";
                                        goto result;
                                    }
                                }
                            }
                            //اگر تعداد کمتر شده باشد
                            else if (count < dayCount)
                            {
                                delCount += dayCount - count;
                                var del = _db.TblFoodReservations
                                    .Where(a => a.Meal == meal && a.Date == i && a.Group == id)
                                    .OrderByDescending(a => a.Id).Take(dayCount - count);
                                _db.TblFoodReservations.RemoveRange(del);
                            }
                        }
                    }

                }
                //پنجشنبه
                else if (i.DayOfWeek == DayOfWeek.Thursday)
                {


                    foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("MealT")))
                    {
                        var meal = Convert.ToInt32(item.Split('-')[1]);
                        var count = Convert.ToInt32(Request.Form[item]);
                        if (count >= 0)
                        {
                            var dayCount = _db.TblFoodReservations.Count(a => a.Group == id && a.Date == i && a.Meal == meal);
                            if (count > dayCount)
                            {
                                var defaultFood = _db.TblRestaurantFoodPrograms
                                    .FirstOrDefault(a =>
                                        a.Restaurant == restaurant && a.Meal == meal && a.Date == i && a.Default);
                                for (var j = dayCount; j < count; j++)
                                {
                                  
                                    if (meal == 0 || defaultFood != null)
                                    {

                                        var entity = new TblFoodReservation
                                        {
                                            Date = i,
                                            Food = meal == 0 ? 0 : defaultFood.Food,
                                            Meal = meal,
                                            Restaurant = restaurant,
                                            Person = -2,
                                            Group = group.Id,
                                            Contract = contract,
                                            ItemNumber = j
                                        };
                                        list.Add(entity);
                                    }
                                    else
                                    {
                                        var mealTitle = MealTitle(meal);

                                        m.Msg = string.Format(Resources.Public.FoodProgramForMealInNull,
                                            new PersianDateTime(i).ToString("yyyy/MM/dd"), mealTitle);
                                        m.Result = false;
                                        m.Type = "error";
                                        goto result;
                                    }
                                }
                            }

                            //اگر تعداد کمتر شده باشد
                            else if (count < dayCount)
                            {
                                delCount += dayCount - count;
                                var del = _db.TblFoodReservations
                                    .Where(a => a.Meal == meal && a.Date == i && a.Group == id)
                                    .OrderByDescending(a => a.Id).Take(dayCount - count);
                                _db.TblFoodReservations.RemoveRange(del);
                            }
                        }
                    }

                }

                //روزهای عادی
                else
                {

                    foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("MealN")))
                    {
                        var meal = Convert.ToInt32(item.Split('-')[1]);
                        var count = Convert.ToInt32(Request.Form[item]);
                        if (count >= 0)
                        {
                            var dayCount = _db.TblFoodReservations.Count(a => a.Group == id && a.Date == i && a.Meal == meal);
                            if (count > dayCount)
                            {
                                var defaultFood = _db.TblRestaurantFoodPrograms
                                    .FirstOrDefault(a =>
                                        a.Restaurant == restaurant && a.Meal == meal && a.Date == i && a.Default);
                                for (var j = dayCount; j < count; j++)
                                {
                                 

                                    if (meal == 0 || defaultFood != null)
                                    {

                                        var entity = new TblFoodReservation
                                        {
                                            Date = i,
                                            Food = meal == 0 ? 0 : defaultFood.Food,
                                            Meal = meal,
                                            Restaurant = restaurant,
                                            Person = -2,
                                            Group = group.Id,
                                            Contract = contract,
                                            ItemNumber = j
                                        };
                                        list.Add(entity);

                                    }
                                    else
                                    {
                                        var mealTitle = MealTitle(meal);

                                        m.Msg = string.Format(Resources.Public.FoodProgramForMealInNull,
                                            new PersianDateTime(i).ToString("yyyy/MM/dd"), mealTitle);
                                        m.Result = false;
                                        m.Type = "error";
                                        goto result;
                                    }
                                }

                            }
                            //اگر تعداد کمتر شده باشد
                            else if (count < dayCount)
                            {
                                delCount += dayCount - count;
                                var del = _db.TblFoodReservations
                                    .Where(a => a.Meal == meal && a.Date == i && a.Group == id)
                                    .OrderByDescending(a => a.Id).Take(dayCount - count);
                                _db.TblFoodReservations.RemoveRange(del);
                            }
                        }
                    }

                }


                
            }
            if (list.Any() || delCount > 0)
            {
                group.TblFoodReservationShift.F0 = Convert.ToInt32(Request.Form["MealF-0"]);
                group.TblFoodReservationShift.F1 = Convert.ToInt32(Request.Form["MealF-1"]);
                group.TblFoodReservationShift.F2 = Convert.ToInt32(Request.Form["MealF-2"]);
                group.TblFoodReservationShift.F3 = Convert.ToInt32(Request.Form["MealF-3"]);
                group.TblFoodReservationShift.F4 = Convert.ToInt32(Request.Form["MealF-4"]);
                group.TblFoodReservationShift.F5 = Convert.ToInt32(Request.Form["MealF-5"]);

                group.TblFoodReservationShift.N0 = Convert.ToInt32(Request.Form["MealN-0"]);
                group.TblFoodReservationShift.N1 = Convert.ToInt32(Request.Form["MealN-1"]);
                group.TblFoodReservationShift.N2 = Convert.ToInt32(Request.Form["MealN-2"]);
                group.TblFoodReservationShift.N3 = Convert.ToInt32(Request.Form["MealN-3"]);
                group.TblFoodReservationShift.N4 = Convert.ToInt32(Request.Form["MealN-4"]);
                group.TblFoodReservationShift.N5 = Convert.ToInt32(Request.Form["MealN-5"]);

                group.TblFoodReservationShift.T0 = Convert.ToInt32(Request.Form["MealT-0"]);
                group.TblFoodReservationShift.T1 = Convert.ToInt32(Request.Form["MealT-1"]);
                group.TblFoodReservationShift.T2 = Convert.ToInt32(Request.Form["MealT-2"]);
                group.TblFoodReservationShift.T3 = Convert.ToInt32(Request.Form["MealT-3"]);
                group.TblFoodReservationShift.T4 = Convert.ToInt32(Request.Form["MealT-4"]);
                group.TblFoodReservationShift.T5 = Convert.ToInt32(Request.Form["MealT-5"]);
                _db.SaveChanges();
                // var count = ;
                if (list.Any())
                {

                    var (resultFood, messageFood) =
                        _unitOfWork.GetRepository<TblFoodReservation>().InsertRangeSync(list, user);

                    m.Msg = resultFood
                        ? "درخواست ویرایش شد"
                        : messageFood;
                    m.Result = resultFood;
                    m.Type = resultFood ? "success" : "error";
                }
                else
                {
                    m.Msg = "درخواست ویرایش شد";
                    m.Result = true;
                    m.Type = "success";
                   
                }

            }
            else
            {
                m.Msg = "مورد جدیدی برای اضافه شدن به جدول موقت وجود نداشت.";
                m.Result = false;
                m.Type = "warning";
            }
            result:
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "285")]
        public ActionResult EditHolidayCount(DateTime date)
        {
            try
            {


                var m = new Message();
                var user = Convert.ToInt32(User.Identity.Name);

                //پیدا کردن درخواست هایی که در این تاریخ غذا دارند و شیفت می باشند
                var groupList = _db.TblFoodReservationGroups
                    .Where(a => a.Type == 4 && a.Remove == false && a.Temp == false)
                    .Where(a => a.TblFoodReservations.Any(f => f.Date == date && f.Remove == false && f.Food != -1))
                    .Select(a => a.Id).ToList();
                var list = new List<TblFoodReservation>();
                foreach (var id in groupList)
                {


                    var group = _db.TblFoodReservationGroups
                        .Include(a => a.TblFoodReservationShift).First(a => a.Id == id);
                    var restaurant = _db.TblFoodReservations.First(a => a.Group == id).Restaurant;
                    var contract = _db.TblFoodReservations.First(a => a.Group == id).Contract;

                    var delCount = 0;


                    var mealFridayCount = new List<int>
                    {
                        group.TblFoodReservationShift.F0,
                        group.TblFoodReservationShift.F1,
                        group.TblFoodReservationShift.F2,
                        group.TblFoodReservationShift.F3,
                        group.TblFoodReservationShift.F4,
                        group.TblFoodReservationShift.F5,

                    };
                    var meal = 0;
                    foreach (var item in mealFridayCount)
                    {

                        var count = item;
                        //if (count >= 0)
                        //{
                        var dayCount = _db.TblFoodReservations.Count(a =>
                            a.Group == id && a.Date == date && a.Meal == meal);
                        if (count > dayCount)
                        {
                            for (var j = dayCount; j < count; j++)
                            {
                                var defaultFood = _db.TblRestaurantFoodPrograms
                                    .FirstOrDefault(a =>
                                        a.Restaurant == restaurant && a.Meal == meal && a.Date == date &&
                                        a.Default);

                                if (meal == 0 || defaultFood != null)
                                {

                                    var entity = new TblFoodReservation
                                    {
                                        Date = date,
                                        Food = meal == 0 ? 0 : defaultFood.Food,
                                        Meal = meal,
                                        Restaurant = restaurant,
                                        Person = -2,
                                        Group = group.Id,
                                        Contract = contract,
                                        ItemNumber = j
                                    };
                                    list.Add(entity);

                                }
                                else
                                {
                                    var mealTitle = MealTitle(meal);

                                    m.Msg = string.Format(Resources.Public.FoodProgramForMealInNull,
                                        new PersianDateTime(date).ToString("yyyy/MM/dd"), mealTitle);
                                    m.Result = false;
                                    m.Type = "error";
                                    goto result;
                                }
                            }

                        }
                        //اگر تعداد کمتر شده باشد
                        else if (count < dayCount)
                        {
                            delCount += dayCount - count;
                            var del = _db.TblFoodReservations
                                .Where(a => a.Meal == meal && a.Date == date && a.Group == id)
                                .OrderByDescending(a => a.Id).Take(dayCount - count);
                            _db.TblFoodReservations.RemoveRange(del);

                            //todo Log 
                            Logs.Log(title: "Update Holiday Count " , string.Join(",",groupList) + "Date:" + date + "PDate:" + new PersianDateTime(date).ToLongDateString(), type: "Update", true, user);

                        }

                        // }
                        meal++;
                    }



                }

                if (list.Any())
                {

                    var (resultFood, messageFood) =
                        _unitOfWork.GetRepository<TblFoodReservation>().InsertRangeSync(list, user);

                    if (resultFood == false)
                    {
                        TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, messageFood);
                        goto result;
                    }

                }


                _db.SaveChanges();
                TempData["successMessage"] =$" {groupList.Count()} درخواست به تاریخ {new PersianDateTime(date).ToLongDateString()} ویرایش شد";
                //m.Msg = groupList.Count() + " درخواست ویرایش شد";
                //m.Result = true;
                //m.Type = "success";
                result:
                return RedirectToAction("Index", "Holiday", new {area = ""});
            }



            catch (Exception e)
            {
                TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
                return RedirectToAction("Index", "Holiday", new {area = ""});

            }
        }

        public async Task<JsonResult> EditList(int id, DateTime? date = null)
        {
            if (date == null)
                date = _db.TblFoodReservations.OrderBy(a => a.Date).First(a => a.Group == id).Date;
            // var user = Convert.ToInt32(User.Identity.Name);
            // var watch = new System.Diagnostics.Stopwatch();
            //watch.Start();
            var list = _db.TblFoodReservations
                .AsNoTracking()
                //.Include(a => a.TblFoodReservationGroup)
                .Include(a => a.TblPerson)
                .Include(a => a.TblRestaurant.TblRestaurantFoodPrograms)
                .Include(a => a.TblVehicle)
                .Where(a => a.Group == id && a.Date == date)
                .OrderBy(a => a.Date)
                .ToList();
            // watch.Stop();
            //var count = list.Count();
            ViewBag.Type = _db.TblFoodReservationGroups.First(a => a.Id == id).Type;
            var meals = _db.TblFoodReservations.Where(a => a.Group == id).Select(s => s.Meal).Distinct().OrderBy(a => a).ToList();
            var listMeals = new List<Item>();
            foreach (var item in meals)
            {
                var i = new Item
                {
                    Id = item,
                    Title = MealTitle(item)
                };
                listMeals.Add(i);
            }
            ViewBag.Meals = listMeals;
            var temp = new List<Reservation.Controllers.FoodController.TempModel>();
            int countPerson = 0;
            //اگر درخواست بی نام وجود داشت
            if (list.Any(a => a.Person == -2))
            {
                temp = list.GroupBy(g => new { g.Date, g.ItemNumber }).Select(a => new Reservation.Controllers.FoodController.TempModel
                {
                    DateEn = a.First().Date,
                    Person = a.First().Person,
                    Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd"),
                    Name = a.First().TblPerson.Name,
                    Family = a.First().TblPerson.Family,
                    NationalCode = a.First().TblPerson.NationalCode,
                    PersonnelCode = a.First().TblPerson.PersonnelCode,
                    ItemNumber = a.First().ItemNumber,
                    Meals = a.GroupBy(g => g.Meal).Select(b => new Reservation.Controllers.FoodController.TempModel.MealsModel
                    {
                        Id = b.First().Id,
                        Meal = b.First().Meal,
                        Food = b.First().Food,
                        Programs = RestaurantFoodItems(a.First().Date, b.First().Meal, restaurant: a.First().Restaurant),
                        Serving = b.First().Serving
                    }).ToList()
                }).ToList();
                countPerson = temp.Count() / list.GroupBy(g => new { g.Date }).Count();
            }
            else
            {
                temp = list.GroupBy(g => new { g.Person, g.Date }).Select(a =>
                      new Reservation.Controllers.FoodController.TempModel
                      {
                          DateEn = a.First().Date,
                          Person = a.First().Person,
                          Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd"),
                          Name = a.First().TblPerson.Name,
                          Family = a.First().TblPerson.Family,
                          NationalCode = a.First().TblPerson.NationalCode,
                          PersonnelCode = a.First().TblPerson.PersonnelCode,
                          Vehicle = a.FirstOrDefault().Vehicle,
                          VehicleTitle = a.FirstOrDefault().TblVehicle?.Title,
                          Meals = a.GroupBy(g => g.Meal).Select(b =>
                              new Reservation.Controllers.FoodController.TempModel.MealsModel
                              {
                                  Id = b.First().Id,
                                  Meal = b.First().Meal,
                                  Food = b.First().Food,
                                  Programs = RestaurantFoodItems(a.First().Date, b.First().Meal,
                                      restaurant: a.First().Restaurant)
                              }).ToList()
                      }).ToList();
                countPerson = temp.Select(a => a.NationalCode).Distinct().Count();

            }

            ViewBag.CountPerson = countPerson;
            return Json(new { PartialView = ConvertViewToString("EditList", temp), countPerson }, JsonRequestBehavior.AllowGet);
            // return Json(null);
        }



        public async Task<ActionResult> EditPersonal(int id)
        {
            const string includeProperties = @"TblOfficeDepartmentAccountNumber,
                                                TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice,
                                                TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                                TblFoodReservations.TblRestaurant,
                                                TblFoodReservationLetter,
                                                TblFoodReservationLetter.TblOffice.TblManagement,
                                                TblFoodReservationLetter.TblOffice,
                                                TblFoodReservationLetter.TblSubsidiary
                                                ";
            var group = await _unitOfWork.GetRepository<TblFoodReservationGroup>().GetSingle(
                a => a.Id == id, includeProperties: includeProperties);

            ViewBag.Id = id;
            return View(group);
        }
        public async Task<JsonResult> EditListPersonal(int id)
        {
            const string includeProperties = @"TblFoodReservationGroup,TblPerson,TblRestaurant.TblRestaurantFoodPrograms";

            var user = Convert.ToInt32(User.Identity.Name);
            var us = await _unitOfWork.GetRepository<TblUser>().GetFirst(a => a.Id == user);
            var list = await _unitOfWork.GetRepository<TblFoodReservation>().Get(
                a => a.TblFoodReservationGroup.Id == id && a.Person == us.Person, includeProperties: includeProperties);

            var meals = list.Select(s => s.Meal).Distinct().OrderBy(a => a).ToList();
            var listMeals = new List<Item>();
            foreach (var item in meals)
            {
                var i = new Item
                {
                    Id = item,
                    Title = MealTitle(item)
                };
                listMeals.Add(i);
            }
            ViewBag.Meals = listMeals;
            var temp = new List<Reservation.Controllers.FoodController.TempModel>();
            int countPerson = 0;

            temp = list.GroupBy(g => new { g.Person, g.Date }).Select(a =>
                  new Reservation.Controllers.FoodController.TempModel
                  {
                      DateEn = a.First().Date,
                      Person = a.First().Person,
                      Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd"),
                      Name = a.First().TblPerson.Name,
                      Family = a.First().TblPerson.Family,
                      NationalCode = a.First().TblPerson.NationalCode,
                      PersonnelCode = a.First().TblPerson.PersonnelCode,
                      Meals = a.GroupBy(g => g.Meal).Select(b =>
                          new Reservation.Controllers.FoodController.TempModel.MealsModel
                          {
                              Id = b.First().Id,
                              Meal = b.First().Meal,
                              Food = b.First().Food,
                              Programs = RestaurantFoodItems(a.First().Date, b.First().Meal,
                                  restaurant: a.First().Restaurant)
                          }).ToList()
                  }).ToList();
            countPerson = temp.Select(a => a.NationalCode).Distinct().Count();


            ViewBag.CountPerson = countPerson;
            return Json(new { PartialView = ConvertViewToString("EditList", temp), countPerson }, JsonRequestBehavior.AllowGet);

        }
        [Authorize(Roles = "211")]

        public async Task<ActionResult> Confirm(int group)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            var entity = _unitOfWork.GetRepository<TblFoodReservationGroup>().GetSingleSync(a =>
             a.Id == group);
            entity.Confirm = true;
            var (result, message) = await _unitOfWork.GetRepository<TblFoodReservationGroup>().Update(entity, user);
            if (result)
            {
                m.Msg = string.Format($"درخواست شماره {group} تایید شد.");
                m.Result = true;
                m.Type = "success";
            }
            else
            {
                var msg = string.Format(Resources.Public.ErrorMessage
                    , message);
                m.Msg = msg;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> EditAccountNumber(int group, int accountNumber)
        {
            var user = Convert.ToInt32(User.Identity.Name);

            var m = new Message();
            var entity = await _unitOfWork.GetRepository<TblFoodReservationGroup>().GetFirst(a =>
                a.Id == group);
            entity.Des = "Set;" + entity.Des;
            entity.AccountNumber = accountNumber;
            var (result, message) = await _unitOfWork.GetRepository<TblFoodReservationGroup>().Update(entity, user);
            if (result)
            {
                m.Msg = string.Format(Resources.Public.EditSuccessMessage, "شماره حساب ");
                m.Result = true;
                m.Type = "success";
            }
            else
            {
                var msg = string.Format(Resources.Public.EditErrorMessage,
                    "شماره حساب ", message);
                m.Msg = msg;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> EditSave()
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            var group = Convert.ToInt32(Request.Form["group"]);
            foreach (var item in Request.Form.AllKeys.Where(a => a.Contains("Person-")))
            {
                var f = item.Split('-');
                if (!string.IsNullOrEmpty(Request.Form[item]))
                {
                    var person = Convert.ToInt32(Request.Form[item]);
                    var itemNumber = Convert.ToInt32(f[1]);
                    var entity = await _unitOfWork.GetRepository<TblFoodReservation>().GetFirst(a =>
                        a.ItemNumber == itemNumber && a.TblFoodReservationGroup.Id == group);
                    entity.Person = person;
                    var alreadyBooked = await _unitOfWork.GetRepository<TblFoodReservation>().Any(a => a.Person == entity.Person && a.Meal == entity.Meal && a.Date == entity.Date && a.TblFoodReservationGroup.Temp == false);
                    if (alreadyBooked)
                    {
                        var msg = string.Format(Resources.Public.AddErrorMessage,
                            "ویرایش درخواست " +
                            new PersianDateTime(entity.Date)
                                .ToLongDateString() +
                            " وعده " + MealTitle(entity.Meal),
                            Resources.Public.AlreadyBooked2);
                        m.Msg = msg;
                        m.Result = false;
                        m.Type = "error";
                        return Json(m, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var res = await _unitOfWork.GetRepository<TblFoodReservation>().Update(entity, user);
                    }
                }
            }
            m.Result = true;
            m.Type = "success";
            m.Msg = string.Format(Resources.Public.AddSuccessMessage, "ویرایش درخواست غذا");
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        private string ConvertViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var writer = new StringWriter())
            {
                var vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }



        [OutputCache(Duration = 1200)]
        public List<Item> RestaurantFoodItems(DateTime date, int meal, int restaurant)
        {
           // var userId = int.Parse(User.Identity.Name);
            var foods = _unitOfWork.GetRepository<TblRestaurantFoodProgram>().GetSync(a => a.Restaurant == restaurant && a.Date == date && a.Meal == meal,
                includeProperties: "TblFood");
            var data = foods.OrderByDescending(a => a.Default).Select(a => new Item
            {
                Id = a.Food,
                Title = a.TblFood.Title
            });
            // var result = (await iResult.Get(a => a.Office == id)).ToList();
            return data.ToList();

        }



        [Authorize()]
        public async Task<ActionResult> Personal()
        {
            var userId = Convert.ToInt32(User.Identity.Name);
            var user = await _unitOfWork.GetRepository<TblUser>().GetSingle(a => a.Id == userId, includeProperties: "TblPerson");
            ViewBag.NationalCode = user.TblPerson.NationalCode;
            return View();
        }


        public ActionResult Restaurant()
        {
            return View();
        }
        [HttpPost]
        public JsonResult ListRestaurant(SearchAdvancedFoodReservation model)
        {
            var user = Convert.ToInt32(User.Identity.Name);
            if (model.Restaurant.Contains(-5))
            {
                model.Restaurant = _db.TblRestaurantUsers.Where(a => a.User == user).Select(a => a.Restaurant).ToList();
            }
            Expression<Func<TblFoodReservation, bool>> filter = (a) =>
                (model.Serving == null || a.Serving == model.Serving) &&
                (model.Group == 0 || a.Group == model.Group) &&
                (string.IsNullOrEmpty(model.UserName) || a.TblFoodReservationGroup.TblUser.UserName == model.UserName) &&
                  (string.IsNullOrEmpty(model.Des) || a.TblFoodReservationGroup.Des.Contains(model.Des)) &&
                (string.IsNullOrEmpty(model.TblPerson.Name) || a.TblPerson.Name.Contains(model.TblPerson.Name)) &&
                (string.IsNullOrEmpty(model.TblPerson.Family) || a.TblPerson.Family.Contains(model.TblPerson.Family)) &&
                (string.IsNullOrEmpty(model.TblPerson.PersonnelCode) ||
                 a.TblPerson.PersonnelCode.Contains(model.TblPerson.PersonnelCode)) &&
                (string.IsNullOrEmpty(model.TblPerson.NationalCode) ||
                 a.TblPerson.NationalCode.Contains(model.TblPerson.NationalCode)) &&
                //(string.IsNullOrEmpty(model.Des) || a.Des.Contains(model.Des)) &&
                //(model.Date.Year == 1 || a.Date == model.Date) &&
                (model.Food.Contains(-5) || model.Food.Contains(a.Food)) &&
                (model.Meal.Contains(-5) || model.Meal.Contains(a.Meal)) &&
                     (model.AccountNumber.Contains(0) || model.AccountNumber.Contains(a.TblFoodReservationGroup.AccountNumber)) &&
                (model.Restaurant.Contains(-5) || model.Restaurant.Contains(a.Restaurant)) &&
                (model.StartDate.Year == 1 || a.Date >= model.StartDate && a.Date <= model.EndDate) &&
                a.TblFoodReservationGroup.Temp == false &&
                a.TblFoodReservationGroup.Remove == false &&
                    a.Food != -1 && a.TblFoodReservationGroup.Confirm == true
                ;
            //if (model.Restaurant.Contains(0))
            //{
            //    var user = int.Parse(User.Identity.Name);

            //    var rest = _db.TblRestaurantUsers.Where(a => a.User == user).Select(b => b.Restaurant).ToList();
            //    model.Restaurant = _db.TblRestaurants.Where(a => rest.Contains(a.Id)).Select(a => a.Id).ToList();

            //}
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = @"TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice,
                                        TblRestaurant,
                                        TblFood,
                                        TblMeal,
                                        TblFoodReservationGroup.TblUser.TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber"
                                        ;
            var info = _db.TblFoodReservations
                    .Where(filter)
                    .Include(a => a.TblFood)
                    .GroupBy(a => a.Food).AsNoTracking()
                ;
            var summaryInfo = info.Select(a => new
            {
                Title = a.FirstOrDefault().TblFood.Title,
                Count = a.Count()
            });


            var (items, count, total) = _unitOfWork.GetRepository<TblFoodReservation>().GetOrderedSync(filter: filter,
                    orderBy: order, take: length, skip: start, includeProperties: includeProperties);



            var data = items?.Select(a => new
            {
                id = " ",
                Person = a.TblPerson.Name + " " + a.TblPerson.Family,
                a.TblFoodReservationGroup.Des,
                PersonId = a.Person,
                User = a.TblFoodReservationGroup.TblUser.TblPerson.Fullname(),
                Office = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Title,
                OfficeDepartment = a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Title,
                Bed = a.Bed,
                Food = a.TblFood.Title,
                FoodId = a.Food,
                Restaurant = a.TblRestaurant.Title,
                Meal = a.TblMeal.Title,
                Date = new PersianDateTime(a.Date).ToString("yyyy/MM/dd dddd"),
                DT_RowId = a.Id,
                a.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber,
                a.Group,
                DateReg = new PersianDateTime(a.TblFoodReservationGroup.Date).ToString("yyyy/MM/dd"),
                TimeReg = new PersianDateTime(a.TblFoodReservationGroup.Date).ToString("HH:mm"),
                MealId = a.Meal,
                DateEn = a.Date.ToString("MM/dd/yyyy"),
                a.Serving,
                Type = a.TblFoodReservationGroup.Type,
                CountFood = string.Join("<span class='ml-3'></span>", items.Where(w => w.Group == a.Group).OrderBy(o => o.Food).GroupBy(gr => new { gr.Group, gr.Food })
                    .Select(b => $"<span class='text-black-50'>{b.First().TblFood.Title}: </span><span class='text-bold'>{b.Count()}</span>")),
                CountMeal = string.Join("<span class='ml-3'></span>", items.Where(w => w.Group == a.Group).OrderBy(o => o.Meal).GroupBy(gr => new { gr.Group, gr.Meal })
                            .Select(b => $"<span class='text-black-50'>{b.First().TblMeal.Title}: </span><span class='text-bold'>{b.Count()}</span>")),
                Count = "<span class='text-black-50'>جمع:</span><span class='text-bold'>" + items.Count(c => c.Group == a.Group) + "</span>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data,
                summaryInfo = summaryInfo
            };
            var jsonResult = Json(res, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        [Authorize(Roles = "267")]
        public ActionResult Shift()
        {
            return View();
        }
        [Authorize(Roles = "267")]
        public JsonResult ListShift(SearchAdvancedFoodReservation model)
        {
            var user = Convert.ToInt32(User.Identity.Name);
            var p = _db.TblUsers.Find(user).TblPerson.Id;
            Expression<Func<TblFoodReservation, bool>> filter = (a) =>
                    (model.Serving == null || a.Serving == model.Serving) &&
                    (model.Group == 0 || a.Group == model.Group) &&

                    (model.StartDate.Year == 1 || a.Date >= model.StartDate && a.Date <= model.EndDate) &&
                    a.TblFoodReservationGroup.Temp == false &&
                    a.TblFoodReservationGroup.Remove == false &&
                    a.Food != -1
                    && a.TblFoodReservationGroup.Type == 4
                  && a.TblFoodReservationGroup.TblFoodReservationShiftPersons.Any(b => b.Person == p)
            ;

            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            // var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = @"TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice,
                                        TblRestaurant,
                                        TblFood,
                                        TblMeal,
                                        TblFoodReservationGroup.TblUser.TblPerson,
                                        TblFoodReservationGroup.TblOfficeDepartmentAccountNumber"
                ;
            var iResult = _unitOfWork.GetRepository<TblFoodReservation>();

            var result = iResult.GetOrderedSync(filter: filter,
                    orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = result;

            // ReSharper disable once PossibleMultipleEnumeration
            var data = items?.GroupBy(g => new { g.Group, g.Person, g.Date }).Select(a => new
            {
                id = " ",
                a.First().TblFoodReservationGroup.Des,
                Person = a.First().TblPerson.Name + " " + a.First().TblPerson.Family,
                PersonId = a.First().Person,
                User = a.First().TblFoodReservationGroup.TblUser.TblPerson.Fullname(),
                Office = a.First().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Title,
                OfficeDepartment = a.First().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Title,
                // Count = items.Count(c => c.Group == a.First().Group),
                CountMeal = string.Join("<span class='ml-3'></span>", items.Where(w => w.Group == a.First().Group).OrderBy(o => o.Meal).GroupBy(gr => new { gr.Group, gr.Meal })
                    .Select(b => $"<span class='text-black-50'>{b.First().TblMeal.Title}: </span><span class='text-bold'>{b.Count()}</span>")),
                //CountMeal = string.Join("<span class='ml-3'></span>", items.Where(w => w.Group == a.First().Group).Count() +" " + a.First().Group),
                Count = "<span class='text-black-50'>جمع:</span><span class='text-bold'>" + items.Count(c => c.Group == a.First().Group) + "</span>",
                CountFood = string.Join("<span class='ml-3'></span>", items.Where(w => w.Group == a.First().Group).OrderBy(o => o.Meal).ThenBy(o => o.Food).GroupBy(gr => new { gr.Group, gr.Food })
                            .Select(b => $"<span class='text-black-50 lh2'>{b.First().TblFood.Title}: </span><span class='text-bold'>{b.Count()}</span>"))
                        ,
                Restaurant = a.First().TblRestaurant.Title,
                Meal = string.Join("<span class='ml-3'></span>", a.GroupBy(gr => gr.Meal)
                       .Select(b => $"<span class='text-black-50'>{b.First().TblMeal.Title}: </span><span class='text-bold'>{b.Count()}</span>")),
                Date = new PersianDateTime(a.First().Date).ToString("yyyy/MM/dd"),
                Day = new PersianDateTime(a.First().Date).ToString("dddd"),
                DT_RowId = a.First().Id,
                a.First().TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.AccountNumber,
                a.First().Group,
                MealId = a.First().Meal,
                DateEn = a.First().Date.ToString("MM/dd/yyyy"),
                DateReg = new PersianDateTime(a.First().TblFoodReservationGroup.Date).ToString("yyyy/MM/dd"),
                TimeReg = new PersianDateTime(a.First().TblFoodReservationGroup.Date).ToString("HH:mm"),
                a.First().TblFoodReservationGroup.Type,
                a.First().TblFoodReservationGroup.Confirm,

            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            //return Json(res, JsonRequestBehavior.AllowGet);

            var jsonResult = Json(res, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }
        [Authorize(Roles = "267")]
        public ActionResult EditShift(int id)
        {
            const string includeProperties = @"TblOfficeDepartmentAccountNumber,
                                                TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice,
                                                TblOfficeDepartmentAccountNumber.TblOfficeDepartment,
                                                TblFoodReservations.TblRestaurant,
                                                TblFoodReservationLetter,
                                                TblFoodReservationShift,
                                                TblFoodReservationLetter.TblOffice.TblManagement,
                                                TblFoodReservationLetter.TblOffice,
                                                TblFoodReservationLetter.TblSubsidiary
                                                ";
            var group = _unitOfWork.GetRepository<TblFoodReservationGroup>().GetSingleSync(
                a => a.Id == id, includeProperties: includeProperties);
            ViewBag.FromDate = _db.TblFoodReservations.OrderBy(a => a.Date).First(a => a.Group == id).Date.ToString("M/dd/yyyy");
            ViewBag.ToDate = _db.TblFoodReservations.OrderByDescending(a => a.Date).First(a => a.Group == id).Date.ToString("M/dd/yyyy");
            ViewBag.Id = id;
            var user = Convert.ToInt32(User.Identity.Name);

            var us = _db.TblUsers.Find(user);

            ViewBag.UserGroup = us?.Group;
            return View(group);
        }


        public async Task<JsonResult> ReservationReport(int id)
        {

            var r = await _unitOfWork.GetRepository<TblFoodReservation>().Get(a => a.Group == id,
                includeProperties: "");
            var data = r.GroupBy(a => new { a.Person, a.Date });
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize()]
        public async Task<JsonResult> Serving(int id)
        {
            var m = new Message();
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var fr = await _unitOfWork.GetRepository<TblFoodReservation>().GetFirst(a => a.Id == id, includeProperties: "TblMeal");
                    if (DateTime.Now > fr.Date.AddHours(fr.TblMeal.StartTime.Hours).AddMinutes(fr.TblMeal.StartTime.Minutes)
                         && DateTime.Now < fr.Date.AddHours(fr.TblMeal.StartTime.Hours).AddMinutes(fr.TblMeal.StartTime.Minutes).AddDays(7))
                    {
                        fr.Serving = true;
                        var (result, message) = await _unitOfWork.GetRepository<TblFoodReservation>().Update(fr, user);
                        m.Msg = result
                            ? Resources.Public.FoodServing
                            : string.Format(Resources.Public.AddErrorMessage, "حضور شخص", message);
                        m.Result = result;
                        m.Type = result ? "success" : "error";
                    }
                    else
                    {
                        m.Msg = "فقط هنگام زمان سرو غذا می توان حضور شخص را ثبت کرد";
                        m.Result = false;
                        m.Type = "error";
                    }
                }
            }
            catch (Exception e)
            {
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize()]
        public async Task<JsonResult> ServingRemove(int id)
        {
            var m = new Message();
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var fr = await _unitOfWork.GetRepository<TblFoodReservation>().GetFirst(a => a.Id == id, includeProperties: "TblMeal");
                    if (DateTime.Now > fr.Date.AddHours(fr.TblMeal.StartTime.Hours).AddMinutes(fr.TblMeal.StartTime.Minutes)
                        && DateTime.Now < fr.Date.AddHours(fr.TblMeal.StartTime.Hours).AddMinutes(fr.TblMeal.StartTime.Minutes).AddDays(7))
                    {
                        fr.Serving = false;
                        var (result, message) = await _unitOfWork.GetRepository<TblFoodReservation>().Update(fr, user);
                        m.Msg = result ? Resources.Public.FoodServingRemove : string.Format(Resources.Public.AddErrorMessage, "عدم حضور شخص", message);
                        m.Result = result;
                        m.Type = result ? "success" : "error";
                    }
                    else
                    {
                        m.Msg = "فقط هنگام زمان سرو غذا می توان عدم حضور شخص را ثبت کرد";
                        m.Result = false;
                        m.Type = "error";
                    }
                }
            }
            catch (Exception e)
            {

                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";


            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// اختصاص تخت به یک درخواست
        /// </summary>
        /// <param name="group"></param>
        /// <param name="person"></param>
        /// <param name="bed"></param>
        /// <param name="des"></param>
        /// <param name="roomNumber"></param>
        /// <param name="bedNumber"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize()]
        public async Task<JsonResult> AddBed(int group, int person, int bed, string des, string roomNumber, string bedNumber, string name)
        {
            var m = new Message();
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var fr = await _unitOfWork.GetRepository<TblFoodReservation>().Get(a => a.Group == group && a.Person == person && a.Food == 0 && a.Bed == null);
                    byte status = 5;
                    if (fr.First().Date.Date == DateTime.Now.Date)
                    {
                        status = 20;
                    }

                    foreach (var item in fr)
                    {
                        item.Bed = bed;
                        item.Serving = true;

                    }

                    //تغییر وضعیت تخت به در حال استفاده
                    var b = await _unitOfWork.GetRepository<TblBed>().GetFirst(a => a.Id == bed);
                    b.Status = status;
                    b.FoodReservationGroup = group;
                    b.Person = person;
                    //ثبت وضعیت
                    var bs = new TblBedStatusHistory
                    {
                        Bed = bed,
                        Date = DateTime.Now,
                        Des = des,
                        Status = status,
                        User = user
                    };
                    _unitOfWork.GetRepository<TblBedStatusHistory>().InsertSync(bs, user);

                    await _unitOfWork.SaveAsync();


                    m.Msg = string.Format(Resources.Public.AddBedToReservationSuccess, roomNumber, bedNumber, name);
                    m.Result = true;
                    m.Type = "success";
                    //}
                    //else
                    //{
                    //    m.Msg = "فقط در روز جاری درخواست می توان به شخص تخت اختصاص داد";
                    //    m.Result = false;
                    //    m.Type = "error";
                    //}
                }
            }
            catch (Exception e)
            {
                m.Msg = string.Format(Resources.Public.ErrorMessage, e.Message);
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize()]
        public async Task<JsonResult> ServingGroup(int id)
        {
            var m = new Message();
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var fr = await _unitOfWork.GetRepository<TblFoodReservation>().Get(a => a.Group == id);
                    foreach (var item in fr)
                    {
                        item.Serving = true;
                    }

                    await _unitOfWork.SaveAsync();
                    var result = true;
                    var message = "";
                    m.Msg = result ? Resources.Public.FoodServingRemove : string.Format(Resources.Public.AddErrorMessage, "حضور شخص", message);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }



        public async Task<bool> TimelyReview(TblFoodReservation entity)
        {

            var result = true;
            var meal = await _unitOfWork.GetRepository<TblMeal>().GetSingle(a => a.Id == entity.Meal);
            var pFood = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().GetFirst(
                a => a.Food == entity.Food && a.Date == entity.Date && a.Restaurant == entity.Restaurant && a.Meal == entity.Meal);
            //بررسی زمانی درخواست غذا
            //اعمال محدودیت های زمانی
            var tempDate = entity.Date.AddDays(meal.ReservationEndDay * -1);
            tempDate = tempDate.Add(meal.ReservationEndTime);
            if (tempDate < DateTime.Now && !pFood.Default)
                result = false;

            //بررسی زمان برای انتخاب غذای پیشفرض
            tempDate = entity.Date.Add(meal.ReservationDefaultFoodTime);
            if (tempDate < DateTime.Now && pFood.Default)
                result = false;


            return result;
        }

        [Authorize()]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {

                var user = int.Parse(User.Identity.Name);
                var us = await _unitOfWork.GetRepository<TblUser>().GetFirst(a => a.Id == user);
                var group = _db.TblFoodReservationGroups.Include(a => a.TblFoodReservations).FirstOrDefault(a => a.Id == id);
                //if (us.Group != 1 && group.TblFoodReservations.Any(a => a.Date < DateTime.Now))
                //{
                //    TempData["errorToast"] = string.Format(Resources.Public.DeleteErrorMessage, "زمان برای حذف این درخواست گذشته است");
                //    goto ret;
                //}

                if (us.Group != 1 && us.Group != 21 && us.Group != 12 && us.Group != 10 && us.Group != 14)
                {
                    foreach (var item in group.TblFoodReservations)
                    {
                        if (!await TimelyReview(item))
                        {
                            TempData["errorToast"] = string.Format(Resources.Public.DeleteErrorMessage, "زمان برای حذف این درخواست گذشته است");
                            goto ret;
                        }
                    }

                }


                if ((us.Group != 1 && us.Group != 21 && us.Group != 12 && us.Group != 10 && us.Group != 14) && group.TblFoodReservations.Any(a => a.Serving))
                {
                    TempData["errorToast"] = string.Format(Resources.Public.DeleteErrorMessage, "این درخواست غذای استفاده شده دارد.");
                    goto ret;
                }

                group.Remove = true;
                // await _unitOfWork.GetRepository<TblFoodReservation>().DeleteRange(a => a.Group == group.Id, user);
                //await _unitOfWork.GetRepository<TblFoodReservationShiftPerson>().DeleteRange(a => a.Group == group.Id, user);
                //await _unitOfWork.GetRepository<TblFoodReservationShift>().Delete(group.Shift, user);
                //await _unitOfWork.GetRepository<TblFoodReservationLetter>().Delete(group.Id, user);

                //var (result, message) = await _unitOfWork.GetRepository<TblFoodReservationGroup>().Delete(group.Id, user);
                try
                {
                    _db.SaveChanges();
                    await Logs.Log(title: "Delete " + "FoodReservations", body: id.ToString(), type: "Delete", true, user);
                    TempData["successToast"] = string.Format(Resources.Public.DeleteSuccessMessage);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["errorToast"] = string.Format(Resources.Public.DeleteErrorMessage, ex.Message);
                }

            }
            catch (Exception e)
            {
                TempData["errorToast"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            ret: return RedirectToAction("Edit", new { id = id });
        }

        [Authorize()]
        public async Task<ActionResult> DeleteFromDate(int id, DateTime date)
        {
            try
            {

                var user = int.Parse(User.Identity.Name);
                var us = await _unitOfWork.GetRepository<TblUser>().GetFirst(a => a.Id == user);
                var group = await _unitOfWork.GetRepository<TblFoodReservationGroup>().GetFirst(a => a.Id == id, includeProperties: "TblFoodReservations");
                //if (group.TblFoodReservations.Any(a => a.Date < DateTime.Now))
                //{
                //    TempData["errorToast"] = string.Format(Resources.Public.DeleteErrorMessage, "زمان برای حذف این درخواست گذشته است");
                //    goto ret;
                //}

                if ((us.Group != 1 && us.Group != 12 && us.Group != 10 && us.Group != 14) && group.TblFoodReservations.Any(a => a.Serving && a.Date >= date))
                {
                    TempData["errorToast"] = string.Format(Resources.Public.DeleteErrorMessage, "این درخواست غذای استفاده شده دارد.");
                    goto ret;
                }
                var (result, count, message) = await _unitOfWork.GetRepository<TblFoodReservation>().DeleteRange(a => a.Group == group.Id && a.Date >= date, user);
                //await _unitOfWork.GetRepository<TblFoodReservationShiftPerson>().DeleteRange(a => a.Group == group.Id, user);
                //await _unitOfWork.GetRepository<TblFoodReservationShift>().Delete(group.Shift, user);
                //await _unitOfWork.GetRepository<TblFoodReservationLetter>().Delete(group.Id, user);

                //var (result, message) = await _unitOfWork.GetRepository<TblFoodReservationGroup>().Delete(group.Id, user);
                if (result)
                {
                    TempData["successToast"] = string.Format(Resources.Public.DeleteSuccessMessage);
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["errorToast"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }

            }
            catch (Exception e)
            {
                TempData["errorToast"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            ret: return RedirectToAction("Edit", new { id = id });
        }
        [HttpPost]
        [Authorize()]
        public async Task<JsonResult> EditJava(int id, int restaurant, int food)
        {
            var m = new Message();
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var rs = await _unitOfWork.GetRepository<TblFoodReservation>().GetFirst(a => a.Id == id);
                    rs.Restaurant = restaurant;
                    rs.Food = food;
                    var (result, message) = await _unitOfWork.GetRepository<TblFoodReservation>().Update(rs, user);
                    if (result)
                    {
                        //ثبت وضعیت درخواست غذا
                        var statusHistory = new TblFoodReservationStatusHistory
                        {
                            Reservation = id,
                            Id = Guid.NewGuid(),
                            Time = DateTime.Now,
                            Status = 10,
                            User = user
                        };
                        var (resultStatus, messageStatus) = await _unitOfWork
                            .GetRepository<TblFoodReservationStatusHistory>().Insert(statusHistory, user);
                    }
                    m.Msg = result ? Resources.Public.EditSuccessMessage : string.Format(Resources.Public.EditErrorMessage, message);
                    m.Result = result;
                    m.Type = result ? "success" : "error";

                }
            }
            catch (Exception e)
            {

                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";


            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize()]
        public async Task<JsonResult> EditDes(int id, string des)
        {
            var m = new Message();
            try
            {

                var user = int.Parse(User.Identity.Name);
                //var group = _db.TblFoodReservationGroups.Single(a => a.Id == id);
                //group.Des = des;
                var g = _unitOfWork.GetRepository<TblFoodReservationGroup>().GetSingleSync(a => a.Id == id);
                g.Des = des;

                var (result, message) = await _unitOfWork.GetRepository<TblFoodReservationGroup>().Update(g, user);

                m.Msg = result ? string.Format(Resources.Public.EditSuccessMessage, "توضیحات") : string.Format(Resources.Public.EditErrorMessage, "توضیحات", message);
                m.Result = result;
                m.Type = result ? "success" : "error";


            }
            catch (Exception e)
            {

                m.Msg = string.Format(Resources.Public.EditErrorMessage, e.Message);
                m.Result = false;
                m.Type = "error";


            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Authorize()]
        public async Task<JsonResult> DeleteJava(int id)
        {
            var m = new Message();
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblFoodReservation>().Delete(id, user);
                    m.Msg = result ? Resources.Public.DeleteSuccessMessage : string.Format(Resources.Public.DeleteErrorMessage, message);
                    m.Result = result;
                    m.Type = result ? "success" : "error";

                }
            }
            catch (Exception e)
            {

                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";


            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize()]
        public async Task<JsonResult> DeleteJavaGroup(int id)
        {
            var m = new Message();
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, _, message) = await _unitOfWork.GetRepository<TblFoodReservation>().DeleteRange(a => a.Group == id, user);
                    m.Msg = result ? Resources.Public.DeleteSuccessMessage : string.Format(Resources.Public.DeleteErrorMessage, message);
                    m.Result = result;
                    m.Type = result ? "success" : "error";

                }
            }
            catch (Exception e)
            {

                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";


            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// بدست آوردن شماره وضعیت تایید درخواست
        /// اگر کاربر گروه رفاهی باشد تایید خدمات رفاهی
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<int> UserConfirmStatusId(int user)
        {
            var result = 0;
            var us = await _unitOfWork.GetRepository<TblUser>().GetFirst(a => a.Id == user, includeProperties: "TblUserGroup");
            if (us.TblUserGroup.Title.Contains("رفاهی"))
            {
                result = 21;
            }
            else if (us.TblUserGroup.Title.Contains("امور اداری"))
            {
                result = 20;
            }
            return result;
        }

        //public async Task<JsonResult> Confirm(int id)
        //{
        //    var m = new Message();
        //    try
        //    {
        //        var reservation = await _unitOfWork.GetRepository<TblFoodReservation>().GetFirst(a => a.Id == id);
        //        var user = int.Parse(User.Identity.Name);

        //        //تایید درخواست شخصی توسط امور اداری
        //        //if (reservation.Status == 1)
        //        //        reservation.Status = 20;


        //        var (result, count, message) = await _unitOfWork.GetRepository<TblFoodReservation>().DeleteRange(a => a.Group == id, user);
        //        m.Msg = result ? Resources.Public.DeleteSuccessMessage : string.Format(Resources.Public.DeleteErrorMessage, message);
        //        m.Result = result;
        //        m.Type = result ? "success" : "error";


        //    }
        //    catch (Exception e)
        //    {

        //        m.Msg = e.Message;
        //        m.Result = false;
        //        m.Type = "error";


        //    }

        //    return Json(m, JsonRequestBehavior.AllowGet);
        //}

        public async Task<JsonResult> UsedFood(int id)
        {
            var m = new Message();
            try
            {
                var reservation = await _unitOfWork.GetRepository<TblFoodReservation>().GetFirst(a => a.Id == id);
                var user = int.Parse(User.Identity.Name);

                //تایید درخواست شخصی توسط امور اداری
                //if (reservation.Status == 1)
                //    reservation.Status = 20;
                reservation.Serving = false;
                // await  _unitOfWork.SaveAsync();
                var (result, message) = await _unitOfWork.GetRepository<TblFoodReservation>().Update(reservation, user);
                m.Msg = result ? Resources.Public.FoodServing : string.Format(Resources.Public.ErrorMessage, message);
                m.Result = result;
                m.Type = result ? "success" : "error";


            }
            catch (Exception e)
            {

                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";


            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(Duration = 60000, VaryByParam = "id")]
        public string MealTitle(int id)
        {
            return _db.TblMeals.Single(a => a.Id == id).Title;

        }
    }



}
