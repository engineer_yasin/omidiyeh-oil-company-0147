﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{

    public class ContractController : Controller
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db;

        public ContractController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
            _db = new dbEntities();

        }
        [AllowAnonymous]
        public JsonResult Items()
        {
            var list = _db.TblContracts.Where(a => a.Status == 1).Select(a=>new
            {
                a.Id, a.Title, a.Number
            });
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "73")]
        // GET: Welfare/Contract
        public ActionResult Index()
        {
            return View();
        }


        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            var sortDirection = Request["order[0][dir]"];
            var draw = Request["draw"];
            length = length == 0 ? 10 : length;
            if (string.IsNullOrEmpty(sortColumnName))
            {
                sortColumnName = "Id";
                sortDirection = "desc";
            }
            var includeProperties = @"TblContractor,
                                    TblContractStatu,
                                    TblContractRestaurants,
                                    TblContractRestaurants.TblRestaurant,
                                    TblContractFoods,
                                    TblContractFoods.TblContractPriceSeriesFoods,
                                    TblContractFoods.TblFood";

            var iResult = _unitOfWork.GetRepository<TblContract>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Title.Contains(searchValue) ||
                                        a.Number.Contains(searchValue) ||
                                        //a.TblContractRestaurants.Select(r=>r.TblRestaurant).Select(rr=>rr.Title).Contains(searchValue) ||
                                        a.Body.Contains(searchValue),
                    orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                    includeProperties: includeProperties) :
                    iResult.GetOrdered(orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                        includeProperties: includeProperties);
            var (items, count, total) = await result;

            var data = items.Where(a => a.Id > 0).Select(a => new
            {
                id = " ",
                a.Title,
                a.Number,
                Contractor = a.TblContractor.Title,
                Restaurant = string.Join(", ", a.TblContractRestaurants.Select(b => b.TblRestaurant.Title).ToList()),
                Status = a.TblContractStatu.Title,
                StartDate = new PersianDateTime(a.StartDate).ToShortDateString(),
                EndDate = new PersianDateTime(a.EndDate).ToShortDateString(),
                InitialQuota = a.InitialQuota.ToString("#,0"),
                RemainingQuota = a.RemainingQuota.ToString("#,0"),
                UsedQuota = a.UsedQuota.ToString("#,0"),
                Amount = a.Amount.ToString("#,0"),
                Foods = a.TblContractFoods.Select(b => new
                {
                    Food = b.TblFood.Title,
                    Price = b.TblContractPriceSeriesFoods.FirstOrDefault()?.Price.ToString("#,0"),
                    FreePrice = b.TblContractPriceSeriesFoods.FirstOrDefault()?.FreePrice.ToString("#,0"),
                    InitialQuota = b.InitialQuota.ToString("#,0"),
                    UsedQuota = b.UsedQuota.ToString("#,0"),
                    RemainingQuota = b.RemainingQuota.ToString("#,0"),
                    b.Id
                }).OrderBy(c => c.Food),
                DT_RowId = a.Id,
                edit = $"<a href='/Welfare/Contract/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-lg'></i></a> " +
                          $"<a href='/Welfare/Contract/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-lg'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> Foods(int contract)
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var includeProperties = @"
                                    TblContractPriceSeriesFoods,
                                    TblFood";

            var iResult = _unitOfWork.GetRepository<TblContractFood>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Contract == contract && a.TblFood.Title.Contains(searchValue),
                        orderBy: order, take: length, skip: start, includeProperties: includeProperties) :
                iResult.GetOrdered(a => a.Contract == contract,
                        orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                a.TblFood.Title,
                Price = a.TblContractPriceSeriesFoods.Sum(b => b.Price).ToString("#,0"),
                FreePrice = a.TblContractPriceSeriesFoods.Sum(b => b.FreePrice).ToString("#,0"),
                InitialQuota = a.InitialQuota.ToString("#,0"),
                UsedQuota = a.UsedQuota.ToString("#,0"),
                RemainingQuota = a.RemainingQuota.ToString("#,0"),
                DT_RowId = a.Id
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        // [HttpPost]
        public async Task<JsonResult> PriceSery(int contract)
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            var includeProperties = @"
                                    TblUser.TblPerson";

            var iResult = _unitOfWork.GetRepository<TblContractPriceSery>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Contract == contract && (a.Title.Contains(searchValue) || a.Des.Contains(searchValue)),
                        orderBy: order, take: length, skip: start, includeProperties: includeProperties) :
                iResult.GetOrdered(a => a.Contract == contract,
                        orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                a.Title,
                a.Des,
                User = a.TblUser.TblPerson.Fullname(),
                FromDate = new PersianDateTime(a.FromDate).ToShortDateString(),
                ToDate = new PersianDateTime(a.ToDate).ToShortDateString(),
                CreateDate = new PersianDateTime(a.CreateDate).ToShortDateString(),
                DT_RowId = a.Id
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> PriceSeryFoods(int id)
        {
            var list = await _unitOfWork.GetRepository<TblContractPriceSeriesFood>().Get(a => a.ContractPriceSeries == id, includeProperties: "TblContractFood.TblFood");
            var data = list.Select(a => new
            {
                a.Id,
                Food = a.TblContractFood.TblFood.Title,
                Price = a.Price.ToString("#,0"),
                FreePrice = a.FreePrice.ToString("#,0")

            }).OrderBy(a => a.Food);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        // GET: Welfare/Contract/Details/5
        public async Task<ActionResult> Details(int id)
        {
            const string includeProperties = @"TblContractor,
                                    TblContractStatu,
                                    TblContractRestaurants,
                                    TblContractRestaurants.TblRestaurant,
                                    TblContractFoods,
                                    TblContractFoods.TblContractPriceSeriesFoods,
                                    TblContractFoods.TblFood";
            var iResult = _unitOfWork.GetRepository<TblContract>();
            var result = await iResult.GetSingle(a => a.Id == id, includeProperties: includeProperties);
            if (result == null)
            {
                return HttpNotFound();
            }


            return View(result);
        }

        [HttpPost]
        public async Task<ActionResult> AddPriceSery(string title, string des, DateTime startDate, DateTime endDate, int contract)
        {
            var m = new Message();
            var user = int.Parse(User.Identity.Name);
            var ps = new TblContractPriceSery
            {
                Contract = contract,
                FromDate = startDate,
                ToDate = endDate,
                Title = title,
                CreateDate = DateTime.Now,
                User = user,
                Des = des
            };
            var (resultPs, messagePs) =
                await _unitOfWork.GetRepository<TblContractPriceSery>().Insert(ps, user);
            if (resultPs)
            {
                var listPrices = new List<TblContractPriceSeriesFood>();
                foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("Price-")))
                {
                    var contractFood = Convert.ToInt32(item.Split('-')[1]);
                    var fp = new TblContractPriceSeriesFood
                    {
                        Price = Convert.ToInt32(Request.Form["Price-" + contractFood].ToRemoveAt(".")),
                        FreePrice = Convert.ToInt32(Request.Form["FreePrice-" + contractFood].ToRemoveAt(".")),
                        ContractPriceSeries = ps.Id,
                        ContractFood = contractFood,
                    };
                    listPrices.Add(fp);
                }

                var (result, message) =
                    await _unitOfWork.GetRepository<TblContractPriceSeriesFood>().InsertRange(listPrices, user);
                if (result)
                {
                    m.Result = true;
                    m.Type = "success";
                    m.Msg = string.Format(Resources.Public.AddSuccessMessage, "ثبت سری قیمت");
                }
                else
                {
                    await _unitOfWork.GetRepository<TblContractPriceSery>().Delete(ps.Id, user);
                    m.Result = false;
                    m.Type = "error";
                    m.Msg = string.Format(Resources.Public.AddErrorMessage, "سری قیمت", message);
                }
            }
            else
            {
                m.Result = false;
                m.Type = "error";
                m.Msg = string.Format(Resources.Public.AddErrorMessage, "سری قیمت", messagePs);
            }
            return Json(m, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "74")]
        // GET: Welfare/Contract/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Contractor = new SelectList(await _unitOfWork.GetRepository<TblContractor>().Get(a => a.Id > 0), "Id", "Title");
            ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblContractStatu>().Get(), "Id", "Title");
            ViewBag.Restaurant = await _unitOfWork.GetRepository<TblRestaurant>().Get();
            return View();
        }

        // POST: Welfare/Contract/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "74")]
        public async Task<ActionResult> Create([Bind(Include = "Id,Title,Contractor,Office,Restaurant,Body,StartDate,EndDate,Number,FoodCount,Status")] TblContract entity, string usedQuota, string amount, string initialQuota, List<int> restaurant)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    entity.Amount = Convert.ToInt64(amount.Replace(",", ""));
                    entity.InitialQuota = Convert.ToInt32(initialQuota.Replace(",", ""));
                    entity.UsedQuota = Convert.ToInt32(usedQuota.Replace(",", ""));
                    entity.RemainingQuota = entity.InitialQuota - entity.UsedQuota;
                    var (result, message) = await _unitOfWork.GetRepository<TblContract>().Insert(entity, user);
                    if (result)
                    {
                        //ذخیره رستوران های یک قرارداد
                        foreach (var cr in restaurant.Select(item => new TblContractRestaurant
                        {
                            Contract = entity.Id,
                            Restaurant = item
                        }))
                        {
                            var (resultR, messageR) = await _unitOfWork.GetRepository<TblContractRestaurant>().Insert(cr, user);
                        }

                        var ps = new TblContractPriceSery
                        {
                            Contract = entity.Id,
                            FromDate = entity.StartDate,
                            ToDate = entity.EndDate,
                            Title = "اول",
                            CreateDate = DateTime.Now,
                            User = user,
                            Des = "ایجاد پیمان"
                        };
                        var (resultPs, messagePs) =
                            await _unitOfWork.GetRepository<TblContractPriceSery>().Insert(ps, user);
                        TempData["successToast"] = string.Format(Resources.Public.CreateSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorToast"] = string.Format(Resources.Public.CreateErrorMessage, entity.Title, message);

                }


            }
            catch (Exception e)
            {
                TempData["errorToast"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            ViewBag.Contractor = new SelectList(await _unitOfWork.GetRepository<TblContractor>().Get(a => a.Id > 0), "Id", "Title");
            ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblContractStatu>().Get(), "Id", "Title");
            ViewBag.Restaurant = await _unitOfWork.GetRepository<TblRestaurant>().Get();
            return View(entity);
        }

        // GET: Welfare/Contract/Edit/5
        [Authorize(Roles = "75")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblContract>();
            var result = await iResult.GetSingle(a => a.Id == id, includeProperties: "TblContractRestaurants");
            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.Contractor = new SelectList(await _unitOfWork.GetRepository<TblContractor>().Get(a => a.Id > 0), "Id", "Title", result.Contractor);
            ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblContractStatu>().Get(), "Id", "Title", result.Status);

            ViewBag.Restaurant = await _unitOfWork.GetRepository<TblRestaurant>().Get();
            return View(result);
        }

        // POST: Welfare/Contract/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "75")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Title,Contractor,Office,Restaurant,Body,StartDate,EndDate,Number,FoodCount,Status")] TblContract entity, string usedQuota, string amount, string initialQuota, List<int> restaurant)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    entity.Amount = Convert.ToInt64(amount.Replace(",", ""));
                    entity.InitialQuota = Convert.ToInt32(initialQuota.Replace(",", ""));
                    entity.UsedQuota = Convert.ToInt32(usedQuota.Replace(",", ""));
                    entity.RemainingQuota = entity.InitialQuota - entity.UsedQuota;
                    var (result, message) = await _unitOfWork.GetRepository<TblContract>().Update(entity, user);
                    if (result)
                    {
                        //ذخیره رستوران های یک قرارداد
                        var newRestaurant = restaurant;
                        var oldRestaurant =
                            ( _unitOfWork.GetRepository<TblContractRestaurant>().GetSync(a => a.Contract == entity.Id))
                            .Select(a => a.Restaurant).ToList();
                        var deleteRestaurant = oldRestaurant.Except(newRestaurant).ToList();
                        if (deleteRestaurant.Any())
                        {
                            var (resultDelete, _, messageDelete) = await _unitOfWork.GetRepository<TblContractRestaurant>()
                                .DeleteRange(a => a.Contract == entity.Id && deleteRestaurant.Contains(a.Restaurant), user);
                        }

                        var list = newRestaurant.Except(oldRestaurant).ToList().Select(item =>
                            new TblContractRestaurant { Contract = entity.Id, Restaurant = item }).ToList();
                        if (list.Any())
                            await _unitOfWork.GetRepository<TblContractRestaurant>().InsertRange(list, user);

                        //بررسی و اصلاح آماری پیمان
                        var contract = _db.TblContracts.Find(entity.Id);
                        var restaurants = _db.TblContractRestaurants
                            .Where(a => a.Contract == entity.Id)
                            .Select(a => a.Restaurant)
                            .ToList();

                        foreach (var item in restaurants)
                        {
                            _db.CheckContract(entity.Id, item, contract.StartDate, contract.EndDate);
                        }
                        //بررسی و اصلاح آماری پیمان

                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Title);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Title, message);
                    return RedirectToAction("Index");
                }
                ViewBag.Contractor = new SelectList(await _unitOfWork.GetRepository<TblContractor>().Get(), "Id", "Title", entity.Contractor);
                ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblContractStatu>().Get(), "Id", "Title", entity.Status);
                ViewBag.Restaurant = await _unitOfWork.GetRepository<TblRestaurant>().Get();
            }
            catch (Exception e)
            {
                ViewBag.Contractor = new SelectList(await _unitOfWork.GetRepository<TblContractor>().Get(), "Id", "Title", entity.Contractor);
                ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblContractStatu>().Get(), "Id", "Title", entity.Status);
                ViewBag.Restaurant = await _unitOfWork.GetRepository<TblRestaurant>().Get();
                TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/Contract/Delete/5
        [Authorize(Roles = "76")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblContract>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Welfare/Contract/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "76")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblContract>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

        [Authorize(Roles = "284")]
        public ActionResult ChangeContract()
        {
            ViewBag.ContractFrom = new SelectList(_unitOfWork.GetRepository<TblContract>().GetSync(a => a.Id > 1), "Id", "Title");
            ViewBag.ContractTo = ViewBag.ContractFrom;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "284")]
        public ActionResult ChangeContract(DateTime startDate, DateTime endDate, int contractTo, int contractFrom)
        {
            var m = new Message();
            try
            {
                var list = _db.TblFoodReservations.Where(a =>
                    a.Contract == contractFrom &&
                    a.Date >= startDate && a.Date <= endDate);
                foreach (var item in list)
                {
                    item.Contract = contractTo;
                }

                _db.SaveChanges();
                m.Msg = string.Format(Resources.Public.EditSuccessMessage,
                    $"پیمان ({list.Count()})");

                m.Result = true;
                m.Type = "success";
            }
            catch (Exception ex)
            {
                m.Msg = ex.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }


        [Authorize(Roles = "284")]
        public ActionResult CheckContract(int id)
        {
            var m = new Message();
            try
            {
                var count = 0;
                var contract = _db.TblContracts.Find(id);
                var restaurants = _db.TblContractRestaurants
                    .Where(a => a.Contract == id)
                    .Select(a => a.Restaurant)
                    .ToList();

                foreach (var item in restaurants)
                {
                  count +=  _db.CheckContract(id, item, contract.StartDate, contract.EndDate) ;
                }
                //var list = _db.TblFoodReservations.Where(a => restaurants.Contains(a.Restaurant) &&
                //                                             a.Contract != id &&
                //                                             a.Date >= contract.StartDate && a.Date <= contract.EndDate
                //                                             ).OrderBy(a=>a.Id);

                //var count = list.Count();
                //var step = 20000;
                //var s = count / step;
                //for (var i = 0; i <= s; i++)
                //{
                //    foreach (var item in list.Skip(i*step).Take(step))
                //    {
                //        item.Contract = id;
                //    }

                //    _db.SaveChanges();
                //}

                //var t1 = new Stopwatch();
               
                //foreach (var item in list.Skip(0).Take(10000))
                //{
                //    item.Contract = id;
                //}
                //t1.Start();
                //_db.BulkSaveChanges();
                //t1.Stop();
                //m.Msg += "t1:" + t1.ElapsedMilliseconds;
                
                
                //var t2 = new Stopwatch();
                
                //foreach (var item in list.Skip(10000).Take(10000))
                //{
                //    item.Contract = id;
                //}
                //t2.Start();
                //_db.BatchSaveChanges();
                //t2.Stop();
                //m.Msg += " t2:" + t2.ElapsedMilliseconds;
               
                
                //var t3 = new Stopwatch();
               
                //foreach (var item in list.Skip(20000).Take(10000))
                //{
                //    item.Contract = id;
                //}
                //t3.Start();
                //_db.SaveChanges();
                //t3.Stop();
                //m.Msg += " t3:" + t3.ElapsedMilliseconds;

               // _db.BulkSaveChanges();

                m.Msg += string.Format(Resources.Public.EditSuccessMessage,
                    $"پیمان ({count})");

                m.Result = true;
                m.Type = "success";
            }
            catch (Exception ex)
            {
                m.Msg += ex.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Checkout()
        {
            return View();
        }


        public async Task<JsonResult> CheckoutList()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            var sortDirection = Request["order[0][dir]"];
            var draw = Request["draw"];
            length = length == 0 ? 10 : length;
            if (string.IsNullOrEmpty(sortColumnName))
            {
                sortColumnName = "Id";
                sortDirection = "desc";
            }
            const string includeProperties = @"TblContract,
                                    TblContract.TblContractor";

            var iResult = _unitOfWork.GetRepository<TblContractRestaurantCheckout>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrderedSync(a => a.TblContract.Title.Contains(searchValue)
                                            || a.TblContract.TblContractor.Title.Contains(searchValue)
                                            || a.TblRestaurant.Title.Contains(searchValue)
                    ,
                    orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                    includeProperties: includeProperties) :
                    iResult.GetOrderedSync(orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                        includeProperties: includeProperties);
            var (items, count, total) = result;

            var data = items.Where(a => a.Id > 0).Select(a => new
            {
                id = " ",
                a.TblContract.Title,

                Contractor = a.TblContract.TblContractor.Title,
                Restaurant = a.TblRestaurant.Title,

                StartDate = new PersianDateTime(a.FromDate).ToShortDateString(),
                EndDate = new PersianDateTime(a.ToDate).ToShortDateString(),
                Date = new PersianDateTime(a.Date).ToShortDateString(),
                User = a.TblUser.TblPerson.Fullname(),
                DT_RowId = a.Id,
                edit = $"<a href='/Welfare/Contract/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-lg'></i></a> " +
                          $"<a href='/Welfare/Contract/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-lg'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

    }
}
