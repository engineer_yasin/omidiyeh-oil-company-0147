﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;

using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{

    public class RestaurantFoodProgramController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public RestaurantFoodProgramController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }

        [OutputCache(Duration = 600)]
        public async Task<List<Item>> RestaurantFood(DateTime date, int meal, int restaurant)
        {
            var userId = int.Parse(User.Identity.Name);
            var foods = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().Get(a => a.Restaurant == restaurant && a.Date == date && a.Meal == meal,
                includeProperties: "TblFood");
            var data = foods.OrderByDescending(a => a.Default).Select(a => new Item
            {
                Id = a.Food,
                Title = a.TblFood.Title
            });
            // var result = (await iResult.Get(a => a.Office == id)).ToList();
            return data.ToList();

        }

        [Authorize(Roles = "171")]
        // GET: Welfare/RestaurantFoodProgram
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            var draw = Request["draw"];
            length = length == 0 ? 10 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);

            var iResult = _unitOfWork.GetRepository<TblRestaurantFoodProgram>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.TblFood.Title.ToString().Contains(searchValue) || a.TblRestaurant.Title.Contains(searchValue) || a.TblMeal.Title.Contains(searchValue),
                    orderBy: order, take: length, skip: start,
                    includeProperties: "TblRestaurant,TblFood,TblMeal") :
                    iResult.GetOrdered(orderBy: order, take: length, skip: start,
                        includeProperties: "TblRestaurant,TblFood,TblMeal");
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                Restaurant = a.TblRestaurant.Title,
                Food = a.TblFood.Title,
                Meal = a.TblMeal.Title,
                Date = new PersianDateTime(a.Date).ToLongDateString(),
                DT_RowId = a.Id,
                edit = $"<a href='/Welfare/RestaurantFoodProgram/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> " +
                      $"<a href='/Welfare/RestaurantFoodProgram/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        // GET: Welfare/RestaurantFoodProgram/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblRestaurantFoodProgram>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        [Authorize(Roles = "241")]
        public async Task<ActionResult> Sync()
        {
            //TempData["errorToast"] = "شسیسشی" ;
            ViewBag.Restaurant = new SelectList((await _unitOfWork.GetRepository<TblRestaurant>().Get()).OrderBy(a => a.Title), "Id", "Title");
            return View();
        }
        [HttpPost]
        [Authorize(Roles = "241")]
        public ActionResult Sync(int year, int month, int restaurant)
        {
            try
            {
                var count = 0;
                var (startDate, endDate) = GetMonthDate(year, month);

                for (var counter = startDate; counter <= endDate; counter = counter.AddDays(1))
                {
                    var items = _unitOfWork.GetRepository<TblRestaurantFoodProgram>().GetSync(
                        a => a.Restaurant == restaurant && a.Date == counter);
                    var foods = items.OrderByDescending(a => a.Default).Select(a => new MealFood { Food = a.Food, Meal = a.Meal, Default = a.Default })
                        .ToList();


                    var list = _unitOfWork.GetRepository<TblFoodReservation>().GetSync(a =>
                    a.Restaurant == restaurant&&
                            a.Remove == false &&
                            a.Date == counter &&
                          //  a.Date >DateTime.Now &&
                            a.TblFoodReservationGroup.Remove == false &&
                            a.TblFoodReservationGroup.Temp == false &&
                            a.Food != -1 &&
                            a.Food != 0 
                           
                           
                           ,includeProperties: "TblMeal")
                        ;
                    foreach (var item in list)
                    {
                        if (!foods.Where(a => a.Meal == item.Meal).Select(a => a.Food).Contains(item.Food))
                        {
                            var f = foods.Any(a => a.Meal == item.Meal && a.Default);
                            if (!f)
                            {
                                TempData["errorMessage"] =  string.Format(Resources.Public.ErrorMessage, $"برنامه غذایی برای تاریخ {new PersianDateTime(counter).ToLongDateString()} و وعده {item.TblMeal.Title} دارای غذای پیش فرض نمی باشد");
                                return RedirectToAction("Sync");
                            }
                            item.Food = foods.First(a => a.Meal == item.Meal && a.Default).Food;
                            count++;
                        }
                    }
                    _unitOfWork.Save();

                }
                TempData["successMessage"] = string.Format(Resources.Public.AddSuccessMessage, $"همگام سازی {count} مورد درخواست غذا");

                return RedirectToAction("Sync");
            }
            catch(Exception e)
            {
                TempData["errorMessage"] += "" + string.Format(Resources.Public.ErrorMessage, e.Message);
                return RedirectToAction("Sync");
            }
        }


        [Authorize(Roles = "173")]
        public async Task<ActionResult> Monthly()
        {
            ViewBag.Restaurant = new SelectList((await _unitOfWork.GetRepository<TblRestaurant>().Get()).OrderBy(a => a.Title), "Id", "Title");
            return View();
        }


        [HttpPost]
        [Authorize(Roles = "173")]
        public ActionResult Monthly(int year, int month, int restaurant)
        {
            return RedirectToAction("Group", new { year, month, restaurant });
        }

        [Authorize(Roles = "176")]
        public async Task<ActionResult> MonthlyCopy()
        {
            ViewBag.RestaurantFrom = new SelectList(await _unitOfWork.GetRepository<TblRestaurant>().Get(), "Id", "Title");
            ViewBag.RestaurantTo = ViewBag.RestaurantFrom;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "176")]
        public async Task<ActionResult> MonthlyCopy(int yearFrom, int monthFrom, int restaurantFrom, int yearTo, int monthTo, int restaurantTo)
        {
            try
            {
                var user = int.Parse(User.Identity.Name);

                //خواندن اطلاعات برنامه مبدا
                var (startDateFrom, endDateFrom) = GetMonthDate(yearFrom, monthFrom);
                var programsFrom = (await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().Get(
                    a => a.Restaurant == restaurantFrom && a.Date >= startDateFrom && a.Date <= endDateFrom)).ToList();

                //حذف برنامه های مقصد
                var (startDateTo, endDateTo) = GetMonthDate(yearTo, monthTo);
                var (resultDeleteRange, countDeleteRange, messageDeleteRange) = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().DeleteRange(
                    a => a.Restaurant == restaurantTo && a.Date >= startDateTo && a.Date <= endDateTo, user);

                if (resultDeleteRange || countDeleteRange == 0)
                {
                    var list = new List<TblRestaurantFoodProgram>();
                    //ثبت اطلاعات مبدا در مقصد
                    foreach (var item in programsFrom)
                    {
                        var dateFa = new PersianDateTime(item.Date);
                        if (dateFa.Day == 31 && monthTo > 6)
                            continue;
                        var dateTo = new PersianDateTime(yearTo, monthTo, dateFa.Day).ToDateTime();
                        var updateId = Guid.NewGuid().ToString();
                        var fp = new TblRestaurantFoodProgram
                        {
                            Id = Guid.NewGuid(),
                            Date = dateTo,
                            Restaurant = restaurantTo,
                            Food = item.Food,
                            Meal = item.Meal,
                            UpdateId = updateId,
                            Default = item.Default
                        };
                        list.Add(fp);
                    }

                    var (result, message) = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>()
                        .InsertRange(list, user);

                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.CreateSuccessMessage,
                                                          "برنامه ماهیانه");

                    }
                    else
                        TempData["errorMessage"] = string.Format(Resources.Public.CreateErrorMessage, "برنامه ماهیانه", message);
                }
                else
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, messageDeleteRange);

            }
            catch (Exception e)
            {
                TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("MonthlyCopy");
        }
        // GET: /Welfare/RestaurantFoodProgram/Group
        [Authorize(Roles = "171")]
        public async Task<ActionResult> Group(int year, int month, int restaurant)
        {
            try
            {
                var (startDate, endDate) = GetMonthDate(year, month);
                var list = new List<ModelFoodProgram>();
                for (var counter = startDate; counter <= endDate; counter = counter.AddDays(1))
                {
                    var items = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().Get(
                        a => a.Restaurant == restaurant && a.Date == counter);
                    var foods = items.OrderByDescending(a => a.Default).Select(a => new MealFood { Food = a.Food, Meal = a.Meal })
                        .ToList();

                    var item = new ModelFoodProgram
                    {
                        Date = counter,
                        Foods = foods
                    };
                    list.Add(item);
                }

                ViewBag.Foods = (await _unitOfWork.GetRepository<TblFood>().Get()).OrderBy(a => a.Title)
                    .ToList();
                ViewBag.Year = year;
                ViewBag.Month = month;
                ViewBag.Restaurant = restaurant;
                ViewBag.RestaurantTitle = (await _unitOfWork.GetRepository<TblRestaurant>().GetSingle(a => a.Id == restaurant)).Title;
                ViewBag.Meal = await _unitOfWork.GetRepository<TblMeal>().Get();
                return View(list);
            }
            catch
            {
                return RedirectToAction("Monthly");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "171")]
        public async Task<ActionResult> Group(int year, int month, int restaurant, string temp, bool? addAnother)
        {
            try
            {
                var req = Request.Form.AllKeys;
                var user = int.Parse(User.Identity.Name);
                var updateId = Guid.NewGuid().ToString();
                foreach (var item in req)
                {

                    if (item.StartsWith("Food-"))
                    {
                        //if (item[2] == '-')
                        //    continue;
                        var fo = item.Split('-');
                        var meal = Convert.ToInt32(fo[1]);

                        var day = Convert.ToInt32(fo[2]);
                        var date = new PersianDateTime(year, month, day).ToDateTime();
                        var foods = Request.Form[item].Split(',');
                        var defaultValue = Request.Form["default-" + meal + "-" + day];
                        foreach (var food in foods)
                        {
                            var def = food == defaultValue;
                            var f = Convert.ToInt32(food);
                            var p = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().Get(a =>
                                  a.Date == date && a.Restaurant == restaurant && a.Meal == meal && a.Food == f);
                            if (!p.Any())
                            {
                                var fp = new TblRestaurantFoodProgram
                                {
                                    Id = Guid.NewGuid(),
                                    Restaurant = restaurant,
                                    Date = date,
                                    Food = f,
                                    Meal = meal,
                                    UpdateId = updateId,
                                    Default = def
                                };
                                var (result, message) =
                                    await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().Insert(fp, user);
                                if (!result)
                                    TempData["errorToast"] += "<br />" +
                                                                string.Format(Resources.Public.CreateErrorMessage, fp.Id, message);
                            }
                            else
                            {
                                var pu = p.Single();
                                pu.UpdateId = updateId;
                                pu.Default = def;
                                var (result2, message2) =
                                    await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().Update(pu, user);
                                if (!result2)
                                    TempData["errorToast"] += "<br />" + string.Format(Resources.Public.EditErrorMessage, pu.Id, message2);
                            }
                        }
                    }
                }
                var (startDate, endDate) = GetMonthDate(year, month);
                var (resultDeleteRange, count, messageDeleteRange) = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().DeleteRange(
                    a => a.Restaurant == restaurant && a.Date >= startDate && a.Date <= endDate && a.UpdateId != updateId, user);

                if (!resultDeleteRange && count != 0)
                    TempData["errorToast"] += "<br />" + string.Format(Resources.Public.DeleteErrorMessage, messageDeleteRange);


                if (!TempData.ContainsKey("errorToast"))
                    TempData["successToast"] = string.Format(Resources.Public.AddSuccessMessage, "ثبت برنامه ماهیانه");

            }
            catch (Exception e)
            {
                TempData["errorToast"] += "<br />" + string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return addAnother == true ? RedirectToAction("Monthly") : RedirectToAction("Group", new { year, month, restaurant }); ;
            // return RedirectToAction("Monthly");
        }

        // GET: Welfare/RestaurantFoodProgram/Create
        public async Task<ActionResult> Create()
        {
            ViewBag.Food = new SelectList(await _unitOfWork.GetRepository<TblFood>().Get(), "Id", "Title");
            ViewBag.Meal = new SelectList(await _unitOfWork.GetRepository<TblMeal>().Get(), "Id", "Title");
            ViewBag.Restaurant = new SelectList(await _unitOfWork.GetRepository<TblRestaurant>().Get(), "Id", "Title");
            return View();
        }

        private static (DateTime startDate, DateTime endDate) GetMonthDate(int year, int month)
        {
            const int startDay = 1;
            var endDay = month > 6 ? 30 : 31;
            var startDate = new PersianDateTime(year, month, startDay).ToDateTime();
            if (!PersianDateTime.TryParse(year + "/" + month + "/" + endDay, out var endDateFa))
            {
                PersianDateTime.TryParse(year + "/" + month + "/" + (endDay - 1), out endDateFa);
            }

            var endDate = endDateFa.ToDateTime();
            return (startDate, endDate);
        }



        // POST: Welfare/RestaurantFoodProgram/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Restaurant,Meal,Date")] TblRestaurantFoodProgram entity, List<int> food)
        {
            try
            {
                var user = int.Parse(User.Identity.Name);
                foreach (var item in food)
                {

                    var tempEntity = new TblRestaurantFoodProgram()
                    {
                        Restaurant = entity.Restaurant,
                        Meal = entity.Meal,
                        Date = entity.Date,
                        Food = item,
                        Id = Guid.NewGuid(),
                        UpdateId = Guid.NewGuid().ToString()

                    };
                    if (ModelState.IsValid)
                    {


                        var (result, message) =
                            await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().Insert(tempEntity, user);
                        if (result)
                        {
                            TempData["successMessage"] += "<br />" +
                                string.Format(Resources.Public.CreateSuccessMessage, tempEntity.Id);

                        }
                        else
                            TempData["errorMessage"] += "<br />" +
                                string.Format(Resources.Public.CreateErrorMessage, tempEntity.Id, message);

                    }
                    else
                    {
                        goto ret;
                    }

                }
                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
        ret:
            ViewBag.Food = new SelectList(await _unitOfWork.GetRepository<TblFood>().Get(), "Id", "Title");
            ViewBag.Meal = new SelectList(await _unitOfWork.GetRepository<TblMeal>().Get(), "Id", "Title");
            ViewBag.Restaurant = new SelectList(await _unitOfWork.GetRepository<TblRestaurant>().Get(), "Id", "Title");
            return View(entity);
        }

        // GET: Welfare/RestaurantFoodProgram/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblRestaurantFoodProgram>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }

            ViewBag.Food = new SelectList(await _unitOfWork.GetRepository<TblFood>().Get(), "Id", "Title");
            ViewBag.Meal = new SelectList(await _unitOfWork.GetRepository<TblMeal>().Get(), "Id", "Title");
            ViewBag.Restaurant = new SelectList(await _unitOfWork.GetRepository<TblRestaurant>().Get(), "Id", "Title");
            return View(result);
        }

        // POST: Welfare/RestaurantFoodProgram/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Food,Restaurant,Meal,Date")] TblRestaurantFoodProgram entity)

        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().Update(entity, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.EditSuccessMessage, entity.Id);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.EditErrorMessage, entity.Id, message);
                    return RedirectToAction("Index");
                }
                ViewBag.Food = new SelectList(await _unitOfWork.GetRepository<TblFood>().Get(), "Id", "Title");
                ViewBag.Meal = new SelectList(await _unitOfWork.GetRepository<TblMeal>().Get(), "Id", "Title");
                ViewBag.Restaurant = new SelectList(await _unitOfWork.GetRepository<TblRestaurant>().Get(), "Id", "Title");
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }
            return View(entity);
        }

        // GET: Welfare/RestaurantFoodProgram/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var iResult = _unitOfWork.GetRepository<TblRestaurantFoodProgram>();
            var result = await iResult.GetSingle(a => a.Id == id);
            if (result == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Welfare/RestaurantFoodProgram/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = int.Parse(User.Identity.Name);
                    var (result, message) = await _unitOfWork.GetRepository<TblRestaurantFoodProgram>().Delete(id, user);
                    if (result)
                    {
                        TempData["successMessage"] = string.Format(Resources.Public.DeleteSuccessMessage);
                        return RedirectToAction("Index");
                    }
                    TempData["errorMessage"] = string.Format(Resources.Public.DeleteErrorMessage, message);
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = TempData["errorMessage"] = string.Format(Resources.Public.ErrorMessage, e.Message);
            }

            return RedirectToAction("Delete", new { id });
        }

    }
}
