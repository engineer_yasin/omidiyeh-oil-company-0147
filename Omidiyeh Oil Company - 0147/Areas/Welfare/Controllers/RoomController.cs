﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;

using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    [Authorize()]
    public class RoomController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public RoomController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }

       

        // GET: Welfare/Room
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> List()
        {
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            var searchValue = Request["search[value]"];
            var sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            var sortDirection = Request["order[0][dir]"];
            var draw = Request["draw"];
            length = length == 0 ? 10 : length;
            if (string.IsNullOrEmpty(sortColumnName))
            {
                sortColumnName = "Id";
                sortDirection = "desc";
            }
            var iResult = _unitOfWork.GetRepository<TblRoom>();
            var result = !string.IsNullOrEmpty(searchValue) ?
                iResult.GetOrdered(a => a.Number.ToString().Contains(searchValue) || a.Des.Contains(searchValue),
                    orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                    includeProperties: "TblRestaurant,TblRoomStatu,TblRoomType") :
                    iResult.GetOrdered(orderBy: sortColumnName + " " + sortDirection, take: length, skip: start,
                        includeProperties: "TblRestaurant,TblRoomStatu,TblRoomType");
            var (items, count, total) = await result;

            var data = items.Select(a => new
            {
                id = " ",
                a.Number,
                a.Capacity,
                Restaurant = a.TblRestaurant.Title,
                Status = a.TblRoomStatu.Title,
                Type = a.TblRoomType.Title,
                DT_RowId = a.Id,
                edit = $"<a href='/Welfare/Bed/Create/?room={a.Id}' title='ایجاد تخت' class='text-info ml-2' ><i class='fas fa-bed fa-2x'></i></a> " + 
                       $"<a href='/Welfare/Room/Edit/{a.Id}' class='text-success ml-2' ><i class='fas fa-edit fa-2x'></i></a> " +
                       $"<a href='/Welfare/Room/Delete/{a.Id}' class='text-danger' ><i class='fas fas fa-trash-alt fa-2x'></i></a>"
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            return Json(res, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Create()
        {
            return PartialView();
        }
        
        public async Task<JsonResult> CreateRoom(int number,int restaurant,byte type)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var r = new TblRoom {Capacity = 0, Number = number, Restaurant = restaurant, Status = 1, Type = type};
                    var (result, msg ) = await _unitOfWork.GetRepository<TblRoom>().Insert(r, user);
                    m.Msg = result ? string.Format(Resources.Public.AddSuccessMessage, "ایجاد اتاق") :
                        string.Format(Resources.Public.AddErrorMessage, "ایجاد اتاق", msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var r = await _unitOfWork.GetRepository<TblRoom>().GetSingle(a => a.Id == id);
            ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblRoomStatu>().Get(), "Id", "Title",r.Status);
            return PartialView(r);
        }

        [HttpPost]
        public async Task<JsonResult> Edit(int id,int number, byte status, byte type, string des)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var r = await _unitOfWork.GetRepository<TblRoom>().GetSingle(a => a.Id == id);
                    r.Number = number;
                    r.Status = status;
                    r.Type = type;
                    r.Des = des;

                    var (result, msg) = await _unitOfWork.GetRepository<TblRoom>().Update(r, user);
                    m.Msg = result ? string.Format(Resources.Public.EditSuccessMessage, " اتاق" + r.Number) :
                        string.Format(Resources.Public.EditErrorMessage, "ویرایش اتاق", msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> Delete(int id)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var (result, msg) = await _unitOfWork.GetRepository<TblRoom>().Delete(id, user);
                    m.Msg = result ? string.Format(Resources.Public.DeleteSuccessMessage) :
                        string.Format(Resources.Public.DeleteErrorMessage, msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
    }

}
