﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    public class TaskController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db = new dbEntities();

        public TaskController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        // GET: Welfare/Task
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// بررسی تغییر وضعیت تخت ها به صورت روزانه
        /// تخت هایی که
        /// وضعیت رزرو 5
        /// مهمان در اتاق حضور دارد 20
        /// بیست چهار ساعت تا پایان اقامت 21
        /// از تاریخ مهمان گذشته است 22
        /// منتظر تمدید 23
        /// </summary>
        /// <returns></returns>
        public string ChangeBedStatus()
        {
            var beds = _db.TblBeds
                //.Include(a=>a.TblFoodReservations)
                .Where(a=> a.Status == 5 || a.Status>=20);
          
            ////وضعیت رزرو 5
            //foreach (var item in beds.Where(a => a.Status == 5))
            //{
            //    var fr = _db.TblFoodReservations
            //        .Where(a => a.Group == item.FoodReservationGroup && a.Person == item.Person)
            //        .OrderBy(a => a.Date);
            //    if (fr.FirstOrDefault().Date.Date == DateTime.Now.Date)
            //        item.Status = 20;
            //}
             
            //وضعیت مهمان در اتاق حضور دارد 20
            foreach (var item in beds.Where(a => a.Status == 20))
            {
                var fr = _db.TblFoodReservations
                        .Where(a => a.Group == item.FoodReservationGroup && a.Person == item.Person)
                        .OrderBy(a => a.Date);
                if (fr.LastOrDefault().Date.Date == DateTime.Now.Date)
                    item.Status = 21;
            }
            
            //وضعیت مهمان در اتاق حضور دارد 20
            foreach (var item in beds.Where(a => a.Status == 21))
            {
                var fr = _db.TblFoodReservations
                        .Where(a => a.Group == item.FoodReservationGroup && a.Person == item.Person)
                        .OrderBy(a => a.Date);
                if (fr.LastOrDefault().Date.Date > DateTime.Now.Date)
                    item.Status = 22;
            }



            return "";
        }
    }
}