﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;
using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    public class SurveyController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly dbEntities _db = new dbEntities();

        public SurveyController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        // GET: Welfare/Survey
        public ActionResult Index()
        {
            return View();
        }
        public  JsonResult QuestionItems(int survey=1)
        {

            var result = ( _unitOfWork.GetRepository<TblSurveyQuestion>().GetOrderedSyncNoTracking(
                a=>a.Survey == survey,orderBy: "Title", take: 1000, skip: 0));
            var (items, count, total) = result;
            return Json(items, JsonRequestBehavior.AllowGet);
        } 
        public  JsonResult SurveyItems()
        {

            var result = ( _unitOfWork.GetRepository<TblSurvey>().GetOrderedSyncNoTracking(
                orderBy: "Title", take: 1000, skip: 0));
            var (items, count, total) = result;
            return Json(items, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Responses()
        {
            return View();
        }

        public ActionResult ResponseList(SearchAdvancedTblSurveyResponse model)
        {
            Expression<Func<TblSurveyResponse, bool>> filter = (a) =>
                  //(model.Serving == null || a.Serving == model.Serving) &&
                  //(model.Type == null || a.TblFoodReservationGroup.Type == model.Type) &&
                  //(model.Confirm == null || a.TblFoodReservationGroup.Confirm == model.Confirm) &&
                  //(model.Group == 0 || a.Group == model.Group) &&
                  //(string.IsNullOrEmpty(model.UserName) || a.TblFoodReservationGroup.TblUser.UserName == model.UserName) &&
                  //(string.IsNullOrEmpty(model.Des) || a.TblFoodReservationGroup.Des.Contains(model.Des)) &&
                  //(string.IsNullOrEmpty(model.TblPerson.Name) || a.TblPerson.Name.Contains(model.TblPerson.Name)) &&
                  //(string.IsNullOrEmpty(model.TblPerson.Family) || a.TblPerson.Family.Contains(model.TblPerson.Family)) &&
                  //(string.IsNullOrEmpty(model.TblPerson.PersonnelCode) || a.TblPerson.PersonnelCode.Contains(model.TblPerson.PersonnelCode)) &&
                  //(string.IsNullOrEmpty(model.TblPerson.NationalCode) || a.TblPerson.NationalCode.Contains(model.TblPerson.NationalCode)) &&
                  //(string.IsNullOrEmpty(model.Des) || a.Des.Contains(model.Des)) &&
                  (model.Food.Contains(-5) || model.Food.Contains(a.Food)) &&
                  (model.Meal.Contains(-5) || model.Meal.Contains(a.Meal)) &&
                  (model.Restaurant.Contains(-5) || model.Restaurant.Contains(a.Restaurant)) &&
                  (model.Question.Contains(-5) || model.Question.Contains(a.Question)) &&
                  (model.Survey.Contains(-5) || model.Survey.Contains(a.Survey)) &&
                  (model.StartTime.Year == 1 || a.Time >= model.StartTime && a.Time <= model.EndTime) &&
                  (model.StartDate.Year == 1 || a.Date >= model.StartDate && a.Date <= model.EndDate) //&&

                  //(model.AccountNumber.Contains(0) || model.AccountNumber.Contains(a.TblFoodReservation.TblFoodReservationGroup.AccountNumber)) &&
                  //(model.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == 0 || a.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company == model.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.TblSubsidiary.Company) &&
                  //(model.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == 0 || a.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary == model.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.TblManagement.Subsidiary) &&
                  //(model.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Management == 0 || a.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Management == model.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.TblOffice.Management) &&
                  //(model.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office == 0 || a.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office == model.TblFoodReservation.TblFoodReservationGroup.TblOfficeDepartmentAccountNumber.TblOfficeDepartment.Office) &&
                  //a.TblFoodReservation.TblFoodReservationGroup.Temp == false &&
                  //a.TblFoodReservation.TblFoodReservationGroup.Remove == false &&
                  //a.TblFoodReservation.Food != -1
          ;
            //Server Side Parameter
            var start = Convert.ToInt32(Request["start"]);
            var length = Convert.ToInt32(Request["length"]);
            // var searchValue = Request["search[value]"];
            var draw = Request["draw"];
            var order = "";
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("order")))
            {
                if (item.EndsWith("[column]"))
                    order += Request["columns[" + Request[item] + "][name]"] + " ";
                else if (item.EndsWith("[dir]"))
                    order += Request[item] + ",";
            }
            length = length == 0 ? 100 : length;
            order = string.IsNullOrEmpty(order) ? "Id desc" : order.Substring(0, order.Length - 1);
            const string includeProperties = @"TblFoodReservation,
                                       TblRestaurant,TblFood,TblMeal,
                                        TblUser.TblPerson,
                                        TblSurveyQuestion.TblSurvey,
                                        TblSurveyQuestionOption,
                                       TblSurveyQuestion
                                        ";
            var iResult = _unitOfWork.GetRepository<TblSurveyResponse>();

            var result = iResult.GetOrderedSyncNoTracking(filter: filter,
                orderBy: order, take: length, skip: start, includeProperties: includeProperties);
            var (items, count, total) = result;

            var data = items?.Select(a => new
            {
                DT_RowId = a.Id,
                id = a.Id,
                TimeFa = new PersianDateTime(a.Time).ToString("yyyy/MM/dd HH:mm"),
                DateFa = new PersianDateTime(a.Date).ToString("yyyy/MM/dd"),
                a.Question,
                QuestionTitle= a.TblSurveyQuestion.Title,
                a.Option,
                OptionTitle = a.TblSurveyQuestionOption.Title,

                a.Survey,
                SurveyTitle = a.TblSurveyQuestion.TblSurvey.Title,

                a.Answer,
                a.FoodReservation,
                Person =    a.TblUser.TblPerson.Fullname(),
                RestaurantTitle=  a.TblRestaurant.Title,
                Restaurant= a.Restaurant,
                FoodTitle = a.TblFood.Title,
                Food = a.Food,
                a.Meal,
                MealTitle = a.TblMeal.Title,
                a.User,
                a.Time,
                
                
            }).ToList();
            var res = new
            {
                draw,
                recordsTotal = total.ToString(),
                recordsFiltered = count.ToString(),
                data
            };
            //return Json(res, JsonRequestBehavior.AllowGet);

            var jsonResult = Json(res, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        public ActionResult Poll()
        {
            var s =  _unitOfWork.GetRepository<TblSurveyQuestion>().GetSync(a => a.Survey == 2 && a.Status, includeProperties: "TblSurveyQuestionOptions");

            return View(s);
        }
        public async Task<ActionResult> Poll2(int foodReservationId)
        {
            ViewBag.FoodReservationId = foodReservationId;
            var s = await _unitOfWork.GetRepository<TblSurveyQuestion>().Get(a => a.Survey == 1 && a.Status, includeProperties: "TblSurveyQuestionOptions");
            return PartialView(s);
        }

        [HttpPost]
        public JsonResult PollSave(int restaurants,int meals,int food, DateTime startDate)
        {
            var m = new Message();
          
            var user = Convert.ToInt32(User.Identity.Name);
            var list = new List<TblSurveyResponse>();
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("Question")))
            {

                //Question-@(item.Id)
                var question = Convert.ToInt32(item.Split('-')[1]);
                var option = Convert.ToInt32(Request[item]);
                var answer = Request["Answer-" + question];
                var entity = new TblSurveyResponse
                {
                    Answer = answer,
                    FoodReservation = null,
                    Option = option,
                    Question = question,
                    Survey = 2,
                    Time = DateTime.Now,
                    User = user,
                    Date = startDate,
                    Restaurant = restaurants,
                    Meal = meals,
                    Food = food
                };
                list.Add(entity);

            }

            var (result, message) =  _unitOfWork.GetRepository<TblSurveyResponse>().InsertRangeSync(list, user);
            if (result)
            {
                
                    m.Msg = string.Format(Resources.Public.AddSuccessMessage, "ثبت نظر شما"); ;
                    m.Result = true;
                    m.Type = "success";

                
            }
            else
            {
                if (message.Contains("duplicate"))
                {
                    message = "شما برای این تاریخ، رستوران، وعده، قبلا در نظرسنجی شرکت کرده اید.";
                }
                m.Msg = string.Format(Resources.Public.AddErrorMessage,"نظر شما", message);
                m.Result = false;
                m.Type = "error";
            }

            //m.Msg = "1";
            return Json(m, JsonRequestBehavior.AllowGet);
        } 
        [HttpPost]
        public async Task<ActionResult> PollSave2()
        {
            var m = new Message();
            var foodReservationId = Convert.ToInt32(Request["FoodReservationId"]);
            var user = Convert.ToInt32(User.Identity.Name);
            var list = new List<TblSurveyResponse>();
            foreach (var item in Request.Form.AllKeys.Where(a => a.StartsWith("Question")))
            {

                //Question-@(item.Id)
                var question = Convert.ToInt32(item.Split('-')[1]);
                var option = Convert.ToInt32(Request[item]);
                var answer = Request["Answer-" + question];
                var entity = new TblSurveyResponse
                {
                    Answer = answer, FoodReservation = foodReservationId, Option = option, Question = question,
                    Survey = 1, Time = DateTime.Now, User = user
                };
                list.Add(entity);

            }

            var (result, message) = await _unitOfWork.GetRepository<TblSurveyResponse>().InsertRange(list, user);
            if (result)
            {
                var fr = await _unitOfWork.GetRepository<TblFoodReservation>().GetFirst(a => a.Id == foodReservationId);
                fr.Survey = true;
                var (resultFr, messageFr) = await _unitOfWork.GetRepository<TblFoodReservation>().Update(fr, user);
                if (resultFr)
                {
                    m.Msg = string.Format(Resources.Public.AddSuccessMessage, "ثبت نظر شما"); ;
                    m.Result = true;
                    m.Type = "success";
                }
                else
                {
                    m.Msg = string.Format(Resources.Public.AddErrorMessage, "نظر شما", message);
                    m.Result = false;
                    m.Type = "error";
                }
            }
            else
            {
                m.Msg = string.Format(Resources.Public.AddErrorMessage,"نظر شما", message);
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Details(int id)
        {
            var survey =  _unitOfWork.GetRepository<TblSurvey>().GetSingleSync(a => a.Id == id);
            return View(survey);
        }
        public async Task<ActionResult> Questions(int id)
        {
            var questions = await _unitOfWork.GetRepository<TblSurveyQuestion>().Get(a => a.Survey == id,
                includeProperties: "TblSurveyQuestionOptions");
            
            return PartialView(questions);
        }
        public PartialViewResult CreateQuestion()
        {
            return PartialView();
        }

        [HttpPost]
        public async Task<JsonResult> CreateQuestion(int survey,string title, bool necessary, bool anatomical)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var q = new TblSurveyQuestion { Survey = survey, Title = title, Necessary = necessary, Anatomical = anatomical };
                    var (result, msg) = await _unitOfWork.GetRepository<TblSurveyQuestion>().Insert(q, user);
                    m.Msg = result ? string.Format(Resources.Public.AddSuccessMessage, "ایجاد پرسش") :
                        string.Format(Resources.Public.AddErrorMessage, "ایجاد پرسش", msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> EditQuestion(int id)
        {
            var q = await _unitOfWork.GetRepository<TblSurveyQuestion>().GetSingle(a=>a.Id==id);
            return PartialView(q);
        }

        [HttpPost]
        public async Task<JsonResult> EditQuestion(int id, string title, bool necessary, bool anatomical, bool status)
          {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var q = await _unitOfWork.GetRepository<TblSurveyQuestion>().GetSingle(a => a.Id == id);
                    q.Anatomical = anatomical;
                    q.Necessary = necessary;
                    q.Title = title;
                    q.Status = status;
                    var (result, msg) = await _unitOfWork.GetRepository<TblSurveyQuestion>().Update(q, user);
                    m.Msg = result ? string.Format(Resources.Public.EditSuccessMessage, "پرسش") :
                        string.Format(Resources.Public.EditErrorMessage, "پرسش", msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeleteQuestion(int id)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var (result, msg) = await _unitOfWork.GetRepository<TblSurveyQuestion>().Delete(id, user);
                    m.Msg = result ? string.Format(Resources.Public.DeleteSuccessMessage) :
                        string.Format(Resources.Public.DeleteErrorMessage, msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult CreateOption(int question, string text)
        {
            ViewBag.Question = question;
            ViewBag.QuestionText = text;
            return PartialView();
        }
        
        public PartialViewResult ResponseOption(int question, string text)
        {
            ViewBag.Question = question;
            ViewBag.QuestionText = text;
            var list = _db.TblSurveyResponses.Where(a => a.Question == question);
            return PartialView(list);
        }

        [HttpPost]
        public async Task<JsonResult> CreateOption(int question, string text, string title)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var o = new TblSurveyQuestionOption { Question = question, Title = title };
                    var (result, msg) = await _unitOfWork.GetRepository<TblSurveyQuestionOption>().Insert(o, user);
                    m.Msg = result ? string.Format(Resources.Public.AddSuccessMessage, "ایجاد پاسخ") :
                        string.Format(Resources.Public.AddErrorMessage, "ایجاد پاسخ", msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> EditOption(int id)
        {
            var q = await _unitOfWork.GetRepository<TblSurveyQuestionOption>().GetSingle(a => a.Id == id);
            return PartialView(q);
        }

        [HttpPost]
        public async Task<JsonResult> EditOption(int id, string title, bool status)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var q = await _unitOfWork.GetRepository<TblSurveyQuestionOption>().GetSingle(a => a.Id == id);
                    q.Title = title;
                    q.Status = status;
                    var (result, msg) = await _unitOfWork.GetRepository<TblSurveyQuestionOption>().Update(q, user);
                    m.Msg = result ? string.Format(Resources.Public.EditSuccessMessage, "پاسخ") :
                        string.Format(Resources.Public.EditErrorMessage, "پاسخ", msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> DeleteOption(int id)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    //var o = await _unitOfWork.GetRepository<TblSurveyQuestionOption>().GetSingle(a=>a.Id==id);
                    var (result, msg) = await _unitOfWork.GetRepository<TblSurveyQuestionOption>().Delete(id, user);
                    m.Msg = result ? string.Format(Resources.Public.DeleteSuccessMessage) :
                        string.Format(Resources.Public.DeleteErrorMessage, msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
    }
}