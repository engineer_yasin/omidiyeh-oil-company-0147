﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MD.PersianDateTime;
using Omidiyeh_Oil_Company___0147.Implementations;
using Omidiyeh_Oil_Company___0147.Interfaces;

using Omidiyeh_Oil_Company___0147.Models;

namespace Omidiyeh_Oil_Company___0147.Areas.Welfare.Controllers
{
    [Authorize()]
    public class BedController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public BedController()
        {
            this._unitOfWork = new UnitOfWork<dbEntities>();
        }
        

        public async Task<JsonResult> Items(int id = 0, int status = 1)
        {
            var list = await _unitOfWork.GetRepository<TblBed>().Get(a => a.Room == id && a.Status == status);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Reception()
        {
            return View();
        }

        public async Task<PartialViewResult> ReceptionBed(int id)
        {
            var r = await _unitOfWork.GetRepository<TblFoodReservation>().GetSingle(a => a.Id == id,includeProperties:"TblPerson");
            ViewBag.Room = new SelectList(await _unitOfWork.GetRepository<TblRoom>().Get(a=>a.Restaurant == r.Restaurant && a.Status == 1), "Id", "Number");
            var gr = await _unitOfWork.GetRepository<TblFoodReservation>().Get(a =>
                a.Group == r.Group && a.Person == r.Person && a.Food == 0 && a.Bed == null);
            ViewBag.FromFa = new PersianDateTime(gr.OrderBy(a => a.Date).First().Date).ToString("yyyy/MM/dd"); 
            ViewBag.ToFa = new PersianDateTime(gr.OrderBy(a => a.Date).Last().Date).ToString("yyyy/MM/dd"); 
            

            return PartialView(r);
        }
        public ActionResult Create(int room,int number)
        {
            ViewBag.Room = room;
            ViewBag.Number = number;
            return PartialView();
        }

        public async Task<JsonResult> CreateBed(int number, byte capacity, int room)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var b = new TblBed { Capacity = capacity, Number = number, Room = room, Status = 1 };
                    var (result, msg) = await _unitOfWork.GetRepository<TblBed>().Insert(b, user);
                    m.Msg = result ? string.Format(Resources.Public.AddSuccessMessage, "ایجاد تخت") :
                        string.Format(Resources.Public.AddErrorMessage, "ایجاد تخت", msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Edit(int id)
        {
            var b = await _unitOfWork.GetRepository<TblBed>().GetSingle(a => a.Id == id);
            ViewBag.Status = new SelectList(await _unitOfWork.GetRepository<TblBedStatu>().Get(), "Id", "Title", b.Status);
            return PartialView(b);
        }

        [HttpPost]
        public async Task<JsonResult> Edit(int id, int number, byte status, byte capacity, string des)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var b = await _unitOfWork.GetRepository<TblBed>().GetSingle(a => a.Id == id);
                    b.Number = number;
                    b.Status = status;
                    b.Des = des;
                    b.Capacity = capacity;
                    var (result, msg) = await _unitOfWork.GetRepository<TblBed>().Update(b, user);
                    m.Msg = result ? string.Format(Resources.Public.EditSuccessMessage, " تخت" + b.Number) :
                        string.Format(Resources.Public.EditErrorMessage, "ویرایش تخت", msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                TempData["errorMessage"] = await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> Delete(int id)
        {
            var m = new Message();
            var user = Convert.ToInt32(User.Identity.Name);
            try
            {
                if (ModelState.IsValid)
                {
                    var (result, msg) = await _unitOfWork.GetRepository<TblBed>().Delete(id, user);
                    m.Msg = result ? string.Format(Resources.Public.DeleteSuccessMessage) :
                        string.Format(Resources.Public.DeleteErrorMessage, msg);
                    m.Result = result;
                    m.Type = result ? "success" : "error";
                }
            }
            catch (Exception e)
            {
                await Logs.CatchLog(e, Convert.ToInt32(User.Identity.Name), Request);
                m.Msg = e.Message;
                m.Result = false;
                m.Type = "error";
            }

            return Json(m, JsonRequestBehavior.AllowGet);
        }
    }
}
