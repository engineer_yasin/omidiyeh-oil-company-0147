﻿using System.Web;
using System.Web.Mvc;

namespace Omidiyeh_Oil_Company___0147
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
